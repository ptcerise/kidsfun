+function ($) {
	$(document).ready(function(){
	    if (!jQuery().wysihtml5) {
	          return;
	      }
	      if ($('.wysihtml5')) {
	          $('.wysihtml5').wysihtml5({
	              "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
	              "font-styles": false,
	              "html": false,
	              "link": false,
	              "image": false
	          });
	      }
  	});
}(jQuery);


+function ($) {
 	$( window ).on( 'mousemove mouseup', function() {
	    var $modal     = $('.modal-dialog')
	      , $backdrop  = $('.modal-backdrop')
	      , el_height  = $modal.innerHeight();
	    $backdrop.css({
	        height: el_height,
	        display: 'none',
	        minHeight: 'auto',
	        margin: 'auto',
	    });
	    $modal.css({
	        padding: '4px',
	        maxWidth: '900px',
	        margin: '20px auto'
	    });
	});
}(jQuery);

+function ($){
	(function(b, o, i, l, e, r) {
	        b.GoogleAnalyticsObject = l;
	        b[l] || (b[l] =
	            function() {
	                (b[l].q = b[l].q || []).push(arguments)
	            });
	        b[l].l = +new Date;
	        e = o.createElement(i);
	        r = o.getElementsByTagName(i)[0];
	        e.src = '//www.google-analytics.com/analytics.js';
	        r.parentNode.insertBefore(e, r)
	    }(window, document, 'script', 'ga'));
	    ga('create', 'UA-XXXXX-X', 'auto');
	    ga('send', 'pageview');

	    $(document).ready(function(){
	        var $menu = $("nav.menu-vertical");
	        $("div.menu-drops").click(function(){
	            var $isopen = $(this).next("nav.menu-vertical").stop(true).toggleClass("menu-open");
	            $menu.not($isopen).stop(true).toggleClass("menu-open",false);
	        });
	    });
}(jQuery);