$(function() {

    var $body = $(document);
    $body.bind('scroll', function() {
        // "Disable" the horizontal scroll.
        if ($body.scrollLeft() !== 0) {
            $body.scrollLeft(0);
        }
    });

});

$(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

/* swap open/close side menu icons */
$('[data-toggle=collapse]').click(function(){
  	// toggle icon
  	$(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
});
});


/* FOR SMOOTH SCROLL TO TAG WITH #main */
$(function() {
  $('a[href*="#main"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 750);
        return false;
      }
    }
  });
});

/* FOOTER */
var h = 0;
$('.footer-border').each(function() {
    if ($(this).height()>h) {
        h= $(this).height();
    };
});
$('.footer-border').each(function() {
    $(this).css('height', h+'px');
});

/* VIDEO */
function play_video(){
  /* Move the video from bg to popup */
  $('#vid-popup').append( $('#vid-background>source') );
  var vid_popup = document.getElementById('vid-popup');
  var vid_bg = document.getElementById('vid-background');
  vid_popup.play();
  if (vid_popup.requestFullscreen) {
      vid_popup.requestFullscreen();
    } else if (vid_popup.mozRequestFullScreen) {
      vid_popup.mozRequestFullScreen();
    } else if (vid_popup.webkitRequestFullscreen) {
      vid_popup.webkitRequestFullscreen();
    }
  vid_bg.pause();
  $.scrollLock( true );
}

function stop_video(){
  /* Return the video from popup to bg */
  $('#vid-background').append( $('#vid-popup>source') );

  var vid_popup = document.getElementById('vid-popup');
  var vid_bg = document.getElementById('vid-background');
  vid_popup.pause();
  vid_bg.play();
  $.scrollLock( false );
}

$(document).keyup(function(e) {
  if (e.keyCode === 27) $('#close-vid-btn').click();   // esc
});

/* INPUT */
$(window).load(function(){
  $(".col-effect input").val("");
  $(".input-effect input").focusout(function(){
    if($(this).val() != ""){
      $(this).addClass("has-content");
    }else{
    $(this).removeClass("has-content");
    }
  });

  $(".col-effect textarea").val("");
  $(".input-effect textarea").focusout(function(){
    if($(this).val() != ""){
      $(this).addClass("has-content");
    }else{
    $(this).removeClass("has-content");
    }
  });

  $(".input-effect select").focusout(function(){
    if($(this).val() == 0){
      $(this).removeClass("has-content");
    }else{
    $(this).addClass("has-content");
    }
  });
});

/* SHARE */
function showShare($id){
    document.getElementById($id).style.visibility = "visible";
}

function closeShare($id){
    document.getElementById($id).style.visibility = "hidden";
}

/* SHOPPING CART */
//Jika input jumlah > 0, btn-minus active
    if($(".value").val() > 0){
        $(".btn-minus").removeAttr('disabled');
    }

    Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
        places = !isNaN(places = Math.abs(places)) ? places : 2;
        symbol = symbol !== undefined ? symbol : "Rp.";
        thousand = thousand || ",";
        decimal = decimal || ".";
        var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
        return  symbol+' '+negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
    };
//hanya value saja yang bisa diinputkan
    $(".value").keydown(function (e) {

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

//button nambah ticket
    $(document).on('click','.btn-plus',function(){

        var subtotal = 0;
        var inputan = $(this).prev();
        var current = parseInt(inputan.val())|0;
        current++;
        inputan.val(current);
        inputan.prev().removeAttr('disabled');

        var detectPrice = $(this).parents('tr').find('.price').html();
        var price = parseInt(detectPrice.replace('.',''));
        subtotal = price*current;
        $(this).parents('tr').find('.subtotal').html(subtotal.formatMoney(0,'','.',','));

        var detectTotal = $(document).find('#total').html();
        var total = parseInt(detectTotal.replace('.',''));
        detectTotal = total+price;

        $(document).find('#total').html(detectTotal.formatMoney(0,'','.',','));
    });

// button ngurangin ticket
    $(document).on('click','.btn-minus',function(){

        var inputan = $(this).next();
        var current = parseInt(inputan.val());
        current--;
        inputan.val(current);
        if(current==0){
            $(this).attr('disabled', 'true');;
        }

        var detectprice = $(this).parents('tr').find('.price').html();
        var price = parseInt(detectprice.replace('.',''));
        subtotal = price*current;
        $(this).parents('tr').find('.subtotal').html(subtotal.formatMoney(0,'','.',','));

        var detectTotal = $(document).find('#total').html();
        var total = parseInt(detectTotal.replace('.',''));
        detectTotal = total-price;

        $(document).find('#total').html(detectTotal.formatMoney(0,'','.',','));
    });

//Otomatis hitung sub total saat load page
    $(document).parents('tr', function(){
        var q = $(this).find('.value').val();
        alert(q);
    });

// kalkulasi perhitungan apabila diperlukan
/*    function hitung () {
        var total = 0;
        $('input[name="ticket"]').each(function() {
            var current = $(this);
            var detectprice = current.parents('.batas').find('.price').html().split(' ');
            var price = parseFloat(detectprice[1].replace('.',''));
            var jumlah = parseInt(current.val());
            total =total+(price*jumlah);
        });
        $('span#total').html(total.formatMoney(2,'Rp.','.',','));
    }*/

    //Menambahkan ke dalam cart

/* TICKETS */
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
        places = !isNaN(places = Math.abs(places)) ? places : 2;
        symbol = symbol !== undefined ? symbol : "Rp.";
        thousand = thousand || ",";
        decimal = decimal || ".";
        var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
        return  symbol+' '+negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
    };

//hanya value saja yang bisa diinputkan
    $(".value").keydown(function (e) {

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

//button nambah tiket
    $(document).on('click','.btn-plus',function(){

        var inputan = $(this).prev();
        var current = parseInt(inputan.val())|0;
        current++;
        inputan.val(current);
        inputan.prev().removeAttr('disabled');

    });

// button ngurangin tiket
    $(document).on('click','.btn-minus',function(){

        var inputan = $(this).next();
        var current = parseInt(inputan.val());
        current--;
        inputan.val(current);
        if(current==0){
            $(this).attr('disabled', 'true');;
        }
    });

    $(document).on('click','.btn-cart',function() {
        var id = "";
        var jml = 0;
        var current = $(this);
        var itemId = current.parents('#accordion').find('.itemId').html();
        var jumlah = parseInt(current.parents('#accordion').find('.value').val());
        id = itemId;
        jml = jumlah;

    });

// kalkulasi perhitungan apabila diperlukan
/*    function hitung () {
        var total = 0;
        $('input[name="tiket"]').each(function() {
            var current = $(this);
            var detectHarga = current.parents('.').find('.harga').html().split(' ');
            var harga = parseFloat(detectHarga[1].replace('.',''));
            var jumlah = parseInt(current.val());
            total =total+(harga*jumlah);
        });
        $('span#total').html(total.formatMoney(2,'Rp.','.',','));
    }*/

    //Menambahkan ke dalam cart


// FOR SHARE CARDS
var card_height = $(".card-share").height();
if(card_height > 400){
    var share_btn = card_height * 0.633;
    var padding_share = card_height * 0.323;
    var close_btn = card_height * 0.316;
}else{
    if(card_height > 360){
        var share_btn = card_height * 0.633;
        var padding_share = card_height * 0.333;
        var close_btn = card_height * 0.317;
    }else{
        var share_btn = card_height * 0.633;
        var padding_share = card_height * 0.333;
        var close_btn = card_height * 0.226;
    }
}

$(".social-share-btn").css("padding-top",padding_share+"px")
                      .css("margin-bottom",0);
$(".card-share").find(".card-btn-more").css("padding-top",close_btn+"px")
                                       .css("margin-top",0);

//SCROLL LOCK
$.scrollLock = ( function scrollLockSimple(){
    var locked   = false;
    var $body;
    var previous;

    function lock(){
      if( !$body ){
        $body = $( 'body' );
      }
      
      previous = $body.css( 'overflow' );
        
      $body.css( 'overflow', 'hidden' );

      locked = true;
    }

    function unlock(){
      $body.css( 'overflow', previous );

      locked = false;
    }

    return function scrollLock( on ) {
        // If an argument is passed, lock or unlock depending on truthiness
        if( arguments.length ) {
            if( on ) {
                lock();
            }
            else {
                unlock();
            }
        }
        // Otherwise, toggle
        else {
            if( locked ){
                unlock();
            }
            else {
                lock();
            }
        }
    };
}() );

//LINKED CARDS
$(".ride-home-cards article .panel").css("cursor", "pointer").click(function(){
    window.location = js_base_url('rides');
});
$(".link-cards").css("cursor", "pointer").click(function(){
    var id = $(this).parents("article").find(".card-id").html();
    var current = $("div.current-page").html();
    if(current == "news"){
        var page = current + "/read/";
    }else{
        var page = current + "/detail/";
    }
    
    window.location = js_base_url(page + id);
});

//GOKART KARTS CONTENT
$(document).ready(function(){
        $(".kart-detail").hide();
    });

    $(".kart-front").css("cursor","pointer").click(function(){
        $(this).parents(".kart-cards").find(".kart-detail").show()
        $(this).parents(".kart-cards").find(".kart-front").hide();
    });

    function closeKartDetail(id){
        $("#kartDetail" + id).hide();
        $("#kartFront" + id).show();
    }

//SHOW MORE BUTTON
$(document).ready(function(){

    var current = $("div.current-page").html();

    if(current == "events"){
        var cards_count1 = $("div#next-accordion article").size();
        var cards_count2 = $("div#past-accordion article").size();
        var x1 = 4;
        var add1 = 5;
        var x2 = 4;
        var add2 = 5;
        
        $("div#next-accordion article:gt("+ x1 +")").hide();
        $("div#next-accordion article:lt("+ x1 +")").show();

        $("div#past-accordion article:gt("+ x2 +")").hide();
        $("div#past-accordion article:lt("+ x2 +")").show();

        var show_cards1 = $("div#next-accordion article:lt("+ x1 +")").size();
        var show_cards2 = $("div#past-accordion article:lt("+ x2 +")").size();
        var z1 = show_cards1+1;
        var z2 = show_cards2+1;
        
        if(z1 == cards_count1){
            $("#showmore1").hide();
        }else if(show_cards1 >= cards_count1){
            $("#showmore1").hide();
        }else{
            $("#showmore1").show();
        }

        if(z2 == cards_count2){
            $("#showmore2").hide();
        }else if(show_cards2 >= cards_count2){
            $("#showmore2").hide();
        }else{
            $("#showmore2").show();
        }

        $("#showmore1").click(function(){
            x1 = (x1 + add1 <= cards_count1) ? x1 + add1 : cards_count1;
            $("div#next-accordion article:gt("+ x1 +")").hide();
            $("div#next-accordion article:lt("+ x1 +")").show();

            show_cards1 = $("div#next-accordion article:lt("+ x1 +")").size();
            if(show_cards1 >= cards_count1){
                $("#showmore1").hide();
            }else{
                $("#showmore1").show();
            }
        });

        $("#showmore2").click(function(){
            x2 = (x2 + add2 <= cards_count2) ? x2 + add2 : cards_count2;
            $("div#past-accordion article:gt("+ x2 +")").hide();
            $("div#past-accordion article:lt("+ x2 +")").show();

            show_cards2 = $("div#past-accordion article:lt("+ x2 +")").size();
            if(show_cards2 >= cards_count2){
                $("#showmore2").hide();
            }else{
                $("#showmore2").show();
            }
        });

    }else{

        var cards_count = $("div.cards article").size();

        if(current == "rides"){
            var x = 7;
            var add = 5;
        }else if(current == "restaurants"){
            var x = 3;
            var add = 4;
        }else{
            var x = 2;
            var add = 3;
        }

        $("div.cards article:gt("+ x +")").hide();
        $("div.cards article:lt("+ x +")").show();

        var show_cards = $("div.cards article:lt("+ x +")").size();
        var z = show_cards+1;
        
        if(z == cards_count){
            $("#showmore").hide();
        }else if(show_cards >= cards_count){
            $("#showmore").hide();
        }else{
            $("#showmore").show();
        }

        $("#showmore").click(function(){
            if((x + add) <= cards_count){x = x + add}else{x = cards_count};
            $("div.cards article:gt("+ x +")").hide();
            $("div.cards article:lt("+ x +")").show();

            if(current == "rides" && add == 5){
                add = 4;
            }else if(current == "restaurants" && add == 4){
                add = 3;
            }else if(current != "rides" || current != "restaurants" && add == 3){
                add = 2;
            };

            show_cards = $("div.cards article:lt("+ x +")").size();
            if(show_cards >= cards_count){
                $("#showmore").hide();
            }else{
                $("#showmore").show();
            }
        });

    }

});

//CATEGORY BUTTON ON RIDES
$(document).ready(function(){
    $("#resetFilter").hide();

    $("#btnKids").click(function(){
        $(this).css("border","solid 2px #00a14c");
        $(this).siblings().css("border","none");

        $("article.ride-card-kids").show();
        $("article.ride-card-family").hide();
        $("article.ride-card-thrill").hide();

        $("#showmore").hide();
        $("#resetFilter").show();
    });

    $("#btnFamily").click(function(){
        $(this).css("border","solid 2px #2f318d");
        $(this).siblings().css("border","none");

        $("article.ride-card-kids").hide();
        $("article.ride-card-family").show();
        $("article.ride-card-thrill").hide();

        $("#showmore").hide();
        $("#resetFilter").show();
    });

    $("#btnThrill").click(function(){
        $(this).css("border","solid 2px #EC2228");
        $(this).siblings().css("border","none");

        $("article.ride-card-kids").hide();
        $("article.ride-card-family").hide();
        $("article.ride-card-thrill").show();

        $("#showmore").hide();
        $("#resetFilter").show();
    });

    $("#resetFilter").click(function(){
        location.reload();
    });
});


// ADD TO CART BUTTON
$('.btn-cart').click(function() {
    var i =  $(this).parents('article').find("input[name='tickets_id']").val();
    var q =  $(this).parents('article').find("input[name='tiket']").val();
    $.ajax({
        type: "POST",
        url: js_base_url('tickets/add_to_cart'),
        dataType: 'json',
        data: { tickets_id: i, tiket:q },
        success:function( msg ) {
            if (msg.sent == 'Success')
            {
                toastr.options.timeOut = 1500;
                toastr.success(msg.notif);
            }
            else
            {
                toastr.options.timeOut = 1500;
                toastr.error(msg.notif);
            }
        }
    });
});

// CHECK SYARAT & KETENTUAN
$(document).ready(function(){
    $('#check-terms').change(function () {
        $('#btn-pay').prop("disabled", !this.checked);
    }).change()
});

// BUTTON CHECK TERMS
$(document).ready(function(){

    // if($('#checkout_fname').val()=='' && $('#checkout_lname').val()=='' && $('#checkout_email').val()=='' && $('#checkout_phone').val()=='' && $('#checkout_address').val()=='' && $('#checkout_city').val()=='' && $('#checkout_zip').val()=='' && $('#checkout_visit').val()==''){
    // $('#btn-terms').attr('disabled', 'disabled');
    // $('#btn-terms').attr('title', 'Isi data anda');    
    // }
    $('input[type="text"]').on('change',function() {
        $('#UserName').val($('#checkout_fname').val() +' '+$('#checkout_lname').val());
        $('#UserEmail').val($('#checkout_email').val());
        $('#UserContact').val($('#checkout_phone').val());

        if($('#checkout_fname').val()!='' && $('#checkout_lname').val()!='' && $('#checkout_email').val()!='' && $('#checkout_phone').val()!='' && $('#checkout_address').val()!='' && $('#checkout_city').val()!='' && $('#checkout_zip').val()!='' && $('#checkout_visit').val()==''){
            $('#btn-terms').removeProp('disabled');
            $('#btn-terms').removeAttr('title');
        }
        else{
            $('#btn-terms').attr('disabled', 'disabled');
            $('#btn-terms').attr('title');
        }
    });

    $('#btn-terms').on('click', function(e) {
        $('#terms').modal('show'); 
    });
});

// BUTTON PAY
$(document).ready(function(){
    $('#btn-pay').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: js_base_url('checkout/insert_checkout'),
            data: $("#ePay").serialize(),
            success: function(data){
                    $('#ePay').submit();
                }
        });
    });
});

// BUTTON PAY
// $(document).ready(function(){
//     $('#btn-pay').on('click', function(e) {

//         // if($('#checkout_fname').val()=='' || $('#checkout_lname').val()=='' || $('#checkout_email').val()=='' || $('#checkout_phone').val()=='' || $('#checkout_address').val()=='' || $('#checkout_city').val()=='' || $('#checkout_zip').val()=='' || $('#checkout_visit').val()==''){
//         //     $('input').prop('required', true);
//         // }
//         // else {
//             e.preventDefault();
//             $.ajax({
//                 type: "POST",
//                 url: js_base_url('checkout/insert_checkout'),
//                 data: $("#ePay").serialize(),
//                 success: function(data){
//                         $('#ePay').submit();
//                     }
//             });
//         // }
//     });
// });

// BUTTON SEND, CONTACT US PAGE
$(document).ready(function(){

    $('#btn-send').click(function(e) {
        
        if($('.contact_name').val()=='' || $('.contact_email').val()=='' || $('.contact_phone').val()=='' || $('.contact_subject').val()=='' || $('.contact_message').val()==''){
            $('input').prop('required', true);
            $('textarea').prop('required', true);
        }
        else {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: js_base_url('contact_us/send_message'),
                data: $("#contact_form").serialize(),
                success: function(data){
                        $('#contact_form').submit();
                    }
            });
        }
    });
});

$(document).ready(function(){
    
    $('#checkout_visit').datetimepicker({ 
        format: 'YYYY/MM/DD',
        minDate: new Date()
    });
});

// $('.btn-pay').click(function() {
//     var a =  $(this).parents('form').find("input[name='UserName']").val();
//     var b =  $(this).parents('form').find("input[name='checkout_lname']").val();
//     var c =  $(this).parents('form').find("input[name='UserEmail']").val();
//     var d =  $(this).parents('form').find("input[name='UserContact']").val();
//     var e =  $(this).parents('form').find("input[name='checkout_address']").html();
//     var f =  $(this).parents('form').find("input[name='checkout_city']").val();
//     var g =  $(this).parents('form').find("input[name='checkout_zip']").val();
//     var h =  $(this).parents('form').find("input[name='checkout_total']").val();
//     $.ajax({
//         type: "POST",
//         data: { UserName: a, checkout_lname:b, UserEmail:c, UserContact:d, checkout_address:e, checkout_city:f, checkout_zip:g, checkout_total:h },
//         url: js_base_url('checkout/insert_checkout')
//     });
// });

// $('#btn-pay').click(function(e) {
//     e.preventDefault();
//     $.ajax({
//         type: "POST",
//         url: js_base_url('checkout/insert_checkout'),
//         data: $("#ePay").serialize(),
//         success: function() {
//             $('#ePay').submit();
//         }
//     });
    
// });
