<?php
class LanguageLoader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');

        // $be_lang = $ci->session->userdata('be_lang');
        // if ($be_lang) {
        //     $ci->lang->load('kidsfun_backend',$be_lang);
        // } else {
        //     $ci->lang->load('kidsfun_backend','english');
        // }

        $fe_lang = $ci->session->userdata('fe_lang');
        if ($fe_lang) {
            $ci->lang->load('message',$fe_lang);
        } else {
            $ci->lang->load('message','english');
        }
    }
}