<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();	
    }
    

    
    function readtable($tablename = '', $select='', $where = '', $join = '', $limit = '',$sort = '',$join_model = '',$group_by ='')
    {    
        if(!empty($sort)){
    		$this->db->order_by($sort);
        }
        if(!empty($where)){
    		$this->db->where($where);
        }
        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }
        if(is_array($join) AND !empty($join)){
            foreach($join as $key => $data){            
                $this->db->join($key, $data,$join_model);
            }
        }
        if(is_array($limit) AND !empty($limit)){                   
            $this->db->limit($limit[0], $limit[1]);
        }
        if(!empty($select)){
            $this->db->select($select);
        }       
        $query = $this->db->get($tablename);
        return $query;
    }
    
    function inserttable($tablename = '', $data = '')
    {
        $this->db->insert($tablename, $data); 
    }
       
    function updatetable($tablename = '', $data = '', $where='')
    {                
        $this->db->where($where);
        $this->db->update($tablename, $data); 
    }
     
    function deletetable($tablename = '',$where='')
    {
        $this->db->delete($tablename, $where); 
    }
}