<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {
        $data['lang'] = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['banner_large'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Contact_us'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Contact_us'))->row();

        $data['banner_caption'] = $this->Access->readtable('general','',array('general_section'=>'banner_caption','general_page'=>'Contact_us'))->row();

        $data['content'] = $this->Access->readtable('general','',array('general_section'=>'content','general_page'=>'Contact_us'))->row();

        $data['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'Contact_us'))->row();

        $data['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Contact_us'))->row();

        $view['content'] = $this->load->view('kidsfun/v_contact_us',$data,TRUE);
         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'contact_us';
        $this->load->view('kidsfun/v_master', $view);
    }

    function send_message()
    {
    	$contact_name = $this->input->post('Name');
        $contact_email = $this->input->post('Email');
        $contact_subject = $this->input->post('Subject');
        $contact_message = $this->input->post('Message');
        $new_message = array(
                            'contact_name' => $this->input->post('Name'),
                            'contact_email' => $this->input->post('Email'),
                            'contact_phone' => $this->input->post('Phone'),
                            'contact_subject' => $this->input->post('Subject'),
                            'contact_message' => $this->input->post('Message'),
                            );
        $this->db->trans_begin();
        $this->db->set('contact_date', 'NOW()', FALSE);
        $this->Access->inserttable('contact',$new_message);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("send_success");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        
	// SEND TO info@kidsfun.co.id
            #Configure the E-mail content
            $config = array();
            $config['charset'] = 'utf-8';
            $config['useragent'] = 'Yogyagokart';
            $config['protocol']= "smtp";
            $config['mailtype']= "html";
            $config['smtp_host']= "ssl://kidsfun.co.id";//smtp
            $config['smtp_port']= "465";
            $config['smtp_timeout']= "10";
            $config['smtp_user']= "notification@kidsfun.co.id"; // sender email
            $config['smtp_pass']= "kidsfun#2016"; // sender password
            $config['crlf']="\r\n";
            $config['newline']="\r\n";
            $config['wordwrap'] = TRUE;
            $config['validate'] = TRUE;
            $config['priority'] = 2;
            $this->email->initialize($config);

            $this->email->from($contact_email,$contact_name);
            $this->email->to('info@kidsfun.co.id');
            $this->email->reply_to($contact_email);
            $this->email->subject($contact_subject);
            $this->email->message($contact_message);

            if($this->email->send()){ #Send the E-mail
                echo "<script type='text/javascript'>alert('Berhasil mengirim!');</script>";
                $_SESSION['info_send'] = $notif;
                $this->session->mark_as_flash('info_send');
            }else{
                echo "<script type='text/javascript'>alert('Gagal mengirim!');</script>";
                $_SESSION['info_send'] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Failed</div>';
                $this->session->mark_as_flash('info_send');
                
            }#Send E-mail END

            //$_SESSION['info_send'] = $notif;
            //$this->session->mark_as_flash('info_send');
            redirect('contact_us#form');
    }
}