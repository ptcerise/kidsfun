<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aquasplash extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {
        $data['lang'] = $this->session->userdata('fe_lang');

        //BANNER
        $data['banner_large'] = $this->Access->readtable('media','',array('media_page'=>'aquasplash', 'media_section'=>'banner_large'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_page'=>'aquasplash', 'media_section'=>'banner_small'))->row();

        //BANNER CAPTION
        $data['caption_en'] = $this->Access->readtable('general', 'general_content_en', array('general_page'=>'aquasplash', 'general_section'=>'banner_caption'))->row();
        $data['caption_in'] = $this->Access->readtable('general', 'general_content_in', array('general_page'=>'aquasplash', 'general_section'=>'banner_caption'))->row();

        //IMAGE CONTENT
        $data['image_content'] = $this->Access->readtable('attraction','attraction_url',array('attraction_type'=>'aquasplash', 'attraction_section'=>'aquasplash_image'))->result();

        //CONTENT
        $data['content_en'] = $this->Access->readtable('attraction','attraction_content_en',array('attraction_type'=>'aquasplash', 'attraction_section'=>'aquasplash_content'))->row();
        $data['content_in'] = $this->Access->readtable('attraction','attraction_content_in',array('attraction_type'=>'aquasplash', 'attraction_section'=>'aquasplash_content'))->row();

        //POOLS CONTENT
        $data['content_pools_en'] = $this->Access->readtable('attraction','attraction_content_en',array('attraction_type'=>'aquasplash', 'attraction_section'=>'pools_content'))->row();
        $data['content_pools_in'] = $this->Access->readtable('attraction','attraction_content_in',array('attraction_type'=>'aquasplash', 'attraction_section'=>'pools_content'))->row();

        //THE SLIDE CARDS
        $data['slide_cards'] = $this->Access->readtable('attraction','',array('attraction_type'=>'aquasplash', 'attraction_section'=>'slides'))->result();

        //POOL IMAGES
        $data['pool_images'] = $this->Access->readtable('attraction','',array('attraction_type'=>'aquasplash', 'attraction_section'=>'pools_image'))->result();

        $view['content'] = $this->load->view('kidsfun/v_aquasplash',$data,TRUE);
         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;
        
        //CURRENT PAGE
        $view['current'] = 'aquasplash';
        $this->load->view('kidsfun/v_master', $view);
    }
}
