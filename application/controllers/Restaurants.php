<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurants extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');     
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {
        //BANNER
        $data['banner_large'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Restaurants'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Restaurants'))->row();

        //BANNER CAPTION
        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Restaurants'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Restaurants'))->row();

        //CONTENT CAPTION
        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Restaurants'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Restaurants'))->row();

        //CARDS RESTAURANTS
        $data['cards'] = $this->Access->readtable('shop','',array('shop_type'=>'restaurant'))->result();

        $data['model'] = $this->Access;

        $data['lang'] = $lang = $this->session->userdata('fe_lang');

        $view['content'] = $this->load->view('kidsfun/v_restaurants',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'restaurants';

        $this->load->view('kidsfun/v_master', $view);
    }

     public function detail($get_url)
    {   
        $id = substr($get_url, 0, strpos($get_url, "-"));
        //BANNER
        $data['banner_large'] = $this->Access->readtable('media','',array('media_section'=>'banner_single_large','media_page'=>'restaurant','temp_id'=>$id))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_section'=>'banner_single_small','media_page'=>'restaurant','temp_id'=>$id))->row();

        //BANNER TITLE
        $data['title_en'] = $this->Access->readtable('media','media_title_en',array('media_section'=>'banner_caption','media_page'=>'restaurant','media_section'=>'banner_single_large','temp_id'=>$id))->row();
        $data['title_in'] = $this->Access->readtable('media','media_title_in',array('media_section'=>'banner_caption','media_page'=>'restaurant','media_section'=>'banner_single_large','temp_id'=>$id))->row();

        //BANNER CAPTION
        $data['caption_en'] = $this->Access->readtable('media','media_content_en',array('media_section'=>'banner_caption','media_page'=>'restaurant','media_section'=>'banner_single_large','temp_id'=>$id))->row();
        $data['caption_in'] = $this->Access->readtable('media','media_content_in',array('media_section'=>'banner_caption','media_page'=>'restaurant','media_section'=>'banner_single_large','temp_id'=>$id))->row();

        //RESTO TYPE
        $data['resto_type'] = $this->Access->readtable('shop','shop_type',array('shop_id'=>$id))->row();

        //IMAGE CONTENT
        $data['image_content'] = $this->Access->readtable('media','media_url',array('media_page'=>'restaurant','media_section'=>'image_content','temp_id'=>$id))->result();

        //CONTENT
        $data['content_en'] = $this->Access->readtable('shop','shop_content_en',array('shop_id'=>$id,'shop_type'=>'restaurant'))->row();
        $data['content_in'] = $this->Access->readtable('shop','shop_content_in',array('shop_id'=>$id,'shop_type'=>'restaurant'))->row();

        //MAP
        $data['image_map'] = $this->Access->readtable('media','media_url',array('media_page'=>'restaurants','media_section'=>'map'))->row();
        
        //CARDS RESTAURANTS
        $data['cards'] = $this->db->query('SELECT * FROM shop WHERE shop_id NOT IN ('.$id.') ORDER BY rand() LIMIT 2')->result();

        //ACCESS to Model on view
        $data['model'] = $this->Access;

        $data['lang'] = $lang = $this->session->userdata('fe_lang');

        $view['content'] = $this->load->view('kidsfun/v_restaurant_single',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'restaurants';
        $title = $this->Access->readtable('shop','',array('shop_id'=>$id,'shop_type'=>'restaurant'))->row();
        if($lang == "english" || $lang == ""){
            $view['title'] = $title->shop_title_en;
        }else{
            $view['title'] = $title->shop_title_in;
        }
        
        $this->load->view('kidsfun/v_master', $view);
    }
}
