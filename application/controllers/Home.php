<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    } 

    public function index()
    {
        $data['lang'] = $this->session->userdata('fe_lang');

        //VIDEO
        $data['video'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'video'))->row();

        //VIDEO FORMAT
        $v_name= $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'video'))->row();
        if(($tmp = strstr($v_name->media_url, '.')) !== false){
            $data['video_format'] = substr($tmp, 1);
        } 

        //VIDEO BANNER
        $data['video_banner'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'video_banner'))->row();
        
        //BANNER VIDEO TITLE
        $data['title'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'video_content'))->row();

        //BANNER VIDEO CONTENT
        $data['video_content'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'video_content'))->row();

        //DIVIDER 1 choose package
        $data['div1'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'divider_top'))->row();

        //CONTENT THE RIDES
        $data['rides_content'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'rides_content'))->row();

        //CARDS RIDES CATEGORY
        $data['ride_cat_content'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'rides'))->result();

        $data['db_access'] = $this->Access;

        //DIVIDER 2 let's race
        $data['div2'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'divider_middle'))->row();

        //BANNER AQUASPLASH
        $data['banner1_large'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'banner1_large'))->row();
        $data['banner1_small'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'banner1_small'))->row();
        ////CAPTION
        $data['banner1_caption'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'banner1_caption'))->row();

        //DIVIDER 3 corporate website
        $data['div3'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'divider_bottom'))->row();

        //BANNER RESTO
        $data['banner2_large'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'banner2_large'))->row();
        $data['banner2_small'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'banner2_small'))->row();
        ////CAPTION
        $data['banner2_caption'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'banner2_caption'))->row();

        //ANNOUCEMENT
        $data['announce'] = $this->Access->readtable('general', '', array('general_page'=>'home', 'general_section'=>'announcement'))->result();

        //BANNER SHOWS
        $data['banner3_large'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'banner3_large'))->row();
        $data['banner3_small'] = $this->Access->readtable('media','media_url',array('media_page'=>'home', 'media_section'=>'banner3_small'))->row();
        ////CAPTION
        $data['banner3_caption'] = $this->Access->readtable('general','',array('general_page'=>'home','general_section'=>'banner3_caption'))->row();

        $view['content']   = $this->load->view('kidsfun/v_home',$data,TRUE);

        //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'home';
        $view['title'] = 'home';

        $this->load->view('kidsfun/v_master',$view);
    }

    // function cek(){
    //     session_destroy();
    // }
}
