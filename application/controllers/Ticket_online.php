<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_online extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }

    public function index(){
        $this->load->view('email/v_email_ticket.php');
    }
}