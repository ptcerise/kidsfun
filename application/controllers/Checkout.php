<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['checkout_content'] = $this->Access->readtable('general','',array('general_section'=>'content_top','general_page'=>'Checkout'))->row();
        $data['cart_tickets'] = $this->session->userdata('cart_tickets');


        // RANDOM REFNO
        $sub1 = date("dmY");
        $sub2 = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 5);
        $ref = $sub1 . $sub2;
        // END

        // SIGNATURE PAYMENT
        $tickets_amount = $this->session->userdata('cart_tickets');
        $amount         = array_sum(array_column($tickets_amount,'total_price'));
        $data['amount']         = $amount*100;
        $merchantkey            = "93VYBuvIZy";
        $data['merchantcode']   = "ID00423";
        $data['refno']          = $ref;
        $data['currency']       = "IDR";
        $sign = $merchantkey.$data['merchantcode'].$data['refno'].$data['amount'].$data['currency'];
        $data['signature']      = $this->iPay88_signature($sign);
        // END SIGNATURE

        // USER DATA
        // $max_id = $this->db->query("select max(checkout_id) as max_id from checkout")->row()->max_id;
        // $data_checkout = $this->Access->readtable('checkout','',array('checkout_id'=>$max_id))->row();
        // $data['user_name'] = $data_checkout->checkout_fname;
        // $data['user_email'] = $data_checkout->checkout_email;
        // $data['user_contact'] = $data_checkout->checkout_phone;
        // END

        // USER DATA
        // $checkout_data = $this->session->userdata('checkout_data');
        // $data['user_name'] = $checkout_data['checkout_fname'];
        // $data['user_email'] = $checkout_data['checkout_email'];
        // $data['user_contact'] = $checkout_data['checkout_phone'];

        $view['content']   = $this->load->view('kidsfun/v_checkout',$data,TRUE);
         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }
        
        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'check out';
        $this->load->view('kidsfun/v_master',$view);
    }

    public function success()
    {
        // INSERT DATA PAYMENT TO DATABASE
        // $cart_tickets = $this->session->userdata('cart_tickets');

        // $data_payment = array(
        //     'checkout_fname' => $this->input->post('UserName'),
        //     'checkout_lname' => $this->input->post('checkout_lname'),
        //     'checkout_email' => $this->input->post('UserEmail'),
        //     'checkout_phone' => $this->input->post('UserContact'),
        //     'checkout_address' => $this->input->post('checkout_address'),
        //     'checkout_city' => $this->input->post('checkout_city'),
        //     'checkout_zip' => $this->input->post('checkout_zip'),
        //     'checkout_total' => $this->input->post('Amount')
        // );
        
        // $this->db->trans_begin();
        // $this->db->set('checkout_date', 'NOW()', FALSE);
        // $this->Access->inserttable('checkout',$data_payment);
        // $this->db->trans_complete();

        // if ($this->db->trans_status() === FALSE)
        // {
        //     $this->db->trans_rollback();
        // }

        // foreach($cart_tickets as $val)
        // {
        //     $checkout_id = $this->db->query("select max(checkout_id) as max_id from checkout")->row()->max_id;

        //     $cart = array(
        //         'checkout_id' => $checkout_id,
        //         'tickets_id' => $val['tickets_id'],
        //         'chart_qty' => $val['chart_qty'],
        //     );
        //     $this->db->insert('chart', $cart);
        // }
        // session_destroy();
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $checkout_data = $this->session->userdata('checkout_data');

        $max_id_c = $this->db->query("select max(checkout_id) as max_id from checkout")->row()->max_id;
        $max_id_p = $this->db->query("select max(payment_id) as max_id_pay from payment")->row()->max_id_pay;

        // EMAIL CONTENT
        $email['data_checkout'] = $this->Access->readtable('checkout','',array('checkout_id'=>$max_id_c))->row();
        $email['data_payment'] = $this->Access->readtable('payment','',array('payment_id'=>$max_id_p))->row();

        // $data_checkout = $this->Access->readtable('checkout','',array('checkout_id'=>$max_id_c))->row();

        // $this->db->select('*');
        // $this->db->from('chart ch');
        // $this->db->join('checkout ck', 'ch.checkout_id=ck.checkout_id');
        // $this->db->join('tickets tc', 'ch.tickets_id=tc.tickets_id');
        // $this->db->where('ch.checkout_id', $max_id_c);
        // $this->db->group_by('tc.tickets_name');
        // $email['data_tickets'] = $this->db->get()->result();

        $email['data_tickets'] = $this->session->userdata('cart_tickets');

        // GENERATE VIEW TO PDF
        //load the view and saved it into $html variable
        $html = $this->load->view('email/v_email_pdf', $email, true);
 
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Kidsfun_Online_Ticket.pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //attach.
        $this->m_pdf->pdf->Output($pdfFilePath, 'F');
        // END GENERATE

        // SEND TO CUSTOMER EMAIL
        #Configure the E-mail content
        $config = array();
        $config['charset'] = 'utf-8';
        $config['useragent'] = 'Yogyagokart';
        $config['protocol']= "smtp";
        $config['mailtype']= "html";
        $config['smtp_host']= "ssl://kidsfun.co.id";//smtp
        $config['smtp_port']= "465";
        $config['smtp_timeout']= "10";
        $config['smtp_user']= "notification@kidsfun.co.id"; // sender email
        $config['smtp_pass']= "kidsfun#2016"; // sender password
        $config['crlf']="\r\n";
        $config['newline']="\r\n";
        $config['wordwrap'] = TRUE;
        $config['validate'] = TRUE;
        $config['priority'] = 2;
        $this->email->initialize($config);

        $this->email->from('info@kidsfun.co.id');
        $this->email->to($checkout_data['checkout_email']);
        // $this->email->reply_to('info@kidsfun.co.id');
        $this->email->subject('Kidsfun Parc Online Ticketing');
        $this->email->message($this->load->view('email/v_email',$email, true));
        $this->email->attach($pdfFilePath);
        $this->email->send();
            
        #Send E-mail END

        $this->session->unset_userdata('cart_tickets');
        $this->session->unset_userdata('checkout_data');


        $data['success_checkout'] = $this->Access->readtable('general','',array('general_section'=>'content_success','general_page'=>'Checkout'))->row();

        $view['content']   = $this->load->view('kidsfun/v_checkout_success',$data,TRUE);
         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }
        //CURRENT PAGE
        $view['current'] = 'check out';
        $this->load->view('kidsfun/v_master',$view);
    }

    function response()
    {

        $merchantcode   = $_REQUEST["MerchantCode"];
        $paymentid      = $_REQUEST["PaymentId"];
        $refno          = $_REQUEST["RefNo"];
        $amount         = $_REQUEST["Amount"];
        $ecurrency      = $_REQUEST["Currency"];
        $remark         = $_REQUEST["Remark"];
        $transid        = $_REQUEST["TransId"];
        $authcode       = $_REQUEST["AuthCode"];
        $estatus        = $_REQUEST["Status"];
        $errdesc        = $_REQUEST["ErrDesc"];
        $signature      = $_REQUEST["Signature"];

        $merchantkey = "93VYBuvIZy";
        $status = "1";

        $sign = $merchantkey.$merchantcode.$paymentid.$refno.$amount.$ecurrency.$status;
        $cek_signature = $this->iPay88_signature($sign);

        if($cek_signature == $signature){

            // INSERT DATA PAYMENT TO DATABASE
            $payment = array(
                'payment_merchantcode'=>$merchantcode,
                'payment_paymentid'=>$paymentid,
                'payment_refno'=>$refno,
                'payment_amount'=>$amount,
                'payment_currency'=>$ecurrency,
                'payment_remark'=>$remark,
                'payment_transid'=>$transid,
                'payment_authcode'=>$authcode,
                'payment_status'=>$estatus,
                'payment_errdesc'=>$errdesc,
                'payment_signature'=>$signature,
            );

            $this->Access->inserttable('payment',$payment);
            // end

            $checkout_data = $this->session->userdata('checkout_data');
        
            $insert_checkout = array(
                'checkout_fname' => $checkout_data['checkout_fname'],
                'checkout_lname' => $checkout_data['checkout_lname'],
                'checkout_email' => $checkout_data['checkout_email'],
                'checkout_phone' => $checkout_data['checkout_phone'],
                'checkout_address' => $checkout_data['checkout_address'],
                'checkout_city' => $checkout_data['checkout_city'],
                'checkout_zip' => $checkout_data['checkout_zip'],
                'checkout_total' => $checkout_data['checkout_total'],
                'checkout_visit_date' => $checkout_data['checkout_visit_date']
                );
            $this->db->set('checkout_date', 'NOW()', FALSE);
            $this->Access->inserttable('checkout', $insert_checkout);

            $cart_tickets = $this->session->userdata('cart_tickets');
            foreach($cart_tickets as $val)
            {
                $checkout_id = $this->db->query("select max(checkout_id) as max_id from checkout")->row()->max_id;

                $cart = array(
                    'checkout_id' => $checkout_id,
                    'tickets_id' => $val['tickets_id'],
                    'chart_qty' => $val['chart_qty'],
                );
                $this->db->insert('chart', $cart);
            }

           //  $max_id_c = $this->db->query("select max(checkout_id) as max_id from checkout")->row()->max_id;
           //  $max_id_p = $this->db->query("select max(payment_id) as max_id_pay from payment")->row()->max_id_pay;

           //  // EMAIL CONTENT
           //  $view['data_checkout'] = $this->Access->readtable('checkout','',array('checkout_id'=>$max_id_c))->row();
           //  $view['data_payment'] = $this->Access->readtable('payment','',array('payment_id'=>$max_id_p))->row();

           //  // $data_checkout = $this->Access->readtable('checkout','',array('checkout_id'=>$max_id_c))->row();

           //  $this->db->select('*');
           //  $this->db->from('chart ch');
           //  $this->db->join('checkout ck', 'ch.checkout_id=ck.checkout_id');
           //  $this->db->join('tickets tc', 'ch.tickets_id=tc.tickets_id');
           //  $this->db->where('ch.checkout_id', $max_id_c);
           //  $this->db->group_by('tc.tickets_name');
           //  $view['data_tickets'] = $this->db->get()->result();

           //  // GENERATE VIEW TO PDF
           //  //load the view and saved it into $html variable
           //  $html = $this->load->view('email/v_email_pdf', $view, true);
     
           //  //this the the PDF filename that user will get to download
           //  $pdfFilePath = "Kidsfun_Online_Ticket.pdf";
     
           //  //load mPDF library
           //  $this->load->library('m_pdf');
     
           // //generate the PDF from the given html
           //  $this->m_pdf->pdf->WriteHTML($html);
     
           //  //attach.
           //  $this->m_pdf->pdf->Output($pdfFilePath, 'F');
           //  // END GENERATE

           //  // SEND TO CUSTOMER EMAIL
           //  #Configure the E-mail content
           //  $config = array();
           //  $config['charset'] = 'utf-8';
           //  $config['useragent'] = 'Yogyagokart';
           //  $config['protocol']= "smtp";
           //  $config['mailtype']= "html";
           //  $config['smtp_host']= "ssl://kidsfun.co.id";//smtp
           //  $config['smtp_port']= "465";
           //  $config['smtp_timeout']= "10";
           //  $config['smtp_user']= "notification@kidsfun.co.id"; // sender email
           //  $config['smtp_pass']= "kidsfun#2016"; // sender password
           //  $config['crlf']="\r\n";
           //  $config['newline']="\r\n";
           //  $config['wordwrap'] = TRUE;
           //  $config['validate'] = TRUE;
           //  $config['priority'] = 2;
           //  $this->email->initialize($config);

           //  $this->email->from('info@kidsfun.co.id');
           //  $this->email->to($checkout_data['checkout_email']);
           //  // $this->email->reply_to('info@kidsfun.co.id');
           //  $this->email->subject('Kidsfun Parc Online Ticketing');
           //  $this->email->message($this->load->view('email/v_email',$view, true));
           //  $this->email->attach($pdfFilePath);
           //  $this->email->send();
                
           //  #Send E-mail END

           //  $this->session->unset_userdata('cart_tickets');
           //  $this->session->unset_userdata('checkout_data');
            redirect('checkout/success');
        }
        else
        {
            $back = base_url();
            echo "Payment canceled<br><br>";
            echo "<a href='$back'>Back to Kidsfun</a>";
        }
    }

    function insert_checkout()
    {
        // $cart_tickets = $this->session->userdata('cart_tickets');

        // INSERT DATA CHECKOUT TO DATABASE
        $data_payment = array(
            'checkout_fname' => $this->input->post('checkout_fname'),
            'checkout_lname' => $this->input->post('checkout_lname'),
            'checkout_email' => $this->input->post('checkout_email'),
            'checkout_phone' => $this->input->post('checkout_phone'),
            'checkout_address' => $this->input->post('checkout_address'),
            'checkout_city' => $this->input->post('checkout_city'),
            'checkout_zip' => $this->input->post('checkout_zip'),
            'checkout_total' => $this->input->post('checkout_total'),
            'checkout_visit_date' => $this->input->post('checkout_visit')
        );

        $this->session->set_userdata('checkout_data', $data_payment);

    }

    function iPay88_signature($source)
    {
        return base64_encode(hex2bin(sha1($source)));
    }

    function hex2bin($hexSource)
    {
        for ($i=0;$i<strlen($hexSource);$i=$i+2)
        {
            $bin .= chr(hexdec(substr($hexSource,$i,2)));
        }
      return $bin;
    }

    // Test for Offline mode
    function test_email()
    {
        $checkout_data = $this->session->userdata('checkout_data');
        
        $insert_checkout = array(
            'checkout_fname' => $checkout_data['checkout_fname'],
            'checkout_lname' => $checkout_data['checkout_lname'],
            'checkout_email' => $checkout_data['checkout_email'],
            'checkout_phone' => $checkout_data['checkout_phone'],
            'checkout_address' => $checkout_data['checkout_address'],
            'checkout_city' => $checkout_data['checkout_city'],
            'checkout_zip' => $checkout_data['checkout_zip'],
            'checkout_total' => $checkout_data['checkout_total'],
            'checkout_visit_date' => $checkout_data['checkout_visit_date']
            );
        $this->db->set('checkout_date', 'NOW()', FALSE);
        $this->Access->inserttable('checkout', $insert_checkout);

        $cart_tickets = $this->session->userdata('cart_tickets');
        foreach($cart_tickets as $val)
        {
            $checkout_id = $this->db->query("select max(checkout_id) as max_id from checkout")->row()->max_id;

            $cart = array(
                'checkout_id' => $checkout_id,
                'tickets_id' => $val['tickets_id'],
                'chart_qty' => $val['chart_qty'],
            );
            $this->db->insert('chart', $cart);
        }
        redirect('checkout/success');
    }

}
