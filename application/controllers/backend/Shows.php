<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shows extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
            $this->load->model('Access');       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['banner_lg'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Shows'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Shows'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Shows'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Shows'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Shows'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Shows'))->row();

        $data['shows_single'] = $this->db->query("select `show`.*,`media`.* from `show`,`media` where show.show_id=media.temp_id AND media_page='Shows' group by show.show_id")->result();
        $data['shows_map'] = $this->Access->readtable('media','',array('media_section'=>'map','media_page'=>'Shows'))->row();


        $view['script']  = $this->load->view('backend/script/shows_single','',TRUE);
        $data['current'] = "shows"; 
		$view['content'] = $this->load->view('backend/shows/v_shows',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update banner for desktop ---------------------
    function banner_large()
    {
        $pic_shows_lg = $_FILES['pic_shows_lg']['name'];
        $break = explode('.', $pic_shows_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'shows_lg_'.$date.'.'.$ext;
        $path = './assets/upload/shows';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_shows_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shows/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shows/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shows/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_large','media_page'=>'Shows'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/shows/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/thumbnail/'.$old_image ));

                    $bg_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_large,array('media_section'=>'banner_large','media_page'=>'Shows'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/Shows');
    }

    // --------------- update banner for mobile ---------------------
    function banner_small()
    {
        $pic_shows_sm = $_FILES['pic_shows_sm']['name'];
        $break = explode('.', $pic_shows_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'shows_sm_'.$date.'.'.$ext;
        $path = './assets/upload/shows';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_shows_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shows/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shows/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shows/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_small','media_page'=>'Shows'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/shows/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/thumbnail/'.$old_image ));

                    $bg_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_small,array('media_section'=>'banner_small','media_page'=>'Shows'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/Shows');
    }

    // --------------- update banner caption ---------------------
    function banner_caption()
    {
        $caption = array(
                        'general_content_en'=>$this->input->post('text_shows_en'),
                        'general_content_in'=>$this->input->post('text_shows_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$caption,array('general_section'=>'banner_caption','general_page'=>'Shows'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Caption edited successfully.</div>';  
        }
        $_SESSION['info_caption'] = $notif;
        $this->session->mark_as_flash('info_caption');
        redirect('backend/Shows#caption');
    }

    // --------------- update show content ---------------------
    function edit_content()
    {
        $content = array(
                'general_content_en' => $this->input->post('text_content_en'),
                'general_content_in' => $this->input->post('text_content_in')
            );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section' => 'content','general_page'=>'Shows'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }

        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/Shows#content');
    }
}