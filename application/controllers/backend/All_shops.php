<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All_shops extends CI_Controller {

    public function __construct() {
        parent::__construct();       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang){
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    } 

    public function index()
    {
        /* ---- Show All Shops Banner Large ---- */
        $data['banner_large']   = $this->db
                            ->where('media_page','all_shops')
                            ->where('media_section','banner_large')
                            ->get('media')->row();

        /* ---- Show All Shops Banner Small ---- */
        $data['banner_small']   = $this->db
                            ->where('media_page','all_shops')
                            ->where('media_section','banner_small')
                            ->get('media')->row();

        /* ---- Show All Shops Banner Text ---- */
        $data['banner_caption']   = $this->db
                            ->where('general_page','all_shops')
                            ->where('general_section','banner_caption')
                            ->get('general')->row();

        /* ---- Show All Shops Content ---- */
        $data['all_shops']   = $this->db
                            ->where('general_page','all_shops')
                            ->where('general_section','content')
                            ->get('general')->row();

        $data['current'] = "All Shops";
		$view['content'] = $this->load->view('backend/shops/v_all_shops',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    /* --------------------- Update All Shops Banner Large ---------------------*/
    function save_banner_large()
    {
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_large_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'all_shops' AND media_section = 'banner_large' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_large_'.$date.'.'.$ext;
            $path       =   './assets/upload/shops/all_shops';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/thumbnail/'.$media_url ));
                   $image_1920px = $this->lang->line('image_1920px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'square')
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/thumbnail/'.$media_url ));
                        $landscape = $this->lang->line('landscape');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/shops/all_shops/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                    
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }
        $this->session->set_flashdata('all_shops_banner_large', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* --------------------- Update All Shops Banner Small --------------------- */
    function save_banner_small()
    {
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_small_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'all_shops' AND media_section = 'banner_small' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_small_'.$date.'.'.$ext;
            $path       =   './assets/upload/shops/all_shops';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 725 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/thumbnail/'.$media_url ));
                    $image_752px = $this->lang->line('image_752px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_752px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'landscape' || $orientation == 'square' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/thumbnail/'.$media_url ));
                        $portrait = $this->lang->line('portrait');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/shops/all_shops/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/shops/all_shops/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                    
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }
        $this->session->set_flashdata('all_shops_banner_small', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* --------------------- Update All Shops Banner Caption --------------------- */
    function save_banner_caption()
    {
        $data = [
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        ];

        $general_id = $this->input->post('general_id');

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $general_id)->update('general',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('all_shops_banner_caption', $notif);
        redirect('backend/all_shops#all_shops_banner_caption');
    }

    /* --------------------- Update All Shops Content --------------------- */
    function save_all_shops_content()
    {
        $data = [
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        ];
        
        $general_id = $this->input->post('general_id');


        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $general_id)->update('general',$data);                    
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('all_shops_content', $notif);
        redirect('backend/all_shops#all_shops_content');
    }
}