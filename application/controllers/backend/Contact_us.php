<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index() {
        
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['banner_lg'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Contact_us'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Contact_us'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Contact_us'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Contact_us'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Contact_us'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Contact_us'))->row();

        $data['message'] = $this->db->query('select * from contact order by contact_date DESC')->result();

        $data['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'Contact_us'))->row();
        $data['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Contact_us'))->row();
        $data['location'] = $this->Access->readtable('general','',array('general_section'=>'location','general_page'=>'contact_us'))->row();

        $data['lang']    = $this->session->userdata('be_lang');
        $data['current'] = "contact_us";
        $view['script']  = $this->load->view('backend/script/contact_us','',TRUE);
        $view['content'] = $this->load->view('backend/contact_us/v_contact_us',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }
    // --------------- update banner for desktop ---------------------
    function banner_large()
    {
        $pic_contact_lg = $_FILES['pic_contact_lg']['name'];
        $break = explode('.', $pic_contact_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'contact_lg_'.$date.'.'.$ext;
        $path = './assets/upload/contact';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_contact_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/contact/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x`</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/contact/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_large','media_page'=>'Contact_us'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/contact/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$old_image ));

                    $bg_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_large,array('media_section'=>'banner_large','media_page'=>'Contact_us'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/contact_us');
    }

    // --------------- update banner for mobile ---------------------
    function banner_small()
    {
        $pic_contact_sm = $_FILES['pic_contact_sm']['name'];
        $break = explode('.', $pic_contact_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'contact_sm_'.$date.'.'.$ext;
        $path = './assets/upload/contact';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_contact_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/contact/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/contact/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_small','media_page'=>'Contact_us'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/contact/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$old_image ));

                    $bg_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_small,array('media_section'=>'banner_small','media_page'=>'Contact_us'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/contact_us');
    }

    // --------------- update banner caption ---------------------
    function banner_caption()
    {
        $caption = array(
                        'general_content_en'=>$this->input->post('text_contact_en'),
                        'general_content_in'=>$this->input->post('text_contact_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$caption,array('general_section'=>'banner_caption','general_page'=>'Contact_us'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_caption'] = $notif;
        $this->session->mark_as_flash('info_caption');
        redirect('backend/contact_us#caption');
    }

    // --------------- update contact us content ---------------------
    function edit_content()
    {
        $content = array(
                'general_content_en' => $this->input->post('div_contact_en'),
                'general_content_in' => $this->input->post('div_contact_in')
            );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section' => 'content','general_page'=>'Contact_us'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/contact_us#content');
    }

    // --------------- update info contact ---------------------
    function info()
    {
        $phone_info = array(
                'general_content_en' => $this->input->post('phone')
            );
        $address_info = array(
                'general_content_en' => $this->input->post('address')
            );
        $location = array(
                'general_content_en' => $this->input->post('location')
            );

        $this->db->trans_begin();
        $this->Access->updatetable('general',$phone_info,array('general_section' => 'phone_number','general_page'=>'Contact_us'));
        $this->Access->updatetable('general',$address_info,array('general_section' => 'address','general_page'=>'Contact_us'));
        $this->Access->updatetable('general',$location,array('general_section' => 'location','general_page'=>'Contact_us'));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_contact'] = $notif;
        $this->session->mark_as_flash('info_contact');
        redirect('backend/contact_us#info');
    }

    // --------------- detail message ---------------------
    function view_message()
    {
        $contact_id = $this->input->post('contact_id');
        $status = array(
                'contact_status' => '1',
            );
        $this->Access->updatetable('contact',$status,array('contact_id'=>$contact_id));

        $data['view_message'] = $this->Access->readtable('contact','',array('contact_id'=>$contact_id))->row();

        $this->load->view('backend/contact_us/v_contact_msg',$data);
    }

    // --------------- delete message ---------------------
    function delete_message($contact_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('contact',array('contact_id'=>$contact_id));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $success    = $this->lang->line("delete");
            $notif      = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_message'] = $notif;
        $this->session->mark_as_flash('info_message');
        redirect('backend/contact_us#message');
    }
}