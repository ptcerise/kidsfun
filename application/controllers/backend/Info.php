<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');        
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['banner_large'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Info'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Info'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Info'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Info'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Info'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Info'))->row();

        $data['info'] = $this->Access->readtable('info','')->result();

        $data['current'] = "info";
        $view['script']  = $this->load->view('backend/script/info','',TRUE);
		$view['content'] = $this->load->view('backend/info/v_info',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update banner for desktop ---------------------
    function banner_large()
    {
        $pic_info_lg = $_FILES['pic_info_lg']['name'];
        $break = explode('.', $pic_info_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'info_lg_'.$date.'.'.$ext;
        $path = './assets/upload/info';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_info_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/info/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/info/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/info/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/info/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span>x</button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_large','media_page'=>'Info'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/info/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/info/thumbnail/'.$old_image ));

                    $bg_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_large,array('media_section'=>'banner_large','media_page'=>'Info'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                       $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/info');
    }

    // --------------- update banner for mobile ---------------------
    function banner_small()
    {
        $pic_info_sm = $_FILES['pic_info_sm']['name'];
        $break = explode('.', $pic_info_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'info_sm_'.$date.'.'.$ext;
        $path = './assets/upload/info';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_info_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/info/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/info/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/info/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/info/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_small','media_page'=>'Info'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/info/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/info/thumbnail/'.$old_image ));

                    $bg_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_small,array('media_section'=>'banner_small','media_page'=>'Info'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/info');
    }

    // --------------- update banner caption ---------------------
    function banner_caption()
    {
        $caption = array(
                        'general_content_en'=>$this->input->post('text_info_en'),
                        'general_content_in'=>$this->input->post('text_info_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$caption,array('general_section'=>'banner_caption','general_page'=>'Info'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
        }
        $_SESSION['info_caption'] = $notif;
        $this->session->mark_as_flash('info_caption');
        redirect('backend/info#caption');
    }

    // --------------- edit info content ---------------------
    function edit_content()
    {
        $content = array(
                'general_content_en' => $this->input->post('div_info_en'),
                'general_content_in' => $this->input->post('div_info_in')
            );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section' => 'content','general_page'=>'Info'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/info#content');
    }

    // --------------- save new info ---------------------
    function add_save()
    {
        $new_info = array(
                            'info_title_en' => $this->input->post('info_title_en'),
                            'info_title_in' => $this->input->post('info_title_in'),
                            'info_content_en' => $this->input->post('info_content_en'),
                            'info_content_in' => $this->input->post('info_content_in'),
                            );
        $this->db->trans_begin();
        $this->db->insert('info',$new_info);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("insert");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/info#info_list');
    }

    // --------------- form edit info ---------------------
    function edit_form()
    {
        $info_id = $this->input->post('info_id');
        $data['edit_info'] = $this->Access->readtable('info','',array('info_id'=>$info_id))->row();

        $this->load->view('backend/info/v_info_edit',$data);
    }

    // --------------- edit info ---------------------
    function edit_save()
    {
        $info_id = $this->input->post('info_id');
        $this->db->trans_begin();
        $edit_info = array(
                            'info_title_en' => $this->input->post('info_title_en'),
                            'info_title_in' => $this->input->post('info_title_in'),
                            'info_content_en' => $this->input->post('info_content_en'),
                            'info_content_in' => $this->input->post('info_content_in'),
                            );

        $this->Access->updatetable('info',$edit_info,array('info_id'=>$info_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/info#info_list');
    }

    // --------------- delete info ---------------------
    function delete_info($info_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('info',array('info_id'=>$info_id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/info#info_list');
    }
}