<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Souvenirs extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');        
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['banner_lg'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Souvenirs'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Souvenirs'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Souvenirs'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Souvenirs'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'main_content','general_page'=>'Souvenirs'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'main_content','general_page'=>'Souvenirs'))->row();

        $data['content_img'] = $this->Access->readtable('shop_type','',array('shop_id'=>'2'))->row();
        
        $data['current'] = "souvenirs"; 
		$view['content'] = $this->load->view('backend/shops/v_souvenirs',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update banner for desktop ---------------------
    function banner_large()
    {
        $pic_souvenirs_lg = $_FILES['pic_souvenirs_lg']['name'];
        $break = explode('.', $pic_souvenirs_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'svn_lg_'.$date.'.'.$ext;
        $path = './assets/upload/shops/souvenirs';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_souvenirs_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_large','media_page'=>'Souvenirs'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$old_image ));

                    $bg_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_large,array('media_section'=>'banner_large','media_page'=>'Souvenirs'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/Souvenirs');
    }

    // ------------------ update banner for mobile ----------------------
    function banner_small()
    {
        $pic_souvenirs_sm = $_FILES['pic_souvenirs_sm']['name'];
        $break = explode('.', $pic_souvenirs_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'svn_sm_'.$date.'.'.$ext;
        $path = './assets/upload/shops/souvenirs';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_souvenirs_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_small','media_page'=>'Souvenirs'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$old_image ));

                    $bg_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_small,array('media_section'=>'banner_small','media_page'=>'Souvenirs'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/Souvenirs');
    }

    // ----------------- update banner caption -----------------
    function banner_caption() {

        $caption = array(
                        'general_content_en'=>$this->input->post('text_souvenirs_en'),
                        'general_content_in'=>$this->input->post('text_souvenirs_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$caption,array('general_section'=>'banner_caption','general_page'=>'Souvenirs'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
        }
        $_SESSION['info_caption'] = $notif;
        $this->session->mark_as_flash('info_caption');
        redirect('backend/Souvenirs#caption');
    }

    // ------------------- update souvenirs content 
    function souvenirs_content() {

        $souvenirs_img = $_FILES['souvenirs_img']['name'];
        if($souvenirs_img == '')
        {
            // -------------- update description ------------------
            $souvenirs = array(
                                'general_content_en' => $this->input->post('text_souvenirs_en'),
                                'general_content_in' => $this->input->post('text_souvenirs_in')
                                );

            $this->Access->updatetable('general',$souvenirs,array('general_section'=>'main_content','general_page'=>'Souvenirs'));

            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        else
        {
            // ------------ upload image content ----------------
            $break = explode('.', $souvenirs_img);
            $ext = $break[count($break) - 1];
            $date = date('dmYHis');
            $name_url = 'souv_img_'.$date.'.'.$ext;
            $path = './assets/upload/shops/souvenirs';

            if( ! file_exists( $path ) )
            {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
            }
            
            $this->piclib->get_config($name_url, $path);
            if( $this->upload->do_upload('souvenirs_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$name_url ));
                    $image_1920px = $this->lang->line('image_1920px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
                }
                else
                {
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$name_url ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                        }
                        $old_image = $this->Access->readtable('shop_type','shop_type_img',array('shop_id'=>'2'))->row()->shop_type_img;        
                        unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/'.$old_image ));
                        unlink( realpath( APPPATH.'../assets/upload/shops/souvenirs/thumbnail/'.$old_image ));
                            
                        $this->db->trans_begin();
                        $souvenirs = array(
                                        'general_content_en' => $this->input->post('text_souvenirs_en'),
                                        'general_content_in' => $this->input->post('text_souvenirs_in')
                                        );
                        $new_img = array(
                                           'shop_type_img' => $name_url ,
                                        );
                        $this->Access->updatetable('general',$souvenirs,array('general_section'=>'main_content','general_page'=>'Souvenirs'));
                        $this->Access->updatetable('shop_type',$new_img,array('shop_id'=>'2'));
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }
                        else {
                            $success = $this->lang->line("update");
                            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                        }
                    }
                }  
            }
            else{
                $error = $this->lang->line("error_image");
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/Souvenirs#content');
    }
}