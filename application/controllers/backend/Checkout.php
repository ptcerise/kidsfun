<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Access');       
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
    	$data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content_top','general_page'=>'Checkout'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content_top','general_page'=>'Checkout'))->row();
        $data['success_checkout'] = $this->Access->readtable('general','',array('general_section'=>'content_success','general_page'=>'Checkout'))->row();
        $data['checkout'] = $this->Access->readtable('checkout','','','','','checkout_date desc')->result();

        $data['lang']    = $this->session->userdata('be_lang');

        //REQUERY
        
        //

        $data['current'] = "checkout";
        $view['script']  = $this->load->view('backend/script/checkout','',TRUE);
        $view['content'] = $this->load->view('backend/tickets/v_checkout',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // ------------------ update checkout content -------------------------
    function checkout_content()
    {
    	$content = array(
                        'general_content_en'=>$this->input->post('text_checkout_en'),
                        'general_content_in'=>$this->input->post('text_checkout_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section'=>'content_top','general_page'=>'Checkout'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_checkout'] = $notif;
        $this->session->mark_as_flash('info_checkout');
        redirect('backend/checkout');
    }

    // ------------------ update checkout success -------------------------
    function checkout_success()
    {
        $content = array(
                        'general_title_en'=>$this->input->post('title_success_en'),
                        'general_title_in'=>$this->input->post('title_success_in'),
                        'general_content_en'=>$this->input->post('success_checkout_en'),
                        'general_content_in'=>$this->input->post('success_checkout_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section'=>'content_success','general_page'=>'Checkout'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_success'] = $notif;
        $this->session->mark_as_flash('info_success');
        redirect('backend/checkout#success');
    }

    function payment_detail()
    {
        $checkout_id = $this->input->post('checkout_id');

        $this->db->select('*');
        $this->db->from('chart ch');
        $this->db->join('checkout ck', 'ch.checkout_id=ck.checkout_id');
        $this->db->join('tickets tc', 'ch.tickets_id=tc.tickets_id');
        $this->db->where('ch.checkout_id', $checkout_id);
        $this->db->group_by('tc.tickets_name');
        $data['data_tickets'] = $this->db->get()->result();

        $data['payment_detail'] = $this->Access->readtable('checkout','',array('checkout_id'=>$checkout_id))->row();

        $this->load->view('backend/tickets/v_checkout_detail',$data);
    }

    function edit_form()
    {
        $checkout_id = $this->input->post('checkout_id');

        $this->db->select('*');
        $this->db->from('chart ch');
        $this->db->join('checkout ck', 'ch.checkout_id=ck.checkout_id');
        $this->db->join('tickets tc', 'ch.tickets_id=tc.tickets_id');
        $this->db->where('ch.checkout_id', $checkout_id);
        $this->db->group_by('tc.tickets_name');
        $data['data_tickets'] = $this->db->get()->result();
        
        $data['edit_payment'] = $this->Access->readtable('checkout','',array('checkout_id'=>$checkout_id))->row();

        $this->load->view('backend/tickets/v_checkout_edit',$data);
    }

    function edit_save()
    {
        $checkout_id = $this->input->post('checkout_id');

        $new_payment = array(
                            'checkout_fname' => $this->input->post('checkout_fname'),
                            'checkout_lname' => $this->input->post('checkout_lname'),
                            'checkout_phone' => $this->input->post('checkout_phone'),
                            'checkout_address' => $this->input->post('checkout_address'),
                            'checkout_city' => $this->input->post('checkout_city'),
                            'checkout_zip' => $this->input->post('checkout_zip'),
                            );
        $this->db->trans_begin();
        $this->db->where('checkout_id',$checkout_id);
        $this->db->update('checkout',$new_payment);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }  
        $_SESSION['info_payment'] = $notif;
        $this->session->mark_as_flash('info_payment');
        redirect('backend/checkout#payment');
    }

    function delete_payment($checkout_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('checkout',array('checkout_id'=>$checkout_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_payment'] = $notif;
        $this->session->mark_as_flash('info_payment');
        redirect('backend/checkout#payment');
    }



    function Requery($MerchantCode, $RefNo, $Amount){
        $query = "https://payment.ipay88.co.id/epayment/enquiry.asp?MerchantCode=" . $MerchantCode . "&RefNo=" . $RefNo . "&Amount=" . $Amount;  
        $url = parse_url($query); 
        $buf = "";
        $host = $url["host"]; 
        $path = $url["path"] . "?" . $url["query"]; 
        $timeout = 1; 
        $fp = fsockopen ($host, 80, $errno, $errstr, $timeout); 
        if ($fp) { 
            fputs ($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n"); 
            while (!feof($fp)) { 
                $buf .= fgets($fp, 128);    
            }    
        $lines = explode("\n", $buf);    
        $Result = $lines[count($lines)-1];    
        fclose($fp); 
        } else {
            echo '<script>';
            echo 'alert("Transaction failed!")';
            echo '</script>';
        }
        return $Result;
    }
}