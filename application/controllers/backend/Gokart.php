<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gokart extends CI_Controller {

    public function __construct() {
        parent::__construct();       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    } 

    public function index()
    {
        /* ---- Show Gokart Banner Large ---- */
        $data['banner_large']   = $this->db
                            ->where('media_page','gokart')
                            ->where('media_section','banner_large')
                            ->get('media')->row();

        /* ---- Show Gokart Banner Small ---- */
        $data['banner_small']   = $this->db
                            ->where('media_page','gokart')
                            ->where('media_section','banner_small')
                            ->get('media')->row();

        /* ---- Show Gokart Banner Text ---- */
        $data['banner_caption']   = $this->db
                            ->where('general_page','gokart')
                            ->where('general_section','banner_caption')
                            ->get('general')->row();

// ======================= ##############  =======================

         /* ---- Show Gokart image ---- */
        $data['gokart_image']   = $this->db
                            ->where('attraction_type','gokart')
                            ->where('attraction_section','gokart_image')
                            ->get('attraction')->result();

        /* ---- Show Gokart Content ---- */
        $data['gokart_content']   = $this->db
                            ->where('attraction_type','gokart')
                            ->where('attraction_section','gokart_content')
                            ->get('attraction')->row();
// ======================= ##############  =======================
        
        /* ---- Show Karts ---- */
        $data['karts']   = $this->db
                            ->where('attraction_type','gokart')
                            ->where('attraction_section','karts')
                            ->order_by('attraction_id','DESC')
                            ->get('attraction')->result();

// ======================= ##############  =======================
        /* ---- Show Tracks image ---- */
        $data['tracks_image']   = $this->db
                            ->where('attraction_type','gokart')
                            ->where('attraction_section','tracks_image')
                            ->get('attraction')->result();

        /* ---- Show Tracks Content ---- */
        $data['tracks_content']   = $this->db
                            ->where('attraction_type','gokart')
                            ->where('attraction_section','tracks_content')
                            ->get('attraction')->row();

        $data['current'] = "gokart";
        $view['script']  = $this->load->view('backend/script/gokart','',TRUE);
		$view['content'] = $this->load->view('backend/attractions/v_gokart',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    /* ============== karts Add ============== */
    function karts_add(){
        $this->load->view('backend/attractions/v_gokart_add');

    }

    /* ============== karts Edit ============== */
    function karts_edit($id=''){
        $data['modal'] = $this->db  ->where('attraction_id',$id)->where('attraction_type','gokart')->where('attraction_section','karts')
                                    ->get('attraction')->row();
        $this->load->view('backend/attractions/v_gokart_edit',$data);

    }

    /* ########## Update Gokart Banner Large ########## */
    function save_banner_large(){
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_large_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'gokart' AND media_section = 'banner_large' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_large_'.$date.'.'.$ext;
            $path       =   './assets/upload/attractions/gokart';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$media_url ));
                   $image_1920px = $this->lang->line('image_1920px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'square' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$media_url ));
                        $landscape = $this->lang->line('landscape');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }

        $this->session->set_flashdata('gokart_banner_large', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* ########## Update Gokart Banner Small ########## */
    function save_banner_small(){
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_small_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'gokart' AND media_section = 'banner_small' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_small_'.$date.'.'.$ext;
            $path       =   './assets/upload/attractions/gokart';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 725 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$media_url ));
                    $image_752px = $this->lang->line('image_752px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_752px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'landscape' || $orientation == 'square' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$media_url ));
                        $portrait = $this->lang->line('portrait');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                    }
                    else
                    { 
                        $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }

        $this->session->set_flashdata('gokart_banner_small', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* ########## Update Gokart Banner Caption ########## */
    function save_banner_caption(){
        $data = array(
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        );

        $general_id = $this->input->post('general_id');

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $general_id)->update('general',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('gokart_banner_caption', $notif);
        redirect('backend/gokart#gokart_banner_caption');
    }

    /* ======================= ################################### ======================= */

    /* ########## Update gokart image ########## */
    function save_gokart_image(){
        if(isset($_FILES['attraction_url']['name'])){
            $id = $this->input->post('attraction_id');
            $aflag = $this->input->post('aflag');

            $gettoId = $this->db->query(" SELECT * FROM attraction WHERE attraction_type = 'gokart' AND attraction_section = 'gokart_image' AND attraction_id = '".$id."' ")->row()->attraction_url;

            $attraction_url  =   $_FILES['attraction_url']['name'];
            $break      =   explode('.', $attraction_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $attraction_url  =   'gokart_image'.$aflag.'_'.$date.'.'.$ext;
            $path       =   './assets/upload/attractions/gokart';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                // $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create )
                    return;
            }
            
            $this->piclib->get_config($attraction_url, $path);
            if( $this->upload->do_upload('attraction_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1024 || $height < 576 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                    // unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$attraction_url ));
                   $image_1024px = $this->lang->line('image_1024px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1024px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                        // unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1024, 576 );
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            // $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                            unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$gettoId ));
                            // unlink( realpath(APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$gettoId ));

                            $attraction_url = array('attraction_url'=> $attraction_url);

                            $this->db->trans_begin();
                            $this->db->set('attraction_date', 'NOW()', FALSE);
                            $this->db->where('attraction_id', $id)->update('attraction',$attraction_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }
        $this->session->set_flashdata('gokart_image'.$aflag, $notif);
        redirect('backend/gokart#gokart_image'.$aflag);
    }

    /* ########## Update gokart Content ########## */
    function save_gokart_content(){
        $id = $this->input->post('attraction_id');
        $data = array(
            'attraction_content_in' => $this->input->post('attraction_content_in'),
            'attraction_content_en' => $this->input->post('attraction_content_en')
        );

        $this->db->trans_begin();
        $this->db->set('attraction_date', 'NOW()', FALSE);
        $this->db->where('attraction_id', $id)->update('attraction',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('gokart_content', $notif);
        redirect('backend/gokart#gokart_content');
    }

    /* ======================= ################################### ======================= */

    function save_karts_add(){
        if(isset($_FILES['attraction_url']['name'])){
            
            $attraction_url  =   $_FILES['attraction_url']['name'];
            $break      =   explode('.', $attraction_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $attraction_url  =   'karts_'.$date.'.'.$ext;
            $path       =   './assets/upload/attractions/gokart';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($attraction_url, $path);
            if( $this->upload->do_upload('attraction_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 512 || $height < 512 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                    $image_512px = $this->lang->line('image_512px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_512px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'landscape' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                        // unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                        $square = $this->lang->line('square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 512, 512);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            
                            $attraction_url = array(
                                'attraction_title_in'        => $this->input->post('attraction_title_in'),
                                'attraction_content_in'      => $this->input->post('attraction_content_in'),
                                'attraction_title_en'        => $this->input->post('attraction_title_en'),
                                'attraction_content_en'      => $this->input->post('attraction_content_en'),
                                'attraction_url'             => $attraction_url,
                                'attraction_type'            => 'gokart',
                                'attraction_section'         => 'karts'
                            );

                            $this->db->trans_begin();
                            $this->db->set('attraction_date', 'NOW()', FALSE);
                            $this->db->insert('attraction',$attraction_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
        }
        $this->session->set_flashdata('karts_insert', $notif);
        redirect('backend/gokart#gokart_karts');
    }

    /* ########## Update karts Update ########## */
    function save_karts_update(){
        $id = $this->input->post('attraction_id');

        $data = array(
            'attraction_title_in'      => $this->input->post('attraction_title_in'),
            'attraction_content_in'    => $this->input->post('attraction_content_in'),
            'attraction_title_en'      => $this->input->post('attraction_title_en'),
            'attraction_content_en'    => $this->input->post('attraction_content_en')
        );

        $this->db->trans_begin();
        $this->db->set('attraction_date', 'NOW()', FALSE);
        $this->db->where('attraction_id', $id)->update('attraction',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }
        
        $gettoId = $this->db->query(" SELECT * FROM attraction WHERE attraction_type = 'gokart' AND attraction_section = 'karts' AND attraction_id = '".$id."' ")->row()->attraction_url;

        $attraction_url  =   $_FILES['attraction_url']['name'];
        $break      =   explode('.', $attraction_url);
        $ext        =   strtolower($break[count($break) - 1]);
        $date       =   date('dmYHis');
        $attraction_url  =   'karts_'.$date.'.'.$ext;
        $path       =   './assets/upload/attractions/gokart';

        

        if( ! file_exists( $path ) ){
            $create = mkdir($path, 0777, TRUE);
            // $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create )
                return;
        }
        
        $this->piclib->get_config($attraction_url, $path);
        if( $this->upload->do_upload('attraction_url') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 512 || $height < 512 )
            {
                unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                // unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$attraction_url ));
                $image_512px = $this->lang->line('image_512px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_512px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'landscape' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                    // unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                    $square = $square = $this->lang->line('square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 512, 512);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();

                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$gettoId ));
                        // unlink( realpath(APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$gettoId ));

                        $attraction_url = array(
                                'attraction_title_in'        => $this->input->post('attraction_title_in'),
                                'attraction_content_in'      => $this->input->post('attraction_content_in'),
                                'attraction_title_en'        => $this->input->post('attraction_title_en'),
                                'attraction_content_en'      => $this->input->post('attraction_content_en'),
                                'attraction_url'             => $attraction_url
                        );

                        $this->db->trans_begin();
                        $this->db->set('attraction_date', 'NOW()', FALSE);
                        $this->db->where('attraction_id', $id)->update('attraction',$attraction_url);
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }else{
                            $updd = $this->lang->line('update');
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                        }
                    }
                }
            }
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';  
        }

        $this->session->set_flashdata('karts_update', $notif);
        redirect('backend/gokart#gokart_karts');
    }

    /*=============== DELETE karts ========================*/
    function karts_delete($id){
        $getold = $this->db->query(" SELECT * FROM attraction WHERE attraction_id = '".$id."' ")->row()->attraction_url;
        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$getold ));
        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$getold ));

        $this->db->where('attraction_id',$id)->delete('attraction');
        
        $delete = $this->lang->line('delete');
        $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$delete.'</div>';
        
        $this->session->set_flashdata('karts_delete', $notif);
        redirect('backend/gokart#gokart_karts');

    }

    /* ########## Update Tracks image ########## */
    function save_tracks_image(){
        if(isset($_FILES['attraction_url']['name'])){
            $id = $this->input->post('attraction_id');
            $pflag = $this->input->post('pflag');

            $gettoId = $this->db->query(" SELECT * FROM attraction WHERE attraction_type = 'gokart' AND attraction_section = 'tracks_image' AND attraction_id = '".$id."' ")->row()->attraction_url;

            $attraction_url  =   $_FILES['attraction_url']['name'];
            $break      =   explode('.', $attraction_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $attraction_url  =   'tracks_image'.$pflag.'_'.$date.'.'.$ext;
            $path       =   './assets/upload/attractions/gokart';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                // $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create )
                    return;
            }
            
            $this->piclib->get_config($attraction_url, $path);
            if( $this->upload->do_upload('attraction_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 512 || $height < 512 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                    // unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$attraction_url ));
                   $image_512px = $this->lang->line('image_512px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_512px.'</div>';
                }else{

                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'landscape' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                        // unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$attraction_url ));
                        $square = $this->lang->line('square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 512, 512);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();

                            unlink( realpath( APPPATH.'../assets/upload/attractions/gokart/'.$gettoId ));
                            // unlink( realpath(APPPATH.'../assets/upload/attractions/gokart/thumbnail/'.$gettoId ));

                            $attraction_url = array('attraction_url'=> $attraction_url);

                            $this->db->trans_begin();
                            $this->db->set('attraction_date', 'NOW()', FALSE);
                            $this->db->where('attraction_id', $id)->update('attraction',$attraction_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }

                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }

        $this->session->set_flashdata('tracks_image'.$pflag, $notif);
        redirect('backend/gokart#tracks_image'.$pflag);
    }


    /* ########## Update tracks Content ########## */
    function save_tracks_content(){
        $id = $this->input->post('attraction_id');
        $data = array(
            'attraction_content_in' => $this->input->post('attraction_content_in'),
            'attraction_content_en' => $this->input->post('attraction_content_en')
        );

        $this->db->trans_begin();
        $this->db->set('attraction_date', 'NOW()', FALSE);
        $this->db->where('attraction_id', $id)->update('attraction',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('tracks_content', $notif);
        redirect('backend/gokart#tracks_content');
    }
}