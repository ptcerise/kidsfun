<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping_cart extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Access');       
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {    
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'header_content','general_page'=>'Shopping_cart'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'header_content','general_page'=>'Shopping_cart'))->row();
        
        $view['script']  = $this->load->view('backend/script/shopping_cart','',TRUE);
        $data['current'] = "cart";
        $view['content'] = $this->load->view('backend/tickets/v_cart',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update shopcart content ---------------------
    function header_content()
    {
        $content = array(
                        'general_content_en'=>$this->input->post('text_cart_en'),
                        'general_content_in'=>$this->input->post('text_cart_in'),
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section'=>'header_content','general_page'=>'Shopping_cart'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['info_cart'] = $notif;
        $this->session->mark_as_flash('info_cart');
        redirect('backend/Shopping_cart');
    }

}