<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shows_single extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Access');        
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {   
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['banner_lg'] = $this->Access->readtable('media','',array('media_section'=>'banner_single_large','media_page'=>'Shows'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('media_section'=>'banner_single_small','media_page'=>'Shows'))->row();

        $data['shows_single'] = $this->db->query("select `show`.*,`media`.* from `show`,`media` where show.show_id=media.temp_id AND media_page='Shows' group by show.show_id")->result();

        $data['current'] = "shows_single";
        $view['script']  = $this->load->view('backend/script/shows_single','',TRUE);
        $view['content'] = $this->load->view('backend/shows/v_shows_single',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update banner for desktop ---------------------
    function banner_large()
    {
        $shows_single_lg = $_FILES['shows_single_lg']['name'];
        $break = explode('.', $shows_single_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_shows_lg_'.$date.'.'.$ext;
        $path = './assets/upload/shows/shows_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('shows_single_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id_lg'),'media_section'=>'banner_single_large','media_page'=>'Shows'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$old_image ));

                    $single_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$single_large,array('media_id'=>$this->input->post('media_id_lg'),'media_section'=>'banner_single_large','media_page'=>'Shows'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Shows#shows_list');
    }

    // --------------- update banner for mobile ---------------------
    function banner_small()
    {
        $shows_single_sm = $_FILES['shows_single_sm']['name'];
        $break = explode('.', $shows_single_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_shows_sm_'.$date.'.'.$ext;
        $path = './assets/upload/shows/shows_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('shows_single_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_752px.'</div>';
            }else{
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id_sm'),'media_section'=>'banner_single_small','media_page'=>'Shows'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$old_image ));

                    $single_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$single_small,array('media_id'=>$this->input->post('media_id_sm'),'media_section'=>'banner_single_small','media_page'=>'Shows'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Shows#shows_list');
    }

    // --------------- add show ---------------------
    function add_shows() {

        // -------- insert show content ----------
        $new_shows = array(
                            'show_title_en' => $this->input->post('show_title_en'),
                            'show_title_in' => $this->input->post('show_title_in'),
                            'show_content_en' => $this->input->post('show_content_en'),
                            'show_content_in' => $this->input->post('show_content_in'),
                            );

        $this->db->insert('show',$new_shows);
        $show_id = $this->db->query("select max(show_id) as max_id from `show`")->row()->max_id;

        //------------- upload image content --------------
        $count = count($_FILES['shows_img']['tmp_name'])-1;
        $files = $_FILES;
        for ($i=0; $i <= $count ; $i++) {

            $_FILES['shows_img']['name']= $files['shows_img']['name'][$i];
            $_FILES['shows_img']['type']= $files['shows_img']['type'][$i];
            $_FILES['shows_img']['tmp_name']= $files['shows_img']['tmp_name'][$i];
            $_FILES['shows_img']['error']= $files['shows_img']['error'][$i];
            $_FILES['shows_img']['size']= $files['shows_img']['size'][$i];

            $shows_img = $files['shows_img']['name'][$i];
            if(isset($shows_img))
            {
                $ext = pathinfo($shows_img,PATHINFO_EXTENSION);
                $date = date('dmYHis');
                $num = $i+1;
                $name_url = 'shows_img'.$num.'_'.$date.'.'.$ext;
                $path = './assets/upload/shows/shows_single';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createThumb )
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('shows_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Image must be at least 1920x939 px.</div>';
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->piclib->crop_image($cek, 939, 939);
                            $this->image_lib->crop();
                            $this->image_lib->clear();
                        }
                        $new_media = array(
                                       'temp_id' => $show_id,
                                       'media_section' => 'content_image',
                                       'media_page' => 'Shows',
                                       'media_url' => $name_url
                                    );
                        $this->db->trans_start();
                        $this->db->set('media_date', 'NOW()', FALSE);
                        $this->db->insert('media',$new_media);
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }
                        else
                        {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                        } 
                    }                
                }
            }
            else{
                $error = $this->lang->line("error_image");
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
            }
        }
        //-------------------------------------------------------------------------------------------------

        //-------------upload large banner---------------
        $shows_single_lg = $_FILES['shows_single_lg']['name'];
        $break = explode('.', $shows_single_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_shows_lg_'.$date.'.'.$ext;
        $path = './assets/upload/shows/shows_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('shows_single_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $bg_large = array(
                                'temp_id' => $show_id,
                                'media_section' => 'banner_single_large',
                                'media_page' => 'Shows',
                                'media_content_en' => $this->input->post('text_shows_en'),
                                'media_content_in' => $this->input->post('text_shows_in'),
                                'media_url' => $name_url
                            );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->db->insert('media',$bg_large);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        //-------------------------------------------------------------------------------------------------

        //--------------upload small banner------------------
        $shows_single_sm = $_FILES['shows_single_sm']['name'];
        $break = explode('.', $shows_single_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_shows_sm_'.$date.'.'.$ext;
        $path = './assets/upload/shows/shows_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('shows_single_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $bg_small = array(
                                'temp_id' => $show_id,
                                'media_section' => 'banner_single_small',
                                'media_page' => 'Shows',
                                'media_content_en' => $this->input->post('text_shows_en'),
                                'media_content_in' => $this->input->post('text_shows_in'),
                                'media_url' => $name_url
                            );

                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->db->insert('media',$bg_small);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        //-------------------------------------------------------------------------------------------------
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Shows#shows_list');
    }

    function save_image() {

        $shows_img1 = $_FILES['shows_img1']['name'];
        if($shows_img1 == '') {
            $shows_img2 = $_FILES['shows_img2']['name'];
            if($shows_img2 == '') {
                //-------- empty image -----------
                redirect ('backend/Shows_single');
            }else{
                // ----------- upload image2 ----------------
                $break = explode('.', $shows_img2);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url2 = 'shows_img2_'.$date.'.'.$ext;
                $path = './assets/upload/shows/shows_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url2, $path);
                if( $this->upload->do_upload('shows_img2') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url2 ));
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url2 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Image must be at least 1920x939 px.</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url2 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url2 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image2 = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id2'),'media_page'=>'Shows'))->row()->media_url;      
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$old_image2 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$old_image2 ));

                            $this->db->trans_begin();
                            $new_img2 = array(
                                               'media_url' => $name_url2
                                            );
                            $this->Access->updatetable('media',$new_img2,array('media_id'=>$this->input->post('media_id2'),'media_page'=>'Shows'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                $error = $this->lang->line("error_image");
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }else{
            $shows_img2 = $_FILES['shows_img2']['name'];
            if($shows_img2 == '') {
                // -------------- upload image 1 -----------------
                $shows_img1 = $_FILES['shows_img1']['name'];

                $break = explode('.', $shows_img1);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url1 = 'shows_img1_'.$date.'.'.$ext;
                $path = './assets/upload/shows/shows_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url1, $path);
                if( $this->upload->do_upload('shows_img1') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url1 ));
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url1 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url1 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url1 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image1 = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id1'),'media_page'=>'Shows'))->row()->media_url;      
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$old_image1 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$old_image1 ));

                            $this->db->trans_begin();
                            $new_img1 = array(
                                               'media_url' => $name_url1
                                            );
                            $this->Access->updatetable('media',$new_img1,array('media_id'=>$this->input->post('media_id1'),'media_page'=>'Shows'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                    $error = $this->lang->line("error_image");
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }else {
                // upload image1 + image2
                $shows_img1 = $_FILES['shows_img1']['name'];
                $shows_img2 = $_FILES['shows_img2']['name'];

                //----------image1---------------
                $break = explode('.', $shows_img1);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url1 = 'shows_img1_'.$date.'.'.$ext;
                $path = './assets/upload/shows/shows_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url1, $path);
                if( $this->upload->do_upload('shows_img1') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url1 ));
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url1 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url1 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url1 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image1 = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id1'),'media_page'=>'Shows'))->row()->media_url;      
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$old_image1 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$old_image1 ));

                            $this->db->trans_begin();
                            $new_img1 = array(
                                               'media_url' => $name_url1
                                            );
                            $this->Access->updatetable('media',$new_img1,array('media_id'=>$this->input->post('media_id1'),'media_page'=>'Shows'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                    $error = $this->lang->line("error_image");
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
                //====================================================================================================

                //-----------image2----------------
                $break = explode('.', $shows_img2);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url2 = 'shows_img2_'.$date.'.'.$ext;
                $path = './assets/upload/shows/shows_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url2, $path);
                if( $this->upload->do_upload('shows_img2') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url2 ));
                        unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url2 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>Image must be at least 1920x939 px.</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url2 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url2 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image2 = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id2'),'media_page'=>'Shows'))->row()->media_url;      
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$old_image2 ));
                            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$old_image2 ));

                            $this->db->trans_begin();
                            $new_img2 = array(
                                               'media_url' => $name_url2
                                            );
                            $this->Access->updatetable('media',$new_img2,array('media_id'=>$this->input->post('media_id2'),'media_page'=>'Shows'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                $error = $this->lang->line("error_image");
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Shows#shows_list');
    }

    function image_banner() {

        $show_id = $this->input->post('show_id');
        $data['shows_title'] = $this->Access->readtable('show','',array('show_id'=>$show_id))->row();

        $data['banner_lg'] = $this->Access->readtable('media','',array('temp_id'=>$show_id,'media_section'=>'banner_single_large','media_page'=>'Shows'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('temp_id'=>$show_id,'media_section'=>'banner_single_small','media_page'=>'Shows'))->row();
        $data['edit_caption'] = $this->Access->readtable('media','',array('temp_id'=>$show_id, 'media_section'=>'banner_single_large','media_page'=>'Shows'))->row();

        $this->load->view('backend/shows/v_shows_banner',$data);
    }

    function edit_caption() {

        $show_id = $this->input->post('show_id');
        
        $new_caption = array(
                            'media_content_en' => $this->input->post('text_single_en'),
                            'media_content_in' => $this->input->post('text_single_in'),
        );
        $this->db->trans_begin();
        $this->db->set('media_date', 'NOW()', FALSE);
        $this->Access->updatetable('media',$new_caption, array('temp_id'=>$show_id, 'media_page'=>'Shows'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Shows#shows_list');
    }

    function edit_form() {

        $show_id = $this->input->post('show_id');
        $data['edit_shows'] = $this->Access->readtable('show','',array('show_id'=>$show_id))->row();
        $data['edit_caption'] = $this->Access->readtable('media','',array('temp_id'=>$show_id, 'media_section'=>'banner_single_large','media_page'=>'Shows'))->row();

        $data['images'] = $this->Access->readtable('media','',array('temp_id'=>$show_id,'media_section'=>'content_image','media_page'=>'Shows'))->result();

        $this->load->view('backend/shows/v_shows_edit',$data);
    }

    function edit_shows() {

        $show_id = $this->input->post('show_id');
        
        $new_shows = array(
                            'show_title_en' => $this->input->post('show_title_en'),
                            'show_title_in' => $this->input->post('show_title_in'),
                            'show_content_en' => $this->input->post('show_content_en'),
                            'show_content_in' => $this->input->post('show_content_in'),
                            );
        $new_caption = array(
                            'media_content_en' => $this->input->post('text_single_en'),
                            'media_content_in' => $this->input->post('text_single_in'),
                            );
        $this->db->trans_begin();
        $this->db->where('show_id',$show_id);
        $this->db->update('show',$new_shows);
        $this->Access->updatetable('media',$new_caption, array('temp_id'=>$show_id, 'media_section'=>'content_image', 'media_page'=>'Shows'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }  
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Shows#shows_list');
    }

    function delete_shows($show_id)
    {
        $image = $this->Access->readtable('media','media_url',array('temp_id'=>$show_id, 'media_page'=>'Shows'))->result();
        foreach ($image as $row) {
            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$row->media_url ));
            unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$row->media_url ));
        };

        $this->db->trans_begin();
        $this->Access->deletetable('media',array('temp_id'=>$show_id, 'media_page'=>'Shows'));
        $this->Access->deletetable('show',array('show_id'=>$show_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Shows#shows_list');
    }

    function edit_map($show_id)
    {
        $shows_map = $_FILES['shows_map']['name'];
        $break = explode('.', $shows_map);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'shows_map_'.$date.'.'.$ext;
        $path = './assets/upload/shows/shows_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('shows_map') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1200 || $height < 1115 )
            {
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                $image_1200px = $this->lang->line('image_1200px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1200px.'</div>';
            }
            else
            {
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span>x</button>'.$lands_square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1200, 1115);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('map_id'),'media_section'=>'map','media_page'=>'Shows'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/shows/shows_single/thumbnail/'.$old_image ));

                    $map = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$map,array('media_id'=>$this->input->post('map_id'),'media_section'=>'map','media_page'=>'Shows'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else
                    {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else
        {
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_map'] = $notif;
        $this->session->mark_as_flash('info_map');
        redirect('backend/Shows#map');
    }
}