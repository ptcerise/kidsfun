<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Find_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['banner_lg'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Find_us'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Find_us'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Find_us'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Find_us'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Find_us'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Find_us'))->row();

        $data['open_time'] = $this->Access->readtable('general','',array('general_section'=>'opening_time','general_page'=>'Find_us'))->row();
        $data['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Find_us'))->row();
        $data['find_us_map'] = $this->Access->readtable('media','',array('media_section'=>'map','media_page'=>'Find_us'))->row();

        $data['current'] = "find_us"; 
		$view['content'] = $this->load->view('backend/find_us/v_find_us',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update banner for desktop ---------------------
    function banner_large()
    {
        $find_us_lg = $_FILES['find_us_lg']['name'];
        $break = explode('.', $find_us_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'find_lg_'.$date.'.'.$ext;
        $path = './assets/upload/find_us';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('find_us_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/find_us/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/find_us/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_large','media_page'=>'Find_us'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/find_us/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$old_image ));

                    $bg_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_large,array('media_section'=>'banner_large','media_page'=>'Find_us'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/find_us');
    }

    // --------------- update banner for mobile ---------------------
    function banner_small()
    {
        $find_us_sm = $_FILES['find_us_sm']['name'];
        $break = explode('.', $find_us_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'find_sm_'.$date.'.'.$ext;
        $path = './assets/upload/find_us';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('find_us_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/find_us/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/find_us/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_small','media_page'=>'Find_us'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/find_us/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$old_image ));

                    $bg_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_small,array('media_section'=>'banner_small','media_page'=>'Find_us'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/find_us');
    }

    // --------------- update banner caption ---------------------
    function banner_caption()
    {
        $caption = array(
                        'general_content_en'=>$this->input->post('text_find_en'),
                        'general_content_in'=>$this->input->post('text_find_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$caption,array('general_section'=>'banner_caption','general_page'=>'Find_us'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_caption'] = $notif;
        $this->session->mark_as_flash('info_caption');
        redirect('backend/find_us#caption');
    }

    // --------------- update content find us ---------------------
    function edit_content()
    {    
        $content = array(
                'general_content_en' => $this->input->post('text_content_en'),
                'general_content_in' => $this->input->post('text_content_in')
            );
        $insert = array(
                'general_section' => 'content',
                'general_page'=>'Find_us'
            );
        $cek = $this->Access->readtable('general','',array('general_section' => 'content','general_page'=>'Find_us'));
        
        $this->db->trans_begin();
        if($cek == '')
        {
            $this->Access->inserttable('general',$insert);
        }
        else
        {
            $this->db->set('general_date', 'NOW()', FALSE);
            $this->Access->updatetable('general',$content,array('general_section' => 'content','general_page'=>'Find_us'));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/find_us#content');
    }

    // --------------- update info find us ---------------------
    function info()
    {
        $find_us_map = $_FILES['find_us_map']['name'];
        if($find_us_map == '')
        {
            $time_info = array(
                'general_content_en' => $this->input->post('open_time')
            );
            $address_info = array(
                'general_content_en' => $this->input->post('address')
            );

            $this->db->trans_begin();
            $this->Access->updatetable('general',$time_info,array('general_section' => 'opening_time','general_page'=>'Find_us'));
            $this->Access->updatetable('general',$address_info,array('general_section' => 'address','general_page'=>'Find_us'));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        else
        {
            $break = explode('.', $find_us_map);
            $ext = $break[count($break) - 1];
            $date = date('dmYHis');
            $name_url = 'find_us_map_'.$date.'.'.$ext;
            $path = './assets/upload/find_us';

            if( ! file_exists( $path ) )
            {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
            }
            
            $this->piclib->get_config($name_url, $path);
            if( $this->upload->do_upload('find_us_map') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1200 || $height < 1115 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/find_us/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$name_url ));
                    $image_1200px = $this->lang->line('image_1200px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1200px.'</div>';
                }
                else
                {
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/find_us/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$name_url ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1200, 1115);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                        }
                        $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('map_id'),'media_section'=>'map','media_page'=>'Find_us'))->row()->media_url;        
                        unlink( realpath( APPPATH.'../assets/upload/find_us/'.$old_image ));
                        unlink( realpath( APPPATH.'../assets/upload/find_us/thumbnail/'.$old_image ));
                        $map = array(
                                        'media_url' => $name_url
                                    );
                        $time_info = array(
                                            'general_content_en' => $this->input->post('open_time')
                                        );
                        $address_info = array(
                                            'general_content_en' => $this->input->post('address')
                                        );

                        $this->db->trans_begin();
                        $this->Access->updatetable('general',$time_info,array('general_section' => 'opening_time','general_page'=>'Find_us'));
                        $this->Access->updatetable('general',$address_info,array('general_section' => 'address','general_page'=>'Find_us'));
                        $this->db->set('media_date', 'NOW()', FALSE);
                        $this->Access->updatetable('media',$map,array('media_id'=>$this->input->post('map_id'),'media_section'=>'map','media_page'=>'Find_us'));
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }
                        else {
                            $success = $this->lang->line("update");
                            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                        }
                    }
                }  
            }
            else{
                $error = $this->lang->line("error_image");
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
            }
        }
        $_SESSION['info_find_us'] = $notif;
        $this->session->mark_as_flash('info_find_us');
        redirect('backend/find_us#info');
    }
}