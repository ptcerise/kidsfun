<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    public function index()
    {
         /* --------------------- Show video  --------------------- */
        $data['video'] = $this->db
                                ->where('media_page','home')
                                ->where('media_section','video')
                                ->get('media')->row();

        /* --------------------- Show video Image  --------------------- */
        $data['video_banner'] = $this->db
                                ->where('media_page','home')
                                ->where('media_section','video_banner')
                                ->get('media')->row();

        /* --------------------- Show video caption  --------------------- */
        $data['video_content'] = $this->db
                                ->where('general_page','home')
                                ->where('general_section','video_content')
                                ->get('general')->row();

        /* --------------------- Show Home Divider 1 --------------------- */
        $data['divider_top'] = $this->db
                                ->where('general_page','home')
                                ->where('general_section','divider_top')
                                ->get('general')->row();

        /* --------------------- Show Home Rides --------------------- */
        $data['rides_content']= $this->db
                                ->where('general_page','home')
                                ->where('general_section','rides_content')
                                ->get('general')->row();

        $data['rides']   = $this->db
                                        ->where('general_page','home')
                                        ->where('general_section','rides')
                                        ->get('general')->result();

        /* --------------------- Show Home Divider 2 --------------------- */
        $data['divider_middle']  = $this->db
                                    ->where('general_page','home')
                                    ->where('general_section','divider_middle')
                                    ->get('general')->row();

        /* --------------------- Show Home Banner 1 Large --------------------- */
         $data['banner1_large']   = $this->db
                                        ->where('media_page','home')
                                        ->where('media_section','banner1_large')
                                        ->get('media')->row();

        /* --------------------- Show Home Banner 1 Small --------------------- */
        $data['banner1_small']   = $this->db
                                        ->where('media_page','home')
                                        ->where('media_section','banner1_small')
                                        ->get('media')->row();

        /* --------------------- Show Home Banner 1 Content --------------------- */
        $data['banner1_caption']   = $this->db
                                        ->where('general_page','home')
                                        ->where('general_section','banner1_caption')
                                        ->get('general')->row();

        /* --------------------- Show Home Divider 3 --------------------- */
        $data['divider_bottom']  = $this->db
                                    ->where('general_page','home')
                                    ->where('general_section','divider_bottom')
                                    ->get('general')->row();

        /* --------------------- Show Home Banner 2 Large --------------------- */
         $data['banner2_large']   = $this->db
                                        ->where('media_page','home')
                                        ->where('media_section','banner2_large')
                                        ->get('media')->row();

        /* ---------------------Show Home Banner 2 Small --------------------- */
        $data['banner2_small']   = $this->db
                                        ->where('media_page','home')
                                        ->where('media_section','banner2_small')
                                        ->get('media')->row();

        /* ---- Show Home banner 2 Text ---- */
        $data['banner2_caption']   = $this->db
                                        ->where('general_page','home')
                                        ->where('general_section','banner2_caption')
                                        ->get('general')->row(); 

        /* --------------------- Show Home Announcement --------------------- */
        $data['announcement']   = $this->db
                                        ->where('general_page','home')
                                        ->where('general_section','announcement')
                                        ->get('general')->result();

        /* --------------------- Show Home Banner 3 Large --------------------- */
        $data['banner3_large']   = $this->db
                                        ->where('media_page','home')
                                        ->where('media_section','banner3_large')
                                        ->get('media')->row();

        /* --------------------- Show Home Banner 3 Small --------------------- */
        $data['banner3_small']   = $this->db
                                        ->where('media_page','home')
                                        ->where('media_section','banner3_small')
                                        ->get('media')->row();

        /* --------------------- Show Home Banner 3 Content --------------------- */
        $data['banner3_caption']   = $this->db
                                        ->where('general_page','home')
                                        ->where('general_section','banner3_caption')
                                        ->get('general')->row();

        $data['current'] = "home";
		$view['content'] = $this->load->view('backend/home/v_home',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }


    /* --------------------- Update Home Video --------------------- */
    function save_video()
    {
        if(isset($_FILES['video']['name'])){
            $id = $this->input->post('media_id');

            $geld = $this->db->query("SELECT * FROM media WHERE media_page = 'home' AND media_section = 'video' AND media_id = '".$id."' ")->row()->media_url;

            $media_video= $_FILES['video']['name'];
            $dt = date('dmYHis');
            $ex = pathinfo($media_video, PATHINFO_EXTENSION);
            $configVideo['upload_path'] = './assets/upload/video';
            $configVideo['allowed_types'] = 'avi|flv|wmv|mp4|mkv';
            $configVideo['max_size'] = 8000;
            $configVideo['overwrite'] = TRUE;
            $configVideo['remove_spaces'] = TRUE;
            $video_name = 'video_'.$dt.'.'.$ex;
            $configVideo['video'] = $video_name;
            $configVideo['file_name'] = $video_name;

            $uploaddata = $video_name;

            if( ! file_exists( $configVideo['upload_path'] ) ){
                $create = mkdir($configVideo['upload_path'], 0777, TRUE);
                if( ! $create )
                    return;
            }


            $this->load->library('upload');
            $this->upload->initialize($configVideo);
            if(!$this->upload->do_upload('video')) {
                $error = $this->upload->display_errors();
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
            }else{
                unlink( realpath( APPPATH.'../assets/upload/video/'.$geld ));
                //$videoDetails = 
                $this->upload->data();
                //$data['video_name']= $configVideo['url'];
                //$data['video_detail'] = $videoDetails;
            
                $this->db->trans_begin();
                $this->db->set('media_date', 'NOW()', FALSE);
                $this->db->where('media_id', $id)->update('media',array('media_url'=> $uploaddata ));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                }else{
                    $updd = $this->lang->line('update');
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                }
            }

        }else{
            echo "Please select a file";    
        }

        $this->session->set_flashdata('video', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* --------------------- Update Video Bannner --------------------- */
    function save_video_banner()
    {
        $id = $this->input->post('media_id');

        $gettoId = $this->db->query("SELECT * FROM media WHERE media_page = 'home' AND media_section = 'video_banner' AND media_id = '".$id."' ")->row()->media_url;

        $media_url  =   $_FILES['media_url']['name'];
        $break      =   explode('.', $media_url);
        $ext        =   strtolower($break[count($break) - 1]);
        $date       =   date('dmYHis');
        $media_url  =   'video_banner_'.$date.'.'.$ext;
        $path       =   './assets/upload/home';

        if( ! file_exists( $path ) ){
            $create = mkdir($path, 0777, TRUE);
            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createTemp )
                return;
        }
        
        $this->piclib->get_config($media_url, $path);
        if( $this->upload->do_upload('media_url') ){
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 ){
                unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait'){
                    unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                    $lands_square = $this->lang->line('lands_square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                }else{
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() ){
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                        
                        unlink( realpath( APPPATH.'../assets/upload/home/'.$gettoId ));
                        unlink( realpath(APPPATH.'../assets/upload/upload/thumbnail/'.$gettoId ));

                        $media_url = array(
                            'media_url'=> $media_url);

                        $this->db->trans_begin();
                        $this->db->set('media_date', 'NOW()', FALSE);
                        $this->db->where('media_id', $id)->update('media',$media_url); 
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE){
                            $this->db->trans_rollback();
                        }else{
                            $updd = $this->lang->line('update');
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                        }
                    }
                }
            }
        }else{
            $error = $this->upload->display_errors();
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
        }

        $this->session->set_flashdata('video_image', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* --------------------- Update video Content --------------------- */
    function save_video_content()
    {
         $data = array(
            'general_title_in'      => $this->input->post('general_title_in'),
            'general_content_in'    => $this->input->post('general_content_in'),
            'general_title_en'      => $this->input->post('general_title_en'),
            'general_content_en'    => $this->input->post('general_content_en')
        );

        $id = $this->input->post('general_id');
        

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $id)->update('general',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('video_content', $notif);
        redirect('backend/home#video_content');
    }

    /* --------------------- Update Home Divider --------------------- */
    function save_dividers()
    {
        $divider_val = $this->input->post('divider_val');
        $div = array(
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en'),
        );
        
        $id = $this->input->post('general_id');

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id',$id)->update('general',$div);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('dividers'.$divider_val, $notif);
        redirect('backend/home#dividers'.$divider_val);
    }
    
    /* --------------------- Update Home Rides --------------------- */
    function save_rides_content()
    {
        $dt = array(
            'general_content_in'    => $this->input->post('general_content_in'),
            'general_content_en'    => $this->input->post('general_content_en')
        );

        $rides_id = $this->input->post('rides_id');
        
        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $rides_id)->update('general',$dt);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('rides_content', $notif);
        redirect('backend/home#rides_content');
    }

    /* --------------------- Update Home rides --------------------- */
    function save_rides()
    {
        $rides_val = $this->input->post('rides_val');
        $dt = array(
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        );
        
        $id = $this->input->post('general_id');
        
        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $id)->update('general',$dt);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('rides'.$rides_val, $notif);
        redirect('backend/home#rides'.$rides_val);
    }

    /* --------------------- Update Home Banner Large --------------------- */
    function save_banner_large()
    {
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_large_id');
            $banner_large_val = $this->input->post('banner_large_val');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'home' AND media_section = 'banner".$banner_large_val."_large' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_large'.$banner_large_val.'_'.$date.'.'.$ext;
            $path       =   './assets/upload/home';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') ){
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 ){
                    unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                    $image_1920px = $this->lang->line('image_1920px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'square'){
                        unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                        $landscape = $this->lang->line('landscape');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                    }else{
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() ){
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/home/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/home/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE){
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>'; 
            }
            
        }
        $this->session->set_flashdata('banner_large'.$banner_large_val, $notif);
        redirect('backend/home#banner_large'.$banner_large_val);
    }

    /* --------------------- Update Home Banner Small --------------------- */
    function save_banner_small()
    {
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_small_id');
            $banner_small_val = $this->input->post('banner_small_val');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'home' AND media_section = 'banner".$banner_small_val."_small' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_small'.$banner_small_val.'_'.$date.'.'.$ext;
            $path       =   './assets/upload/home';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') ){
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 752 || $height < 939 ){
                    unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                    $image_752px = $this->lang->line('image_752px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_752px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'landscape' || $orientation == 'square' ){
                        unlink( realpath( APPPATH.'../assets/upload/home/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$media_url ));
                        $portrait = $this->lang->line('portrait');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                    }else{
                        $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                        if( $this->image_lib->resize() ){
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/home/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/home/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE){
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }
        $this->session->set_flashdata('banner_small'.$banner_small_val, $notif);
        redirect('backend/home#banner_small'.$banner_small_val);
    }

    /* --------------------- Update Home Banner caption --------------------- */
    function save_banner_caption()
    {
        $banner_caption_val = $this->input->post('banner_caption_val'); 
        $data = array(
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        );
        $general_id = $this->input->post('general_id');       

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $general_id)->update('general',$data);               
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('banner_caption'.$banner_caption_val, $notif);
        redirect('backend/home#banner_caption'.$banner_caption_val);
    }

    
    /* --------------------- Update Home aAnnouncement --------------------- */
    function save_announcement()
    {
        $announcement_val = $this->input->post('announcement_val');
        $dt = array(
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        );
        
        $id = $this->input->post('general_id');
        
        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $id)->update('general',$dt);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('announcement'.$announcement_val, $notif);
        redirect('backend/home#announcement'.$announcement_val);
    }

}