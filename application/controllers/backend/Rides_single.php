<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rides_single extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
            $this->load->model('Access');        
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang)
            {
                $this->lang->load('kidsfun_backend',$be_lang);
            }
            else 
            {
                $this->lang->load('kidsfun_backend','english');
            }
    }
    
    function index()
    {   
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null)
        {
            $id = 2;   
        }
        else
        {
            $id = 1;
        }
        $rides_id = $this->input->post('rides_id');
        $data['rides_single'] = $this->db->query("select rides.*,rides_img.* from rides,rides_img where rides.rides_id=rides_img.rides_id group by rides.rides_id")->result();
        $data['rides_img'] = $this->Access->readtable('rides_img','',array('rides_id'=>$rides_id))->row();

    }

    //-------- update banner for desktop -----------
    function banner_large()
    {
        $rides_single_lg = $_FILES['rides_single_lg']['name'];
        $break = explode('.', $rides_single_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_rides_lg_'.$date.'.'.$ext;
        $path = './assets/upload/attractions/rides_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('rides_single_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
            }
            else
            {
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id_lg'),'media_section'=>'banner_single_large','media_page'=>'Rides'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$old_image ));

                    $single_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$single_large,array('media_id'=>$this->input->post('media_id_lg'),'media_section'=>'banner_single_large','media_page'=>'Rides'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else
                    {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else
        {
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Rides#rides_list');
    }

    //-------- update banner for mobile -----------
    function banner_small()
    {
        $rides_single_sm = $_FILES['rides_single_sm']['name'];
        $break = explode('.', $rides_single_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_rides_sm_'.$date.'.'.$ext;
        $path = './assets/upload/attractions/rides_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('rides_single_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_752px.'</div>';
            }
            else
            {
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('media_id_sm'),'media_section'=>'banner_single_small','media_page'=>'Rides'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$old_image ));

                    $single_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$single_small,array('media_id'=>$this->input->post('media_id_sm'),'media_section'=>'banner_single_small','media_page'=>'Rides'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else
                    {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else
        {
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Rides#rides_list');
    }

    //------------------ add rides ----------------------
    function add_rides()
    {
        $new_rides = array(
                            'rides_name' => $this->input->post('rides_name'),
                            'rides_category' => $this->input->post('rides_category'),
                            'rides_desc_en' => $this->input->post('rides_desc_en'),
                            'rides_desc_in' => $this->input->post('rides_desc_in'),
                            );

        $this->db->insert('rides',$new_rides);
        $rides_id = $this->db->query("select max(rides_id) as max_id from rides")->row()->max_id;

        //------------- upload 2 image content --------------
        $count = count($_FILES['rides_img']['tmp_name'])-1;
        $files = $_FILES;
        for ($i=0; $i <= $count ; $i++)
        {
            $_FILES['rides_img']['name']= $files['rides_img']['name'][$i];
            $_FILES['rides_img']['type']= $files['rides_img']['type'][$i];
            $_FILES['rides_img']['tmp_name']= $files['rides_img']['tmp_name'][$i];
            $_FILES['rides_img']['error']= $files['rides_img']['error'][$i];
            $_FILES['rides_img']['size']= $files['rides_img']['size'][$i];

            $rides_img = $files['rides_img']['name'][$i];
            if(isset($rides_img))
            {
                $ext = pathinfo($rides_img,PATHINFO_EXTENSION);
                $date = date('dmYHis');
                $num = $i+1;
                $name_url = 'rides_img'.$num.'_'.$date.'.'.$ext;
                $path = './assets/upload/attractions/rides_single';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createThumb )
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('rides_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->piclib->crop_image($cek, 939, 939);
                            $this->image_lib->crop();
                            $this->image_lib->clear();
                        }
                    }
                    $new_img = array(
                                       'rides_id' => $rides_id,
                                       'img_url' => $name_url
                                    );
                    $this->db->trans_start();
                    $this->db->insert('rides_img',$new_img);
                    $this->db->trans_complete();

                    $success = $this->lang->line("insert");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                }
                else
                {
                    $error = $this->lang->line("error_image");
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
                }
            }
        }

        //------------------- upload large banner ----------------------------
        $rides_single_lg = $_FILES['rides_single_lg']['name'];
        $break = explode('.', $rides_single_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_rides_lg_'.$date.'.'.$ext;
        $path = './assets/upload/attractions/rides_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('rides_single_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
            }
            else
            {
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square')
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $bg_large = array(
                                'temp_id' => $rides_id,
                                'media_section' => 'banner_single_large',
                                'media_page' => 'Rides',
                                'media_content_en' => $this->input->post('text_single_en'),
                                'media_content_in' => $this->input->post('text_single_in'),
                                'media_url' => $name_url
                            );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->db->insert('media',$bg_large);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else
                    {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }

        // ------------upload small banner---------------
        $rides_single_sm = $_FILES['rides_single_sm']['name'];
        $break = explode('.', $rides_single_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'bg_rides_sm_'.$date.'.'.$ext;
        $path = './assets/upload/attractions/rides_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('rides_single_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $bg_small = array(
                                'temp_id' => $rides_id,
                                'media_section' => 'banner_single_small',
                                'media_page' => 'Rides',
                                'media_content_en' => $this->input->post('text_single_en'),
                                'media_content_in' => $this->input->post('text_single_in'),
                                'media_url' => $name_url
                            );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->db->insert('media',$bg_small);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        // -------------------------------------------------------------------------------------
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Rides#rides_list');
    }

    // ------------------------ update image content --------------------------------
    function save_image() {

        $rides_img1 = $_FILES['rides_img1']['name'];
        if($rides_img1 == '') {
            $rides_img2 = $_FILES['rides_img2']['name'];
            if($rides_img2 == '') {
                //--------- empty image -----------
                redirect ('backend/Rides');
            }else{
                // ------- upload image2 -----------
                $break = explode('.', $rides_img2);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url2 = 'rides_img2_'.$date.'.'.$ext;
                $path = './assets/upload/attractions/rides_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url2, $path);
                if( $this->upload->do_upload('rides_img2') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url2 ));
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url2 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url2 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url2 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image2 = $this->Access->readtable('rides_img','img_url',array('img_id'=>$this->input->post('img_id2')))->row()->img_url;      
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$old_image2 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$old_image2 ));

                            $this->db->trans_begin();
                            $new_img2 = array(
                                               'img_url' => $name_url2
                                            );
                            $this->Access->updatetable('rides_img',$new_img2,array('img_id'=>$this->input->post('img_id2')));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                    $error = $this->lang-line("error_image");
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }else{
            $rides_img2 = $_FILES['rides_img2']['name'];
            if($rides_img2 == '') {
                // ------------- upload image 1 -------------------
                $rides_img1 = $_FILES['rides_img1']['name'];

                $break = explode('.', $rides_img1);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url1 = 'rides_img1_'.$date.'.'.$ext;
                $path = './assets/upload/attractions/rides_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url1, $path);
                if( $this->upload->do_upload('rides_img1') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url1 ));
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url1 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url1 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url1 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image1 = $this->Access->readtable('rides_img','img_url',array('img_id'=>$this->input->post('img_id1')))->row()->img_url;      
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$old_image1 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$old_image1 ));

                            $this->db->trans_begin();
                            $new_img1 = array(
                                               'img_url' => $name_url1
                                            );
                            $this->Access->updatetable('rides_img',$new_img1,array('img_id'=>$this->input->post('img_id1')));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                $error = $this->lang->line("error_image");
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
            else {
                // ------------ upload image1 + image2 -------------------------
                $rides_img1 = $_FILES['rides_img1']['name'];
                $rides_img2 = $_FILES['rides_img2']['name'];

                //----------image1---------------

                $break = explode('.', $rides_img1);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url1 = 'rides_img1_'.$date.'.'.$ext;
                $path = './assets/upload/attractions/rides_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url1, $path);
                if( $this->upload->do_upload('rides_img1') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url1 ));
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url1 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url1 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url1 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image1 = $this->Access->readtable('rides_img','img_url',array('img_id'=>$this->input->post('img_id1')))->row()->img_url;      
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$old_image1 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$old_image1 ));

                            $this->db->trans_begin();
                            $new_img1 = array(
                                               'img_url' => $name_url1
                                            );
                            $this->Access->updatetable('rides_img',$new_img1,array('img_id'=>$this->input->post('img_id1')));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                $error = $this->lang->line("error_image");
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }

                //-------------- image2 ----------------
                $break = explode('.', $rides_img2);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url2 = 'rides_img2_'.$date.'.'.$ext;
                $path = './assets/upload/attractions/rides_single';

                if( ! file_exists( $path ) )
                {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
                }

                $this->piclib->get_config($name_url2, $path);
                if( $this->upload->do_upload('rides_img2') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1920 || $height < 939 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url2 ));
                        unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url2 ));
                        $image_1920px = $this->lang->line('image_1920px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url2 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url2 ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                $this->piclib->crop_image($cek, 939, 939);
                                $this->image_lib->crop();
                                $this->image_lib->clear();
                            }
                            $old_image2 = $this->Access->readtable('rides_img','img_url',array('img_id'=>$this->input->post('img_id2')))->row()->img_url;      
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$old_image2 ));
                            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$old_image2 ));

                            $this->db->trans_begin();
                            $new_img2 = array(
                                               'img_url' => $name_url2
                                            );
                            $this->Access->updatetable('rides_img',$new_img2,array('img_id'=>$this->input->post('img_id2')));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                            }
                        }
                    }
                }
                else{
                    $error = $this->lang-line("error_image");
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Rides#rides_list');
    }

    // ----------------- form edit banner ---------------------------
    function image_banner() {

        $rides_id = $this->input->post('rides_id');
        $data['rides_name'] = $this->Access->readtable('rides','',array('rides_id'=>$rides_id))->row();

        $data['banner_lg'] = $this->Access->readtable('media','',array('temp_id'=>$rides_id,'media_section'=>'banner_single_large','media_page'=>'Rides'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('temp_id'=>$rides_id,'media_section'=>'banner_single_small','media_page'=>'Rides'))->row();

        $data['edit_caption'] = $this->Access->readtable('media','',array('temp_id'=>$rides_id, 'media_section'=>'banner_single_large','media_page'=>'Rides'))->row();


        $this->load->view('backend/attractions/v_rides_banner',$data);
    }

    // ----------------- update banner caption -----------------------
    function edit_caption() {

        $rides_id = $this->input->post('rides_id');

        $new_caption = array(
                            'media_content_en' => $this->input->post('text_single_en'),
                            'media_content_in' => $this->input->post('text_single_in'),
                            );

        $this->db->trans_begin();
        $this->db->set('media_date', 'NOW()', FALSE);
        $this->Access->updatetable('media',$new_caption, array('temp_id'=>$rides_id, 'media_page'=>'Rides'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }  
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Rides#rides_list');

    }

    // --------------------- form edit ride -------------------------
    function edit_form() {

        $rides_id = $this->input->post('rides_id');
        $data['edit_rides'] = $this->Access->readtable('rides','',array('rides_id'=>$rides_id))->row();
        $data['edit_img'] = $this->Access->readtable('rides_img','',array('rides_id'=>$rides_id))->row();
        $data['images'] = $this->Access->readtable('rides_img','',array('rides_id'=>$rides_id))->result();
        $this->load->view('backend/attractions/v_rides_edit',$data);
    }

    // ---------------------- save edited ride ------------------------
    function edit_rides() {

        $rides_id = $this->input->post('rides_id');
        
        $new_rides = array(
                            'rides_name' => $this->input->post('rides_name'),
                            'rides_category' => $this->input->post('rides_category'),
                            'rides_desc_en' => $this->input->post('rides_desc_en'),
                            'rides_desc_in' => $this->input->post('rides_desc_in'),
                            );

        $this->db->trans_begin();
        $this->db->where('rides_id',$rides_id);
        $this->db->update('rides',$new_rides);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }  
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Rides#rides_list');
    }

    // ---------------------- delete ride ----------------------
    function delete_rides($rides_id)
    {
        $content = $this->Access->readtable('rides_img','img_url',array('rides_id'=>$rides_id))->result();
        foreach ($content as $row) {
            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$row->img_url ));
            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$row->img_url ));
        };

        $banner = $this->Access->readtable('media','media_url',array('temp_id'=>$rides_id, 'media_page'=>'Rides'))->result();
        foreach ($banner as $row) {
            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$row->media_url ));
            unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$row->media_url ));
        };

        $this->db->trans_begin();
        $this->Access->deletetable('rides_img',array('rides_id'=>$rides_id));
        $this->Access->deletetable('rides',array('rides_id'=>$rides_id));
        $this->Access->deletetable('media',array('temp_id'=>$rides_id, 'media_page'=>'Rides'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/Rides#rides_list');
    }

    function edit_map($rides_id)
    {
        $rides_map = $_FILES['rides_map']['name'];
        $break = explode('.', $rides_map);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'rides_map_'.$date.'.'.$ext;
        $path = './assets/upload/attractions/rides_single';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('rides_map') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1200 || $height < 1115 )
            {
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                $image_1200px = $this->lang->line('image_1200px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1200px.'</div>';
            }
            else
            {
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span>x</button>'.$lands_square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1200, 1115);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$this->input->post('map_id'),'media_section'=>'map','media_page'=>'Rides'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/attractions/rides_single/thumbnail/'.$old_image ));

                    $map = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$map,array('media_id'=>$this->input->post('map_id'),'media_section'=>'map','media_page'=>'Rides'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else
                    {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else
        {
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
        }
        $_SESSION['info_map'] = $notif;
        $this->session->mark_as_flash('info_map');
        redirect('backend/Rides#map');
    }
}