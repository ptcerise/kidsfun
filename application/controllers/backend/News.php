<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
           
    } 
    public function index()
    {
        /* --------------------- Show News Banner Large --------------------- */
        $data['banner_large']   = $this->db
                            ->where('media_page','news')
                            ->where('media_section','banner_large')
                            ->get('media')->row();

        /* --------------------- Show News Banner Small --------------------- */
        $data['banner_small']   = $this->db
                            ->where('media_page','news')
                            ->where('media_section','banner_small')
                            ->get('media')->row();

        /* --------------------- Show News Banner Caption --------------------- */
        $data['banner_caption']   = $this->db
                            ->where('general_page','news')
                            ->where('general_section','banner_caption')
                            ->get('general')->row();

        /* --------------------- Show News Content --------------------- */
        $data['news']   = $this->db
                            ->where('general_page','news')
                            ->where('general_section','content')
                            ->get('general')->row();

        /* --------------------- Show news --------------------- */
        $data['show'] = $this->db
                             ->order_by('news_date','DESC')
                             ->get('news')->result();

        $data['current'] = "news"; 
        $view['script']  = $this->load->view('backend/script/news','',TRUE);
		$view['content'] = $this->load->view('backend/news/v_news',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    /* ---------------------  News Add --------------------- */
    function news_add()
    {
        $this->load->view('backend/news/v_news_add');
    }

    /* ---------------------  News Edit banner --------------------- */
    function news_edit_banner($banner=''){
        $data['media1'] = $this->db->where('temp_id',$banner)->where('media_section','banner_single_large')->get('media')->row();
        $data['media2'] = $this->db->where('temp_id',$banner)->where('media_section','banner_single_small')->get('media')->row();
        $this->load->view('backend/news/v_news_edit_banner',$data);
    }

    /* ---------------------  news Edit Content --------------------- */
    function news_edit_content($id='')
    {
        $data['modal'] = $this->db->where('news_id',$id)->get('news')->row();
        $data['media'] = $this->db->where('temp_id',$id)->where('media_section','image_content')->get('media')->result();
        $this->load->view('backend/news/v_news_edit_content',$data);
    }

    /* --------------------- Update News Banner Large --------------------- */
    function save_banner_large()
    {
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_large_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'news' AND media_section = 'banner_large' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_large_'.$date.'.'.$ext;
            $path       =   './assets/upload/news';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                    $image_1920px = $this->lang->line('image_1920px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'square')
                    {
                        unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                        $landscape = $this->lang->line('landscape');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/news/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/news/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }
        $this->session->set_flashdata('news_banner_large', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* --------------------- Update news Banner Small --------------------- */
    function save_banner_small()
    {
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('banner_small_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'news' AND media_section = 'banner_small' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_small_'.$date.'.'.$ext;
            $path       =   './assets/upload/news';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 752 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                    $image_752px = $this->lang->line('image_752px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_752px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'landscape' || $orientation == 'square' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                        $portrait = $this->lang->line('portrait');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/news/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/news/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                   
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }
        $this->session->set_flashdata('news_banner_small', $notif);
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* --------------------- Update news Banner Caption --------------------- */
    function save_banner_caption()
    {
        $data = array(
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        );
       
        $general_id = $this->input->post('general_id');

        $this->db->trans_begin(); 
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $general_id)->update('general',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('news_banner_caption', $notif);
        redirect('backend/news#news_banner_caption');
    }

    /* --------------------- Update news Content --------------------- */
    function save_news_content()
    {
        $data = array(
            'general_content_in' => $this->input->post('general_content_in'),
            'general_content_en' => $this->input->post('general_content_en')
        );
       
        $general_id = $this->input->post('general_id');

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->db->where('general_id', $general_id)->update('general',$data);                      
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata('news_content', $notif);
        redirect('backend/news#news_content');
    }

    // add News
    function save(){
        $news_url1      =   $_FILES['news_url1']['name'];
        $news_url2      =   $_FILES['news_url2']['name'];
        $banner_large   =   $_FILES['banner_large']['name'];
        $banner_small   =   $_FILES['banner_small']['name'];
        $ext1           =   pathinfo($news_url1,PATHINFO_EXTENSION);
        $ext2           =   pathinfo($news_url2,PATHINFO_EXTENSION);
        $ext3           =   pathinfo($banner_large,PATHINFO_EXTENSION);
        $ext4           =   pathinfo($banner_small,PATHINFO_EXTENSION);
        $date           =   date('dmYHis');
        $url_name1      =   'news_url1_'.$date.'.'.$ext1;
        $url_name2      =   'news_url2_'.$date.'.'.$ext2;
        $url_name3      =   'banner_single_large_'.$date.'.'.$ext3;
        $url_name4      =   'banner_single_small_'.$date.'.'.$ext4;
        $path           =   './assets/upload/news';

        if( ! file_exists( $path ) ){
            $create = mkdir($path, 0777, TRUE);
            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createTemp )
                return;
        }

        /* --------------------- CEK IMAGE NEWS 1 --------------------- */
        $this->piclib->get_config($url_name1, $path);
        if( $this->upload->do_upload('news_url1') ){
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 ){
                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait'){

                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                    
                    $lands_square = $this->lang->line('lands_square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                }else{

                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() ){
                        $this->image_lib->clear();
                        $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->piclib->crop_image($cek, 939, 939);
                        $this->image_lib->crop();
                        $this->image_lib->clear();
                        
                        /* --------------------- CEK IMAGE NEWS 2 --------------------- */
                        $this->piclib->get_config($url_name2, $path);
                        if( $this->upload->do_upload('news_url2') ){

                            $image = array('upload_data' => $this->upload->data());
                            $source_path = $image['upload_data']['full_path'];
                            $width = $image['upload_data']['image_width'];
                            $height = $image['upload_data']['image_height'];
                            
                            if( $width < 1920 || $height < 939 ) {

                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name2 ));
                                unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                                // unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name2 ));

                                $image_1920px = $this->lang->line('image_1920px');
                                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';

                            }else{
                                
                                $orientation = $this->piclib->orientation($source_path);
                                if( $orientation == 'portrait'){

                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name2 ));
                                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                                    // unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name2 ));
                                    
                                    $lands_square = $this->lang->line('lands_square');
                                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                                }else{

                                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                                    if( $this->image_lib->resize() ){
                                        $this->image_lib->clear();
                                        $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                        $this->piclib->crop_image($cek, 939, 939);
                                        $this->image_lib->crop();
                                        $this->image_lib->clear();
                                        
                                        /* --------------------- CEK BANNER LARGE --------------------- */
                                        $this->piclib->get_config($url_name3, $path);
                                        if( $this->upload->do_upload('banner_large') ){

                                            $image = array('upload_data' => $this->upload->data());
                                            $source_path = $image['upload_data']['full_path'];
                                            $width = $image['upload_data']['image_width'];
                                            $height = $image['upload_data']['image_height'];
                                            
                                            if( $width < 1920 || $height < 939 ) {

                                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name2 ));
                                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name3 ));
                                                unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                                                unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name2 ));
                                                // unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name3 ));

                                                $image_1920px = $this->lang->line('image_1920px');
                                                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';

                                            }else{
                                                
                                                $orientation = $this->piclib->orientation($source_path);
                                                if( $orientation == 'portrait' || $orientation == 'square'){

                                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name2 ));
                                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name3 ));
                                                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                                                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name2 ));
                                                    // unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name3 ));
                                                    
                                                    $landscape = $this->lang->line('landscape');
                                                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                                                }else{

                                                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                                                    if( $this->image_lib->resize() ){
                                                        $this->image_lib->clear();
                                                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                                                        $this->image_lib->resize();
                                                        
                                                        /* --------------------- CEK BANNER SMALL --------------------- */
                                                        $this->piclib->get_config($url_name4, $path);
                                                        if( $this->upload->do_upload('banner_small') ){

                                                            $image = array('upload_data' => $this->upload->data());
                                                            $source_path = $image['upload_data']['full_path'];
                                                            $width = $image['upload_data']['image_width'];
                                                            $height = $image['upload_data']['image_height'];
                                                            
                                                            if( $width < 752 || $height < 939 ){

                                                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                                                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name2 ));
                                                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name3 ));
                                                                unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name4 ));
                                                                unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                                                                unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name2 ));
                                                                unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name3 ));
                                                                // unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name4 ));

                                                                $image_752px = $this->lang->line('image_752px');
                                                                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_752px.'</div>';
                                                            }else{
                                                                
                                                                $orientation = $this->piclib->orientation($source_path);
                                                                if( $orientation == 'landscape' || $orientation == 'square' ){

                                                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name1 ));
                                                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name2 ));
                                                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name3 ));
                                                                    unlink( realpath( APPPATH.'../assets/upload/news/'.$url_name4 ));
                                                                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name1 ));
                                                                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name2 ));
                                                                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name3 ));
                                                                    // unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$url_name4 ));

                                                                    $portrait = $this->lang->line('portrait');
                                                                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                                                                }else{

                                                                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                                                                    if( $this->image_lib->resize() ){
                                                                        $this->image_lib->clear();
                                                                        $this->piclib->resize_image($source_path, $width, $height, 165, 165, $path.'/thumbnail');
                                                                        $this->image_lib->resize();
// DATA TO PASS TO CORPORATE
$n_title_in = $this->input->post('news_title_in');
$n_title_en = $this->input->post('news_title_en');
$n_content_in = $this->input->post('news_content_in');
$n_content_en = $this->input->post('news_content_en');
// -------------------------
                                                                        // Insert data news
                                                                        $data = array(
                                                                            'news_title_in'     => $this->input->post('news_title_in'),
                                                                            'news_content_in'   => $this->input->post('news_content_in'),
                                                                            'news_title_en'     => $this->input->post('news_title_en'),
                                                                            'news_content_en'   => $this->input->post('news_content_en'),
                                                                        );


                                                                        $this->db->set('news_date', 'NOW()', FALSE);
                                                                        $this->db->insert('news',$data);
                                                                        $max_id = $this->db->query("SELECT MAX(news_id) AS news_id FROM news")->row()->news_id;
//PASSING DATA TO CORPORATE
$token_key = "kidsfun2016";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://produkrekreasi.com/backend/news/save_news");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "news_title_in=".$n_title_in."&news_title_en=".$n_title_en."&news_content_in=".$n_content_in."&news_content_en=".$n_content_en."&temp_id=".$max_id."&token_key=".$token_key);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);
//-------------------------

                                                                        // Insert data news image 1
                                                                        $news_img1 = array(
                                                                            'media_content_in'  => $this->input->post('news_content_in'),
                                                                            'media_content_en'  => $this->input->post('news_content_en'),
                                                                            'media_url'         => $url_name1,
                                                                            'media_page'        => 'news',
                                                                            'media_section'     => 'image_content',
                                                                            'temp_id'           => $max_id
                                                                        );

                                                                        // Insert data news image 2
                                                                        $news_img2 = array(
                                                                            'media_content_in'  => $this->input->post('news_content_in'),
                                                                            'media_content_en'  => $this->input->post('news_content_en'),
                                                                            'media_url'         => $url_name2,
                                                                            'media_page'        => 'news',
                                                                            'media_section'     => 'image_content',
                                                                            'temp_id'           => $max_id
                                                                        );
                                                                        // Insert data Banner Large
                                                                        $news_img3 = array(
                                                                            'media_title_in'    => $this->input->post('media_title_in'),
                                                                            'media_content_in'  => $this->input->post('media_content_in'),
                                                                            'media_title_en'    => $this->input->post('media_title_en'),
                                                                            'media_content_en'  => $this->input->post('media_content_en'),
                                                                            'media_url'         => $url_name3,
                                                                            'media_page'        => 'news',
                                                                            'media_section'     => 'banner_single_large',
                                                                            'temp_id'           => $max_id
                                                                        );
                                                                        // Insert data Banner small
                                                                        $news_img4 = array(
                                                                            'media_title_in'    => $this->input->post('media_title_in'),
                                                                            'media_content_in'  => $this->input->post('media_content_in'),
                                                                            'media_title_en'    => $this->input->post('media_title_en'),
                                                                            'media_content_en'  => $this->input->post('media_content_en'),
                                                                            'media_url'         => $url_name4,
                                                                            'media_page'        => 'news',
                                                                            'media_section'     => 'banner_single_small',
                                                                            'temp_id'           => $max_id
                                                                        );

                                                                        $this->db->trans_begin();
                                                                        $this->db->set('media_date', 'NOW()', FALSE);
                                                                        $this->db->insert('media',$news_img1);
                                                                        $this->db->set('media_date', 'NOW()', FALSE);
                                                                        $this->db->insert('media',$news_img2);
                                                                        $this->db->set('media_date', 'NOW()', FALSE);
                                                                        $this->db->insert('media',$news_img3);
                                                                        $this->db->set('media_date', 'NOW()', FALSE);
                                                                        $this->db->insert('media',$news_img4);
                                                                        $this->db->trans_complete();

                                                                        if ($this->db->trans_status() === FALSE){
                                                                            $this->db->trans_rollback();
                                                                        }else{
                                                                            $insert = $this->lang->line('insert');
                                                                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$insert.'</div>';
                                                                        }
                                                                        
                                                                    }
                                                                }
                                                            }
                                                                
                                                        }
                                                        else{
                                                            if( 'landscape'== TRUE || 'square' == TRUE ){
                                                                $portrait = $this->lang->line('portrait');
                                                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>'; 
                                                            }else{
                                                                $error = $this->lang->line('error_image');
                                                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>'; 
                                                            }
                                                        }
                                                        /* --------------------- END BANNER SMALL --------------------- */ 

                                                    }
                                                }                    
                                            }
                                        }else{
                                            if('portrait'== TRUE || 'square' == TRUE){
                                                $landscape = $this->lang->line('landscape');
                                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>'; 
                                            }else{
                                                $error = $this->lang->line('error_image');
                                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>'; 
                                            } 
                                        }
                                        /* --------------------- END BANNER LARGE --------------------- */ 

                                    }
                                }                    
                            }
                        }else{
                            if('portrait' == TRUE){
                                $lands_square = $this->lang->line('lands_square');
                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>'; 
                            }else{
                                $error = $this->lang->line('error_image');
                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>'; 
                            }  
                        }
                        /* --------------------- END IMAGE NEWS 2 --------------------- */ 
                    }
                }                    
            }
        }else{
            if('portrait' == TRUE){
                $lands_square = $this->lang->line('lands_square');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>'; 
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>'; 
            } 
        }
        /* --------------------- END IMAGE NEWS 1 --------------------- */

        $this->session->set_flashdata('news_insert', $notif);
        redirect('backend/news#news'); 
    }

    /* --------------------- UPDATE BANNER SINGLE LARGE --------------------- */
    function update_banner_single_large(){
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('media1_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'news' AND media_section = 'banner_single_large' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_single_large_'.$date.'.'.$ext;
            $path       =   './assets/upload/news';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                    $image_1920px = $this->lang->line('image_1920px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1920px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'square')
                    {
                        unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                        $landscape = $this->lang->line('landscape');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/news/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/news/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                    
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>'; 
            }
            
        }
        $this->session->set_flashdata('news_update_banner', $notif);
        redirect('backend/news#news');
    }

    /* --------------------- UPDATE BANNER SINGLE SMALL --------------------- */
    function update_banner_single_small(){
        if(isset($_FILES['media_url']['name'])){
            $id = $this->input->post('media2_id');

            $gettoId = $this->db->query(" SELECT * FROM media WHERE media_page = 'news' AND media_section = 'banner_single_small' AND media_id = '".$id."' ")->row()->media_url;

            $media_url  =   $_FILES['media_url']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'banner_single_small_'.$date.'.'.$ext;
            $path       =   './/assets/upload/news';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }
            
            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('media_url') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 752 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                    $image_752px = $this->lang->line('image_752px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_752px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'landscape' || $orientation == 'square' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/news/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$media_url ));
                        $portrait = $this->lang->line('portrait');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 165, 165, $path.'/thumbnail');
                            $this->image_lib->resize();
                            
                            unlink( realpath( APPPATH.'../assets/upload/news/'.$gettoId ));
                            unlink( realpath(APPPATH.'../assets/upload/news/thumbnail/'.$gettoId ));

                            $media_url = array('media_url'=> $media_url);

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->db->where('media_id', $id)->update('media',$media_url); 
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }else{
                                $updd = $this->lang->line('update');
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }
                        }
                    }
                    
                }
            }else{
                $error = $this->lang->line('error_image');
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
            }
            
        }
        $this->session->set_flashdata('news_update_banner', $notif);
        redirect('backend/news#news');
    }

    /* --------------------- UPDATE BANNER SINGLE CAPTION --------------------- */
    function update_banner_single_caption(){
        $id1 = $this->input->post('media1_id');
        $id2 = $this->input->post('media2_id');
        $data = array(
            'media_title_in'    => $this->input->post('media_title_in'),
            'media_content_in'  => $this->input->post('media_content_in'),
            'media_title_en'    => $this->input->post('media_title_en'),
            'media_content_en'  => $this->input->post('media_content_en'),
            'media_page'        => 'news',
        );
        $this->db->trans_begin();
        $this->db->set('media_date', 'NOW()', FALSE);
        $this->db->where('media_id', $id1)->update('media',$data); 
        $this->db->set('media_date', 'NOW()', FALSE);
        $this->db->where('media_id', $id2)->update('media',$data); 
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }else{
            $updd = $this->lang->line('update');
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }
        $this->session->set_flashdata('news_update_banner', $notif);
        redirect('backend/news#news');
    }

    /* --------------------- UPDATE CONTENT + IMAGE 1 + IMAGE 2 --------------------- */
    function news_update(){
        $media_url1 = $_FILES['media_url1']['name'];
        $media_url2 = $_FILES['media_url2']['name'];

        $news_id= $this->input->post('news_id');
        $id1    = $this->input->post('media_id1');
        $id2    = $this->input->post('media_id2');

        /* --------------------- IMAGE 1 --------------------- */ 
        $image1 = 
            $ext        =   pathinfo($media_url1,PATHINFO_EXTENSION);
            $date       = date('dmYHis');
            $news_url1  = 'news_url1_'.$date.'.'.$ext;
            $path       = './assets/upload/news';

            if( ! file_exists( $path ) )
            {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
            }

            $this->piclib->get_config($news_url1, $path);
            if( $this->upload->do_upload('media_url1') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/news/'.$news_url1 ));
                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$news_url1 ));
                    $image_1920px = $this->lang->line('image_1920px');
                    $notif = '&nbsp;<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/news/'.$news_url1 ));
                        unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$news_url1 ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->piclib->crop_image($cek, 939, 939);
                            $this->image_lib->crop();
                            $this->image_lib->clear();
                            
                            $old_image1 = $this->db->query(" SELECT * FROM media WHERE media_page = 'news' AND media_section = 'image_content' AND media_id = '".$id1."' ")->row()->media_url;

                            unlink( realpath( APPPATH.'../assets/upload/news/'.$old_image1 ));
                            unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$old_image1 ));

                            $this->db->trans_begin();
                            $new_img1 = array(
                                               'media_url' => $news_url1
                                            );
                            $this->db->where('media_id',$id1)->update('media',$new_img1);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $notif = '&nbsp;<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Image updated successfully.</div>';  
                            }
                        }
                    }
                    
                }
            }else{
                $error = $this->upload->display_errors();
                $notif = '&nbsp;<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
        ;

        /* --------------------- IMAGE 2 --------------------- */
        $image2 = 
            $ext       =   pathinfo($media_url2,PATHINFO_EXTENSION);
            $date       = date('dmYHis');
            $news_url2  = 'news_url2_'.$date.'.'.$ext;
            $path       = './assets/upload/news';

            if( ! file_exists( $path ) )
            {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
            }

            $this->piclib->get_config($news_url2, $path);
            if( $this->upload->do_upload('media_url2') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1920 || $height < 939 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/news/'.$news_url2 ));
                    unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$news_url2 ));
                    $image_1920px = $this->lang->line('image_1920px');
                    $notif = '&nbsp;<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1920px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/news/'.$news_url2 ));
                        unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$news_url2 ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $cek = $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                            $this->piclib->crop_image($cek, 939, 939);
                            $this->image_lib->crop();
                            $this->image_lib->clear();
                            
                            $old_image2 = $this->db->query(" SELECT * FROM media WHERE media_page = 'news' AND media_section = 'image_content' AND media_id = '".$id2."' ")->row()->media_url;

                            unlink( realpath( APPPATH.'../assets/upload/news/'.$old_image2 ));
                            unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$old_image2 ));

                            $this->db->trans_begin();
                            $new_img2 = array(
                                               'media_url' => $news_url2
                                            );
                            $this->db->where('media_id',$id2)->update('media',$new_img2);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $notif = '&nbsp;<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Image updated successfully.</div>';  
                            }
                        }
                    }
                    
                }
            }else{
                $error = $this->upload->display_errors();
                $notif = '&nbsp;<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
        ;

        /* --------------------- CONTENT --------------------- */

// DATA TO PASS TO CORPORATE
$n_title_in = $this->input->post('news_title_in');
$n_title_en = $this->input->post('news_title_en');
$n_content_in = $this->input->post('news_content_in');
$n_content_en = $this->input->post('news_content_en');
// -------------------------
//PASSING DATA TO CORPORATE
$token_key = "kidsfun2016";
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"https://produkrekreasi.com/backend/news/update_news");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,
            "news_title_in=".$n_title_in."&news_title_en=".$n_title_en."&news_content_in=".$n_content_in."&news_content_en=".$n_content_en."&temp_id=".$news_id."&token_key=".$token_key);

// in real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS, 
//          http_build_query(array('postvar1' => 'value1')));

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);
//-------------------------
        $content = 
            $data = array(
                    'news_title_in'     => $this->input->post('news_title_in'),
                    'news_content_in'   => $this->input->post('news_content_in'),
                    'news_title_en'     => $this->input->post('news_title_en'),
                    'news_content_en'   => $this->input->post('news_content_en')
                );
                $this->db->trans_begin();
                $this->db->set('news_date', 'NOW()', FALSE);
                $this->db->where('news_id', $news_id)->update('news',$data); 
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }else{
                    $updd = $this->lang->line('update');
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                }
        ;



        if($media_url1 == ''){
            /* ------------------- CEK IMAGE 2 ------------------- */
            if($media_url2 == ''){
                /* ------------------- UPDATE CONTENT NO IMAGE  ------------------- */
                $content;
            }else{
                /* ------------------- UPDATE CONTENT + IMAGE 2  ------------------- */
                $content;
                $image2;
            }
        }else{
            /* ------------------- CEK IMAGE 2 ------------------- */
            if($media_url2 == ''){
                /* ------------------- UPDATE CONTENT + IMAGE 1  ------------------- */
                $content;
                $image1;
            }else{
                /* ------------------- UPDATE CONTENT + IMAGE 1 + IMAGE 2  ------------------- */
                $content;
                $image1;
                $image2;
            }
        }

        $this->session->set_flashdata('news_update_content', $notif);
        redirect('backend/news#news');
    }

    /* --------------------- DELETE news --------------------- */
    function delete($id){
        $getold = $this->db->query(" SELECT * FROM media WHERE temp_id = '".$id."' ")->result();

        foreach ($getold as $key) {
            unlink( realpath( APPPATH.'../assets/upload/news/'.$key->media_url ));
            unlink( realpath( APPPATH.'../assets/upload/news/thumbnail/'.$key->media_url ));
        }

        $this->db->where('news_id',$id)->delete('news');
        $this->db->where('temp_id',$id)->delete('media');

        //PASSING DATA TO CORPORATE
        $token_key = "kidsfun2016";
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://produkrekreasi.com/backend/news/delete_news");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "temp_id=".$id."&token_key=".$token_key);

        // in real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS, 
        //          http_build_query(array('postvar1' => 'value1')));

        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);

        curl_close ($ch);
        //-------------------------
        
        $delete = $this->lang->line('delete');
        $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$delete.'</div>';
        
        $this->session->set_flashdata('news_delete', $notif);
        redirect('backend/news#news');
    }
}