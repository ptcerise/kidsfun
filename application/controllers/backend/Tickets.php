<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['banner_lg'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Tickets'))->row();
        $data['banner_sm'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Tickets'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Tickets'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Tickets'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Tickets'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Tickets'))->row();

        $data['tickets'] = $this->Access->readtable('tickets','')->result();

        $data['current'] = "tickets";
        $view['script']  = $this->load->view('backend/script/tickets','',TRUE);
		$view['content'] = $this->load->view('backend/tickets/v_tickets',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // --------------- update banner for desktop ---------------------
    function banner_large()
    {
        $pic_tickets_lg = $_FILES['pic_tickets_lg']['name'];
        $break = explode('.', $pic_tickets_lg);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'tickets_lg_'.$date.'.'.$ext;
        $path = './assets/upload/tickets';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_tickets_lg') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1920 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/tickets/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/tickets/thumbnail/'.$name_url ));
                $image_1920px = $this->lang->line('image_1920px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1920px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/tickets/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/tickets/thumbnail/'.$name_url ));
                    $landscape = $this->lang->line('landscape');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$landscape.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1920, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 250, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_large','media_page'=>'Tickets'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/tickets/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/tickets/thumbnail/'.$old_image ));

                    $bg_large = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_large,array('media_section'=>'banner_large','media_page'=>'Tickets'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/tickets');
    }

    // --------------- update banner for mobile ---------------------
    function banner_small()
    {
        $pic_tickets_sm = $_FILES['pic_tickets_sm']['name'];
        $break = explode('.', $pic_tickets_sm);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'tickets_sm_'.$date.'.'.$ext;
        $path = './assets/upload/tickets';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('pic_tickets_sm') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 752 || $height < 939 )
            {
                unlink( realpath( APPPATH.'../assets/upload/tickets/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/tickets/thumbnail/'.$name_url ));
                $image_752px = $this->lang->line('image_752px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_752px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'square' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/tickets/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/tickets/thumbnail/'.$name_url ));
                    $portrait = $this->lang->line('portrait');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$portrait.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 752, 939);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'banner_small','media_page'=>'Tickets'))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/tickets/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/tickets/thumbnail/'.$old_image ));

                    $bg_small = array(
                                        'media_url' => $name_url
                                    );
                    $this->db->trans_begin();
                    $this->db->set('media_date', 'NOW()', FALSE);
                    $this->Access->updatetable('media',$bg_small,array('media_section'=>'banner_small','media_page'=>'Tickets'));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
                    }
                }
            }
        }
        else{
            $error = $this->lang->line("error_image");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$error.'</div>';
        }
        $_SESSION['info_banner'] = $notif;
        $this->session->mark_as_flash('info_banner');
        redirect('backend/tickets');
    }

    // --------------- update banner caption ---------------------
    function banner_caption()
    {
        $caption = array(
                        'general_content_en'=>$this->input->post('text_tickets_en'),
                        'general_content_in'=>$this->input->post('text_tickets_in')
                    );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$caption,array('general_section'=>'banner_caption','general_page'=>'Tickets'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
        }
        $_SESSION['info_caption'] = $notif;
        $this->session->mark_as_flash('info_caption');
        redirect('backend/tickets#caption');
    }

    // --------------- update ticket content ---------------------
    function edit_content()
    {
        $content = array(
                'general_content_en' => $this->input->post('div_tickets_en'),
                'general_content_in' => $this->input->post('div_tickets_in')
            );

        $this->db->trans_begin();
        $this->db->set('general_date', 'NOW()', FALSE);
        $this->Access->updatetable('general',$content,array('general_section' => 'content','general_page'=>'Tickets'));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/tickets#content');
    }

    // --------------- add ticket ---------------------
    function add_save()
    {
        $this->db->trans_begin();
        $raw_price = $this->input->post('tickets_price');
        $new_tickets = array(
                            'tickets_name' => $this->input->post('tickets_name'),
                            'tickets_category' => $this->input->post('tickets_category'),
                            'tickets_content_en' => $this->input->post('tickets_content_en'),
                            'tickets_content_in' => $this->input->post('tickets_content_in'),
                            'tickets_price' => str_replace(array("Rp ",".",", "),"",$raw_price),
                            'discount' => $this->input->post('discount')
                            );

        $this->db->insert('tickets',$new_tickets);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("insert");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/tickets#tickets_list');
    }

    // --------------- form edit ticket ---------------------
    function edit_form()
    {
        $tickets_id = $this->input->post('tickets_id');
        $data['edit_tickets'] = $this->Access->readtable('tickets','',array('tickets_id'=>$tickets_id))->row();
        $data['price'] = $this->Access->readtable('tickets','tickets_price',array('tickets_id'=>$tickets_id))->row()->tickets_price;

        $this->load->view('backend/tickets/v_tickets_edit',$data);
    }

    // --------------- save edit ticket ---------------------
    function edit_save()
    {
        $tickets_id = $this->input->post('tickets_id');
        $raw_price = $this->input->post('tickets_price');
        
        $this->db->trans_begin();
        $edit_tickets = array(
                            'tickets_name' => $this->input->post('tickets_name'),
                            'tickets_category' => $this->input->post('tickets_category'),
                            'tickets_content_en' => $this->input->post('tickets_content_en'),
                            'tickets_content_in' => $this->input->post('tickets_content_in'),
                            'tickets_price' => str_replace(array("Rp ",".",", "),"",$raw_price),
                            'discount' => $this->input->post('discount')
                            );

        $this->Access->updatetable('tickets',$edit_tickets,array('tickets_id'=>$tickets_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/tickets#tickets_list');
    }

    // --------------- delete ticket ---------------------
    function delete_tickets($tickets_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('tickets',array('tickets_id'=>$tickets_id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
        }
        $_SESSION['info_list'] = $notif;
        $this->session->mark_as_flash('info_list');
        redirect('backend/tickets#tickets_list');
    }

}