<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');     
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {   
        $data['lang'] = $this->session->userdata('fe_lang');

        //BANNER
        $data['banner_large'] = $this->Access->readtable('media','media_url', array('media_page'=>'news','media_section'=>'banner_large'))->row();
        $data['banner_small'] = $this->Access->readtable('media','media_url', array('media_page'=>'news','media_section'=>'banner_small'))->row();

        //BANNER CAPTION
        $data['banner_caption'] = $this->Access->readtable('general','',array('general_page'=>'news','general_section'=>'banner_caption'))->row();

        //CONTENT
        $data['content'] = $this->Access->readtable('general','',array('general_page'=>'news','general_section'=>'content'))->row();


        //NEWS CARD
        $data['news_cards'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //ACCESS
        $data['access_db'] = $this->Access;

        $view['content'] = $this->load->view('kidsfun/v_news',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;
        
        //CURRENT PAGE
        $view['current'] = 'news';

        $this->load->view('kidsfun/v_master', $view);
    }

    public function read($get_url)
    {
        $id = substr($get_url, 0, strpos($get_url, "-"));

        $data['lang'] = $lang = $this->session->userdata('fe_lang');
        //BANNER
        $data['banner_large'] = $this->Access->readtable('media', 'media_url', array('media_page'=>'news', 'media_section'=>'banner_single_large', 'temp_id'=>$id))->row();
        $data['banner_small'] = $this->Access->readtable('media', 'media_url', array('media_page'=>'news', 'media_section'=>'banner_single_small', 'temp_id'=>$id))->row();
        //BANNER CAPTION
        $data['banner_caption'] = $this->Access->readtable('general','',array('general_page'=>'news','general_section'=>'banner_caption'))->row();

        //NEWS TITLE, CONTENT, DATE
        $data['news'] = $this->Access->readtable('news','',array('news_id'=>$id))->row();

        //CAPTION
        $data['caption'] = $this->Access->readtable('media','', array('media_page'=>'news', 'media_section'=>'banner_single_large', 'temp_id'=>$id))->row();

        //IMAGE CONTENT
        $data['image_content'] = $this->Access->readtable('media','media_url',array('media_page'=>'news', 'media_section'=>'image_content', 'temp_id'=>$id))->result();


        $view['content'] = $this->load->view('kidsfun/v_news_single',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;
        
        //CURRENT PAGE
        $view['current'] = 'news';
        $title = $this->Access->readtable('news','',array('news_id'=>$id))->row();
        if($lang == "english" || $lang == ""){
            $view['title'] = $title->news_title_en;
        }else{
            $view['title'] = $title->news_title_in;
        }

        $this->load->view('kidsfun/v_master', $view);
    }

}
