<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shows extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Access");      
        $fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('kidsfun_frontend',$fe_lang);
        } else {
            $this->lang->load('kidsfun_frontend','english');
        }
    }
    
    public function index()
    {   
        $data['banner_large'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Shows'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Shows'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Shows'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Shows'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Shows'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Shows'))->row();

        $data['shows_single'] = $this->db->query("select `show`.*,`media`.* from `show`,`media` where show.show_id=media.temp_id AND media.media_section='banner_single_large' AND media_page='Shows' group by show.show_id order by RAND()")->result();

        $data['lang'] = $this->session->userdata('fe_lang');
 
        $view['content'] = $this->load->view('kidsfun/v_shows',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'shows';

        $this->load->view('kidsfun/v_master', $view);
    }

    function detail($get_url)
    {
        $show_id = substr($get_url, 0, strpos($get_url, "-"));
        
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['banner_large'] = $this->Access->readtable('media','',array('temp_id' => $show_id, 'media_section' => 'banner_single_large','media_page'=>'Shows'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('temp_id' => $show_id, 'media_section' => 'banner_single_small','media_page'=>'Shows'))->row();   

        $data['shows_single'] = $this->Access->readtable('show','',array('show_id'=>$show_id))->row();
        $data['shows_image'] = $this->Access->readtable('media','',array('temp_id'=>$show_id, 'media_page'=>'Shows', 'media_section'=>'content_image'))->result();
        
        $data['shows_card'] = $this->db->query("select `show`.*,`media`.* from `show`,`media` where `show`.show_id=`media`.temp_id AND `media`.media_section='content_image' AND `media`.media_page='Shows' AND `show`.show_id NOT IN (select show_id from `show` where show_id=$show_id) group by `show`.show_id order by RAND() limit 2")->result();
        $data['shows_map'] = $this->Access->readtable('media','',array('media_section'=>'map','media_page'=>'Shows'))->row();
        $view['content'] = $this->load->view('kidsfun/v_shows_single',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $lang = $this->session->userdata('fe_lang');
        if($lang == "english" || $lang == ""){
            $title_page = $this->Access->readtable('show','',array('show_id'=>$show_id))->row()->show_title_en;
        }else{
            $title_page = $this->Access->readtable('show','',array('show_id'=>$show_id))->row()->show_title_in;
        }
        $view['title'] = $title_page;
        $view['current'] = 'shows';
        
        $this->load->view('kidsfun/v_master', $view);
    }
}
