<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        /* --------------------- Show events Banner Large --------------------- */
        $data['banner_large']   = $this->db
                            ->where('media_page','events')
                            ->where('media_section','banner_large')
                            ->get('media')->row();

        /* --------------------- Show events Banner Small --------------------- */
        $data['banner_small']   = $this->db
                            ->where('media_page','events')
                            ->where('media_section','banner_small')
                            ->get('media')->row();

        /* --------------------- Show events Banner Text --------------------- */
        $data['banner_caption']   = $this->db
                            ->where('general_page','events')
                            ->where('general_section','banner_caption')
                            ->get('general')->row();

        /* --------------------- Show events Content --------------------- */
        $data['events_content']   = $this->db
                            ->where('general_page','events')
                            ->where('general_section','content')
                            ->get('general')->row();

        $date_now = date('Y-m-d H:i:s');
        $data['next_events'] = $this->db->query('SELECT * FROM events WHERE events_date >= "'.$date_now.'" ORDER BY events_date ASC')->result();
        $data['past_events'] = $this->db->query('SELECT * FROM events WHERE events_date < "'.$date_now.'" ORDER BY events_date DESC')->result();

        $view['content'] = $this->load->view('kidsfun/v_events',$data,TRUE);
         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;
        
        //CURRENT PAGE
        $view['current'] = 'events';
        $this->load->view('kidsfun/v_master', $view);
    }
}
