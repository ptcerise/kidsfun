<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rides extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');

            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['banner_large'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Rides'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Rides'))->row();

        $data['caption_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'banner_caption','general_page'=>'Rides'))->row();
        $data['caption_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'banner_caption','general_page'=>'Rides'))->row();

        $data['content_en'] = $this->Access->readtable('general','general_content_en',array('general_section'=>'content','general_page'=>'Rides'))->row();
        $data['content_in'] = $this->Access->readtable('general','general_content_in',array('general_section'=>'content','general_page'=>'Rides'))->row();

        $data['rides_single'] = $this->db->query("select rides.*,rides_img.img_url from rides,rides_img where rides.rides_id=rides_img.rides_id group by rides.rides_name")->result();

        $data['current'] = "rides";
        $view['content'] = $this->load->view('kidsfun/v_rides',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'rides';

        $this->load->view('kidsfun/v_master', $view);
    }

    public function detail($get_url){
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $rides_id = substr($get_url, 0, strpos($get_url, "-"));
        $data['banner_large'] = $this->Access->readtable('media','',array('temp_id'=>$rides_id, 'media_page'=>'Rides', 'media_section'=>'banner_single_large'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('temp_id'=>$rides_id, 'media_page'=>'Rides', 'media_section'=>'banner_single_small'))->row();

        $data['rides_single'] = $this->Access->readtable('rides','',array('rides_id'=>$rides_id))->row();
        $data['rides_image'] = $this->Access->readtable('rides_img','',array('rides_id'=>$rides_id))->result();

        $data['rides_card'] = $this->db->query("select rides.*,rides_img.img_url from rides,rides_img where rides.rides_id=rides_img.rides_id and rides.rides_id NOT IN (select rides_id from rides where rides_id=$rides_id) group by rides.rides_name limit 2")->result();
        $data['rides_map'] = $this->Access->readtable('media','',array('media_section'=>'map','media_page'=>'Rides'))->row();
        
        $view['content'] = $this->load->view('kidsfun/v_rides_single',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'rides';
        $title = $this->Access->readtable('rides','rides_name',array('rides_id'=>$rides_id))->row();
        $view['title'] = $title->rides_name;
        
        $this->load->view('kidsfun/v_master', $view);
    }
}
