<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    function index()
    {
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['banner_large'] = $this->Access->readtable('media','',array('media_section'=>'banner_large','media_page'=>'Tickets'))->row();
        $data['banner_small'] = $this->Access->readtable('media','',array('media_section'=>'banner_small','media_page'=>'Tickets'))->row();

        $data['banner_caption'] = $this->Access->readtable('general','',array('general_section'=>'banner_caption','general_page'=>'Tickets'))->row();

        $data['tickets_content'] = $this->Access->readtable('general','',array('general_section'=>'content','general_page'=>'Tickets'))->row();

        $data['vip_cards'] = $this->Access->readtable('tickets','',array('tickets_category'=>'vip cards'))->result();
        $data['tickets'] = $this->Access->readtable('tickets','',array('tickets_category'=>'tickets'))->result();
        $data['gokart'] = $this->Access->readtable('tickets','',array('tickets_category'=>'gokart'))->result();
        $data['package'] = $this->Access->readtable('tickets','',array('tickets_category'=>'package'))->result();
        $data['lunch_pack'] = $this->Access->readtable('tickets','',array('tickets_category'=>'lunch pack'))->result();
        $data['birthday'] = $this->Access->readtable('tickets','',array('tickets_category'=>'birthday'))->result();
        $data['cart_tickets'] = $this->session->userdata('cart_tickets');

        $view['content'] = $this->load->view('kidsfun/v_tickets',$data,TRUE);

         //LATEST NEWS ON FOOTER
        $today = date('Y-m-d H:i:s');
        $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        //NEXT EVENT
        $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        $view['check'] = count($check);
        if(count($check) == 0){
            $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        }else{
            $view['next_event'] = $check;
        }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        //CURRENT PAGE
        $view['current'] = 'tickets';
        
        $this->load->view('kidsfun/v_master', $view);
    }

    // function set_cookie()
    // {

    //     $cookie = array(
    //         'name' => 'You',
    //         'value' => 'Temporary data',
    //         'expire' => '86500',
    //     );
    //     $this->input->set_cookie($cookie);
    // }

    // function get_cookie()
    // {
    //     echo $this->input->cookie('You', true)
    // }

    function add_to_cart()
    {
        $check_qty = $this->input->post('tiket');

        if ($check_qty != 0)
        {
            $tickets_id = $this->input->post('tickets_id');
            $tickets_data = $this->Access->readtable('tickets','',array('tickets_id'=>$tickets_id));
            $detail = $this->Access->readtable('tickets','',array('tickets_id'=>$tickets_id))->row();

            if ($tickets_data->num_rows() > 0)
            {
                $cart_tickets = $this->session->userdata('cart_tickets');
                $chart_qty = $this->input->post('tiket');
                
                if(!isset($cart_tickets))
                {
                    if($detail->discount!=''){
                        // $tickets_price = (100 - $detail->discount)/100*$detail->tickets_price;

                        // HITUNG HARGA DISKON
                        $count_disc = (100 - $detail->discount)/100*$detail->tickets_price;
                        // PEMBULATAN HARGA
                            $mod = ($count_disc) % 500;
                            if ($mod > 0) {
                                $round = 500 - $mod;
                                $result = ($count_disc) + $round; 
                            }
                            else {
                                $result = $count_disc;
                            }
                        $tickets_price = $result;
                        $total_price = $result*$chart_qty;
                    }
                    else{
                        $tickets_price = $detail->tickets_price;
                        $total_price = $tickets_price*$chart_qty;
                    }

                    $chart_qty = $this->input->post('tiket');

                    $tickets = array(
                        'tickets_id' => $this->input->post('tickets_id'),
                        'chart_qty' => $chart_qty,
                        'tickets_name' => $detail->tickets_name,
                        'tickets_price' => $tickets_price,
                        'total_price' => $total_price,
                        'discount' => $detail->discount
                    );

                    $_SESSION['cart_tickets'] = array();
                    array_push($_SESSION['cart_tickets'],$tickets);

                    $data = array(
                        'sent'=>'Success',
                        'notif'=> $tickets_data->row()->tickets_name.' '.$this->lang->line("cart_success")
                    );
                }
                else
                {
                    $cart_tickets = $this->session->userdata('cart_tickets');
                    $chart_qty = $this->input->post('tiket');

                    if($detail->discount!=''){
                        // HITUNG HARGA DISKON
                        $count_disc = (100 - $detail->discount)/100*$detail->tickets_price;
                        // PEMBULATAN HARGA
                            $mod = ($count_disc) % 500;
                            if ($mod > 0) {
                                $round = 500 - $mod;
                                $result = ($count_disc) + $round; 
                            }
                            else {
                                $result = $count_disc;
                            }
                        $tickets_price = $result;
                        $total_price = $result*$chart_qty;
                    }
                    else{
                        $tickets_price = $detail->tickets_price;
                        $total_price = $tickets_price*$chart_qty;
                    }
                    
                    $tickets = array(
                        'tickets_id' => $this->input->post('tickets_id'),
                        'chart_qty' => $chart_qty,
                        'tickets_name' => $detail->tickets_name,
                        'tickets_price' => $tickets_price,
                        'total_price' => $total_price,
                        'discount' => $detail->discount
                    );

                    array_push($_SESSION['cart_tickets'],$tickets);

                    $data = array(
                        'sent'=>'Success',
                        'notif'=> $tickets_data->row()->tickets_name.' '.$this->lang->line("cart_success")
                    );
                }
            }
        }
        else
        {
            $data = array(
                'sent'=>'Failed',
                'notif'=>$this->lang->line("cart_failed")
            );
        }
        echo json_encode($data);
    }
}
