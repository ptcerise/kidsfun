<?php
// sidbar
$lang['home'] 			= 'Home';
$lang['attractions']	= 'Attractions';
$lang['all_attractions']= 'All Attractions';
$lang['rides']			= 'Rides';
$lang['rides_single']	= 'Rides single';
$lang['gokart']			= 'GoKart';
$lang['aquasplash']		= 'Aquasplash';
$lang['shops']			= 'Shops';
$lang['all_shops']		= 'All Shops';
$lang['restaurants']	= 'Restaurants';
$lang['souvenirs']		= 'Souvenirs';
$lang['kiosks']			= 'Kiosks';
$lang['shows']			= 'Shows';
$lang['mainpage']		= 'Main page';
$lang['shows_single']	= 'Shows single';
$lang['events']			= 'Events';
$lang['news']			= 'News';
$lang['info']			= 'Info';
$lang['contact_us']		= 'Contact Us';
$lang['tickets_pack'] 	= 'Tikets and Package';
$lang['tickets']		= 'Tickets';
$lang['cart']   		= 'Shopping cart';
$lang['checkout']		= 'Checkout';
$lang['find_us']  		= 'Find Us';
$lang['follow_us']      = 'Follow Us';

// btn
$lang['btn_edit'] 		= 'Edit';
$lang['btn_detail'] 	= 'Detail';
$lang['btn_cancel'] 	= 'Cancel';
$lang['btn_add'] 		= 'Add';
$lang['btn_update'] 	= 'Update';
$lang['btn_delete'] 	= 'Delete';
$lang['btn_save'] 		= 'Save';
$lang['btn_close'] 		= 'Close';

//notif

$lang['edit'] 			= 'Edit';
$lang['detail'] 		= 'Detail';
$lang['cancel'] 		= 'Cancel';
$lang['insert'] 		= 'Insert Data Successful';
$lang['update'] 		= 'Update Data Successful';
$lang['delete'] 		= 'Delete Data Successful';
$lang['update_pass']	= 'Update Password Successful';
$lang['wrong_pass'] 	= 'Wrong Current Password!';
$lang['save'] 			= 'Save';
$lang['user_check'] 	= 'Username already exist! Please try another username';
$lang['pass_check'] 	= 'Password must be match!';
$lang['update_profile']	= 'Your profile has been changed!';
$lang['update_password']	= 'Your profile has been changed. Please re-login!';

//hover
$lang['update_data'] 	= 'Update Data';
$lang['save_data'] 		= 'Save Data';
$lang['view'] 			= 'View Detail';

// content
$lang['action']			= 'Action';
$lang['confirm']		= 'Are you sure to delete this? ';
$lang['language'] 		= 'Language';
$lang['image'] 			= 'Image';
$lang['content'] 		= 'Content';
$lang['title_en'] 		= 'Title English :';
$lang['title_in']		= 'Title Indonesia :';
$lang['caption_en'] 	= 'Caption English :';
$lang['caption_in'] 	= 'Caption Indonesia :';
$lang['content_en'] 	= 'Content English :';
$lang['content_in'] 	= 'Content Indonesia :';
$lang['price'] 			= 'Price :';

$lang['choose_file']	= 'Choose File :';
$lang['choose_video']	= 'Choose Video :';
$lang['choose_image'] 	= 'Choose Picture :';
$lang['choose_date']	= 'Choose Date :';
$lang['banner']			= 'Banner';
$lang['caption']		= 'Caption';
$lang['image_caption']	= 'Image Caption';
$lang['caption_en'] 	= 'Caption English :';
$lang['caption_in'] 	= 'Caption Indonesia :';
$lang['desc'] 			= 'Description';
$lang['desc_en'] 		= 'Description English :';
$lang['desc_in']  		= 'Description Indonesia :';
$lang['banner_image']  	= 'Banner image :';
$lang['content_image']  = 'Content image :';

$lang['divider']		= 'Divider';
$lang['title']			= 'Title';
$lang['image_size']		= 'Image size min :';
$lang['category']		= 'Category :';
$lang['name']			= 'Name :';
$lang['dividers']   	= 'Divider';
$lang['dividers1']   	= 'Divider 1';
$lang['dividers2']   	= 'Divider 2';
$lang['dividers3']   	= 'Divider 3';
$lang['image1']       	= 'Image 1';
$lang['image2']       	= 'Image 2';
$lang['image3']       	= 'Image 3';
$lang['date']  			= 'Date';
$lang['image_1920px']  	= 'Minimum Image size : 1920 x 939 px';
$lang['image_752px']  	= 'Minimum Image size : 752 x 939 px';
$lang['image_512px']  	= 'Minimum Image size : 512 x 512 px';
$lang['image_1024px']  	= 'Minimum Image size : 1024 x 576 px';
$lang['image_1200px']  	= 'Minimum Image size : 1200 x 1115 px';

$lang['video_8mb']  	= 'Maximum video size : 8 MB';
$lang['video_type']  	= 'Upload must be ( mp4 )';

$lang['file_image']  	= 'Maximum file 2MB';
$lang['landscape']  	= 'Image must be Landscape or File size to Large (Max 2MB) ';
$lang['square']  		= 'Image must be Square or File size to Large (Max 2MB) ';
$lang['portrait']  		= 'Image must be Portrait or File size to Large (Max 2MB) ';
$lang['lands_square']	= 'Image must be Landscape or Square or File size to Large (Max 2MB) ';

$lang['landscape_alert']= 'Image must be Landscape ';
$lang['square_alert']  	= 'Image must be Square ';
$lang['portrait_alert'] = 'Image must be Portrait ';
$lang['lands_square_alert']= 'Image must be Landscape or Square ';

$lang['big_image']  	= 'Image for Desktop';
$lang['small_image']  	= 'Image for Mobile';
$lang['choose_banner_large']  	= 'Choose Banner for Desktop :';
$lang['choose_banner_small']  	= 'Choose Banner for Mobile :';
$lang['error_image']  	= 'You did not select a file to upload.';
$lang['edit_banner']  	= 'Edit Banner';
$lang['map']			= 'Map';
$lang['admin_confirm']	= 'Admin Confirmation';
$lang['status']			= 'Status';

// Tabel
$lang['name_tabel']			= 'Name';
$lang['category_tabel']		= 'Category';
$lang['title_tabel_en'] 	= 'Title English';
$lang['title_tabel_in']		= 'Title Indonesia';
$lang['content_tabel_en']	= 'Content English';
$lang['content_tabel_in']	= 'Content Indonesia';
$lang['desc_tabel_en']		= 'Description English';
$lang['desc_tabel_in']		= 'Description Indonesia';
$lang['date']				= 'Date';
$lang['username_tabel'] 	= 'Username';
$lang['fullname_tabel'] 	= 'Fullname';
$lang['price_tabel'] 		= 'Price';
$lang['userlevel_tabel'] 	= 'Level User';
$lang['discount_tabel'] 	= 'Discount (%)';

//Profile
$lang['account_setting'] 	= 'Account Setting';
$lang['profile'] 			= 'Profile';
$lang['pass'] 				= 'Password';
$lang['sign_out'] 			= 'Sign Out';

// Home
$lang['op'] 			= 'Opening Times :';
$lang['me'] 			= 'Minimum age';
$lang['wtb'] 			= 'What to bring?';
$lang['acc'] 			= 'Accessibility';
$lang['announcement'] 	= 'Announcement';

// rides
$lang['new_rides'] 		= 'New Ride';
$lang['rides_list'] 	= 'List of rides';
$lang['add_rides']   	= 'Add Ride';
$lang['edit_rides']  	= 'Edit Ride';
$lang['delete_rides']  	= 'Delete Ride';
$lang['kids_rides']   = 'Kids Rides';
$lang['family_rides']   = 'Family Rides';
$lang['thrill_rides']   = 'Thrill Rides';

$lang['kids_rides']  	= 'Kids Rides';
$lang['family_rides']  	= 'Family Rides';
$lang['thrill_rides']  	= 'Thrill Rides';

// news
$lang['news_date']  = 'News Date';
$lang['news_title'] = 'News Title';
$lang['add_news']           = 'Add News';

// Contact us
$lang['address']  	= 'Address :';
$lang['phone']  	= 'Phone Number :';
$lang['message']  	= 'Message';
$lang['sender']  	= 'Sender';
$lang['subject']  	= 'Subject';
$lang['detail_msg'] = 'Message Detail';
$lang['from']  		= 'From';
$lang['delete_message']  = 'Delete Message';
$lang['detail_message']  = 'Message Detail';
$lang['message_received']  = 'Message received';

// events
$lang['add_events']         = 'Add Events';

//follow us
$lang['links']         = 'Links';

// kiosks
$lang['kiosks']  = 'Kiosks';

// restaurant 
$lang['restaurant_title'] 	= 'Restaurant Title';
$lang['choose_image1'] 		= 'Choose Picture 1 :';
$lang['choose_image2'] 		= 'Choose Picture 2 :';
$lang['add_restaurant']     = 'Add Restaurant';

// show
$lang['new_shows'] 		= 'New Show';
$lang['shows_list'] 	= 'List of Shows';
$lang['add_shows']      = 'Add Show';
$lang['edit_shows']  	= 'Edit Show';
$lang['delete_shows']  	= 'Delete Show';

//info
$lang['new_info'] = 'New Info';
$lang['info_list'] 	= 'List of Info';
$lang['add_info']      = 'Add Info';
$lang['edit_info']  	= 'Edit Info';
$lang['delete_info']  	= 'Delete Info';

// tickets
$lang['ticket'] 			= 'Ticket';
$lang['add_tickets']     	= 'Add Ticket';
$lang['edit_tickets']  		= 'Edit Ticket';
$lang['delete_tickets']  	= 'Delete Ticket';
$lang['new_tickets'] 		= 'New Ticket';
$lang['choose_category'] 	= 'Choose category';
$lang['discount'] 			= 'Discount (Optional) :';

// checkout
$lang['payment'] 		= 'Payment';
$lang['detail_payment'] = 'Payment Detail';
$lang['delete_payment'] = 'Delete Payment';
$lang['edit_payment'] 	= 'Edit Payment';
$lang['first_name'] 	= 'First Name';
$lang['last_name'] 		= 'Last Name';
$lang['city'] 			= 'City';
$lang['zip'] 			= 'Postal Code';
$lang['total_pay'] 		= 'Total Payment';
$lang["visit_date"]		= "Visit date";

//user
$lang['user'] 			= 'User';
$lang['user_list'] 		= 'List of Users';
$lang['new_user'] 		= 'New User';
$lang['username'] 		= 'Username :';
$lang['fullname'] 		= 'Fullname :';
$lang['password'] 		= 'Password :';
$lang['current_pass'] 	= 'Current Password :';
$lang['new_pass'] 		= 'New Password :';
$lang['add_user']       = 'Add User';
$lang['edit_user']  	= 'Edit User';
$lang['delete_user']  	= 'Delete User';

$lang['confirm_pass'] 	= 'Confirm Password :';
$lang['user_level'] 	= 'User Level :';

$lang['image_video'] 	= 'Image Video';