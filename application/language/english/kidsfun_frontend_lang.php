<?php

# MASTER
## LINKS
$lang["nav_contact"] 			= "Contact";
$lang["nav_location"]			= "Location";
$lang["nav_parkmap"]			= "Parkmap";
$lang["nav_attractions"]		= "Attractions";
$lang["nav_all_atrractions"]	= "All Attractions";
$lang["nav_home"] 				= "Home";
$lang["nav_rides"]				= "Rides";
$lang["nav_gokart"]				= "GoKart";
$lang["nav_aquasplash"]			= "Aquasplash";
$lang["nav_shops"]				= "Shops";
$lang["nav_all_shops"]			= "All Shops";
$lang["nav_restaurants"]		= "Restaurants";
$lang["nav_souvenirs"]			= "Souvenirs";
$lang["nav_kiosks"]				= "Kiosks";
$lang["nav_shows"]				= "Shows";
$lang["nav_events"]				= "Events";
$lang["nav_news"]				= "News";
$lang["nav_info"]				= "Info";
$lang["nav_tickets"]			= "Tickets";
$lang["nav_find_us"]			= "Find Us";
$lang["park_open_day"]			= "The park is open 365 days";
$lang["park_open_time"]			= "Today : 8 am to 10 pm";
## FOOTER
$lang["btn_yogya_gokart"]		= "Yogya Gokart";
$lang["btn_corporate"]			= "Corporate";
$lang["footer_latest_news"]		= "Latest news";
$lang["footer_next_events"]		= "Next events";
$lang["footer_past_events"]		= "Past events";
$lang["footer_attractions"]		= "Attractions";
$lang["footer_shops"]			= "Shops";
$lang["footer_info"]			= "Info";
$lang["footer_shows"]			= "Shows";
$lang["footer_product"]			= "Produk Rekreasi";
$lang["footer_follow"]			= "Follow Us";
$lang["footer_visitors"]		= "Visitors : ";

# SHARE
$lang["share_on"]				= "Share on";

# BUTTONS
$lang["btn_readmore"]			= "Read More";
$lang["btn_showmore"]			= "Show More";
$lang["btn_buy_tickets"]		= "Buy Tickets";
$lang["btn_discover"]			= "Discover";
$lang["btn_learn"]				= "Learn More";
$lang["btn_share"]				= "Share";
$lang["btn_close"]				= "Close";
$lang["btn_add_to_cart"]		= "Add To Cart";
$lang["btn_checkout"]			= "Checkout";
$lang["btn_cancel"]				= "Cancel";
$lang["btn_next"]				= "Next";

# HOME
$lang["title_the_rides"]		= "The rides";
$lang["title_aquasplash"]		= "Aquasplash";
$lang["title_our_restaurants"]	= "Our restaurants";
$lang["title_our_shows"]		= "Our shows";
$lang["title_opening_times"]	= "Opening times";
$lang["title_minimum_age"]		= "Minimum age";
$lang["title_what_to_bring"]	= "What to bring?";
$lang["title_accessibility"]	= "Accessibility";
$lang["btn_explore"]			= "Explore";
$lang["btn_choose_package"]		= "Choose A Package";
$lang["btn_lets_race"]			= "Let's Race";
$lang["btn_corporate_website"]	= "Corporate Website";
$lang["title_kids_ride"]		= "Kids rides";
$lang["title_family_ride"]		= "Family rides";
$lang["title_thrill_ride"]		= "Thrill rides";

# AQUASPLASH
$lang["banner_aquasplash"]		= "Aquasplash";
$lang["title_the_slides"]		= "The Slides";
$lang["title_the_pools"]		= "The Pools";

# ATTRACTIONS
$lang["banner_attractions"]		= "Attractions";

# CHECKOUT
$lang["checkout"]				= "Checkout";
$lang["form_fname"]				= "First name";
$lang["form_lname"]				= "Last name";
$lang["form_email"]				= "Email address";
$lang["form_phone"]				= "Phone number";
$lang["form_address"]			= "Address";
$lang["form_city"]				= "City";
$lang["form_post_code"]			= "Post code";
$lang["form_visit_date"]		= "Visit date";
$lang["payment_total"]			= "PAYMENT TOTAL";
$lang["btn_pay"]				= "Pay Now";
$lang["btn_continue"]			= "Continue My Visit";
$lang["terms_agree"]			= "I accept the terms & conditions of Kidsfun above";
$lang["title_pay"]				= "Please complete your data";


# CHECKOUT SUCCESS
$lang["thanks"]					= "Thanks";
$lang["looking_forward"]		= "we are looking forward to welcoming you!";

# CONTACT
$lang["banner_contact"]			= "Contact Us";
$lang["contact_name"]			= "Name";
$lang["contact_email"]			= "Email";
$lang["contact_phone"]			= "Phone";
$lang["contact_category"]		= "Category";
$lang["contact_subject"]		= "Subject";
$lang["contact_message"]		= "Message";
$lang["btn_send"]				= "Send";
$lang["send_success"]			= "Your message sent successfully";

# EVENTS
$lang["banner_events"]			= "Events";
$lang["next_events"]			= "Next Events";
$lang["past_events"]			= "Past Events";

# FIND US
$lang["banner_find_us"]			= "Find Us";

# GOKART
$lang["banner_gokart"]			= "GoKart";
$lang["title_the_karts"]		= "The Karts";
$lang["title_the_tracks"]		= "The Tracks";
$lang["btn_start_racing"]		= "Start Racing";

# KIOSKS
$lang["banner_kiosks"]			= "Kiosks";

# NEWS
$lang["banner_news"]			= "The Park News";

# PRACTICAL INFO
$lang["banner_info"]			= "Practical Info";

# RESTAURANTS
$lang["banner_restaurants"]		= "Restaurants";

# RESTAURANT SINGLE
$lang["title_where_to_eat"]		= "Where to eat next?";

# RIDES
$lang["banner_rides"]			= "Rides";
$lang["btn_kids"]				= "Kids";
$lang["btn_family"]				= "Family";
$lang["btn_thrill"]				= "Thrill";

# RIDES SINGLE
$lang["title_what_to_do"]		= "What to do next?";

# SHOPPING CART
$lang["title_shopping_cart"]	= "Shopping Cart";
$lang["btn_remove"]				= "Remove";

# SHOPS
$lang["banner_shops"]			= "Shops";

# SHOWS
$lang["banner_shows"]			= "Shows";

# SHOW SINGLE
$lang["title_what_to_see"]		= "What to see next?";

# SOUVENIRS
$lang["banner_souvenirs"]		= "Souvenirs";

# TICKETS
$lang["banner_tickets"]			= "Tickets & Packages";
$lang["title_vip_cards"]		= "VIP CARDS";
$lang["title_tickets"]			= "TICKETS";
$lang["title_gokart_tickets"]	= "GOKART";
$lang["title_package"]			= "PACKAGE";
$lang["title_lunch_pack"]		= "LUNCH PACKAGE";
$lang["title_birthday"]			= "BIRTHDAY";
$lang["min_people"]				= "Min 15 people";
$lang["cart_success"]			= "Successfully Added to Cart!";
$lang["cart_failed"]			= "Please set Ticket Quantity!";
$lang['confirm']				= 'Are you sure to remove this item? ';
$lang['discount']				= "Off";