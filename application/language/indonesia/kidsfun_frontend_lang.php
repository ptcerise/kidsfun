<?php

# MASTER
## LINKS
$lang["nav_contact"] 			= "Kontak";
$lang["nav_location"]			= "Lokasi";
$lang["nav_parkmap"]			= "Peta Taman";
$lang["nav_attractions"]		= "Atraksi";
$lang["nav_all_atrractions"]	= "Semua Atraksi";
$lang["nav_home"] 				= "Beranda";
$lang["nav_rides"]				= "Wahana";
$lang["nav_gokart"]				= "GoKart";
$lang["nav_aquasplash"]			= "Aquasplash";
$lang["nav_shops"]				= "Toko";
$lang["nav_all_shops"]			= "Semua Toko";
$lang["nav_restaurants"]		= "Restoran";
$lang["nav_souvenirs"]			= "Suvenir";
$lang["nav_kiosks"]				= "Kios";
$lang["nav_shows"]				= "Pertunjukan";
$lang["nav_events"]				= "Event";
$lang["nav_news"]				= "Berita";
$lang["nav_info"]				= "Info";
$lang["nav_tickets"]			= "Tiket";
$lang["nav_find_us"]			= "Temukan Kami";
$lang["park_open_day"]			= "Taman terbuka 365 hari";
$lang["park_open_time"]			= "Hari ini : 08.00 - 22.00 WIB";
## FOOTER
$lang["btn_yogya_gokart"]		= "Yogya Gokart";
$lang["btn_corporate"]			= "Korporasi";
$lang["footer_latest_news"]		= "Berita terbaru";
$lang["footer_next_events"]		= "Event selanjutnya";
$lang["footer_past_events"]		= "Event sebelumnya";
$lang["footer_attractions"]		= "Atraksi";
$lang["footer_shops"]			= "Toko";
$lang["footer_info"]			= "Info";
$lang["footer_shows"]			= "Pertunjukan";
$lang["footer_product"]			= "Produk Rekreasi";
$lang["footer_follow"]			= "Ikuti Kami";
$lang["footer_visitors"]		= "Pengunjung : ";

# SHARE
$lang["share_on"]				= "Bagikan melalui";

# BUTTONS
$lang["btn_readmore"]			= "Baca lebih lanjut";
$lang["btn_showmore"]			= "Tampilkan lebih banyak";
$lang["btn_buy_tickets"]		= "Beli Tiket";
$lang["btn_discover"]			= "Temukan";
$lang["btn_learn"]				= "Pelajari";
$lang["btn_share"]				= "Bagikan";
$lang["btn_close"]				= "Tutup";
$lang["btn_add_to_cart"]		= "Add to cart";
$lang["btn_checkout"]			= "Checkout";
$lang["btn_cancel"]				= "Batal";
$lang["btn_next"]				= "Lanjutkan";

# HOME
$lang["title_the_rides"]		= "Wahana";
$lang["title_aquasplash"]		= "Aquasplash";
$lang["title_our_restaurants"]	= "Restoran kami";
$lang["title_our_shows"]		= "Pertunjukan kami";
$lang["title_opening_times"]	= "Waktu buka";
$lang["title_minimum_age"]		= "Usia minimal";
$lang["title_what_to_bring"]	= "Apa yang dibawa?";
$lang["title_accessibility"]	= "Aksesibilitas";
$lang["btn_explore"]			= "Jelajahi";
$lang["btn_choose_package"]		= "Pilih Paket";
$lang["btn_lets_race"]			= "Mari Balapan";
$lang["btn_corporate_website"]	= "Website Korporasi";
$lang["title_kids_ride"]		= "Wahana Anak";
$lang["title_family_ride"]		= "Wahana Keluarga";
$lang["title_thrill_ride"]		= "Wahana Menegangkan";

# AQUASPLASH
$lang["banner_aquasplash"]		= "Aquasplash";
$lang["title_the_slides"]		= "Seluncuran";
$lang["title_the_pools"]		= "Kolam";

# ATTRACTIONS
$lang["banner_attractions"]		= "Atraksi";

# CHECKOUT
$lang["checkout"]				= "Checkout";
$lang["form_fname"]				= "Nama depan";
$lang["form_lname"]				= "Nama belakang";
$lang["form_email"]				= "Alamat email";
$lang["form_phone"]				= "Nomor telepon";
$lang["form_address"]			= "Alamat";
$lang["form_city"]				= "Kota";
$lang["form_post_code"]			= "Kode pos";
$lang["form_visit_date"]		= "Tanggal kedatangan";
$lang["payment_total"]			= "TOTAL BAYAR";
$lang["btn_pay"]				= "Bayar";
$lang["btn_continue"]			= "Lanjut berkunjung";
$lang["terms_agree"]			= "Saya menerima persyaratan dari Kidsfun diatas";
$lang["title_pay"]				= "Silakan lengkapi data anda";

# CHECKOUT SUCCESS
$lang["thanks"]					= "Terima kasih";
$lang["looking_forward"]		= "kami tunggu kedatanganmu!";

# CONTACT
$lang["banner_contact"]			= "Hubungi Kami";
$lang["contact_name"]			= "Nama";
$lang["contact_email"]			= "Email";
$lang["contact_phone"]			= "Phone";
$lang["contact_category"]		= "Kategori";
$lang["contact_subject"]		= "Subyek";
$lang["contact_message"]		= "Pesan";
$lang["btn_send"]				= "Kirim";
$lang["send_success"]			= "Pesan anda berhasil dikirim";

# EVENTS
$lang["banner_events"]			= "Event";
$lang["next_events"]			= "Event Selanjutnya";
$lang["past_events"]			= "Event Sebelumnya";

# FIND US
$lang["banner_find_us"]			= "Temukan Kami";

# GOKART
$lang["banner_gokart"]			= "GoKart";
$lang["title_the_karts"]		= "Kart";
$lang["title_the_tracks"]		= "Lintasan";
$lang["btn_start_racing"]		= "Mulai Balapan";

# KIOSKS
$lang["banner_kiosks"]			= "Kios";

# NEWS
$lang["banner_news"]			= "Berita Taman Hiburan";

# PRACTICAL INFO
$lang["banner_info"]			= "Info Praktis";

# RESTAURANTS
$lang["banner_restaurants"]		= "Restoran";

# RESTAURANT SINGLE
$lang["title_where_to_eat"]		= "Makan di mana selanjutnya?";

# RIDES
$lang["banner_rides"]			= "Wahana";
$lang["btn_kids"]				= "Anak";
$lang["btn_family"]				= "Keluarga";
$lang["btn_thrill"]				= "Menegangkan";

# RIDES SINGLE
$lang["title_what_to_do"]		= "Melakukan apa selanjutnya?";

# SHOPPING CART
$lang["title_shopping_cart"]	= "Keranjang Belanja";
$lang["btn_remove"]				= "Hapus";

# SHOPS
$lang["banner_shops"]			= "Toko";

# SHOWS
$lang["banner_shows"]			= "Pertunjukan";

# SHOW SINGLE
$lang["title_what_to_see"]		= "Menonton apa selanjutnya?";

# SOUVENIRS
$lang["banner_souvenirs"]		= "Suvenir";

# TICKETS
$lang["banner_tickets"]			= "Tiket & Paket";
$lang["title_vip_cards"]		= "KARTU VIP";
$lang["title_tickets"]			= "TIKET";
$lang["title_gokart_tickets"]	= "GOKART";
$lang["title_package"]			= "PAKET";
$lang["title_lunch_pack"]		= "PAKET MAKAN";
$lang["title_birthday"]			= "ULANG TAHUN";
$lang["min_people"]				= "Min 15 orang";
$lang["cart_success"]			= "Sukses Ditambah ke Keranjang!";
$lang["cart_failed"]			= "Silakan Isi Jumlah Tiket!";
$lang['confirm']				= 'Apakah anda yakin ingin menghapus item ini? ';
$lang['diskon']					= "Diskon ";