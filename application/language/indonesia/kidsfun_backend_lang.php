<?php
// // sidbar
$lang['home'] 			= 'Beranda';
$lang['attractions']	= 'Objek Wisata';
$lang['all_attractions']= 'Semua Objek Wisata';
$lang['rides']			= 'Wahana';
$lang['rides_single']	= 'Detail Wahana';
$lang['gokart']			= 'GoKart'; 
$lang['aquasplash']		= 'Aquasplash';
$lang['shops']			= 'Toko';
$lang['all_shops']		= 'Semua Toko';
$lang['restaurants']	= 'Restoran';
$lang['souvenirs']		= 'Suvenir';
$lang['kiosks']			= 'Kios';
$lang['shows']			= 'Pertunjukan';
$lang['mainpage']		= 'Halaman Utama';
$lang['shows_single']	= 'Pertunjukan tunggal';
$lang['events']			= 'Events';
$lang['news']			= 'Berita';
$lang['info']			= 'Info';
$lang['contact_us']		= 'Hubungi Kami';
$lang['tickets_pack'] 	= 'Tiket dan Paket';
$lang['tickets']		= 'Tiket';
$lang['cart']   		= 'Keranjang belanja';
$lang['checkout']		= 'Periksa';
$lang['find_us']  		= 'Temukan Kami';
$lang['follow_us']         = 'Follow Us';

// btn
$lang['btn_edit'] 		= 'Ubah';
$lang['btn_detail'] 	= 'Detil';
$lang['btn_cancel'] 	= 'Batal';
$lang['btn_add'] 		= 'Tambah';
$lang['btn_update'] 	= 'Perbarui';
$lang['btn_delete'] 	= 'Hapus';
$lang['btn_save'] 		= 'Simpan';
$lang['btn_close'] 		= 'Keluar';

// notif
$lang['edit'] 			= 'Ubah';
$lang['detail'] 		= 'Detil';
$lang['cancel'] 		= 'Batal';
$lang['insert'] 		= 'Data Berhasil ditambah';
$lang['update'] 		= 'Data Berhasil diperbarui';
$lang['delete'] 		= 'Data Berhasil dihapus';
$lang['update_pass']	= 'Sandi Berhasil diperbarui';
$lang['wrong_pass'] 	= 'Sandi Lama Salah!';
$lang['save'] 			= 'Simpan';
$lang['user_check'] 	= 'Nama sudah ada! Silakan coba nama lain';
$lang['pass_check'] 	= 'Kata sandi harus sama!';
$lang['update_profile']	= 'Profil anda berhasil diubah!';
$lang['update_password']	= 'Sandi anda berhasil diubah. Silakan login kembali!';

//hover
$lang['update_data'] 	= 'Perbarui Data';
$lang['save_data'] 		= 'Simpan Data';
$lang['view'] 			= 'Lihat Detil';

// content
$lang['action']			= 'Aksi';
$lang['confirm']		= 'Apakah anda yakin ingin menghapus data ini?';
$lang['language'] 		= 'Bahasa';
$lang['image'] 			= 'Gambar';
$lang['content'] 		= 'Konten';
$lang['title_en'] 		= 'Judul English :';
$lang['title_in']		= 'Judul Indonesia :';
$lang['caption_en'] 	= 'Keterangan English :';
$lang['caption_in'] 	= 'Keterangan Indonesia :';
$lang['content_en'] 	= 'Konten English :';
$lang['content_in'] 	= 'Konten Indonesia :';
$lang['price'] 			= 'Harga :';

$lang['choose_file']	= 'Pilih FIle :';
$lang['choose_video']	= 'Pilih Video :';
$lang['choose_image']	= 'Pilih Gambar :';
$lang['choose_date']	= 'Pilih Tanggal :';
$lang['banner']			= 'Banner';
$lang['caption']		= 'Keterangan';
$lang['image_caption']	= 'Keterangan Gambar';
$lang['caption_en'] 	= 'Keterangan Indonesia :';
$lang['caption_in'] 	= 'Keterangan English :';
$lang['desc'] 			= 'Deskripsi';
$lang['desc_en']  		= 'Deskripsi English :';
$lang['desc_in']  		= 'Deskripsi Indonesia :';
$lang['banner_image']  	= 'Gambar Banner :';
$lang['content_image']  = 'Gambar Konten :';

$lang['divider']		= 'Pembatas';
$lang['title']			= 'Judul';
$lang['category']		= 'Kategori :';
$lang['name']			= 'Nama :';
$lang['dividers']   	= 'Divider';
$lang['dividers1']    	= 'Batas 1';
$lang['dividers2']    	= 'Batas 2';
$lang['dividers3']    	= 'Batas 3';
$lang['image1']       	= 'Gambar 1';
$lang['image2']       	= 'Gambar 2';
$lang['image3']       	= 'Gambar 3';
$lang['large_image']  	= 'Gambar Besar';
$lang['small_image']  	= 'Gambar Kecil';
$lang['date']  			= 'Tanggal';

$lang['image_1920px']  	= 'Ukuran gambar minimal : 1920 x 939 px';
$lang['image_752px']  	= 'Ukuran gambar minimal : 752 x 939 px';
$lang['image_512px']  	= 'Ukuran gambar minimal : 512 x 512 px';
$lang['image_1024px']  	= 'Ukuran gambar minimal : 1024 x 576 px';
$lang['image_1200px']  	= 'Ukuran gambar minimal : 1200 x 1115 px';

$lang['video_8mb']  	= 'Maksimum ukuran video : 8 MB';
$lang['video_type']  	= 'Unggah video harus ( mp4 )';

$lang['file_image']  	= 'File Maksimal 2MB';
$lang['landscape']  	= 'Gambar Harus Landscape Atau Ukuran File Terlalu besar (Maks 2MB)';
$lang['square']  		= 'Gambar Harus Square Atau Ukuran File Terlalu besar (Maks 2MB)';
$lang['portrait']  		= 'Gambar Harus Portrait Atau Ukuran File Terlalu besar (Maks 2MB)';
$lang['lands_square']	= 'Gambar Harus Landscape atau Square Atau Ukuran File Terlalu besar (Maks 2MB)';

$lang['landscape_alert']= 'Gambar Harus Landscape ';
$lang['square_alert']  	= 'Gambar Harus Square ';
$lang['portrait_alert'] = 'Gambar Harus Portrait ';
$lang['lands_square_alert']= 'Gambar Harus Landscape atau Square ';

$lang['big_image']  	= 'Gambar Untuk Desktop';
$lang['small_image']  	= 'Gambar Untuk Mobile';
$lang['choose_banner_large']  	= 'Pilih Banner untuk Desktop :';
$lang['choose_banner_small']  	= 'Pilih Banner untuk Mobile :';
$lang['error_image']  	= 'Anda tidak memilih file untuk meng-upload.';
$lang['edit_banner']  	= 'Ubah Banner';
$lang['map']			= 'Peta';
$lang['admin_confirm']	= 'Konfirmasi Admin';
$lang['status']			= 'Status';

// Tabel
$lang['name_tabel']			= 'Nama';
$lang['category_tabel']		= 'Kategori';
$lang['title_tabel_en'] 	= 'Judul English';
$lang['title_tabel_in']		= 'Judul Indonesia';
$lang['content_tabel_en']	= 'Konten English';
$lang['content_tabel_in']	= 'Konten Indonesia';
$lang['desc_tabel_en']		= 'Deskripsi English';
$lang['desc_tabel_in']		= 'Deskripsi Indonesia';
$lang['date']				= 'Tanggal';
$lang['username_tabel'] 	= 'Nama Pengguna';
$lang['fullname_tabel'] 	= 'Nama Lengkap';
$lang['price_tabel'] 		= 'Harga';
$lang['userlevel_tabel'] 	= 'Level User';
$lang['discount_tabel'] 	= 'Diskon (%)';

//Profile
$lang['account_setting'] 	= 'Pengaturan Akun';
$lang['profile'] 			= 'Profil';
$lang['pass'] 				= 'Sandi';
$lang['sign_out'] 			= 'Keluar';

// Home
$lang['op'] 			= 'Waktu buka :';
$lang['me'] 			= 'Usia Minimal';
$lang['wtb'] 			= 'Apa yang Dibawa ?';
$lang['acc'] 			= 'Aksesibilitas';
$lang['announcement'] 	= 'Pengumuman';

// rides
$lang['new_rides'] 		= 'Wahana Baru';
$lang['rides_list'] 	= 'Daftar Wahana';
$lang['add_rides']  	= 'Tambah Wahana';
$lang['edit_rides'] 	= 'Ubah Wahana';
$lang['delete_rides']  	= 'Hapus Wahana';
$lang['kids_rides']   	= 'Wahana Anak-anak';
$lang['family_rides']   = 'Wahana Keluarga';
$lang['thrill_rides']   = 'Wahana Menegengkan';

$lang['kids_rides']  	= 'Wahana Anak-anak';
$lang['family_rides']  	= 'Wahana Keluarga';
$lang['thrill_rides']  	= 'Wahana Menegengkan';



// news
$lang['news_date']  = 'Tanggal';
$lang['news_title'] = 'Judul Berita';
$lang['add_news']   = 'Tambah Berita';

// Contact us
$lang['address']  	= 'Alamat :';
$lang['phone']		= 'Nomor Telepon :';
$lang['message']  	= 'Pesan';
$lang['sender']  	= 'Pengirim';
$lang['subject']  	= 'Subyek';
$lang['detail_msg'] = 'Detil Pesan';
$lang['from']  		= 'Dari';
$lang['delete_message']  = 'Hapus Pesan';
$lang['detail_message']  = 'Detil Pesan';
$lang['message_received']  = 'Pesan diterima';

// events
$lang['add_events']	= 'Tambah Events';

//follow us
$lang['links']         = 'Tautan';

// kiosks
$lang['kiosks'] 	= 'Kios';

// restaurant 
$lang['restaurant_title'] = 'Judul Restoran';
$lang['restaurant_add']   = 'Tambah Restoran';
$lang['choose_image1'] 	  = 'Pilih Gambar 1 :';
$lang['choose_image2'] 	  = 'Pilih Gambar 2 :';
$lang['add_restaurant']     = 'Tambah Restoran';

// show
$lang['new_shows'] 		= 'Pertunjukan Baru';
$lang['shows_list'] 	= 'Daftar Pertunjukan';
$lang['add_shows']         = 'Tambah Pertunjukan';
$lang['edit_shows']  = 'Ubah Pertunjukan';
$lang['delete_shows']  	= 'Hapus Pertunjukan';

//info
$lang['new_info'] = 'Info Baru';
$lang['info_list'] 	= 'Daftar Info';
$lang['add_info']      = 'Tambah Info';
$lang['edit_info']  	= 'Ubah Info';
$lang['delete_info']  	= 'Hapus Info';

// tickets
$lang['ticket'] = 'Tiket';
$lang['add_tickets']     	= 'Tambah Tiket';
$lang['edit_tickets']  		= 'Ubah Tiket';
$lang['delete_tickets']  	= 'Hapus Tiket';
$lang['new_tickets'] 		= 'Tiket Baru';
$lang['choose_category'] 	= 'Pilih Kategori';
$lang['discount'] 			= 'Diskon (Opsional) :';

// checkout
$lang['payment'] 		= 'Pembayaran';
$lang['detail_payment'] = 'Detil Pembayaran';
$lang['delete_payment'] = 'Hapus Pembayaran';
$lang['edit_payment'] 	= 'Ubah Pembayaran';
$lang['first_name'] 	= 'Nama Depan';
$lang['last_name'] 		= 'Nama Belakang';
$lang['city'] 			= 'Kota';
$lang['zip'] 			= 'Kode Pos';
$lang['total_pay'] 		= 'Total Pembayaran';
$lang["visit_date"]		= "Tanggal kedatangan";

//user
$lang['user'] 			= 'Pengguna';
$lang['user_list'] 		= 'Daftar Pengguna';
$lang['new_user'] 		= 'Pengguna Baru';
$lang['username'] 		= 'Nama Pengguna :';
$lang['fullname'] 		= 'Nama Lengkap :';
$lang['password'] 		= 'Kata Sandi :';
$lang['current_pass'] 	= 'Sandi Lama :';
$lang['new_pass'] 		= 'Sandi Baru :';
$lang['add_user']       = 'Tambah Pengguna';
$lang['edit_user']  	= 'Ubah Pengguna';
$lang['delete_user']  	= 'Hapus Pengguna';

$lang['confirm_pass'] 	= 'Konfirmasi Sandi :';
$lang['user_level'] 	= 'Level User :';

$lang['image_video'] 	= 'Banner Video';