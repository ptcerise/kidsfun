<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shops/souvenirs/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/shops/souvenirs/'.$banner_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shops/souvenirs/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner" />
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_souvenirs"); ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($caption_en->general_content_en) ? $caption_en->general_content_en:"";
                    }else{
                        echo !empty($caption_in->general_content_in) ? $caption_in->general_content_in:"";
                    } ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down" />
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="bg-white container-fluid content">
    <div class="container shops">

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

            <div class="par lead">
                <?php if($lang == "english" || $lang == ""){
                    echo !empty($content_en->general_content_en) ? $content_en->general_content_en:"[EMPTY!]";
                }else{
                    echo !empty($content_in->general_content_in) ? $content_in->general_content_in:"[KOSONG!]";
                } ?>
            </div>

        </div>

        <figure class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <img class="img-thumbnail img-responsive"
            src="<?php echo !empty($content_img->shop_type_img) ? base_url('assets/upload/shops/souvenirs/thumbnail').'/'.$content_img->shop_type_img:base_url('assets/img/blank_img.png'); ?>"
            alt="kidsfun-souvenir" />
        </figure>
    </div>
</div>