<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/rides/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/attractions/rides/'.$banner_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/rides/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner-<?php echo $current; ?>">
        </picture>
        
        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_rides"); ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($caption_en->general_content_en) ? $caption_en->general_content_en:"";
                    }else{
                        echo !empty($caption_in->general_content_in) ? $caption_in->general_content_in:"";
                    } ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container content">

    <header class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 description text-center">
            <div class="par lead">
                <?php if($lang == "english" || $lang == ""){
                    echo !empty($content_en->general_content_en) ? $content_en->general_content_en:"[EMPTY!]";
                }else{
                    echo !empty($content_in->general_content_in) ? $content_in->general_content_in:"[KOSONG!]";
                } ?>
            </div>
        </div>
    </header>

    <aside class="row">
    <div class="col-xs-12 text-center">
        <div class="btn-group">
            <button id="btnKids" class="text-green btn btn-raised">
                <?php echo $this->lang->line("btn_kids"); ?>
            </button>
            <button id="btnFamily" class="text-blue btn btn-raised">
                <?php echo $this->lang->line("btn_family"); ?>
            </button>
            <button id="btnThrill" class="text-red btn btn-raised">
                <?php echo $this->lang->line("btn_thrill"); ?>
            </button>
        </div>
    </div>
    </aside>

    <aside class="row text-center">
        <button id="resetFilter" class="btn btn-xs btn-raised btn-bg-red">Reset Filter</button>
    </aside>
        
    <div class="row content cards">
        <?php $i = 0; ?>
        <?php foreach($rides_single as $key => $row){ ?>
            
            <article class="col-xs-12 col-sm-6 col-md-3 padding-clear ride-card-<?php echo $row->rides_category; ?>">
                <div class="panel panel-default">
                    <?php
                        #CUSTOM URL
                        $rides_name = strtolower($row->rides_name);
                        $url_single = $row->rides_id."-".url_title($rides_name);
                    ?>
                    
                    <div class="link-cards">
                        <div class="card-id"><?php echo $url_single; ?></div>
                        <figure>
                            <img src="<?php echo base_url('assets/upload/attractions/rides_single/thumbnail').'/'.$row->img_url; ?>" class="img-responsive" alt="icon"/>
                        </figure>
                        <header class="panel-body card-display">
                            <div class="row">
                                <div class="col-xs-9 text-left">
                                    <h4 class="display">
                                        <?php
                                            if(strlen($row->rides_name) > 16){
                                                echo substr($row->rides_name, 0, 12)."..";
                                            }else{
                                               echo $row->rides_name; 
                                            }
                                         ?>
                                    </h4>
                                </div>
                                <div class="col-xs-3 text-right">
                                    <?php if($row->rides_category == "thrill"){ ?>
                                        <img class="rides-icon" src="<?php echo base_url('assets/icon/icon_thrill_rides.png');?>" alt="rides">
                                    <?php }else{
                                        if($row->rides_category == "family"){ ?>
                                            <img class="rides-icon" src="<?php echo base_url('assets/icon/icon_family_rides.png');?>" alt="rides">
                                        <?php }else{ ?>
                                                <img class="rides-icon" src="<?php echo base_url('assets/icon/icon_kids_rides.png');?>" alt="rides">
                                        <?php }
                                    }?>
                                </div>
                            </div>
                        </header>
                    </div>
                    <div class="panel-body card-btn-more text-right">
                        <a class="btn btn-more btn-share" onclick="showShare('share<?php echo $key; ?>')">
                            <?php echo $this->lang->line("btn_share"); ?>
                        </a>
                        <a class="btn btn-more" href="<?php echo base_url('rides/detail/'.$url_single); ?>">
                            <?php echo $this->lang->line("btn_discover"); ?>
                        </a>
                    </div>
                    
                    <!-- SHARE -->

                    <div class="panel panel-default card-share" id="share<?php echo $key; ?>">
                        <div class="panel-body text-center social-share-btn">
                            <?php
                                $share_link = base_url('rides/detail/'.$url_single);
                                $share_title = $row->rides_name;
                                if($lang == "english" || $lang == ""){
                                    $share_text = $row->rides_name." on Kidsfun Yogyakarta. Click ";
                                }else{
                                    $share_text = $row->rides_name." di Kidsfun Yogyakarta. Klik ";
                                }
                            ?>
                            <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                            <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                            </a>
                            <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                            </a>
                            <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                            </a>
                        </div>
                        <br>
                        <div class="panel-body card-btn-more">
                            <a class="btn btn-more pull-right" onclick="closeShare('share<?php echo $key; ?>')">
                                <?php echo $this->lang->line("btn_close"); ?>
                            </a>
                        </div>
                    </div>

                </div>
            </article>
        <?php } ?>

    </div>

    <aside class="row text-center">
        <button id="showmore" class="btn btn-bg-red btn-raised">
            <?php echo $this->lang->line("btn_showmore"); ?>
        </button>
    </aside>
</div>