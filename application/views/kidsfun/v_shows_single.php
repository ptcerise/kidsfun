<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shows/shows_single').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/shows/shows_single').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shows/shows_single').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content text-center">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($shows_single->show_title_en) ? $shows_single->show_title_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($shows_single->show_title_in) ? $shows_single->show_title_in : "[ KOSONG! ]";
                    } ?>
                </h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($banner_large->media_content_en) ? $banner_large->media_content_en : "";
                    }else{
                        echo !empty($banner_large->media_content_in) ? $banner_large->media_content_in : "";;
                    } ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<section id="main" class="container">
    <header class="main-header text-center">
        <h2>Show</h2>
    </header>
    
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1">
            <?php foreach ($shows_image as $row) { ?>
            <figure class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <img alt="kidsfun-shows" class="img-thumbnail img-responsive" src="<?php echo base_url('assets/upload/shows/shows_single').'/'.$row->media_url;?>">
            </figure>
            <?php } ?>
        </div>
    </div>
    
    <article>
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 description">
                <div class="par lead text-center">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($shows_single->show_content_en) ? $shows_single->show_content_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($shows_single->show_content_in) ? $shows_single->show_content_in : "[ KOSONG! ]";;
                    } ?>
                </div>
            </div>
        </div>
    </article>

    <!-- SHARE *SHOWS ON SHARE BTN PRESSED -->
        <div id="share" class="modal fade">
          <div class="modal-dialog modal-share">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <?php
                        $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                        if($lang == "english" || $lang == ""){
                            $show_name = $shows_single->show_title_en;
                        }else{
                            $show_name = $shows_single->show_title_in;
                        }

                        $share_title = $show_name;
                        if($lang == "english" || $lang == ""){
                            $share_text = $show_name." show on Kidsfun Yogyakarta. Click ";
                        }else{
                            $share_text = "pertunjukan ".$show_name." di Kidsfun Yogyakarta. Klik ";
                        }
                    ?>
                    <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                    </a>
                    <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                    </a>
                    <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                    </a>
                </div>
                <div class="panel-body modal-close text-right">
                    <a class="btn btn-md btn-more" data-dismiss="modal">
                        <?php echo $this->lang->line("btn_close"); ?>
                    </a>
                </div> 
            </div>
          </div>
        </div>

    <aside class="row text-center">
        <button class="btn btn-raised share" data-toggle="modal" data-target="#share">
            <?php echo $this->lang->line("btn_share"); ?>
        </button>
        <button class="btn btn-bg-red btn-raised" onclick="window.location='<?php echo base_url(); ?>tickets'">
            <?php echo $this->lang->line("btn_buy_tickets"); ?>
        </button>
    </aside>    

    <!-- MAP -->
    <div class="row content">
        <figure class="col-xs-12 col-sm-8 col-sm-offset-2">
            <img class="img-thumbnail img-responsive" src="<?php echo !empty($shows_map->media_url) ? base_url('assets/upload/shows/shows_single/'.$shows_map->media_url) : base_url('assets/img/blank_img.png'); ?>" alt="kidsfun-map">
        </figure>
    </div>
    
    <section class="row">
        <header class="text-center">
            <h3 class="display"><?php echo $this->lang->line("title_what_to_see"); ?></h3>
        </header>

        <div class="row content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 padding-clear">
                <?php foreach($shows_card as $key => $row) { ?>
                    <?php
                        #CUSTOM URL
                        if($lang == "english" || $lang == ""){
                            $show_name = strtolower($row->show_title_en);
                        }else{
                            $show_name = strtolower($row->show_title_in);
                        }
                        $url_single = $row->show_id."-".url_title($show_name);
                    ?>
               <article class="col-xs-12 col-sm-6 padding-clear shows-cards">
                    <div class="panel panel-default">
                        <div class="link-cards">
                            <div class="card-id"><?php echo $url_single; ?></div>
                            <figure>
                                <img src="<?php echo base_url('assets/upload/shows/shows_single/thumbnail').'/'.@$row->media_url; ?>" class="img-responsive" alt="icon"/>
                            </figure>
                            <header class="panel-body card-display">
                                <div class="row">
                                    <div class="col-xs-12 text-left">
                                        <h4 class="display">
                                            <?php if($lang == "english" || $lang == ""){
                                                echo !empty($row->show_title_en) ? $row->show_title_en : "[ EMPTY! ]";
                                            }else{
                                                echo !empty($row->show_title_in) ? $row->show_title_in : "[ KOSONG! ]";;
                                            } ?>
                                        </h4>
                                    </div>
                                </div>
                            </header>
                        </div>
                        <div class="panel-body card-btn-more text-right">
                            <a class="btn btn-more btn-share" onclick="showShare('share<?php echo $key; ?>')">
                                <?php echo $this->lang->line("btn_share"); ?>
                            </a>
                            <a class="btn btn-more" href="<?php echo base_url('shows/detail').'/'.@$url_single; ?>">
                                <?php echo $this->lang->line("btn_discover"); ?>
                            </a>
                        </div>
                        
                        <!-- SHARE -->

                        <div class="panel panel-default card-share" id="share<?php echo $key; ?>">
                            <div class="panel-body text-center social-share-btn">
                                <?php
                                    $share_link = base_url('shows/detail/'.$url_single);
                                    if($lang == "english" || $lang == ""){
                                        $show_name = $row->show_title_en;
                                    }else{
                                        $show_name = $row->show_title_in;
                                    }

                                    $share_title = $show_name;
                                    if($lang == "english" || $lang == ""){
                                        $share_text = $show_name." show on Kidsfun Yogyakarta. Click ";
                                    }else{
                                        $share_text = "pertunjukan ".$show_name." di Kidsfun Yogyakarta. Klik ";
                                    }
                                ?>
                                <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                </a>
                                <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                </a>
                                <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                </a>
                            </div>
                            <br>
                            <div class="panel-body card-btn-more">
                                <a class="btn btn-more pull-right" onclick="closeShare('share<?php echo $key; ?>')">
                                    <?php echo $this->lang->line("btn_close"); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php } ?>
            </div>
        </div>
    </section>
</section>