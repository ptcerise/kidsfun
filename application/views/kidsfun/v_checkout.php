<section id="main" class="container no-banner">
<?php print_r($_SESSION); ?>
    <!-- HEADER -->
    <header class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
            <h1 class="display"><?php echo $this->lang->line("checkout"); ?></h1>
        
            <div class="par lead">
                <?php if($lang == "indonesia"){
                    echo !empty($checkout_content->general_content_in) ? $checkout_content->general_content_in : "[ KOSONG! ]";
                }else{

                    echo !empty($checkout_content->general_content_en) ? $checkout_content->general_content_en : "[ EMPTY! ]";
                } ?>
            </div>
        </div>
    </header>

    <!-- FORM -->
    <div class="row content">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-clear">
            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="panel panel-default">
                    <!-- <form method="POST" id="ePay" name="ePayment" action="https://payment.ipay88.co.id/epayment/entry.asp"> -->

                    <!-- FOR OFFLINE TEST -->
                    <form method="POST" id="ePay" name="ePayment" action="<?php echo base_url('checkout/test_email'); ?>">
                        <div class="panel-body">
                        <!-- THE FORM -->
                            <div class="col-effect input-effect">
                                <input id="checkout_fname" type="text" name="checkout_fname" class="effect-label">
                                <label><?php echo $this->lang->line("form_fname"); ?>*</label>
                                <span class="focus-border"></span>
                            </div>
                            <div class="col-effect input-effect">
                                <input id="checkout_lname" type="text" name="checkout_lname" class="effect-label" >
                                <label><?php echo $this->lang->line("form_lname"); ?>*</label>
                                <span class="focus-border"></span>
                            </div>
                            <div class="col-effect input-effect">
                                <input id="checkout_email" type="email" name="checkout_email" class="effect-label">
                                <label><?php echo $this->lang->line("form_email"); ?>*</label>
                                <span class="focus-border"></span>
                            </div>
                            <div class="col-effect input-effect">
                                <input id="checkout_phone" type="text" name="checkout_phone" class="effect-label">
                                <label><?php echo $this->lang->line("form_phone"); ?>*</label>
                                <span class="focus-border"></span>
                            </div>
                            <div class="col-effect input-effect">
                                <input id="checkout_address" type="text" name="checkout_address" class="effect-label">
                                <label><?php echo $this->lang->line("form_address"); ?>*</label>
                                <span class="focus-border"></span>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">  
                                    <div class="col-effect input-effect">
                                        <input id="checkout_city" type="text" name="checkout_city" class="effect-label">
                                        <label><?php echo $this->lang->line("form_city"); ?>*</label>
                                        <span class="focus-border"></span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-5">
                                    <div class="col-effect input-effect">
                                        <input id="checkout_zip" type="text" name="checkout_zip" class="effect-label" >
                                        <label><?php echo $this->lang->line("form_post_code"); ?>*</label>
                                        <span class="focus-border"></span>
                                    </div>     
                                </div>
                            </div>
                            <div class="col-effect input-effect">
                                <input id="checkout_visit" type="text" name="checkout_visit" class="effect-label">
                                <label><?php echo $this->lang->line("form_visit_date"); ?>*</label>
                                <span class="focus-border"></span>
                            </div>
                            <?php if($cart_tickets==!NULL) { ?>
                                <?php $total_price = array_sum(array_column($cart_tickets,'total_price')); ?>
                            <?php } ?>
                            <input type="hidden" name="checkout_total" class="effect-label" value="<?php if($cart_tickets==!NULL) { echo ($total_price); } ?>">

                            <!-- PAYMENT REQUEST -->
                            <!-- <input type="hidden" name="MerchantCode"    value="<?php echo $merchantcode; ?>">
                            <input type="hidden" name="PaymentId"       value="1">
                            <input type="hidden" name="RefNo"           value="<?php echo $refno; ?>">
                            <input type="hidden" name="Amount"          value="<?php echo $amount; ?>">
                            <input type="hidden" name="Currency"        value="<?php echo $currency; ?>">
                            <input type="hidden" name="Remark"          value="">
                            <input type="hidden" name="UserName"        id="UserName">
                            <input type="hidden" name="UserEmail"       id="UserEmail">
                            <input type="hidden" name="UserContact"     id="UserContact">
                            <input type="hidden" name="Lang"            value="UTF-8">
                            <input type="hidden" name="ProdDesc"        value="Kidsfun Tickets Package">
                            <input type="hidden" name="Signature"       value="<?php echo $signature; ?>" >
                            <input type="hidden" name="ResponseURL"     value="<?php echo base_url('checkout/response');?>" >
                            <input type="hidden" name="BackendURL" value="<?php echo base_url('backend_response');?>" > -->
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php if($cart_tickets==!NULL) { ?>
                            <?php $total_price = array_sum(array_column($cart_tickets,'total_price')); ?>
                        <?php } ?>
                        <div class="col-xs-12 col-sm-7">
                            <?php echo $this->lang->line("payment_total"); ?>
                        </div>
                        <div class="col-xs-12 col-sm-5 text-center">
                            Rp. <span id="total"><?php if($cart_tickets==!NULL) { echo number_format($total_price,0,',','.'); } ?></span>
                        </div>
                        
                        <!-- SUBMIT BTN -->
                        <?php if($cart_tickets==!NULL) { ?>
                        <div class="col-xs-12 col-sm-offset-2 col-md-offset-3 text-center">
                            <button id="btn-terms" class="btn btn-bg-red btn-raised btn-terms" disabled title="<?php echo $this->lang->line("title_pay"); ?>"><?php echo $this->lang->line("btn_pay"); ?></button>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <img class="img-responsive" alt="maybank" src="<?php echo base_url('assets/img/logo maybank.png'); ?>">
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <img class="img-responsive" alt="mastercard" src="<?php echo base_url('assets/img/MasterCard logo.jpg'); ?>">
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <img class="img-responsive" alt="visa" src="<?php echo base_url('assets/img/Visa Logo.jpg'); ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-3 ">
                                <img class="img-responsive" alt="visa-verified" src="<?php echo base_url('assets/img/Verified-by-VISA-Logo.jpg'); ?>">
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <img class="img-responsive" alt="jcb" src="<?php echo base_url('assets/img/jcb.png'); ?>">
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <img class="img-responsive" alt="jcb-secure" src="<?php echo base_url('assets/img/JsecureColorTif.png'); ?>">
                            </div>
                            <div class="col-xs-12 col-sm-3">
                                <img class="img-responsive" alt="mastercard-secure" src="<?php echo base_url('assets/img/mastercard-securecode.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- MODAL TERMS & CONDITIONS -->
            <div class="modal fade" id="terms" tabindex="-1" role="dialog" aria-labelledby="">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title display text-center">
                                SYARAT & KETENTUAN
                            </h4>
                        </div>
                        <div class="modal-body">
                            <textarea class="terms" readonly><?php $this->load->view('kidsfun/v_terms') ?></textarea>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-left">
                                <input id="check-terms" type="checkbox" name="agreement" value="Agree">
                            </div>
                            <label for="check-terms"><p><?php echo $this->lang->line("terms_agree"); ?></p></label>
                        </div>
                        
                        <div class="modal-footer card-btn-more text-right">
                            <a class="btn btn-more" data-dismiss="modal"><?php echo $this->lang->line("btn_cancel"); ?></a>
                            <button type="submit" class="btn btn-bg-red btn-raised" id="btn-pay"><?php echo $this->lang->line("btn_next"); ?></button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
