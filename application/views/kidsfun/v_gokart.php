<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/gokart').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/attractions/gokart').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/gokart').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_gokart"); ?></h1>
                <p class="lead">
                    <?php
                        if($lang == "english" || $lang == ""){
                            echo !empty($caption_en->general_content_en) ? $caption_en->general_content_en : "";
                        }else{
                            echo !empty($caption_in->general_content_in) ? $caption_in->general_content_in : "";
                        }
                    ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container content">

    <div class="row content">
        <div class="col-xs-12">
        <?php foreach($image_content as $img_content){ ?>
            <figure class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <img class="img-thumbnail img-responsive"
                     src="<?php echo !empty($img_content->attraction_url) ? base_url('assets/upload/attractions/gokart').'/'.$img_content->attraction_url : base_url('assets/img/blank_img.png');?>"
                     alt="kidsfun-gokart">
            </figure>
        <?php } ?>
        </div>
        
        <article>
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 description text-center">
                <div class="par lead">
                    <?php
                        if($lang == "english" || $lang == ""){
                            echo !empty($content_en->attraction_content_en) ? $content_en->attraction_content_en : "[ EMPTY! ]";
                        }else{
                            echo !empty($content_in->attraction_content_in) ? $content_in->attraction_content_in : "[ KOSONG! ]";
                        }
                    ?>
                </div>
            </div>
        </article>
    </div>
    
    <!-- THE KARTS -->
    <section>
        <header class="row">
            <h2 class="display"><?php echo $this->lang->line("title_the_karts"); ?></h2>
        </header>
        
        <div class="row">
            <?php foreach($card_karts as $key=>$card){ ?>
                <article class="col-xs-12 col-sm-3 padding-clear kart-cards">
                    <div class="panel panel-default">
                        <div id="kartFront<?php echo $card->attraction_id; ?>" class="kart-front">
                            <figure>
                                <img src="<?php echo !empty($card->attraction_url) ? base_url('assets/upload/attractions/gokart/'.$card->attraction_url) : base_url('assets/img/blank_img.png'); ?>"  class="img-responsive" alt="karts">
                            </figure>
                            <header class="panel-body card-display">
                                <div class="row">
                                    <div class="col-xs-12 text-left">
                                        <h4 class="display">
                                            <?php
                                                if($lang == "english" || $lang == ""){
                                                    echo !empty($card->attraction_title_en) ? $card->attraction_title_en : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($card->attraction_title_in) ? $card->attraction_title_in : "[ KOSONG! ]";
                                                }
                                            ?>
                                        </h4>
                                    </div>
                                </div>
                            </header>
                            <div class="panel-body card-btn-more">
                                <a class="btn btn-more pull-right detail-kart">
                                    <?php echo $this->lang->line("btn_discover"); ?>
                                </a>
                            </div>
                        </div>
                        <div id="kartDetail<?php echo $card->attraction_id; ?>" class="kart-detail panel-body">
                            <header class="panel-body card-display">
                                <div class="row">
                                    <div class="col-xs-12 text-left">
                                        <h4 class="display">
                                            <?php
                                                if($lang == "english" || $lang == ""){
                                                    echo !empty($card->attraction_title_en) ? $card->attraction_title_en : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($card->attraction_title_in) ? $card->attraction_title_in : "[ KOSONG! ]";
                                                }
                                            ?>
                                        </h4>
                                    </div>
                                </div>
                            </header>
                            <div class="par">
                                <?php
                                    if($lang == "english" || $lang == ""){
                                        echo isset($card->attraction_content_en)?$card->attraction_content_en : "[ EMPTY! ]";
                                    }else{
                                        echo isset($card->attraction_content_in)?$card->attraction_content_in : "[ KOSONG! ]";
                                    }
                                ?>
                            </div>
                            <div class="panel-body card-btn-more">
                                <a class="btn btn-more pull-right close-kart" onclick="closeKartDetail(<?php echo $card->attraction_id; ?>);">
                                    <?php echo $this->lang->line("btn_close"); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </div>   
    </section>
    
    <!-- THE TRACKS -->
    <section class="container">
        <header class="row ">
            <h2 class="display"><?php echo $this->lang->line("title_the_tracks"); ?></h2>
        </header>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="par">
                    <?php
                    if($lang == "english" || $lang == ""){
                        echo !empty($content_track_en->attraction_content_en) ? $content_track_en->attraction_content_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($content_track_in->attraction_content_in) ? $content_track_in->attraction_content_in : "[ KOSONG! ]";
                    }
                ?>
                </div class="par">
            </div>

            <div class="col-xs-12 col-sm-6">
                <?php foreach($track_images as $tracks){ ?>
                <figure class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <img class="img-thumbnail img-responsive" src="<?php echo !empty($tracks->attraction_url) ? base_url('assets/upload/attractions/gokart/'.$tracks->attraction_url) : base_url('assets/img/blank_img.png'); ?>" alt="img1">
                </figure>
                <?php } ?>
            </div>
        </div>
    </section>

    <!-- SHARE *SHOWS ON SHARE BTN PRESSED -->
        <div id="share" class="modal fade">
          <div class="modal-dialog modal-share">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <?php
                        $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        $page_title = $this->lang->line("banner_gokart");
                        $share_title = $page_title;
                        if($lang == "english" || $lang == ""){
                            $share_text = $page_title." on Kidsfun Yogyakarta. Click ";
                        }else{
                            $share_text = $page_title." di Kidsfun Yogyakarta. Klik ";
                        }
                    ?>
                    <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                    </a>
                    <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                    </a>
                    <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                    </a>
                </div>
                <div class="panel-body modal-close">
                    <a class="btn btn-md btn-more pull-right" data-dismiss="modal">
                        <?php echo $this->lang->line("btn_close"); ?>
                    </a>
                </div> 
            </div>
          </div>
        </div>
    
    <!-- BUTTONS -->
    <aside class="container buttons text-center">
        <button class="btn btn-raised share" data-toggle="modal" data-target="#share">
            <?php echo $this->lang->line("btn_share"); ?>
        </button>
        <button class="btn btn-bg-red btn-raised">
            <?php echo $this->lang->line("btn_start_racing"); ?>
        </button>
        <button class="btn btn-raised white">
            <?php echo $this->lang->line("btn_learn"); ?>
        </button>
    </aside>
</div>