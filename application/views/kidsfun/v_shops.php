<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <!-- Wrapper for slides -->
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo base_url('assets/img/resto-large.jpg'); ?>" />
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo base_url('assets/img/resto-small.jpg'); ?>"/>
            <img class="img-responsive"
                src="<?php echo base_url('assets/img/resto-large.jpg'); ?>" 
                alt="kidsfun-banner" />
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_shops"); ?></h1>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed d, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed d, Lorem ipsum dolor sit amet.</p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container content">
    <header class="row main-header">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 description">
            <p class="lead text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
        </div>
    </header>
    
    <div class="row">
        <article class="col-xs-12 col-sm-4 padding-clear">
            <div class="panel panel-default">
                <header class="panel-body card-header">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <h3 class="display card-ride">
                                Souvenirs
                            </h3>
                        </div>
                    </div>
                </header>
                <figure>
                    <img src="<?php echo base_url('assets/img/castle.jpg'); ?>" class="img-responsive img-home-rides" alt="Souvenirs" />
                </figure>
                <div class="panel-body">
                    
                    <?php $miniContent = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."; ?>

                    <p class="visible-md visible-lg"><?php echo substr($miniContent, 0, 160); ?> ...</p>
                    <p class="visible-sm"><?php echo substr($miniContent, 0, 140); ?> ...</p>
                    <p class="visible-xs"><?php echo substr($miniContent, 0, 100); ?> ...</p>
                    
                </div>
                <div class="panel-body card-btn-more">
                    <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>kidsfun/souvenirs">
                        <?php echo $this->lang->line("btn_discover"); ?>
                    </a>
                </div>
            </div>
        </article>

        <article class="col-xs-12 col-sm-4 padding-clear">
            <div class="panel panel-default">
                <header class="panel-body card-header">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <h3 class="display card-ride">
                                Restaurants
                            </h3>
                        </div>
                    </div>
                </header>
                <figure>
                    <img src="<?php echo base_url('assets/img/castle.jpg'); ?>"  class="img-responsive img-home-rides" alt="Restaurants" />
                </figure>
                <div class="panel-body">
                    
                    <?php $miniContent = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."; ?>

                    <p class="visible-md visible-lg"><?php echo substr($miniContent, 0, 160); ?> ...</p>
                    <p class="visible-sm"><?php echo substr($miniContent, 0, 140); ?> ...</p>
                    <p class="visible-xs"><?php echo substr($miniContent, 0, 100); ?> ...</p>
                </div>
                <div class="panel-body card-btn-more">
                    <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>kidsfun/restaurants">
                        <?php echo $this->lang->line("btn_discover"); ?>
                    </a>
                </div>
            </div>
        </article>

        <article class="col-xs-12 col-sm-4 padding-clear">
            <div class="panel panel-default">
                <header class="panel-body card-header">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <h3 class="display card-ride">
                                Kiosks
                            </h3>
                        </div>
                    </div>
                </header>
                <figure>
                    <img src="<?php echo base_url('assets/img/castle.jpg'); ?>"   class="img-responsive img-home-rides" alt="Kiosks" />
                </figure>
                <div class="panel-body">
                    
                    <?php $miniContent = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."; ?>

                    <p class="visible-md visible-lg"><?php echo substr($miniContent, 0, 160); ?> ...</p>
                    <p class="visible-sm"><?php echo substr($miniContent, 0, 140); ?> ...</p>
                    <p class="visible-xs"><?php echo substr($miniContent, 0, 100); ?> ...</p> 
                </div>
                <div class="panel-body card-btn-more">
                    <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>kidsfun/kiosks">
                        <?php echo $this->lang->line("btn_discover"); ?>
                    </a>
                </div>
            </div>
        </article>
    </div>
    
    <aside class="row text-center">
        <button class="btn btn-bg-red btn-raised">
            <?php echo $this->lang->line("btn_corporate_website"); ?>
        </button>
    </aside>
</div>