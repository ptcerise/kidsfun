<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/aquasplash/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/attractions/aquasplash/'.$banner_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                 src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/aquasplash/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                 alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_aquasplash"); ?></h1>
                <p class="lead">
                    <?php
                        if($lang == "indonesia"){
                            echo !empty($caption_in->general_content_in) ? $caption_in->general_content_in : "";
                            
                        }else{
                            echo !empty($caption_en->general_content_en) ? $caption_en->general_content_en : "";
                        }
                    ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container-fluid content">

    <!-- INTRO -->
    <div class="row content">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
            <?php foreach($image_content as $img_content){ ?>
            <figure class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <img class="img-thumbnail img-responsive"
                     src="<?php echo !empty($img_content->attraction_url) ? base_url('assets/upload/attractions/aquasplash/'.$img_content->attraction_url) : base_url('assets/img/blank_img.png');?>"
                     alt="img-aquasplash-kidsfun">
            </figure>
            <?php } ?>
        </div>
        <article>
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 description text-center">
                <div class="par lead">
                    <?php
                        if($lang == "indonesia"){
                            echo !empty($content_in->attraction_content_in) ? $content_in->attraction_content_in : "[ KOSONG! ]";

                        }else{
                            echo !empty($content_en->attraction_content_en) ? $content_en->attraction_content_en : "[ EMPTY! ]";
                        }
                    ?>
                </div>
            </div>
        </article>
    </div>
    
    <!-- THE SLIDES
    <section>
        <div class="row">
            <header class="col-xs-12 col-sm-10 col-sm-offset-1">
                <h2 class="display"><?php echo $this->lang->line("title_the_slides"); ?></h2>
            </header>
        </div>

        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 padding-clear">
                <?php foreach($slide_cards as $key=>$slide){ ?>
                    <article class="col-xs-12 col-sm-6 col-md-3 padding-clear">
                        <div class="panel panel-default">
                            <figure>
                                <img src="<?php echo !empty($slide->attraction_url) ? base_url('assets/upload/attractions/aquasplash/'.$slide->attraction_url) : base_url('assets/img/blank_img.png'); ?>"  class="img-responsive" alt="kidsfun-aquasplash-slides">
                            </figure>
                            <header class="panel-body card-display">
                                <div class="row">
                                    <div class="col-xs-12 text-left">
                                        <h4 class="display">
                                            <?php
                                                if($lang == "indonesia"){
                                                    echo !empty($slide->attraction_title_in) ? $slide->attraction_title_in : "[ KOSONG! ]";
                                                }else{

                                                    echo !empty($slide->attraction_title_en) ? $slide->attraction_title_en : "[ EMPTY! ]";
                                                }
                                            ?>
                                        </h4>
                                    </div>
                                </div>
                            </header>
                            <div class="panel-body card-btn-more">
                                <a class="btn btn-more pull-right" href="#">
                                    <?php echo $this->lang->line("btn_discover"); ?>
                                </a>
                            </div>
                        </div>
                    </article>
                <?php } ?>
            </div>
        </div>
    </section>
     -->
    <!-- THE POOLS -->
    <section class="row">
        <header class="col-xs-12 col-sm-10 col-sm-offset-1">
            <h2 class="display"><?php echo $this->lang->line("title_the_pools"); ?></h2>
        </header>

        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="par">
                    <?php
                        if($lang == "indonesia"){
                            echo !empty($content_pools_in->attraction_content_in) ? $content_pools_in->attraction_content_in : "[ KOSONG! ]";

                        }else{
                            echo !empty($content_pools_en->attraction_content_en) ? $content_pools_en->attraction_content_en : "[ EMPTY! ]";
                        }
                    ?>
                </div>
            </div>

            <!-- 4 Images -->
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <?php foreach($pool_images as $key=>$pool){ ?>
                <figure class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <img class="img-thumbnail img-responsive" src="<?php echo !empty($pool->attraction_url) ? base_url('assets/upload/attractions/aquasplash/'.$pool->attraction_url) : base_url('assets/img/blank_img.png'); ?>" alt="kidsfun-aquasplash-pool">
                </figure>
                <?php } ?>
            </div>
        </div>
    </section>
    
    <!-- SHARE *SHOWS ON SHARE BTN PRESSED -->
        <div id="share" class="modal fade">
          <div class="modal-dialog modal-share">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <?php
                        $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        $page_title = $this->lang->line("banner_aquasplash");
                        $share_title = $page_title;
                        if($lang == "indonesia"){
                            $share_text = $page_title." di Kidsfun Yogyakarta. Klik ";

                        }else{
                            $share_text = $page_title." on Kidsfun Yogyakarta. Click ";
                        }
                    ?>
                    <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                    </a>
                    <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                    </a>
                    <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                    </a>
                </div>
                <div class="panel-body modal-close">
                    <a class="btn btn-md btn-more pull-right" data-dismiss="modal">
                        <?php echo $this->lang->line("btn_close"); ?>
                    </a>
                </div> 
            </div>
          </div>
        </div>
    
    <!-- BUTTONS -->
    <aside class="row text-center">
        <button class="btn btn-raised share" data-toggle="modal" data-target="#share">
            <?php echo $this->lang->line("btn_share"); ?>
        </button>
        <button class="btn btn-bg-red btn-raised">
            <?php echo $this->lang->line("btn_buy_tickets"); ?>
        </button>
    </aside>
</div>

