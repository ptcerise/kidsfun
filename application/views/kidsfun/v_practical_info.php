<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/info').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/info').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/info').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_info"); ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($caption_en->general_content_en) ? $caption_en->general_content_en : "";
                    }else{
                        echo !empty($caption_in->general_content_in) ? $caption_in->general_content_in : "";
                    } ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container content">
    <header class="row main-header">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
            <div class="par lead">
                <?php if($lang == "english" || $lang == ""){
                    echo !empty($content_en->general_content_en) ? $content_en->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($content_in->general_content_in) ? $content_in->general_content_in : "[ KOSONG! ]";
                } ?>
            </div>
        </div>
    </header>
    
    <div class="row content">
        <?php foreach($info as $row){ ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 padding-clear">
                <a class="show-modal-info" data-toggle="modal" data-target="#info<?php echo $row->info_id; ?>">
                    <div class="panel panel-default practical-info">
                        
                        <article class="panel-body">
                            <header class="col-xs-12 text-left">
                                <h4 class="display card-info">
                                   <strong>
                                       <?php if($lang == "english" || $lang == ""){
                                            echo !empty($row->info_title_en) ? $row->info_title_en : "[ EMPTY! ]";
                                            $info_title = $row->info_title_en;
                                        }else{
                                            echo !empty($row->info_title_in) ? $row->info_title_in : "[ KOSONG! ]";
                                            $info_title = $row->info_title_in;
                                        } ?>
                                   </strong>
                                </h4>
                            </header>
                            <div class="par card-info">
                                <?php
                                    if($lang == "english" || $lang == ""){
                                        $info_content = !empty($row->info_content_en) ? $row->info_content_en : "[ EMPTY! ]";
                                    }else{
                                        $info_content = !empty($row->info_content_in) ? $row->info_content_in : "[ KOSONG! ]";
                                    }

                                    if(strlen($info_title) <= 10){
                                        echo substr($info_content, 0, 250)."...";
                                    }elseif(strlen($info_title) <= 20){
                                        echo substr($info_content, 0, 200)."...";
                                    }elseif(strlen($info_title) <= 30){
                                        echo substr($info_content, 0, 120)."...";
                                    }elseif(strlen($info_title) <= 40){
                                        echo substr($info_content, 0, 100)."...";
                                    }elseif(strlen($info_title) <= 50){
                                        echo substr($info_content, 0, 75)."...";
                                    }else{
                                        if($lang == "english" || $lang == ""){
                                            echo "Click for details...";
                                        }else{
                                            echo "Klik untuk detail...";
                                        }
                                    }
                                ?>

                            </div>
                            <button class="modal-detail" data-toggle="modal" data-target="#info<?php echo $row->info_id; ?>"></button>
                        </article>
                        
                    </div>
                </a>
            </div>

            <div class="modal fade" id="info<?php echo $row->info_id; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-info<?php echo $row->info_id; ?>">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title display" id="modal-info<?php echo $row->info_id; ?>">
                        <?php echo $info_title; ?>
                    </h4>
                  </div>
                  <div class="modal-body">
                    <div class="par">
                        <?php echo $info_content; ?>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-more" data-dismiss="modal">Close</a>
                  </div>
                </div>
              </div>
            </div>

        <?php } ?>
    </div>
</div>