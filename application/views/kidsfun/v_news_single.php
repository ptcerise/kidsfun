<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/news/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/news/'.$banner_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/news/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                alt="banner-kidsfun">
        </picture>

        <header class="intro-banner-content">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1 class="display"><?php echo $this->lang->line("banner_news"); ?></h1>
                    <p class="lead">
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($banner_caption->general_content_en) ? $banner_caption->general_content_en : "";
                            }else{
                                echo !empty($banner_caption->general_content_in) ? $banner_caption->general_content_in : "";
                            }
                        ?>
                    </p>
                    <a href="#main">
                        <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                    </a>
                </div>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="bg-white container-fluid content">
    <article class="container">
        <header class="row main-header">
            <div class="col-xs-12">
                <h2 class="bold">
                    <?php
                        if($lang == "english" || $lang == ""){
                            echo !empty($news->news_title_en) ? $news->news_title_en : "[ EMPTY! ]";
                        }else{
                            echo !empty($news->news_title_in) ? $news->news_title_in : "[ KOSONG! ]";
                        }
                    ?>
                </h2>
                <h4>
                    <?php
                        echo !empty($news->news_date) ? date('d/m/Y', strtotime($news->news_date)) : date('d/M/Y');
                    ?>
                </h4>
                <div class="par lead">
                    <?php
                        if($lang == "english" || $lang == ""){
                            echo !empty($caption->media_content_en) ? $caption->media_content_en : "";
                        }else{
                            echo !empty($caption->media_content_in) ? $caption->media_content_in : "";
                        }
                    ?>
                </div>
            </div>
        </header>

        <?php
            #FOR THE CONTENT
            if($lang == "english" || $lang == ""){
                $news_content = !empty($news->news_content_en) ? $news->news_content_en : "[ EMPTY! ]";
            }else{
                $news_content = !empty($news->news_content_in) ? $news->news_content_in : "[ KOSONG! ]";
            }
        ?>

        <?php
        #FOR PICTURES, ADDING VALUE TO ARRAY
            $i = 0;
            foreach($image_content as $key=>$value){
                $img_content[$i] = $value->media_url;
                $i++;
            }
        ?>
        
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="par">
                    <?php
                        echo !empty($news_content) ? shorten_string($news_content, 150) : "[' EMPTY! ']";
                        $continue = strlen(shorten_string($news_content, 150));
                    ?>
                </div>
            </div>

            <figure class="col-xs-12 col-sm-6">
                <img class="img-thumbnail img-responsive" src="<?php echo !empty($img_content[0]) ? base_url('assets/upload/news/'.$img_content[0]) : base_url('assets/img/blank_img.png');?>" alt="kidsfun-news">
            </figure>
        </div>
        <br>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <img class="img-thumbnail img-responsive" src="<?php echo !empty($img_content[1]) ? base_url('assets/upload/news/'.$img_content[1]) : base_url('assets/img/blank_img.png');?>" alt="kidsfun-news">
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="par">
                    <?php
                        $limit = strlen($news_content);
                        echo !empty($news_content) ? substr($news_content, $continue, $limit) : "[ EMPTY! ]";
                    ?>
                </div>
            </div>
        </div>

    </article>
</div>

<?php
#TO CUT STRING PER WORDS, NOT PER CHARACTERS
    function shorten_string($string , $wordsreturned)
    {
      $retval = $string;
      $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
      $string = str_replace("\n", " ", $string);
      $array = explode(" ", $string);
      if (count($array)<=$wordsreturned)
      {
        $retval = $string;
      }
      else
      {
        array_splice($array, $wordsreturned);
        $retval = implode(" ", $array);
      }
      return $retval;
    }
?>