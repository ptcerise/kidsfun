<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shows').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/shows').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shows').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content text-center">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_shows"); ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($caption_en->general_content_en) ? $caption_en->general_content_en : "";
                    }else{
                        echo !empty($caption_in->general_content_in) ? $caption_in->general_content_in : "";
                    } ?>
                </p>
                    <a href="#main">
                        <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                    </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="bg-white content">
    <div class="container">

        <header class="row">
            <div class="col-xs-10 col-xs-offset-1 description text-center">
                <div class="par lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($content_en->general_content_en) ? $content_en->general_content_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($content_in->general_content_in) ? $content_in->general_content_in : "[ KOSONG! ]";;
                    } ?>
                </div>
            </div>
        </header>

        <div class="row cards"> <!-- CARDS -->
            <?php foreach ($shows_single as $key => $row) { ?>
                <?php
                    #CUSTOM URL
                    if($lang == "english" || $lang == ""){
                        $show_name = strtolower($row->show_title_en);
                    }else{
                        $show_name = strtolower($row->show_title_in);
                    }
                    $url_single = $row->show_id."-".url_title($show_name);
                ?>
                <article class="col-xs-12 col-sm-4 padding-clear shows-cards">
                    <div class="panel panel-default">
                        <div class="link-cards">
                            <div class="card-id"><?php echo $url_single; ?></div>
                            <div class="panel-body card-header">
                                <div class="row">
                                    <div class="col-xs-12 text-left">
                                        <h3 class="display card-ride">
                                            <?php if($lang == "english" || $lang == ""){
                                                echo !empty($row->show_title_en) ? $row->show_title_en : "[ EMPTY! ]";
                                            }else{
                                                echo !empty($row->show_title_in) ? $row->show_title_in : "[ KOSONG! ]";;
                                            } ?>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <figure>
                                <img alt="" src="<?php echo base_url('assets/upload/shows/shows_single/'.@$row->media_url); ?>"  class="img-responsive img-home-rides">
                            </figure>
                            <div class="panel-body shows-content">
                                <?php
                                    if($lang == "english" || $lang == ""){
                                        $miniContent = !empty($row->show_content_en) ? $row->show_content_en : "[ EMPTY! ]";
                                    }else{
                                        $miniContent = !empty($row->show_content_in) ? $row->show_content_in : "[ KOSONG! ]";
                                    }
                                ?>
                                <div class="par visible-md visible-lg"><?php echo substr($miniContent, 0, 180); ?> ...</div>
                                <div class="par visible-sm"><?php echo substr($miniContent, 0, 150); ?> ...</div>
                                <div class="par visible-xs"><?php echo substr($miniContent, 0, 100); ?> ...</div>
                            </div>
                        </div>
                        <div class="panel-body card-btn-more text-right">
                            <a class="btn btn-more btn-share" onclick="showShare('share<?php echo $key; ?>')">
                                <?php echo $this->lang->line("btn_share"); ?>
                            </a>
                            <a class="btn btn-more" href="<?php echo base_url('shows/detail/'.@$url_single); ?>">
                                <?php echo $this->lang->line("btn_discover"); ?>
                            </a>
                        </div>
                        

                        <!-- SHARE -->

                        <div class="panel panel-default card-share" id="share<?php echo $key; ?>">
                            <div class="panel-body social-share-btn text-center">
                                <?php
                                    $share_link = base_url('shows/detail/'.$url_single);
                                    if($lang == "english" || $lang == ""){
                                        $show_name = $row->show_title_en;
                                    }else{
                                        $show_name = $row->show_title_in;
                                    }

                                    $share_title = $show_name;
                                    if($lang == "english" || $lang == ""){
                                        $share_text = $show_name." show on Kidsfun Yogyakarta. Click ";
                                    }else{
                                        $share_text = "pertunjukan ".$show_name." di Kidsfun Yogyakarta. Klik ";
                                    }
                                ?>
                                <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                </a>
                                <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                </a>
                                <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                </a>
                            </div>
                            <br>
                            <div class="panel-body card-btn-more">
                                <a class="btn btn-more pull-right" onclick="closeShare('share<?php echo $key; ?>')">
                                    <?php echo $this->lang->line("btn_close"); ?>
                                </a>
                            </div>
                        </div>

                    </div>
                </article>
            <?php } ?>
        </div> <!-- CARDS -->

        <aside class="row text-center">
            <button id="showmore" class="btn btn-bg-red btn-raised">
                <?php echo $this->lang->line("btn_showmore"); ?>
            </button>
        </aside>
    </div>
</div>