<section id="main" class="bg-white container-fluid no-banner">
    
    <header class="row text-center">
        <figure class="col-xs-12">
            <i class="fa fa-smile-o text-red checkout-smile"></i>
        </figure>

        <h2>
            <?php if($lang == "indonesia"){

            }else{
                echo !empty($success_checkout->general_title_in) ? $success_checkout->general_title_in : "";
            } ?>
        </h2>
    </header>

    <div class="row description">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
            <div class="par lead">
                <?php if($lang == "indonesia"){
                    echo !empty($success_checkout->general_content_in) ? $success_checkout->general_content_in : "";

                }else{
                    echo !empty($success_checkout->general_content_en) ? $success_checkout->general_content_en : "";
                } ?>
            </div>
        </div>
    </div>

    <!-- SHARE *SHOWS ON SHARE BTN PRESSED -->
        <div id="share" class="modal fade">
          <div class="modal-dialog modal-share">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <p>Share on :</p>
                    <a href="">
                        <img alt="facebook" src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>">
                    </a>
                    <a href="">
                        <img alt="twitter" src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>">
                    </a>
                    <a href="">
                        <img alt="google-plus" src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>">
                    </a>
                </div>
                <div class="panel-body modal-close">
                    <a class="btn btn-md btn-more pull-right" data-dismiss="modal">Close</a>
                </div> 
            </div>
          </div>
        </div>

    <aside class="row">
        <div class="col-xs-12 bottom-btn text-center">
            <button class="btn btn-raised share" data-toggle="modal" data-target="#share">
                <?php echo $this->lang->line("btn_share"); ?>
            </button>
            <button class="btn btn-bg-red btn-raised" onclick="window.location='<?php echo base_url();?>'">
                <?php echo $this->lang->line("btn_continue"); ?>
            </button>
        </div>
    </aside>
</section>