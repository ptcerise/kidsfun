<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <!-- Wrapper for slides -->
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/tickets').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/tickets').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/tickets').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_tickets"); ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($banner_caption->general_content_en) ? $banner_caption->general_content_en : "";
                    }else{
                        echo !empty($banner_caption->general_content_in) ? $banner_caption->general_content_in : "";
                    } ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container content tickets-page">
    <header class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 description text-center">
            <div class="par lead">
                <?php if($lang == "english" || $lang == ""){
                    echo !empty($tickets_content->general_content_en) ? $tickets_content->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($tickets_content->general_content_in) ? $tickets_content->general_content_in : "[ KOSONG! ]";
                } ?>
            </div>
        </div>
    </header>

    <!-- VIP CARDS -->
    <?php if(count($vip_cards) != 0) { ?>
    <section class="row">
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("title_vip_cards"); ?></h2>
        </header>
        
        <div class="col-xs-12 padding-clear">
            <div class="tickets panel-group" id="accordion_vip" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                    <?php foreach($vip_cards as $key => $row){ ?>
                    <article>  
                        <div class="row">
                            <div role="tab" id="vip-heading<?php echo $key; ?>">
                                <div class="panel-body">
                                    <header class="col-xs-12 col-sm-7">
                                        <h3>
                                        <a class="itemId" role="button" data-toggle="collapse" data-parent="#accordion" href="#vip-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="vip-collapse<?php echo $key; ?>">
                                                <?php echo !empty($row->tickets_name) ? $row->tickets_name : "[ EMPTY! ]"; ?>

                                            <?php $cek_disc = $row->discount; ?>
                                            <?php if($cek_disc!='') { ?>
                                                <?php echo '<span class="text-red">'.$this->lang->line("diskon").$row->discount.'% '.$this->lang->line("discount").'</span>'; ?>
                                            <?php } ?> 
                                        </a>
                                        </h3>
                                    </header>

                                    <div class="col-xs-12 col-sm-3 ">
                                        <input type="hidden" name="tickets_id" value="<?php echo $row->tickets_id; ?>">
                                        <button type="button" class="btn-minus btn btn-custom btn-default" disabled><i class="fa fa-minus"></i></button>
                                        <input type="text" name="tiket" value="0" placeholder="" class="value">
                                        <button type="button" class="btn-plus btn btn-custom btn-default"><i class="fa fa-plus"></i></button>

                                        <!-- BTN ADD TO CART -->
                                        <button title="<?php echo $this->lang->line("btn_add_to_cart"); ?>" class="btn btn-raised btn-cart text-right">
                                            <i class="fa fa-cart-plus"></i>
                                        </button>
                                    </div>

                                    <div class="col-xs-12 col-sm-2">
                                        <h3 class="harga">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#vip-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="vip-collapse<?php echo $key; ?>">
                                                <?php if($cek_disc!='') { ?>
                                                        <!-- HITUNG HARGA DISKON -->
                                                        <?php $count_disc = (100 - $row->discount)/100*$row->tickets_price;  ?>

                                                        <!-- PEMBULATAN HARGA -->
                                                        <?php
                                                            $mod = ($count_disc) % 500;
                                                            if ($mod > 0) {
                                                                $round = 500 - $mod;
                                                                $result = ($count_disc) + $round; 
                                                            }
                                                            else {
                                                                $result = $count_disc;
                                                            }
                                                        ?>
                                                        
                                                        <!-- HASIL -->
                                                        Rp <?php echo !empty($result) ? number_format($result,0,',','.') : 0; ?>
                                                    <div class="text-red">
                                                        <s>
                                                        Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                        </s>
                                                    </div>
                                                <?php } else { ?>
                                                    Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                <?php } ?>
                                            </a>    
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div id="vip-collapse<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="vip-heading<?php echo $key; ?>">
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-10">
                                        <div class="par">
                                            <?php if($lang == "english" || $lang == ""){
                                                echo !empty($row->tickets_content_en) ? $row->tickets_content_en : "[ EMPTY! ]";
                                            }else{
                                                echo !empty($row->tickets_content_in) ? $row->tickets_content_in : "[ KOSONG! ]";;
                                            } ?>
                                        </div >
                                    </div>
                                    <!-- <div class="col-xs-12 col-sm-2 text-center">
                                        <button class="btn btn-raised btn-cart">
                                            <?php echo $this->lang->line("btn_add_to_cart"); ?>
                                        </button>
                                    </div> -->
                                    <!-- <div class="col-xs-12 col-sm-2 text-left">
                                        <button class="red btn btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                            <?php echo $this->lang->line("btn_checkout"); ?>
                                        </button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </article>          
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    
    <!-- TICKETS -->
    <?php if(count($tickets) != 0) { ?>
    <section class="row">
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("title_tickets"); ?></h2>
        </header>

        <div class="col-xs-12 padding-clear">
            <div class="tickets panel-group" id="accordion_tickets" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <?php foreach($tickets as $key => $row){ ?>
                        <article>
                            <div class="row">
                                <div role="tab" id="ticket-heading<?php echo $key; ?>">
                                    <div class="panel-body">       

                                        <header class="col-xs-12 col-sm-7">
                                            <h3>
                                            <a class="itemId" role="button" data-toggle="collapse" data-parent="#accordion" href="#ticket-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="ticket-collapse<?php echo $key; ?>">
                                                <?php echo !empty($row->tickets_name) ? $row->tickets_name : "[ EMPTY! ]"; ?>
                                                
                                                <?php $cek_disc = $row->discount; ?>
                                                <?php if($cek_disc!='') { ?>
                                                    <?php echo '<span class="text-red">'.$this->lang->line("diskon").$row->discount.'% '.$this->lang->line("discount").'</span>'; ?>
                                                <?php } ?>        
                                            </a>
                                            </h3>
                                        </header>

                                        <div class="col-xs-12 col-sm-3">
                                            <input type="hidden" name="tickets_id" value="<?php echo $row->tickets_id; ?>">
                                            <button type="button" class="btn-minus btn btn-custom btn-default" disabled><i class="fa fa-minus"></i></button>
                                            <input type="text" name="tiket" value="0" placeholder="" class="value">
                                            <button type="button" class="btn-plus btn btn-custom btn-default"><i class="fa fa-plus"></i></button>

                                            <!-- BTN ADD TO CART -->
                                            <button title="<?php echo $this->lang->line("btn_add_to_cart"); ?>" class="btn btn-raised btn-cart text-right">
                                                <i class="fa fa-cart-plus"></i>
                                            </button>
                                        </div>

                                        <div class="col-xs-12 col-sm-2">
                                            <h3 class="harga">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ticket-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="ticket-collapse<?php echo $key; ?>">
                                                    <?php if($cek_disc!='') { ?>
                                                        <!-- HITUNG HARGA DISKON -->
                                                        <?php $count_disc = (100 - $row->discount)/100*$row->tickets_price;  ?>

                                                        <!-- PEMBULATAN HARGA -->
                                                        <?php
                                                            $mod = ($count_disc) % 500;
                                                            if ($mod > 0) {
                                                                $round = 500 - $mod;
                                                                $result = ($count_disc) + $round; 
                                                            }
                                                            else {
                                                                $result = $count_disc;
                                                            }
                                                        ?>
                                                        
                                                        <!-- HASIL -->
                                                        Rp <?php echo !empty($result) ? number_format($result,0,',','.') : 0; ?>
                                                        <div class="text-red">
                                                            <s>
                                                            Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                            </s>
                                                        </div>
                                                    <?php } else { ?>
                                                        Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                    <?php } ?>
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="ticket-collapse<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ticket-heading<?php echo $key; ?>">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-10">
                                            <div class="par">
                                                <?php if($lang == "english" || $lang == ""){
                                                    echo !empty($row->tickets_content_en) ? $row->tickets_content_en : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($row->tickets_content_in) ? $row->tickets_content_in : "[ KOSONG! ]";;
                                                } ?>
                                            </div>
                                        </div>
                                        <!-- <div class="col-xs-12 col-sm-2 text-center">
                                            <button class="btn btn-raised btn-cart">
                                                <?php echo $this->lang->line("btn_add_to_cart"); ?>
                                            </button>
                                        </div> -->
                                        <!-- <div class="col-xs-12 col-sm-2 text-left">
                                            <button class="red btn btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                                <?php echo $this->lang->line("btn_checkout"); ?>
                                            </button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php } ?>
                </div>
            </div> 
        </div>
    </section>
    <?php } ?>

    <!-- GOKART -->
    <?php if(count($gokart) != 0) { ?>
    <section class="row">
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("title_gokart_tickets"); ?></h2>
        </header>

        <div class="col-xs-12 padding-clear">
            <div class="tickets panel-group" id="accordion_gokart" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <?php foreach($gokart as $key => $row){ ?>
                        <article>
                            <div class="row">
                                <div role="tab" id="gokart-heading<?php echo $key; ?>">
                                    <div class="panel-body">       

                                        <header class="col-xs-12 col-sm-7">
                                            <h3>
                                            <a class="itemId" role="button" data-toggle="collapse" data-parent="#accordion" href="#gokart-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="gokart-collapse<?php echo $key; ?>">
                                                <?php echo !empty($row->tickets_name) ? $row->tickets_name : "[ EMPTY! ]"; ?>
                                                
                                                <?php $cek_disc = $row->discount; ?>
                                                <?php if($cek_disc!='') { ?>
                                                    <?php echo '<span class="text-red">'.$this->lang->line("diskon").$row->discount.'% '.$this->lang->line("discount").'</span>'; ?>
                                                <?php } ?>
                                            </a>
                                            </h3>
                                        </header>

                                        <div class="col-xs-12 col-sm-3">
                                            <input type="hidden" name="tickets_id" value="<?php echo $row->tickets_id; ?>">
                                            <button type="button" class="btn-minus btn btn-custom btn-default" disabled><i class="fa fa-minus"></i></button>
                                            <input type="text" name="tiket" value="0" placeholder="" class="value">
                                            <button type="button" class="btn-plus btn btn-custom btn-default"><i class="fa fa-plus"></i></button>

                                            <!-- BTN ADD TO CART -->
                                            <button title="<?php echo $this->lang->line("btn_add_to_cart"); ?>" class="btn btn-raised btn-cart text-right">
                                                <i class="fa fa-cart-plus"></i>
                                            </button>
                                        </div>

                                        <div class="col-xs-12 col-sm-2">
                                            <h3 class="harga">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#gokart-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="gokart-collapse<?php echo $key; ?>">
                                                    <?php if($cek_disc!='') { ?>
                                                        <!-- HITUNG HARGA DISKON -->
                                                        <?php $count_disc = (100 - $row->discount)/100*$row->tickets_price;  ?>

                                                        <!-- PEMBULATAN HARGA -->
                                                        <?php
                                                            $mod = ($count_disc) % 500;
                                                            if ($mod > 0) {
                                                                $round = 500 - $mod;
                                                                $result = ($count_disc) + $round; 
                                                            }
                                                            else {
                                                                $result = $count_disc;
                                                            }
                                                        ?>
                                                        
                                                        <!-- HASIL -->
                                                        Rp <?php echo !empty($result) ? number_format($result,0,',','.') : 0; ?>
                                                        <div class="text-red">
                                                            <s>
                                                            Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                            </s>
                                                        </div>
                                                    <?php } else { ?>
                                                        Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                    <?php } ?>
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="gokart-collapse<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="gokart-heading<?php echo $key; ?>">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-10">
                                            <div class="par">
                                                <?php if($lang == "english" || $lang == ""){
                                                    echo !empty($row->tickets_content_en) ? $row->tickets_content_en : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($row->tickets_content_in) ? $row->tickets_content_in : "[ KOSONG! ]";;
                                                } ?>
                                            </div>
                                        </div>
                                        <!-- <div class="col-xs-12 col-sm-2 text-center">
                                            <button class="btn btn-raised btn-cart">
                                                <?php echo $this->lang->line("btn_add_to_cart"); ?>
                                            </button>
                                        </div> -->
                                        <!-- <div class="col-xs-12 col-sm-2 text-left">
                                            <button class="red btn btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                                <?php echo $this->lang->line("btn_checkout"); ?>
                                            </button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php } ?>
                    <div class="row">
                        <div class="panel-body">
                            <header class="col-xs-12 col-sm-6 text-red">
                                <?php if($lang == 'indonesia') { ?>
                                    <?php echo "* Harga Gokart belum termasuk Balaclava (Rp. 4.500)" ?>
                                <?php } else { ?>
                                    <?php echo "* Gokart Price exclude Balaclava (Rp. 4.500)" ?>
                                <?php } ?>
                            </header>
                        </div>
                    </div>
                </div>
            </div> 
        </div>

    </section>
    <?php } ?>

    <!-- PACKAGE -->
    <?php if(count($package) != 0) { ?>
    <section class="row">
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("title_package"); ?></h2>
        </header>

        <div class="col-xs-12 padding-clear">
            <div class="tickets panel-group" id="accordion_package" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <?php foreach($package as $key => $row){ ?>
                        <article>
                            <div class="row">
                                <div role="tab" id="package-heading<?php echo $key; ?>">
                                    <div class="panel-body">       

                                        <header class="col-xs-12 col-sm-7">
                                            <h3>
                                            <a class="itemId" role="button" data-toggle="collapse" data-parent="#accordion" href="#package-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="package-collapse<?php echo $key; ?>">
                                                <?php echo !empty($row->tickets_name) ? $row->tickets_name : "[ EMPTY! ]"; ?>
                                                    
                                                <?php $cek_disc = $row->discount; ?>
                                                <?php if($cek_disc!='') { ?>
                                                    <?php echo '<span class="text-red">'.$this->lang->line("diskon").$row->discount.'% '.$this->lang->line("discount").'</span>'; ?>
                                                <?php } ?>
                                            </a>
                                            </h3>
                                        </header>

                                        <div class="col-xs-12 col-sm-3">
                                            <input type="hidden" name="tickets_id" value="<?php echo $row->tickets_id; ?>">
                                            <button type="button" class="btn-minus btn btn-custom btn-default" disabled><i class="fa fa-minus"></i></button>
                                            <input type="text" name="tiket" value="0" placeholder="" class="value">
                                            <button type="button" class="btn-plus btn btn-custom btn-default"><i class="fa fa-plus"></i></button>

                                            <!-- BTN ADD TO CART -->
                                            <button title="<?php echo $this->lang->line("btn_add_to_cart"); ?>" class="btn btn-raised btn-cart text-right">
                                                <i class="fa fa-cart-plus"></i>
                                            </button>
                                        </div>

                                        <div class="col-xs-12 col-sm-2">
                                            <h3 class="harga">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#package-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="package-collapse<?php echo $key; ?>">
                                                    <?php if($cek_disc!='') { ?>
                                                        <!-- HITUNG HARGA DISKON -->
                                                        <?php $count_disc = (100 - $row->discount)/100*$row->tickets_price;  ?>

                                                        <!-- PEMBULATAN HARGA -->
                                                        <?php
                                                            $mod = ($count_disc) % 500;
                                                            if ($mod > 0) {
                                                                $round = 500 - $mod;
                                                                $result = ($count_disc) + $round; 
                                                            }
                                                            else {
                                                                $result = $count_disc;
                                                            }
                                                        ?>
                                                        
                                                        <!-- HASIL -->
                                                        Rp <?php echo !empty($result) ? number_format($result,0,',','.') : 0; ?>
                                                        <div class="text-red">
                                                            <s>
                                                            Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                            </s>
                                                        </div>
                                                    <?php } else { ?>
                                                        Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                    <?php } ?>
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="package-collapse<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="package-heading<?php echo $key; ?>">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-10">
                                            <div class="par">
                                                <?php if($lang == "english" || $lang == ""){
                                                    echo !empty($row->tickets_content_en) ? $row->tickets_content_en : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($row->tickets_content_in) ? $row->tickets_content_in : "[ KOSONG! ]";;
                                                } ?>
                                            </div class="par">
                                        </div>
                                        <!-- <div class="col-xs-12 col-sm-2 text-center">
                                            <button class="btn btn-raised btn-cart">
                                                <?php echo $this->lang->line("btn_add_to_cart"); ?>
                                            </button>
                                        </div> -->
                                        <!-- <div class="col-xs-12 col-sm-2 text-left">
                                            <button class="red btn btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                                <?php echo $this->lang->line("btn_checkout"); ?>
                                            </button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php } ?>
                </div>
            </div> 
        </div>
    </section>
    <?php } ?>

    <!-- LUNCH PACKAGE -->
    <?php if(count($lunch_pack) != 0) { ?>
    <section class="row">
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("title_lunch_pack"); ?></h2>
        </header>

        <div class="col-xs-12 padding-clear">
            <div class="tickets panel-group" id="accordion_lunch" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <?php foreach($lunch_pack as $key => $row){ ?>
                        <article>
                            <div class="row">
                                <div role="tab" id="lunch-heading<?php echo $key; ?>">
                                    <div class="panel-body">       

                                        <header class="col-xs-12 col-sm-7">
                                            <h3>
                                            <a class="itemId" role="button" data-toggle="collapse" data-parent="#accordion" href="#lunch-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="lunch-collapse<?php echo $key; ?>">
                                                <?php echo !empty($row->tickets_name) ? $row->tickets_name : "[ EMPTY! ]"; ?>
                                                
                                                <?php $cek_disc = $row->discount; ?>
                                                <?php if($cek_disc!='') { ?>
                                                    <?php echo '<span class="text-red">'.$this->lang->line("diskon").$row->discount.'% '.$this->lang->line("discount").'</span>'; ?>
                                                <?php } ?>
                                            </a>
                                            </h3>
                                        </header>

                                        <div class="col-xs-12 col-sm-3">
                                            <input type="hidden" name="tickets_id" value="<?php echo $row->tickets_id; ?>">
                                            <button type="button" class="btn-minus btn btn-custom btn-default" disabled><i class="fa fa-minus"></i></button>
                                            <input type="text" name="tiket" value="0" placeholder="" class="value">
                                            <button type="button" class="btn-plus btn btn-custom btn-default"><i class="fa fa-plus"></i></button>

                                            <!-- BTN ADD TO CART -->
                                            <button title="<?php echo $this->lang->line("btn_add_to_cart"); ?>" class="btn btn-raised btn-cart text-right">
                                                <i class="fa fa-cart-plus"></i>
                                            </button>
                                        </div>

                                        <div class="col-xs-12 col-sm-2">
                                            <h3 class="harga">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#lunch-collapse<?php echo $key; ?>">
                                                    <?php if($cek_disc!='') { ?>
                                                        <!-- HITUNG HARGA DISKON -->
                                                        <?php $count_disc = (100 - $row->discount)/100*$row->tickets_price;  ?>

                                                        <!-- PEMBULATAN HARGA -->
                                                        <?php
                                                            $mod = ($count_disc) % 500;
                                                            if ($mod > 0) {
                                                                $round = 500 - $mod;
                                                                $result = ($count_disc) + $round; 
                                                            }
                                                            else {
                                                                $result = $count_disc;
                                                            }
                                                        ?>
                                                        
                                                        <!-- HASIL -->
                                                        Rp <?php echo !empty($result) ? number_format($result,0,',','.') : 0; ?>
                                                        <div class="text-red">
                                                            <s>
                                                            Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                            </s>
                                                        </div>
                                                    <?php } else { ?>
                                                        Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                    <?php } ?>
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="lunch-collapse<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="lunch-heading<?php echo $key; ?>">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-sm-10">
                                            <div class="par">
                                                <?php if($lang == "english" || $lang == ""){
                                                    echo !empty($row->tickets_content_en) ? $row->tickets_content_en : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($row->tickets_content_in) ? $row->tickets_content_in : "[ KOSONG! ]";;
                                                } ?>
                                            </div>
                                        </div>
                                        <!-- <div class="col-xs-12 col-sm-2 text-center">
                                            <button class="btn btn-raised btn-cart">
                                                <?php echo $this->lang->line("btn_add_to_cart"); ?>
                                            </button>
                                        </div> -->
                                        <!-- <div class="col-xs-12 col-sm-2 text-left">
                                            <button class="red btn btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                                <?php echo $this->lang->line("btn_checkout"); ?>
                                            </button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php } ?>
                </div>
            </div> 
        </div>
    </section>
    <?php } ?>
    
    <!-- BIRTHDAY -->
    <?php if(count($birthday) != 0) { ?>
    <section class="row">
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("title_birthday"); ?></h2>
        </header>

        <div class="col-xs-12 padding-clear">
            <div class="tickets panel-group" id="accordion_bday" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                <?php foreach($birthday as $key => $row){ ?>
                    <article>
                        <div class="row">
                            <div role="tab" id="bday-heading<?php echo $key; ?>">
                                <div class="panel-body">
                                    
                                        <header class="col-xs-12 col-sm-7">
                                            <h3>
                                            <a class="itemId" role="button" data-toggle="collapse" data-parent="#accordion" href="#bday-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="bday-collapse<?php echo $key; ?>">
                                                <?php echo !empty($row->tickets_name) ? $row->tickets_name : "[ EMPTY! ]"; ?>
                                                <?php $cek_disc = $row->discount; ?>
                                                <?php if($cek_disc!='') { ?>
                                                    <?php echo '<span class="text-red">'.$this->lang->line("diskon").$row->discount.'% '.$this->lang->line("discount").'</span>'; ?>
                                                <?php } ?>
                                            </a>
                                            </h3>
                                        </header>

                                        <div class="col-xs-12 col-sm-3">
                                            <input type="hidden" name="tickets_id" value="<?php echo $row->tickets_id; ?>">
                                            <button type="button" class="btn-minus btn btn-custom btn-default" disabled><i class="fa fa-minus"></i></button>
                                            <input type="text" name="tiket" value="0" placeholder="" class="value">
                                            <button type="button" class="btn-plus btn btn-custom btn-default"><i class="fa fa-plus"></i></button>

                                            <!-- BTN ADD TO CART -->
                                            <button title="<?php echo $this->lang->line("btn_add_to_cart"); ?>" class="btn btn-raised btn-cart text-right">
                                                <i class="fa fa-cart-plus"></i>
                                            </button>
                                        </div>

                                        <div class="col-xs-12 col-sm-2">
                                            <h3 class="harga">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#bday-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="bday-collapse<?php echo $key; ?>">
                                                    <?php if($cek_disc!='') { ?>
                                                        <!-- HITUNG HARGA DISKON -->
                                                        <?php $count_disc = (100 - $row->discount)/100*$row->tickets_price;  ?>

                                                        <!-- PEMBULATAN HARGA -->
                                                        <?php
                                                            $mod = ($count_disc) % 500;
                                                            if ($mod > 0) {
                                                                $round = 500 - $mod;
                                                                $result = ($count_disc) + $round; 
                                                            }
                                                            else {
                                                                $result = $count_disc;
                                                            }
                                                        ?>
                                                        
                                                        <!-- HASIL -->
                                                        Rp <?php echo !empty($result) ? number_format($result,0,',','.') : 0; ?>
                                                        <div class="text-red">
                                                            <s>
                                                            Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                            </s>
                                                        </div>
                                                    <?php } else { ?>
                                                        Rp <?php echo !empty($row->tickets_price) ? number_format($row->tickets_price,0,',','.') : 0; ?>
                                                    <?php } ?>
                                                </a>
                                            </h3>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div id="bday-collapse<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="bday-heading<?php echo $key; ?>">
                                <div class="panel-body">
                                    <div class="col-xs-12 col-sm-10">
                                        <div class="par">
                                            <?php if($lang == "english" || $lang == ""){
                                                echo !empty($row->tickets_content_en) ? $row->tickets_content_en : "[ EMPTY! ]";
                                            }else{
                                                echo !empty($row->tickets_content_in) ? $row->tickets_content_in : "[ KOSONG! ]";;
                                            } ?>
                                        </div>
                                    </div> 
                                    <!-- <div class="col-xs-12 col-sm-2 text-center">
                                        <button class="btn btn-raised btn-cart"
                                            data-
                                        >
                                            <?php echo $this->lang->line("btn_add_to_cart"); ?>
                                        </button>
                                    </div> -->
                                    <!-- <div class="col-xs-12 col-sm-2 text-left">
                                        <button class="red btn btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                            <?php echo $this->lang->line("btn_checkout"); ?>
                                        </button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </article>
                <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    <section class="row">
        <div class="col-xs-12 padding-clear">
            <div class="tickets panel-group">
                <div class="panel panel-default">
                    <div class="row">
                        <div class="panel-body">
                            <div class="col-xs-12 col-sm-12 text-center">
                                <button class="red btn btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                    <?php echo $this->lang->line("btn_checkout"); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>