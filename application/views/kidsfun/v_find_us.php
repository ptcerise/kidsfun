<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <!-- Wrapper for slides -->
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo base_url('assets/upload/find_us').'/'.$banner_large->media_url; ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/find_us').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/find_us').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_find_us"); ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($banner_caption->general_content_en) ? $banner_caption->general_content_en : "";
                    }else{
                        echo !empty($banner_caption->general_content_in) ? $banner_caption->general_content_in : "";
                    } ?> 
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="bg-grey container-fluid content">

    <header class="row main-header">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 text-center">
            <div class="par lead">
                <?php if($lang == "english" || $lang == ""){
                    echo !empty($content->general_content_en) ? $content->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($content->general_content_in) ? $content->general_content_in : "[ KOSONG! ]";
                } ?> 
            </div>
        </div>
    </header>

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 padding-clear">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-clear">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div class="par">
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($opening_time->general_content_en) ? $opening_time->general_content_en : "[ EMPTY! ]";
                            }else{
                                echo !empty($opening_time->general_content_in) ? $opening_time->general_content_in : "[ KOSONG! ]";
                            }
                        
                        ?>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div class="par">
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($address->general_content_en) ? $address->general_content_en : "[ EMPTY! ]";
                            }else{
                                echo !empty($address->general_content_in) ? $address->general_content_in : "[ KOSONG! ]";
                            }
                        
                        ?> 
                        </div>           
                    </div>
                </div>
            </div>

            <figure class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img class="img-responsive" src="<?php echo !empty($find_us_map->media_url) ? base_url('assets/upload/find_us').'/'.$find_us_map->media_url:base_url('assets/img/card-image-rect.jpg'); ?>" alt="info_map">
            </figure>

        </div>
    </div>
</div>