<!-- GET IP VISITORS -->
<?php
    $ip_address = $_SERVER['REMOTE_ADDR'];
    $date       = date('Y-m-d');

    if (!isset($_COOKIE["visitor"]))
    {
        setcookie("visitor", "$ip_address", time() +3600);
        // mysql_connect("localhost", "user", "password"); //sesuaikan host, user, dan password-nya !
        // mysql_select_db(“nama_db”) or die(mysql_error()); //sesuaikan nama database-nya

        $this->db->query("INSERT INTO visitors VALUES ('$ip_address', '$_SERVER[HTTP_USER_AGENT]', '$date')");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="KIDS FUN YOGYAKARTA comes with 21 rides for kids and 10 rides for adults. Feel the thrill of rafting, water bikes, riding a Harley Davidson and a bumper car and many more. Besides being attractive, you also do not need to worry because all of these rides also completed with good standard security so it is safe for kids and families.">
    <meta name="author" content="">

    <link rel="canonical" href="https://kidsfun.co.id">
    <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico');?>">

    <!-- Twitter tag -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:url" content="https://twitter.com/Kidsfun_Yogya">
    <meta name="twitter:title" content="Kidsfun Parc Yogyakarta">
    <meta name="twitter:description" content="Official Twitter Kids Fun Parcs - Pusat Rekreasi Keluarga Yogyakarta dan Kids Fun Family Entertainment Centers.">
    <meta name="twitter:image" content="https://pbs.twimg.com/profile_images/786500046061645824/WsOFWT5N.jpg">

    <title>Kidsfun | <?php
        $current_page = str_replace("_", " ", html_escape($current));
        echo isset($title) ? ucfirst($title) : ucfirst($current_page); 
    ?></title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oleo+Script:400,700' rel='stylesheet' type='text/css'>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.2.1/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.2.1/jssocials-theme-flat.css" />

    <!-- Toastr Notification -->
    <link href='<?php echo base_url('assets/css/toastr.min.css') ?>' rel='stylesheet' type='text/css'>

    <!-- Datepicker -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/css/bootstrap-datetimepicker.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link type='text/css' href="<?php echo base_url('assets/css/styles.min.css');?>" rel="stylesheet">
</head>
<body>
    <?php include_once("analyticstracking.php") ?>
    <?php $lang = $this->session->userdata('fe_lang'); ?>

    <!-- Navigation -->
    <header>
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container nav-container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a href="<?php echo base_url(); ?>">
                    <!--[if lt IE 7]>
                    <img src="<?php echo base_url('assets/icon/logo_kidsfun.png');?>" alt="kidsfun" class="img-circle hidden-xs logo-header">
                    <![endif]-->
                        <img item src="<?php echo base_url('assets/icon/logo_kidsfun.svg');?>" alt="kidsfun" class="img-circle hidden-xs logo-header">
                    </a>
                    <div class="panel navbar-brand-shadow visible-lg visible-md"></div>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="row narrow-row">
                    <ul class="nav navbar-nav navbar-right nb-second">
                        <li>
                            <a class="nb-second" href="<?php echo base_url(); ?>contact_us">
                                <?php echo $this->lang->line("nav_contact"); ?>
                            </a>
                        </li>
                        <li>
                            <?php
                                $gmap = $this->db->query("SELECT * FROM general WHERE general_page='Contact_us' AND general_section='location'")->row()->general_content_en;
                            ?>
                            <a class="nb-second" target="_blank" href="<?php echo $gmap; ?>">
                                <?php echo $this->lang->line("nav_location"); ?>
                            </a>
                        </li>
                        <li>
                            <?php
                                $parkmap = $this->db->query("SELECT * FROM media WHERE media_page='Find_us' AND media_section='map'")->row()->media_url;
                            ?>
                            <a class="nb-second" target="_blank" href="<?php echo base_url('assets/upload/find_us').'/'.$parkmap; ?>">
                                <?php echo $this->lang->line("nav_parkmap"); ?>
                            </a>
                        </li>
                        <li>
                            <a class="nb-second" href="<?php echo base_url(); ?>shopping_cart"><i class="fa fa-shopping-cart fa-lg"></i></a>
                        </li>
                        <li class="flag">
                            <?php if($lang == 'english' || $lang == ''): ?>
                                    <a href="<?php echo base_url('languageSwitcher/switchLang/indonesia');?>">
                                    <img alt="EN" src="<?php echo base_url('assets/icon/indonesia.png'); ?>">
                                    </a>
                            <?php else:?>
                                    <a href="<?php echo base_url('languageSwitcher/switchLang/english');?>">
                                    <img alt="ID" src="<?php echo base_url('assets/icon/english.png'); ?>">
                                    </a>
                            <?php endif; ?>
                                
                        </li>
                    </ul>
                </div>
                <div class="row narrow-row">
                    <ul class="nav navbar-nav navbar-right nb-main">
                        <li class="visible-xs">
                            <a href="<?php echo base_url(); ?>" class="nb-main header-title-line2">
                                <?php echo $this->lang->line("nav_home"); ?>
                            </a>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle header-title-line2 nb-main" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php echo $this->lang->line("nav_attractions"); ?>
                          <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>rides" class="nb-main header-title-line2">
                                <?php echo $this->lang->line("nav_rides"); ?>
                            </a></li>
                            <li><a href="<?php echo base_url(); ?>gokart" class="nb-main header-title-line2">
                                <?php echo $this->lang->line("nav_gokart"); ?>
                            </a></li>
                            <li><a href="<?php echo base_url(); ?>aquasplash" class="nb-main header-title-line2" >
                                <?php echo $this->lang->line("nav_aquasplash"); ?>
                            </a></li>
                          </ul>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="nb-main dropdown-toggle header-title-line2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php echo $this->lang->line("nav_shops"); ?> <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>restaurants" class="nb-main header-title-line2">
                                <?php echo $this->lang->line("nav_restaurants"); ?>
                            </a></li>
                            <li><a href="<?php echo base_url(); ?>souvenirs" class="nb-main header-title-line2">
                                <?php echo $this->lang->line("nav_souvenirs"); ?>
                            </a></li>
                            <li><a href="<?php echo base_url(); ?>kiosks" class="nb-main header-title-line2">
                                <?php echo $this->lang->line("nav_kiosks"); ?>
                            </a></li>
                          </ul>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>shows" class="nb-main header-title-line2"><?php echo $this->lang->line("nav_shows"); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>events" class="nb-main header-title-line2"><?php echo $this->lang->line("nav_events"); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>news" class="nb-main header-title-line2"><?php echo $this->lang->line("nav_news"); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>practical_info" class="nb-main header-title-line2"><?php echo $this->lang->line("nav_info"); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>tickets" class="text-red nb-main header-title-line2"><?php echo $this->lang->line("nav_tickets"); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    </header>
    <!-- THE OPENING TIME -->
        <aside class="card-times">
            <div class="open-time">
                <p><?php echo $this->lang->line("park_open_day"); ?><br>  <?php echo $this->lang->line("park_open_time"); ?></p>
            </div>
        </aside>
    <div class="current-page"><?php echo $current; ?></div>
    <main>
        <?php echo $content; ?>
    </main>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row footer-top">
                <div class="col-xs-12">
                    <article class="col-xs-12 col-sm-6 col-md-6 col-lg-3 footer-border">
                        <h4 class="bold">
                            <?php echo $this->lang->line("footer_latest_news"); ?>
                        </h4>
                        <?php foreach($latest_news as $key=>$news){ ?>
            <?php
                #CUSTOM URL
                if($lang == "english" || $lang == ""){
                    $news_title = strtolower($news->news_title_en);
                }else{
                    $news_title = strtolower($news->news_title_in);
                }
                $url_single = $news->news_id."-".str_replace(array(" ",","), "-", $news_title)."-".date('d-m-Y',strtotime($news->news_date)) ;
            ?>
                        <div class="content-footer">
                            <a href="<?php echo base_url('news/read/'.$url_single); ?>" class="footer-news-title">
                                <h5>
                                    <?php
                                        if($lang == "english" || $lang == ""){
                                            $fn_title = !empty($news->news_title_en) ? $news->news_title_en : "[ EMPTY! ]";
                                        }else{
                                            $fn_title = !empty($news->news_title_in) ? $news->news_title_in : "[ KOSONG! ]";
                                        }

                                        if(strlen($fn_title) <= 40){
                                            echo $fn_title;
                                        }else{
                                            echo substr($fn_title, 0, 40)."...";
                                        }
                                    ?>
                                </h5>
                            </a>
                            <p class="text-grey"><?php echo date('d/m/Y', strtotime($news->news_date)); ?></p>
                        
                            <div class="par footer-news" style="min-height:125px">
                                <?php
                                    if($lang == "english" || $lang == ""){
                                        $fn_content = !empty($news->news_content_en) ? $news->news_content_en : "[ EMPTY! ]";
                                    }else{
                                        $fn_content = !empty($news->news_content_in) ? $news->news_content_in : "[ KOSONG! ]";
                                    }
                                    
                                    if(strlen($fn_title) < 23){
                                        echo substr($fn_content,0,100)."...";
                                    }else{
                                        echo substr($fn_content,0,72)."...";
                                    }
                                ?>
                            </div>
                        </div>
                        <a class="btn btn-more pull-right" href="<?php echo base_url('news/read/'.$url_single); ?>"><b>
                            <?php echo $this->lang->line("btn_readmore"); ?>
                        </b></a>
                        <?php break;} ?>
                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-6 col-lg-3 footer-border">
                        <h4 class="bold">
                            <?php
                                if($check > 0){
                                    echo $this->lang->line("footer_next_events");
                                }else{
                                    echo $this->lang->line("footer_past_events");
                                }
                             ?>
                        </h4>
                        <?php foreach($next_event as $key=>$ne){ ?>
                        <div class="content-footer">
                            <a href="<?php echo base_url('events'); ?>" class="footer-news-title">
                                <h5>
                                    <?php
                                        if($lang == "english" || $lang == ""){
                                            $ne_title = !empty($ne->events_title_en) ? $ne->events_title_en : "[ EMPTY! ]";
                                        }else{
                                            $ne_title = !empty($ne->events_title_in) ? $ne->events_title_in : "[ KOSONG! ]";
                                        }

                                        if(strlen($ne_title) <= 40){
                                            echo $ne_title;
                                        }else{
                                            echo substr($ne_title, 0, 40)."...";
                                        }
                                    ?>
                                </h5>
                            </a>
                            <p class="text-grey"><?php echo date('d/m/Y', strtotime($ne->events_date)); ?></p>
                        
                            <div class="par footer-news" style="min-height:125px">
                                <?php
                                    if($lang == "english" || $lang == ""){
                                        $ne_content = !empty($ne->events_content_en) ? $ne->events_content_en : "[ EMPTY! ]";
                                    }else{
                                        $ne_content = !empty($ne->events_content_in) ? $ne->events_content_in : "[ KOSONG! ]";
                                    }
                                    if(strlen($ne_title) < 23){
                                        echo substr($ne_content,0,100)."...";
                                    }else{
                                        echo substr($ne_content,0,72)."...";
                                    }
                                ?>
                            </div>
                        </div>
                        <a class="btn btn-more pull-right" href="<?php echo base_url('events'); ?>"><b>
                            <?php echo $this->lang->line("btn_readmore"); ?>
                        </b></a>
                        <?php break;} ?>
                    </article>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 footer-border">
                        <div class="col-xs-6">
                            <h4 class="bold">
                                <?php echo $this->lang->line("footer_attractions"); ?>
                            </h4>
                            <nav>
                                <a href="<?php echo base_url(); ?>rides">
                                    <?php echo $this->lang->line("nav_rides"); ?>
                                </a><br>
                                <a href="<?php echo base_url(); ?>gokart">
                                    <?php echo $this->lang->line("nav_gokart"); ?>
                                </a><br>
                                <a href="<?php echo base_url(); ?>aquasplash">
                                    <?php echo $this->lang->line("nav_aquasplash"); ?>
                                </a>
                            </nav>
                        </div>
                        <div class="col-xs-6">
                            <h4 class="bold">
                                <?php echo $this->lang->line("footer_shops"); ?>
                            </h4>
                            <nav>
                                <a href="<?php echo base_url(); ?>restaurants">
                                    <?php echo $this->lang->line("nav_restaurants"); ?>
                                </a><br>
                                <a href="<?php echo base_url(); ?>souvenirs">
                                    <?php echo $this->lang->line("nav_souvenirs"); ?>
                                </a><br>
                                <a href="<?php echo base_url(); ?>kiosks">
                                    <?php echo $this->lang->line("nav_kiosks"); ?>
                                </a>
                            </nav>
                        </div>
                        <div class="col-xs-6">
                            <h4 class="bold">
                                <?php echo $this->lang->line("footer_info"); ?>
                            </h4>
                            <nav>
                                <a href="<?php echo base_url(); ?>contact_us">
                                    <?php echo $this->lang->line("nav_contact"); ?>
                                </a><br>
                                <a href="<?php echo base_url(); ?>find_us">
                                    <?php echo $this->lang->line("nav_find_us"); ?>
                                </a><br>
                                <a target="_blank" href="<?php echo base_url('assets/upload/find_us').'/'.$parkmap; ?>">
                                    <?php echo $this->lang->line("nav_parkmap"); ?>
                                </a>
                            </nav>
                        </div>
                        <div class="col-xs-6">
                            <h4 class="bold">
                                <?php echo $this->lang->line("footer_shows"); ?>
                            </h4>
                            <nav>
                                <a href="<?php echo base_url(); ?>tickets" class="btn btn-raised buy-tickets">
                                    <?php echo $this->lang->line("btn_buy_tickets"); ?>
                                </a>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center footer-border">
                        <h4 class="bold">
                            <?php echo $this->lang->line("footer_product"); ?>
                        </h4>
                        <nav>
                            <a href="http://yogyagokart.com" class="btn btn-md btn-footer-rekreasi">
                                <?php echo $this->lang->line("btn_yogya_gokart"); ?>
                            </a>
                            <a href="http://produkrekreasi.com" class="btn btn-md btn-footer-rekreasi">
                                <?php echo $this->lang->line("btn_corporate"); ?>
                            </a>
                        </nav>

                        <!-- VISITORS COUNTER -->
                        <!-- <h4 class="bold">
                            <?php 
                                echo $this->lang->line("footer_visitors");

                                $visitors = $this->db->query("SELECT * FROM visitors");
                                if($visitors->num_rows() > 499){
                                    echo $visitors->num_rows();
                                }
                                else{
                                    echo "";
                                }
                            ?>
                        </h4> -->
                                

                        <h4 class="bold follow">
                            <?php echo $this->lang->line("footer_follow"); ?>
                        </h4>
                        
                        <nav>
                            <a href="<?php echo $trip_advisor;?>" class="socmed-footer" target="_blank">
                                <img alt="tripadvisor" src="<?php echo base_url('assets/icon/icon_tripadvisor_hover.svg'); ?>">
                            </a>  
                            <a href="<?php echo $facebook;?>" class="socmed-footer" target="_blank">
                                <img alt="facebook" src="<?php echo base_url('assets/icon/icon_facebook_hover.svg'); ?>">
                            </a>
                             <a href="<?php echo $twitter;?>" class="socmed-footer" target="_blank">
                                <img alt="twitter" src="<?php echo base_url('assets/icon/icon_twitter_hover.svg'); ?>">
                             </a>
                             <a href="<?php echo $instagram;?>" class="socmed-footer" target="_blank">
                                <img alt="instagram" src="<?php echo base_url('assets/icon/icon_instagram_hover.svg'); ?>">
                             </a>
                             <a href="<?php echo $youtube;?>" class="socmed-footer" target="_blank">
                                <img alt="youtube" src="<?php echo base_url('assets/icon/icon_youtube_hover.svg'); ?>">
                             </a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="row footer-bottom-logo">
            <div class="col-xs-10 col-xs-offset-1 col-sm-offset-3">
                <div class="col-xs-10 col-sm-1"> 
                    <img class="img-responsive" alt="maybank" src="<?php echo base_url('assets/img/logo maybank.png'); ?>">
                </div>
                <div class="col-xs-10 col-sm-1"> 
                    <img class="img-responsive" alt="mastercard" src="<?php echo base_url('assets/img/MasterCard logo.jpg'); ?>">
                </div>
                <div class="col-xs-10 col-sm-1"> 
                    <img class="img-responsive" alt="mastercard-secure" src="<?php echo base_url('assets/img/mastercard-securecode.png'); ?>">
                </div>
                <div class="col-xs-10 col-sm-1"> 
                    <img class="img-responsive" alt="visa" src="<?php echo base_url('assets/img/Visa Logo.jpg'); ?>">
                </div>
                <div class="col-xs-10 col-sm-1"> 
                    <img class="img-responsive" alt="visa-verified" src="<?php echo base_url('assets/img/Verified-by-VISA-Logo.jpg'); ?>">
                </div>
                <div class="col-xs-10 col-sm-1"> 
                    <img class="img-responsive" alt="jcb" src="<?php echo base_url('assets/img/jcb.png'); ?>">
                </div>
                <div class="col-xs-10 col-sm-1"> 
                    <img class="img-responsive" alt="jcb-secure" src="<?php echo base_url('assets/img/JsecureColorTif.png'); ?>">
                </div>
            </div>
        </div>
        
        <div class="row footer-bottom">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="footer-left">
                    <p class="copyright text-muted">&copy; Kids Fun 2016 - all right reserved</p>
                </div>
                <div class="footer-right">
                    <p class="copyright text-muted">A concept by <a href="http://ptcerise.com/en/about-us.html"><img alt="Cerise" src="<?php echo base_url('assets/icon/logo-cerise.svg');?>"></a></p>
                </div> 
            </div>
        </div>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.2.1/jssocials.min.js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/clean.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.validation.min.js')?>"></script>

    <!-- Toastr Notification -->
    <script src="<?php echo base_url('assets/js/toastr.min.js') ?>"></script>

    <!-- TO USE BASE_URL IN JS FILE -->
    <script type="text/javascript">
        var js_base_url = function( urlText ){
        var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
        return urlTmp;
        }
    </script>

    <!-- Datepicker Js -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/js/bootstrap-datetimepicker.min.js"></script>

    <script src="<?php echo base_url('assets/js/scripts.js')?>"></script>
    <script type="text/javascript">
        $('#contact_form').validate();
    </script>
    <!-- <script type="text/javascript">
        window.onload = function () {
          var form = document.getElementById('contact_form');
          form.button.onclick = function (){
            for(var i=0; i < form.elements.length; i++){
              if(form.elements[i].value === ''){
                alert('There are some required fields!');
                return false;
              }
            }
            form.submit();
          }; 
        };
    </script> -->
    <!-- <script type="text/javascript">
    $(".practical-info").css("cursor", "pointer").click(function(){
        $(this).find("button.modal-detail").click();
    });
</script> -->

<script type="text/javascript">
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1064645813622300',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    $("#share-btns").jsSocials({
        url: js_base_url('rides'),
        text: "",
        showLabel: false,
        showCount: false,
        shareIn: "popup",
        shares: ["twitter", "facebook", "googleplus"]
    });
</script>

<script type="text/javascript">
    var lang = <?php echo $this->lang->line('title_pay'); ?>;
    var btnTitle = <?php echo json_encode($lang); ?>;
</script>

<!-- MARKUP KIDSFUN -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "name" : "kidsfun.co.id",
  "url": "https://kidsfun.co.id/"
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "name" : "tickets & package",
  "url": "https://kidsfun.co.id/tickets"
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name" : "kidsfun.co.id",
  "url" : "https://kidsfun.co.id/",
  "logo" : "https://kidsfun.co.id/assets/icon/logo_kidsfun.svg",
  "contactPoint" : [
    { 
      "@type" : "ContactPoint",
      "telephone" : "+62274-435-34-35",
      "contactType" : "customer service"
    } ]
}
</script>


</body>
</html>
