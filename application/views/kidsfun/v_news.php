<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/news/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/news/'.$banner_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/news/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                alt="banner-kidsfun">
        </picture>

        <header class="intro-banner-content text-center">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1 class="display"><?php echo $this->lang->line("banner_news"); ?></h1>
                    <p class="lead">
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($banner_caption->general_content_en) ? $banner_caption->general_content_en : "";
                            }else{
                                echo !empty($banner_caption->general_content_in) ? $banner_caption->general_content_in : "";
                            }
                        ?>
                    </p>
                    <a href="#main">
                        <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                    </a>
                </div>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container-fluid content">
    
    <!-- DESCRIPTION -->
    <header class="main-header description text-center col-xs-12 col-sm-6 col-sm-offset-3">
        <div class="par lead">
            <?php
                if($lang == "english" || $lang == ""){
                    echo !empty($content->general_content_en) ? $content->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($content->general_content_in) ? $content->general_content_in : "[ KOSONG! ]";
                }
            ?>
        </div>
    </header>

    <!-- NEWS -->
    <div class="row content cards">       
        <?php foreach($news_cards as $card){ ?>
            <?php
                #CUSTOM URL
                if($lang == "english" || $lang == ""){
                    $news_title = strtolower($card->news_title_en);
                }else{
                    $news_title = strtolower($card->news_title_in);
                }
                $url_single = $card->news_id."-".url_title($news_title)."-".date('d-m-Y',strtotime($card->news_date)) ;
            ?>
        
            <article class="col-xs-12 col-md-8 col-md-offset-2 padding-clear news-cards">
                <div class="panel panel-default">
                    <div class="row link-cards">
                        <div class="card-id"><?php echo $url_single; ?></div>
                        <figure class="col-xs-12 col-sm-4">
                            <?php $card_img = $access_db->readtable('media','media_url',array('media_page'=>'news', 'media_section'=>'image_content','temp_id'=>$card->news_id))->row(); ?>
                            <img alt="kidsfun-news" class="img-responsive"
                                src="<?php echo !empty($card_img->media_url) ? base_url('assets/upload/news/thumbnail/'.$card_img->media_url) : base_url('assets/img/blank_img.png'); ?>" />
                        </figure>

                        <div class="col-xs-12 col-sm-8">
                        
                            <!-- News Content -->
                            <div class="panel-body panel-news">
                                <div class="col-xs-12">
                                    <header>
                                        <h3 class="bold">
                                            <?php
                                                if($lang == "english" || $lang == ""){
                                                    echo !empty($card->news_title_en)? $card->news_title_en : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($card->news_title_in)? $card->news_title_in : "[ KOSONG! ]";
                                                }
                                            ?>
                                        </h3>
                                        <p>
                                            <?php
                                                echo !empty($card->news_date) ? date(' d/m/y',strtotime($card->news_date)) : date('d/m/y');
                                            ?>
                                        </p>
                                    </header>
                                    <?php
                                        if($lang == "english" || $lang == ""){
                                            $content = !empty($card->news_content_en)? $card->news_content_en : '[ EMPTY! ]';
                                        }else{
                                            $content = !empty($card->news_content_in)? $card->news_content_in : '[ KOSONG! ]';
                                        }
                                    ?>
                                    <div class="par visible-lg">
                                        <?php

                                                echo substr($content, 0, 140);                                            
                                        ?> ...
                                    </div>
                                    <div class="par visible-md visible-sm visible-xs"><?php echo substr($content, 0, 120); ?> ...</div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="panel-body card-btn-more">
                                    <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>news/read/<?php echo $url_single; ?>">
                                        <?php echo $this->lang->line("btn_readmore"); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        <?php } ?>
    </div>
    
    <!-- BTN -->
    <aside class="text-center">
        <button id="showmore" class="btn btn-bg-red btn-raised">
            <?php echo $this->lang->line("btn_showmore"); ?>
        </button>
    </aside>

</div>