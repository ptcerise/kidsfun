<section id="main" class="container-fluid no-banner">

    <header class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
        <h1 class="display"><?php echo $this->lang->line("title_shopping_cart"); ?></h1>
        <div class="par lead">
            <?php if($lang == "english" || $lang == ""){
                        echo !empty($content->general_content_en) ? $content->general_content_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($content->general_content_in) ? $content->general_content_in : "[ KOSONG! ]";
                    } ?>
        </div>
    </header>
    <?php if(count($cart_tickets) != 0) { ?>
    <div class="container content">
        <div class="row">
            <div class="col-xs-12 padding-clear">
                <div class="panel panel-default table-responsive">
                    
                    <table class="table table-striped table-hover">
                    <?php foreach (array_values($_SESSION['cart_tickets']) as $key => $val) { ?>
                        <tr class="item">
                            <td class="cart-col-1"><?php echo $val['tickets_name']; ?></td>
                            <td class="cart-col-2">Rp. <span class="price"><?php echo number_format($val['tickets_price'],0,',','.'); ?></span></td>
                            <td class="cart-col-3">
                                <button type="button" class="btn-minus btn btn-custom btn-default" disabled><i class="fa fa-minus"></i></button>
                                <input type="text" name="ticket" value="<?php echo $val['chart_qty']; ?>" placeholder="" class="value cart">
                                <button type="button" class="btn-plus btn btn-custom btn-default"><i class="fa fa-plus"></i></button>
                            </td>
                            <td class="cart-col-4">Rp. <span class="subtotal"><?php echo number_format($val['total_price'],0,',','.'); ?></span></td>
                            <td class="cart-col-5"><a class="btn btn-sm btn-more" onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('shopping_cart/delete_cart/'.@$key);?>">
                                <?php echo $this->lang->line("btn_remove"); ?>
                            </a></td>
                        </tr>
                    <?php } ?>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-6 padding-clear">
                <div class="panel panel-default text-center">
                    <div class="panel-body total-cart">
                    <?php $total_price = array_sum(array_column($cart_tickets,'total_price')); ?>
                        <div class="col-xs-12 col-sm-12 col-md-3">
                            <p>TOTAL</p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 total">
                            <p>Rp. <span id="total"><?php echo number_format($total_price,0,',','.'); ?></span></p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 checkout-cart">
                            <button class="btn btn-bg-red btn-raised" onclick="window.location='<?php echo base_url(); ?>checkout'">
                                <?php echo $this->lang->line("btn_checkout"); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>