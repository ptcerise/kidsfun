<section class="video-banner">
    <!-- VIDEO ON BACKGROUND -->
    <video class="vid-background"
           id="vid-background"
           muted=""
           autoplay=""
           poster="<?php echo !empty($video_banner->media_url) ? base_url('assets/upload/home/'.$video_banner->media_url) : base_url('assets/img/blank_large.png');?>">

        <source id="vid-source"
                src="<?php echo !empty($video->media_url) ? base_url('assets/upload/video/'.$video->media_url) : base_url('assets/upload/video/default.mp4');?>"
                type="video/<?php echo !empty($video_format) ? $video_format : "mp4"; ?>"
        >
    </video>

    <header class="video-title text-center">
        <h1 class="display">
            <?php
                if($lang == "english" || $lang == ""){
                    echo !empty($title->general_title_en) ? $title->general_title_en : "[ EMPTY! ]" ;
                }else{
                    echo !empty($title->general_title_in) ? $title->general_title_in : "[ KOSONG! ]" ;;
                }
            ?>
        </h1>
        <p class="lead">
            <?php
                if($lang == "english" || $lang == ""){
                    echo !empty($video_content->general_content_en) ? $video_content->general_content_en : "";
                }else{
                    echo !empty($video_content->general_content_in) ? $video_content->general_content_in : "";
                }
            ?>
        </p>
            <a href="#popup" class="btn-play" onclick="play_video()"><img class="icon-play" src="<?php echo base_url('assets/icon/playbutton.svg');?>" alt="play-icon"></a>
    </header>

    <!-- VIDEO POP UP -->
    <div id="popup" class="popup">
        <div class="window container-fluid">
            <div class="col-xs-12">
                <button id="close-vid-btn" onclick="stop_video();document.location.href='#'" class="btn red btn-raised btn-sm" title="Close">
                    <i class="fa fa-times-circle close-vid" aria-hidden="true"></i> Close
                </button>
            </div>

            <div class="col-xs-12">
                <video id="vid-popup" class="vid-popup" controls>
                    <!-- PLACE FOR VIDEO POPUP, THE VIDEO WILL BE MOVED HERE, LEAVE THIS LINE EMPTY -->
                </video>
            </div> 
        </div>           
    </div>
</section>

<div class="bg-grey">
    <div class="video-bottom container-fluid">
        <div class="row">
            <div class="panel panel-default video-div">
                <div class="panel-body">
                    <div class="bottom-caption"><span>
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($div1->general_content_en) ? $div1->general_content_en : "[ EMPTY! ]";
                            }else{
                                echo !empty($div1->general_content_in) ? $div1->general_content_in : "[ KOSONG! ]";
                            }
                        ?>
                    </span>
                    <a href="<?php echo base_url(); ?>tickets" class="btn btn-bg-red btn-raised">
                        <?php echo $this->lang->line("btn_choose_package"); ?>
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>   

    <section class="container">
        <div class="row">
            <header class="col-xs-12 col-sm-8 col-sm-offset-2 main-header text-center">
                <h2 class="display">
                    <?php echo $this->lang->line("title_the_rides"); ?>
                </h2>
                <div class="par lead">
                    <?php
                        if($lang == "english" || $lang == ""){
                            echo !empty($rides_content->general_content_en) ? $rides_content->general_content_en : "[ EMPTY! ]";
                        }else{
                            echo !empty($rides_content->general_content_in) ? $rides_content->general_content_in : "[ KOSONG! ]";
                        }
                    ?>
                </div>
            </header>
        </div>
        <div class="row ride-home-cards">
            <?php $i=0; ?>
            <?php $card_title = array("kids","family","thrill"); ?>
            <?php $title_count = count($card_title);?>
            <?php foreach($ride_cat_content as $key=>$row){ ?>
            <?php if($i == $title_count){break;} ?>
            <article class="col-xs-12 col-sm-12 col-md-4 col-lg-4 padding-clear">
            
                <div class="panel panel-default">
                    <header class="panel-body card-header">
                        <div class="row">
                            <div class="col-xs-10 text-left">
                                <h3 class="display card-ride">
                                    <?php echo $this->lang->line("title_".$card_title[$i]."_ride"); ?>
                                </h3>
                            </div>
                            <div class="col-xs-2 text-right">
                                <img alt="kidsfun-icon" class="rides-icon" src="<?php echo base_url('assets/icon/icon_'.$card_title[$i].'_rides.png');?>">
                            </div>
                        </div>
                    </header>
                    <figure>
                        <?php
                            $id_rides = $db_access->db->query("SELECT * FROM rides WHERE rides_category='".$card_title[$i]."' LIMIT 1")->row();
                            $media_url = $db_access->readtable('media', '',array('media_page'=>'rides', 'media_section'=>'banner_single_large','temp_id'=>$id_rides->rides_id))->row();
                        ?>
                        <img alt="kidsfun-rides" src="<?php echo isset($media_url->media_url)?base_url('assets/upload/attractions/rides_single/'.$media_url->media_url):base_url('assets/img/blank_large.png'); ?>"  class="img-responsive img-home-rides">
                    </figure>
                    <div class="panel-body">
                        <div class="par card-text rides-cat-content">
                            <?php
                                if($lang == "english" || $lang == ""){
                                    echo isset($row->general_content_en)? $row->general_content_en : "[ EMPTY! ]";
                                }else{
                                    echo isset($row->general_content_in)? $row->general_content_in : "[ KOSONG! ]";
                                }
                            ?>
                        </div>
                    </div>
                    <div class="panel-body card-btn-more">
                        <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>rides">
                            <?php echo $this->lang->line("btn_discover"); ?>
                        </a>
                    </div>
                </div>
            
            </article>
            <?php $i++; } ?>
        </div>
    </section>

    <div class="col-xs-12 text-center divider">
        <span>
           <?php
                if($lang == "english" || $lang == ""){
                    echo !empty($div2->general_content_en) ? $div2->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($div2->general_content_in) ? $div2->general_content_in : "[ KOSONG! ]";
                }
            ?>
        </span>
        <a href="http://yogyagokart.com/" target="_blank" class="btn btn-bg-red btn-raised">
            <?php echo $this->lang->line("btn_lets_race"); ?>
        </a>
    </div>

    <div class="panel-home bg-white home">
        <article class="banner">
            <div class="banner-inner">
                <figure>
                    <picture>
                        <source 
                            media="(min-width: 768px)"
                            srcset="<?php echo !empty($banner1_large->media_url) ? base_url('assets/upload/home/'.$banner1_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
                        <source 
                            media="(max-width: 768px)"
                            srcset="<?php echo !empty($banner1_small->media_url) ? base_url('assets/upload/home/'.$banner1_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
                        <img class="img-responsive"
                            src="<?php echo !empty($banner1_large->media_url) ? base_url('assets/upload/home/'.$banner1_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                            alt="kidsfun-aquasplash">
                    </picture>
                </figure>

                <div class="home-banner-content">
                    <div class="col-xs-12 col-sm-6">
                        <header>
                            <h2 class="display"><?php echo $this->lang->line("title_aquasplash"); ?></h2>
                        </header>
                        <div class="par lead">
                            <?php
                                if($lang == "english" || $lang == ""){
                                    echo !empty($banner1_caption->general_content_en) ? $banner1_caption->general_content_en : "[ EMPTY! ]";
                                }else{
                                    echo !empty($banner1_caption->general_content_in) ? $banner1_caption->general_content_in : "[ KOSONG! ]";
                                }
                            ?>
                        </div>
                        <div class="ghost-btn">
                            <a href="<?php echo base_url(); ?>aquasplash" class="btn btn-lg">
                                <?php echo $this->lang->line("btn_explore"); ?>
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
    
    <div class="col-xs-12 text-center divider">
        <span>
           <?php
                if($lang == "english" || $lang == ""){
                    echo !empty($div3->general_content_en) ? $div3->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($div3->general_content_in) ? $div3->general_content_in : "[ KOSONG! ]";
                }
            ?>
        </span>
        <a href="http://produkrekreasi.com/" target="_blank" class="btn btn-bg-red btn-raised">
            <?php echo $this->lang->line("btn_corporate_website"); ?>
        </a> 
    </div>

    <div class="bg-white home">
        <article class="banner">
            <div class="banner-inner">
                <figure>
                    <picture>
                        <source 
                            media="(min-width: 768px)"
                            srcset="<?php echo !empty($banner2_large->media_url) ? base_url('assets/upload/home/'.$banner2_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
                        <source 
                            media="(max-width: 768px)"
                            srcset="<?php echo !empty($banner2_small->media_url) ? base_url('assets/upload/home/'.$banner2_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
                        <img class="img-responsive"
                            src="<?php echo !empty($banner2_large->media_url) ? base_url('assets/upload/home/'.$banner2_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                            alt="kidsfun-aquasplash">
                    </picture>
                </figure>
            
                <div class="home-banner-content">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                        <header>
                            <h2 class="display"><?php echo $this->lang->line("title_our_restaurants"); ?></h2>
                        </header>
                        <div class="par lead">
                            <?php
                                if($lang == "english" || $lang == ""){
                                    echo !empty($banner2_caption->general_content_en) ? $banner2_caption->general_content_en : "[ EMPTY! ]";
                                }else{
                                    echo !empty($banner2_caption->general_content_in) ? $banner2_caption->general_content_in : "[ KOSONG! ]";
                                }
                            ?>
                        </div>
                        <div class="ghost-btn">
                            <a href="<?php echo base_url(); ?>restaurants" class="btn btn-lg">
                                <?php echo $this->lang->line("btn_explore"); ?>
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>

    <div class="container home info-cards-home">
        <div class="row">
            <?php
                $i=0;
                $info_title = array('title_opening_times','title_minimum_age','title_what_to_bring','title_accessibility');
            ?>
            <?php foreach($announce as $info){ ?>
            <article class="col-xs-12 col-sm-6 col-md-6 col-lg-3 padding-clear">
                <a href="<?php echo base_url(); ?>practical_info">
                    <div class="panel panel-default info-home">
                        <div class="panel-body">
                            <div class="row">
                                <header class="col-xs-12 text-left">
                                    <h4 class="display card-info">
                                       <?php echo $this->lang->line($info_title[$i]); ?>
                                    </h4>
                                </header>
                            </div>
                            <div class="par home-card-info">
                                <?php
                                    if($lang == "english" || $lang == ""){
                                        echo !empty($info->general_content_en) ? $info->general_content_en : "[ EMPTY! ]";
                                    }else{
                                        echo !empty($info->general_content_in) ? $info->general_content_in : "[ KOSONG! ]";
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </a>
            </article>
            <?php $i++;}?>
        </div>
    </div>

    <div class="home">
        <article class="banner">
          <div class="banner-inner">
            <figure>
                <picture>
                    <source 
                        media="(min-width: 768px)"
                        srcset="<?php echo !empty($banner3_large->media_url) ? base_url('assets/upload/home/'.$banner3_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
                    <source 
                        media="(max-width: 768px)"
                        srcset="<?php echo !empty($banner3_small->media_url) ? base_url('assets/upload/home/'.$banner3_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
                    <img class="img-responsive"
                        src="<?php echo !empty($banner3_large->media_url) ? base_url('assets/upload/home/'.$banner3_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                        alt="kidsfun-aquasplash">
                </picture>
            </figure>

            <div class="home-banner-content">
                <div class="col-xs-12 col-sm-6">
                    <header>
                        <h2 class="display"><?php echo $this->lang->line("title_our_shows"); ?></h2>
                    </header>
                    <div class="par lead">
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($banner3_caption->general_content_en) ? $banner3_caption->general_content_en : "[ EMPTY! ]";
                            }else{
                                echo !empty($banner3_caption->general_content_in) ? $banner3_caption->general_content_in : "[ KOSONG! ]";
                            }
                        ?>
                    </div>
                    <div class="ghost-btn">
                        <a href="<?php echo base_url(); ?>shows" class="btn btn-lg">
                            <?php echo $this->lang->line("btn_explore"); ?>
                        </a> 
                    </div>
                </div>
            </div>
          </div>
        </article>
    </div>
</div>