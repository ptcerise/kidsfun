<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/rides_single/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/attractions/rides_single/'.$banner_small->media_url) : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/rides_single/'.$banner_large->media_url) : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $rides_single->rides_name; ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($banner_large->media_content_en) ? $banner_large->media_content_en:"";
                    }else{
                        echo !empty($banner_large->media_content_in) ? $banner_large->media_content_in:"";
                    } ?>     
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<section id="main" class="container">

    <header class="row text-center main-header">
        <div class="col-xs-10 col-xs-offset-1">
            <h2><?php echo ucwords($rides_single->rides_category); ?></h2>
        </div>
    </header>

    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 ">
            <?php foreach ($rides_image as $row) { ?>
            <figure class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <img class="img-thumbnail img-responsive" src="<?php echo !empty($row->img_url) ? base_url('assets/upload/attractions/rides_single/'.$row->img_url) : base_url('assets/img/blank_img.png'); ?>" alt="kidsfun-rides">
            </figure>
            <?php } ?>
        </div>
    </div> 
    
    <article>
        <div class="row content">
            <div class="col-xs-10 col-xs-offset-1 desription text-center">
                <div class="par">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($rides_single->rides_desc_en) ? $rides_single->rides_desc_en:"[EMPTY!]";
                    }else{
                        echo !empty($rides_single->rides_desc_in) ? $rides_single->rides_desc_in:"[KOSONG!]";
                    } ?>
                </div><br>
            </div>
        </div>
    </article>
    
        <!-- SHARE *SHOWS ON SHARE BTN PRESSED -->
        <div id="share" class="modal fade">
          <div class="modal-dialog modal-share">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <?php
                        $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        $share_title = $rides_single->rides_name;
                        if($lang == "english" || $lang == ""){
                            $share_text = $rides_single->rides_name." on Kidsfun Yogyakarta. Click ";
                        }else{
                            $share_text = $rides_single->rides_name." di Kidsfun Yogyakarta. Klik ";
                        }
                    ?>
                    <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                    </a>
                    <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                    </a>
                    <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                        <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                    </a>
                </div>
                <div class="panel-body modal-close">
                    <a class="btn btn-md btn-more pull-right" data-dismiss="modal">
                        <?php echo $this->lang->line("btn_close"); ?>
                    </a>
                </div> 
            </div>
          </div>
        </div>

    <aside class="row text-center">
        <button class="btn btn-raised share" data-toggle="modal" data-target="#share">
            <?php echo $this->lang->line("btn_share"); ?>
        </button>
        <button class="btn btn-bg-red btn-raised" onclick="window.location='<?php echo base_url(); ?>tickets'">
            <?php echo $this->lang->line("btn_buy_tickets"); ?>
        </button>
    </aside>  

    <!-- MAP -->
    <div class="row content">
        <figure class="col-xs-12 col-sm-8 col-sm-offset-2">
            <img class="img-thumbnail img-responsive" src="<?php echo !empty($rides_map->media_url) ? base_url('assets/upload/attractions/rides_single/'.$rides_map->media_url) : base_url('assets/img/blank_img.png'); ?>" alt="kidsfun-rides">
        </figure>
    </div>
    
    <section class="row">
        <header class="text-center">
            <h3 class="display"><?php echo $this->lang->line("title_what_to_do"); ?></h3>
        </header>  

        <div class="row content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 padding-clear">
                <?php foreach ($rides_card as $key => $row) { ?>
                <?php
                    #CUSTOM URL
                    $rides_name = strtolower($row->rides_name);
                    $url_single = $row->rides_id."-".url_title($rides_name) ;
                ?>
                <article class="col-xs-12 col-sm-6 padding-clear">
                    <div class="panel panel-default">
                        <div class="link-cards">
                            <div class="card-id"><?php echo $url_single; ?></div>
                            <figure>
                                <img src="<?php echo !empty($row->img_url) ? base_url('assets/upload/attractions/rides_single/thumbnail/'.$row->img_url) : base_url('assets/img/blank_img.png'); ?>" class="img-responsive" alt="icon"/>
                            </figure>
                            <header class="panel-body card-display">
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <h4 class="display">
                                            <?php echo $row->rides_name; ?>
                                        </h4>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <?php if($row->rides_category == "thrill"){ ?>
                                            <img class="rides-icon" src="<?php echo base_url('assets/icon/icon_thrill_rides.png');?>" alt="rides">
                                        <?php }else{
                                            if($row->rides_category == "family"){ ?>
                                                <img class="rides-icon" src="<?php echo base_url('assets/icon/icon_family_rides.png');?>" alt="rides">
                                            <?php }else{ ?>
                                                    <img class="rides-icon" src="<?php echo base_url('assets/icon/icon_kids_rides.png');?>" alt="rides">
                                            <?php }
                                        }?>
                                    </div>
                                </div>
                            </header>
                        </div>
                        <div class="panel-body card-btn-more text-right">
                            <a class="btn btn-more btn-share" onclick="showShare('share<?php echo $key; ?>')">
                                <?php echo $this->lang->line("btn_share"); ?>
                            </a>
                            <a class="btn btn-more" href="<?php echo base_url(); ?>rides/detail/<?php echo $url_single; ?>">
                                <?php echo $this->lang->line("btn_discover"); ?>
                            </a>
                        </div>
                        
                        <!-- SHARE -->

                        <div class="panel panel-default card-share" id="share<?php echo $key; ?>">
                            <div class="panel-body text-center social-share-btn">
                                <?php
                                    $share_link = base_url('rides/detail/'.$url_single);
                                    $share_title = $row->rides_name;
                                    if($lang == "english" || $lang == ""){
                                        $share_text = $row->rides_name." on Kidsfun Yogyakarta. Click ";
                                    }else{
                                        $share_text = $row->rides_name." di Kidsfun Yogyakarta. Klik ";
                                    }
                                ?>
                                <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                </a>
                                <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                </a>
                                <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                </a>
                            </div>
                            <br>
                            <div class="panel-body card-btn-more">
                                <a class="btn btn-more pull-right" onclick="closeShare('share<?php echo $key; ?>')">
                                    <?php echo $this->lang->line("btn_close"); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </article>
                <?php } ?>
            </div>
        </div>
    </section>
</section>