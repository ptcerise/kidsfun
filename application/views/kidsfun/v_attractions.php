<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <!-- Wrapper for slides -->
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <div class="intro-banner-content">
            <header class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_attractions"); ?></h1>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed d, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed d, Lorem ipsum dolor sit amet.</p>
                    <a href="#main">
                        <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                    </a>
            </header>
        </div>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="bg-white container-fluid content">
    <div class="container">
        <header class="row main-header">
            <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 text-center">
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
        </header>
    </div>

    <div class="container padding-clear">
    <div class="row content">         
        <article class="col-xs-12 col-sm-4 padding-clear">
            <div class="panel panel-default">
                <header class="panel-body card-display">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <h3 class="display">
                                Rides
                            </h3>
                        </div>
                    </div>
                </header>
                <figure>
                    <img src="<?php echo base_url('assets/img/16x9.jpg'); ?>"  class="img-responsive" alt="kidsfun-rides">
                </figure>
                <div class="panel-body">
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
                <div class="panel-body card-btn-more">
                    <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>kidsfun/rides">
                        <?php echo $this->lang->line("btn_learn"); ?>
                    </a>
                </div>
            </div>
        </article>

        <article class="col-xs-12 col-sm-4 padding-clear">
            <div class="panel panel-default">
                <header class="panel-body card-display">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <h3 class="display">
                                Go Kart
                            </h3>
                        </div>
                    </div>
                </header>
                <figure>
                    <img src="<?php echo base_url('assets/img/16x9.jpg'); ?>"  class="img-responsive" alt="kidsfun-gokart">
                </figure>
                <div class="panel-body">
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
                <div class="panel-body card-btn-more">
                    <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>kidsfun/gokart">
                        <?php echo $this->lang->line("btn_learn"); ?>
                    </a>
                </div>
            </div>
        </article>

        <article class="col-xs-12 col-sm-4 padding-clear">
            <div class="panel panel-default">
                <header class="panel-body card-display">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <h3 class="display">
                                Aquasplash
                            </h3>
                        </div>
                    </div>
                </header>
                <figure>
                    <img src="<?php echo base_url('assets/img/16x9.jpg'); ?>"  class="img-responsive" alt="kidsfun-aquasplash">
                </figure>
                <div class="panel-body">
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
                <div class="panel-body card-btn-more">
                    <a class="btn btn-more pull-right" href="<?php echo base_url(); ?>kidsfun/aquasplash">
                        <?php echo $this->lang->line("btn_learn"); ?>
                    </a>
                </div>
            </div>
        </article>
    </div>
    </div>
</div>