<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <!-- Wrapper for slides -->
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/events').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/events').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/events').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content text-center">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_events"); ?></h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($banner_caption->general_content_en) ? $banner_caption->general_content_en : "";
                    }else{
                        echo !empty($banner_caption->general_content_in) ? $banner_caption->general_content_in : "";
                    } ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down">
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container content">
    <header class="main-header text-center col-xs-12 col-sm-10 col-sm-offset-1">
        <div class="par lead">
            <?php if($lang == "english" || $lang == ""){
                echo !empty($events_content->general_content_en) ? $events_content->general_content_en : "[ EMPTY! ]";
            }else{
                echo !empty($events_content->general_content_in) ? $events_content->general_content_in : "[ KOSONG! ]";
            } ?>
        </div>
    </header>

    <section>
        <!-- NEXT EVENTS -->
        
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("next_events"); ?></h2>
        </header>
        
        <!-- ACCORDION -->
        <div class="col-xs-12 padding-clear">

            <div class="panel-group padding-clear" id="next-accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <?php
                    if(count($next_events) == 0){
                        echo '
                            <div class="panel-body text-center">                           
                                <h3>
                        ';
                            if($lang == "english" || $lang == "")
                            {
                                echo "NO EVENTS";
                            }else{
                                echo "BELUM ADA EVENT";
                            }
                        echo'
                                </h3>  
                            </div> 
                        ';
                    }
                ?>
                <?php foreach($next_events as $n_event){ ?>
                <article>
                    <div class="panel-body" role="tab" id="next-heading<?php echo $n_event->events_id; ?>">
                        <div class="row">
                            <div class="panel-title">
                                <!-- EVENT'S DATE -->
                                <div class="col-xs-12 col-sm-2">
                                    <h3>
                                        <?php echo date(' d/m/y',strtotime($n_event->events_date)) ?>
                                    </h3>
                                </div>
                                <header class="col-xs-12 col-sm-7">
                                    <h3 class="event-title">
                                        <?php
                                            if($lang == "english" ||$lang == ""){
                                                echo !empty($n_event->events_title_en) ? $n_event->events_title_en : "[ EMPTY! ]";
                                            }else{
                                                echo !empty($n_event->events_title_in) ? $n_event->events_title_in : "[ KOSONG! ]";
                                            }
                                        ?>
                                    </h3>
                                </header>
                                <div class="col-xs-12 col-sm-3 text-center">
                                    <a class="btn btn-lg btn-more" role="button" data-toggle="collapse" data-parent="#accordion" href="#next-collapse<?php echo $n_event->events_id; ?>" aria-expanded="true" aria-controls="next-collapse<?php echo $n_event->events_id; ?>">
                                        <?php echo $this->lang->line("btn_readmore"); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="next-collapse<?php echo $n_event->events_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="next-heading<?php echo $n_event->events_id; ?>">
                      <div class="panel-body">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                                <div class="par">
                                    <?php
                                        if($lang == "english" || $lang == ""){
                                            echo !empty($n_event->events_content_en) ? $n_event->events_content_en : "[ EMPTY! ]";
                                        }else{
                                            echo !empty($n_event->events_content_in) ? $n_event->events_content_in : "[ KOSONG! ]";
                                        }
                                    ?>
                                </div class="par">

                                <div class="text-right">
                                    <a class="btn btn-sm btn-more share" data-toggle="modal" data-target="#share<?php echo $n_event->events_id; ?>">
                                        <?php echo $this->lang->line("btn_share"); ?>
                                    </a>
                                    <a class="btn btn-sm btn-more" role="button" data-toggle="collapse" data-parent="#accordion" href="#next-collapse<?php echo $n_event->events_id; ?>" aria-expanded="true" aria-controls="next-collapse<?php echo $n_event->events_id; ?>">
                                        <?php echo $this->lang->line("btn_close"); ?>
                                    </a>
                                </div>
                            </div>
                      </div>
                    </div>

                    <div id="share<?php echo $n_event->events_id; ?>" class="modal fade">
                      <div class="modal-dialog modal-share">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <?php
                                    $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                                    if($lang == "english" ||$lang == ""){
                                        $event_name = $n_event->events_title_en;
                                    }else{
                                        $event_name = $n_event->events_title_in;
                                    }

                                    $share_title = $event_name;
                                    if($lang == "english" || $lang == ""){
                                        $share_text = $event_name." event on Kidsfun Yogyakarta. Click ";
                                    }else{
                                        $share_text = "event ".$event_name." di Kidsfun Yogyakarta. Klik ";
                                    }
                                ?>
                                <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                </a>
                                <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                </a>
                                <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                </a>
                            </div>
                            <div class="panel-body modal-close text-right">
                                <a class="btn btn-md btn-more" data-dismiss="modal">
                                    <?php echo $this->lang->line("btn_close"); ?>
                                </a>
                            </div> 
                        </div>
                      </div>
                    </div>

                </article>
                <?php } ?>

              </div>
            </div>
        </div>
        
        <!-- SHOW MORE BTN -->
        <aside class="text-center">
            <button id="showmore1" class="btn btn-bg-red btn-raised"><?php echo $this->lang->line("btn_showmore"); ?></button>
        </aside>
    </section>
            
    <section>
        <!-- PAST-EVENTS -->
        <header class="col-xs-12">
            <h2 class="display"><?php echo $this->lang->line("past_events"); ?></h2>
        </header>
        <!-- ACCORDION -->
        <div class="col-xs-12 padding-clear">
            <div class="panel-group padding-clear" id="past-accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <?php foreach($past_events as $p_event){ ?>
                <article>
                    <div class="panel-body" role="tab" id="next-heading<?php echo $p_event->events_id; ?>">
                        <div class="row">
                            <div class="panel-title">
                                <!-- EVENT'S DATE -->
                                <div class="col-xs-12 col-sm-2">
                                    <h3 class="event-title">
                                        <?php echo date(' d/m/y',strtotime($p_event->events_date)) ?>
                                    </h3>
                                </div>
                                <header class="col-xs-12 col-sm-7">
                                    <h3>
                                        <?php
                                            if($lang == "english" ||$lang == ""){
                                                echo !empty($p_event->events_title_en) ? $p_event->events_title_en : "[ EMPTY! ]";
                                            }else{
                                                echo !empty($p_event->events_title_in) ? $p_event->events_title_in : "[ KOSONG! ]";
                                            }
                                        ?>
                                    </h3>
                                </header>
                                <div class="col-xs-12 col-sm-3 text-center">
                                    <a class="btn btn-lg btn-more" role="button" data-toggle="collapse" data-parent="#accordion" href="#next-collapse<?php echo $p_event->events_id; ?>" aria-expanded="true" aria-controls="next-collapse<?php echo $p_event->events_id; ?>">
                                        <?php echo $this->lang->line("btn_readmore"); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="next-collapse<?php echo $p_event->events_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="next-heading<?php echo $p_event->events_id; ?>">
                      <div class="panel-body">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                                <div class="par">
                                    <?php
                                        if($lang == "english" || $lang == ""){
                                            echo !empty($p_event->events_content_en) ? $p_event->events_content_en : "[ EMPTY! ]";
                                        }else{
                                            echo !empty($p_event->events_content_in) ? $p_event->events_content_in : "[ KOSONG! ]";
                                        }
                                    ?>
                                </div class="par">

                                <div class="text-right">
                                    <a class="btn btn-sm btn-more share" data-toggle="modal" data-target="#share<?php echo $p_event->events_id; ?>">
                                        <?php echo $this->lang->line("btn_share"); ?>
                                    </a>
                                    <a class="btn btn-sm btn-more" role="button" data-toggle="collapse" data-parent="#accordion" href="#next-collapse<?php echo $p_event->events_id; ?>" aria-expanded="true" aria-controls="next-collapse<?php echo $p_event->events_id; ?>">
                                        <?php echo $this->lang->line("btn_close"); ?>
                                    </a>
                                </div>
                            </div>
                      </div>
                    </div>

                    <div id="share<?php echo $p_event->events_id; ?>" class="modal fade">
                      <div class="modal-dialog modal-share">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <?php
                                    $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                                    if($lang == "english" ||$lang == ""){
                                        $event_name = $p_event->events_title_en;
                                    }else{
                                        $event_name = $p_event->events_title_in;
                                    }

                                    $share_title = $event_name;
                                    if($lang == "english" || $lang == ""){
                                        $share_text = $event_name." event on Kidsfun Yogyakarta. Click ";
                                    }else{
                                        $share_text = "event ".$event_name." di Kidsfun Yogyakarta. Klik ";
                                    }
                                ?>
                                <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                </a>
                                <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                </a>
                                <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                    <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                </a>
                            </div>
                            <div class="panel-body modal-close text-right">
                                <a class="btn btn-md btn-more" data-dismiss="modal">
                                    <?php echo $this->lang->line("btn_close"); ?>
                                </a>
                            </div> 
                        </div>
                      </div>
                    </div>

                </article>
                <?php } ?>

              </div>
            </div>
        </div>
        
        <!-- SHOW MORE BTN -->
        <aside class="text-center">
            <button id="showmore2" class="btn btn-bg-red btn-raised"><?php echo $this->lang->line("btn_showmore"); ?></button>
        </aside>

    </section>
</div>    