<!-- THE HEADER IMAGE -->
<section class="container-fluid home-banner">
    <figure class="banner">
      <!-- Wrapper for slides -->
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shops/restaurants').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/shops/restaurants').'/'.$banner_small->media_url : base_url('assets/img/blank_small.png'); ?>">
            <img class="img-responsive"
                src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shops/restaurants').'/'.$banner_large->media_url : base_url('assets/img/blank_large.png'); ?>" 
                alt="kidsfun-banner" />
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($title_en->media_title_en) ? $title_en->media_title_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($title_in->media_title_in) ? $title_in->media_title_in : "[ KOSONG! ]";
                    } ?>
                </h1>
                <p class="lead">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($caption_en->media_content_en) ? $caption_en->media_content_en : "";
                    }else{
                        echo !empty($caption_in->media_content_in) ? $caption_in->media_content_in : "";
                    } ?>
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down" />
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<section id="main" class="container">

    <header class="row main-header">
        <div class="text-center col-xs-10 col-xs-offset-1">
            <h2>
                <?php echo ucfirst($resto_type->shop_type); ?>
            </h2>
        </div>
    </header>
    
    <!-- TWO IMAGES -->
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <?php foreach($image_content as $img_content){ ?>
                <figure class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <img class="img-thumbnail img-responsive"
                         src="<?php echo !empty($img_content->media_url) ? base_url('assets/upload/shops/restaurants/'.$img_content->media_url) : base_url('assets/img/blank_img.png');?>"
                         alt="<?php echo $img_content->media_url; ?>" />
                </figure>
            <?php } ?>
        </div>
    </div>
    
    <article>
        <div class="row content">
            <div class="col-xs-10 col-xs-offset-1 description">
                <div class="par lead text-center">
                <?php if($lang == "english" || $lang == ""){
                    echo !empty($content_en->shop_content_en) ? $content_en->shop_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($content_in->shop_content_in) ? $content_in->shop_content_in : "[ KOSONG! ]";
                } ?>
                </div>
            </div>
        </div>
    </article>
    
    <!-- SHARE *SHOWS ON SHARE BTN PRESSED -->
    <div id="share" class="modal fade">
      <div class="modal-dialog modal-share">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <?php
                    $share_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                    if($lang == "english" || $lang == ""){
                        $resto_name = $title_en->media_title_en;
                    }else{
                       $resto_name = $title_in->media_title_in;
                    }

                    $share_title = $resto_name;
                    if($lang == "english" || $lang == ""){
                        $share_text = $resto_name." on Kidsfun Yogyakarta. Click ";
                    }else{
                        $share_text = $resto_name." di Kidsfun Yogyakarta. Klik ";
                    }
                ?>
                <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                    <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                </a>
                <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                    <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                </a>
                <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                    <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                </a>
            </div>
            <div class="panel-body modal-close">
                <a class="btn btn-md btn-more pull-right" data-dismiss="modal">
                    <?php echo $this->lang->line("btn_close"); ?>
                </a>
            </div> 
        </div>
      </div>
    </div>

    <aside class="text-center content">
        <button class="btn btn-raised share" data-toggle="modal" data-target="#share">
            <?php echo $this->lang->line("btn_share"); ?>
        </button>
        <button class="btn btn-bg-red btn-raised" onclick="window.location='<?php echo base_url(); ?>tickets'">
            <?php echo $this->lang->line("btn_buy_tickets"); ?>
        </button>
    </aside> 
    
    <!-- MAPS -->
    <div class="row content">
        <figure class="col-xs-12 col-sm-8 col-sm-offset-2">
            <img class="img-thumbnail img-responsive" src="<?php echo base_url('assets/upload/shops/restaurants/'.$image_map->media_url);?>" alt="kidsfun-map" />
        </figure>
    </div>

    <section class="row">
        <header class="text-center">
            <h3 class="display"><?php echo $this->lang->line("title_where_to_eat"); ?></h3>
        </header>
        
        <div class="row content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 padding-clear">
                <?php foreach($cards as $row){ ?>
                    <?php $i = $row->shop_id; ?>
                    <?php
                        #CUSTOM URL
                        if($lang == "english" || $lang == ""){
                            $shop_name = strtolower($row->shop_title_en);
                        }else{
                            $shop_name = strtolower($row->shop_title_in);
                        }
                        $url_single = $row->shop_id."-".url_title($shop_name) ;
                    ?>
                    <article class="col-xs-12 col-sm-6 padding-clear resto-cards">
                        
                        <div class="panel panel-default">
                            <div class="link-cards">
                                <div class="card-id"><?php echo $url_single; ?></div>
                                <figure>
                                    <?php
                                        $card_img = $model->readtable('media','media_url',array('media_page'=>'restaurant', 'media_section'=>'image_content','temp_id'=>$i))->row();
                                    ?>
                                    <img src="<?php echo base_url('assets/upload/shops/restaurants/thumbnail/'.$card_img->media_url); ?>"   class="img-responsive" alt="kidsfun-restaurant" />
                                </figure>
                                <header class="panel-body card-display">
                                    <div class="row">
                                        <div class="col-xs-12 text-left">
                                            <h4 class="display">
                                                <?php
                                                    $card_title = $model->readtable('media','',array('temp_id'=>$i,'media_page'=>'restaurant','media_section'=>'banner_single_large'))->result();
                                                    foreach($card_title as $row){
                                                        if($lang == "english" || $lang == ""){
                                                            echo !empty($row->media_title_en) ? $row->media_title_en : "[ EMPTY! ]";
                                                        }else{
                                                            echo !empty($row->media_title_in) ? $row->media_title_in : "[ KOSONG! ]";
                                                        }
                                                        break;
                                                    }
                                                ?>
                                            </h4>
                                        </div>
                                    </div>
                                </header>
                                <div class="panel-body card-resto-content">
                                    <p>
                                        <?php
                                            $card_title = $model->readtable('shop','',array('shop_id'=>$i,'shop_type'=>'restaurant'))->result();
                                            foreach($card_title as $row){
                                                if($lang == "english" || $lang == ""){
                                                    echo !empty($row->shop_content_en) ? substr($row->shop_content_en, 0, 20)." ..." : "[ EMPTY! ]";
                                                }else{
                                                    echo !empty($row->shop_content_in) ? substr($row->shop_content_in, 0, 20)." ..." : "[ KOSONG! ]";
                                                }
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <div class="panel-body card-btn-more text-right">
                                <a class="btn btn-more" onclick="showShare('share<?php echo $i; ?>')">
                                    <?php echo $this->lang->line("btn_share"); ?>
                                </a>
                                <a class="btn btn-more" href="<?php echo base_url('restaurants/detail/'.$i); ?>">
                                    <?php echo $this->lang->line("btn_discover"); ?>
                                </a>
                            </div>

                            <!-- SHARE -->

                            <div class="panel panel-default card-share" id="share<?php echo $i; ?>">
                                <div class="panel-body text-center social-share-btn">
                                    <?php
                                        $share_link = base_url('restaurants/detail/'.$url_single);
                                        $resto_name = ucwords($shop_name);

                                        $share_title = $resto_name;
                                        if($lang == "english" || $lang == ""){
                                            $share_text = $resto_name." on Kidsfun Yogyakarta. Click ";
                                        }else{
                                            $share_text = $resto_name." di Kidsfun Yogyakarta. Klik ";
                                        }
                                    ?>
                                    <h5><?php echo $this->lang->line("share_on"); ?> :</h5>
                                    <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $share_link; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/icon/icon_facebook_true.svg'); ?>" alt="facebook">
                                    </a>
                                    <a href="http://twitter.com/intent/tweet/?text=<?php echo $share_text; ?>&url=<?php echo $share_link; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/icon/icon_twitter_true.svg'); ?>" alt="twitter">
                                    </a>
                                    <a href="https://plus.google.com/share?url=<?php echo $share_link; ?>" target="_blank">
                                        <img src="<?php echo base_url('assets/icon/icon_googleplus_true.svg'); ?>" alt="google-plus">
                                    </a>
                                </div>
                                <div class="panel-body card-btn-more">
                                    <a class="btn btn-more pull-right" onclick="closeShare('share<?php echo $i; ?>')">
                                        <?php echo $this->lang->line("btn_close"); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        
                    </article>
                <?php } ?>  
            </div>
        </div>
    </section>
</section>