<section class="container-fluid home-banner">
    <figure class="banner">
      <!-- Wrapper for slides -->
      <div class="banner-inner">
        <picture>
            <source 
                media="(min-width: 700px)"
                srcset="<?php echo base_url('assets/upload/contact').'/'.$banner_large->media_url; ?>">
            <source 
                media="(max-width: 700px)"
                srcset="<?php echo base_url('assets/upload/contact').'/'.$banner_small->media_url; ?>">
            <img class="img-responsive"
                src="<?php echo base_url('assets/upload/contact').'/'.$banner_large->media_url; ?>" 
                alt="kidsfun-banner">
        </picture>

        <header class="intro-banner-content">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <h1 class="display"><?php echo $this->lang->line("banner_contact"); ?></h1>
                <p class="lead">
                    <?php if($lang == "indonesia"){
                        echo !empty($banner_caption->general_content_in) ? $banner_caption->general_content_in:"";

                    }else{
                        echo !empty($banner_caption->general_content_en) ? $banner_caption->general_content_en:"";
                    } ?> 
                </p>
                <a href="#main">
                    <img class="icon" src="<?php echo base_url('assets/icon/arrowdown.svg');?>" alt="scroll-down" />
                </a>
            </div>
        </header>
      </div>
    </figure>
</section>

<!-- THE CONTENT -->
<div id="main" class="container-fluid content">
    <header class="row main-header">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
            <div class="par lead text-center">
                <?php if($lang == "indonesia"){
                    echo !empty($content->general_content_in) ? $content->general_content_in:"[EMPTY!]";

                }else{
                    echo !empty($content->general_content_en) ? $content->general_content_en:"[EMPTY!]";
                } ?> 
            </div>
        </div>
    </header>
    
    <div class="row" id="form">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 padding-clear">
            <!-- CONTACT FORM -->
            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="panel panel-default padding-clear">
                
                <!-- THE FORM -->
                <?php echo $this->session->flashdata('info_send'); ?>
                <form method="POST" id="contact_form" name="contact_form">
                    <div class="panel-body">


                        <div class="col-effect input-effect">
                            <input class="effect-label contact_name" placeholder="" type="text" name="Name">
                            <label><?php echo $this->lang->line("contact_name"); ?></label>
                            <span class="focus-border"></span>
                        </div>



                        <div class="col-effect input-effect">
                            <input class="effect-label contact_email" placeholder="" type="email" name="Email">
                            <label><?php echo $this->lang->line("contact_email"); ?></label>
                            <span class="focus-border"></span>
                        </div>


			            <div class="col-effect input-effect">
                            <input class="effect-label contact_phone" placeholder="" type="text" name="Phone">
                            <label><?php echo ($lang=='indonesia')?'No HP':'Phone'; ?></label>
                            <span class="focus-border"></span>
                        </div>
                        


                        <div class="col-effect input-effect">
                            <input class="effect-label contact_subject" placeholder="" type="text" name="Subject">
                            <label><?php echo $this->lang->line("contact_subject"); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <textarea class="effect-label contact_message" placeholder="" rows="4" name="Message"></textarea>
                            <label><?php echo $this->lang->line("contact_message"); ?></label>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                            <input type="button" id="btn-send" class="btn btn-bg-red btn-raised" name="button" value="<?php echo $this->lang->line("btn_send"); ?>">
                        </div>
                    </div>
                </form>
                </div>
            </div>
            
            <!-- CONTACT INFO -->
            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="row">
                    <div class="col-xs-12 padding-clear">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                
                                <?php
                                    $phone = explode(';',str_replace("<p>","",$phone->general_content_en));
                                    // echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
                                    echo $phone[0]." ";
                                    foreach($phone as $key => $val){
                                        if(count($phone) == $key+1){break;}
                                        $toPrint = str_replace('-', ' ', str_replace('</p>','',$phone[$key+1]));
                                        $toLink = str_replace('-', '', str_replace('</p>','',$phone[$key+1]));
                                        $toCall = 'tel:+62'.str_replace(str_split('()'),'',substr($phone[0], 2)).$toLink;
                                        
                                ?>
                                    <a class="phonecall" href="<?php echo $toCall; ?>">
                                        <?php
                                            echo $toPrint;
                                        ?>
                                    </a>
                                <?php
                                    if($key+1 < count($phone)-1){echo " / ";};
                                    }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 padding-clear">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <?php echo !empty($address->general_content_en) ? $address->general_content_en:"[EMPTY!]"; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>