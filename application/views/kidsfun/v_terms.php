Syarat dan Ketentuan

SYARAT PENGGUNAAN DI BAWAH INI HARUS DIBACA SEBELUM MELAKUKAN TRANSAKSI PEMBELIAN TIKET. PENGGUNAAN SITUS INI MENUNJUKKAN PENERIMAAN DAN KEPATUHAN TERHADAP SYARAT DAN KETENTUAN DI BAWAH INI.

Kidsfun.co.id, produkrekreasi.com, dan yogyagokart.com adalah jasa online yang dimiliki dan dioperasikan oleh PT. Produk Rekreasi. Dengan mengakses dan/atau menggunakan website ini, Anda mengakui bahwa Anda telah membaca, memahami, dan menyetujui untuk terikat oleh syarat penggunaan yang ditetapkan di bawah ini.
Dalam waktu yang tidak ditentukan syarat dan ketentuan akan berubah. Penggunaan Anda atas website ini harus mengikuti setiap perubahan yang merupakan bagian dari perjanjian dan terikat oleh persyaratan sesuai dengan perubahan yang ada tanpa terkecuali.

KEBIJAKAN PEMESANAN
1.	Pelanggan yang melakukan reservasi melalui website kami, harap memperhatikan data yang didaftarkan pada saat melakukan reservasi termasuk Tanggal Kunjungan, Nama, Alamat, Tempat dan Tangal Lahir,  E-mail, Alamat, Kota, Propinsi,  Jenis dan Jumlah Tiket
2.	Khusus pembelian tiket masuk Taman Bermain, Kolam Renang Aqua Splash, Paket Taman Bermain dan Kolam Renang akan mendapatkan diskon 25%.
3.	Reservasi Online Ticket dilakukan paling lambat SATU HARI sebelum melakukan kunjungan.
4.	Penukaran Online Ticket harus sesuai dengan tanggal kunjungan.
5.	Tanggal kunjungan tidak dapat diubah.
6.	Jenis dan jumlah tiket harus sesuai dengan yang direservasi melalui Online Ticket. Penambahan tiket di hari-H kunjungan dikenakan harga reguler.
7.	Penukaran tiket dilakukan di kasir entrance dengan menunjukkan form bukti pembayaran Online Ticket.
8.	Bukti valid penukaran tiket hanya dapat dilakukan menggunakan Form Online Ticket. Jika dalam 1×24 jam Anda belum menerima Form Online Ticket segera hubungi Bagian Marketing di 0274-4353435 atau melalui email info@kidsfun.co.id 
9.	Tiket tidak dapat direfund.
_________________________________________________________________

KEBIJAKAN PEMBAYARAN
Kami memberikan beberapa metode pembayaran:
•	Pembayaran dengan Credit Cards : Visa and Mastercard
•	Pembayaran dengan ATM Transfer dan Internet Banking
Kami tidak bertanggung jawab atas penyalahgunaan kartu kredit atau alat pembayaran apapun yang merupakan hak milik anda dan di luar jangkauan pengawasan kami.
Mata uang yang digunakan dan berlaku sah dalam pembelian pada website ini adalah dalam Indonesia Rupiah (IDR). (Semua transaksi akan diproses dalam mata uang Rupiah).
Semua transaksi akan diproses dalam kurun waktu 1x 24 jam 
Customer kemudian diwajibkan untuk menyelesaikan pembayaran dan apabila transaksi pembayaran telah berhasil dilakukan, kami akan mengirimkan tanda terima pembayaran/online ticket melalui email yang tercantum dalam data pribadi pembeli dengan kurun waktu max 1 (satu) hari setelah proses pembayaran selesai dilakukan
_________________________________________________________________

BUKTI PEMBAYARAN
1.	Bukti pembayaran merupakan alat bukti yang sah untuk menunjukkan bahwa Anda telah menyelesaikan biaya administrasi pemesanan tiket online.
2.	Simpan bukti pembayaran untuk kelancaran transaksi Anda selanjutnya.
_________________________________________________________________

KEBIJAKAN PENGEMBALIAN DANA (REFUND) DAN PEMBATALAN (CANCELLATION) PEMESANAN
Kami tidak melayani pengembalian dana (refund) kecuali apabila ada kesalahan dari pihak kami dan di luar dari yang sudah ditetapkan dalam syarat dan ketentuan pada halaman ini. Pengembalian dana yang terjadi di luar dari kesepakatan penyelesaian masalah pembelian yang telah disetujui kedua belah pihak bukan merupakan tanggung jawab dan bagian dari pelayanan kami.
_________________________________________________________________

Pembatalan Pesanan akan dilakukan secara sepihak apabila:
•	Anda belum melakukan pembayaran dalam waktu 3×24 jam setelah order atau atas permintaan pelanggan
•	Kami belum menerima pembayaran anda
•	Tanggal kedatangan tidak sesuai dengan tanggal kedatangan yang tercantum dalam online ticket
_________________________________________________________________

KEBIJAKAN PRIVASI
Kidsfun.co.id, yogyagokart.com, dan produkrekreasi.com menjaga privasi Anda dengan sangat serius. Privasi elektronik sangat penting bagi keberhasilan berkelanjutan dari Internet.
Informasi ini hanya dan harus digunakan untuk membantu kami menyediakan layanan yang lebih baik. Itulah sebabnya kami telah menempatkan kebijakan untuk melindungi informasi pribadi Anda.
Ringkasan Kebijakan
Beberapa bagian dari website kami memerlukan pendaftaran untuk mengaksesnya, walaupun biasanya semua yang diminta adalah berupa alamat e-mail dan beberapa informasi dasar tentang Anda.
Ada bagian di mana kami akan meminta informasi tambahan. Kami melakukan ini untuk dapat lebih memahami kebutuhan Anda, dan memberikan Anda palayanan yang kami percaya mungkin berharga bagi Anda. Beberapa contoh informasi website kami butuhkan seperti nama, email, alamat rumah, dan info pribadi. Kami memberikan Anda kesempatan untuk memilih untuk tidak menerima materi informasi dari kami.
Melindungi privasi Anda
Kami akan mengambil langkah yang tepat untuk melindungi privasi Anda. Setiap kali Anda memberikan informasi yang sensitif (misalnya, nomor kartu kredit untuk melakukan pembelian), kami akan mengambil langkah-langkah yang wajar untuk melindungi, seperti enkripsi nomor kartu Anda. Kami juga akan mengambil langkah-langkah keamanan yang wajar untuk melindungi informasi pribadi Anda dalam penyimpanan. Nomor kartu kredit hanya digunakan untuk proses pembayaran dan bukan disimpan untuk tujuan pemasaran.
Kami tidak akan memberikan informasi pribadi Anda kepada perusahaan lain atau individu tanpa izin Anda. “Proses order anda kami pastikan aman dengan protokol  Secure Sockets Layer (SSL) dimana SSL menyediakan setiap pelanggan keamanan penuh dan kebebasan untuk belanja online tanpa rasa khawatir mengenai kemungkinan pencurian informasi  kartu kredit”
Keamanan
Situs ini memiliki langkah-langkah keamanan untuk melindungi kehilangan, penyalahgunaan dan perubahan informasi di dalam kendali kita. Langkah-langkah ini meliputi metode perlindungan data dasar dan kompleks, dalam penyimpanan offline tertentu informasi dan securitizing server database kami.
Situs ini memberikan pilihan bagi para untuk menghapus informasi mereka dari database kami untuk tidak menerima informasi kedepannya atau untuk tidak lagi menerima layanan kami.


_________________________________________________________________
KRITIK DAN SARAN
Silahkan kirimkan kritik dan saran melalui info@kidsfun.co.id
_________________________________________________________________
JAM OPERASIONAL
Kami akan mengirimkan korfimasi pembayaran melalui jam operasional, yaitu Senin s/d Jumat pukul 08.30-16.30, Sabtu/Minggu/Libur Nasional pukul 08.00-18.00 WIB

