<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line("souvenirs"); ?>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i>
                <a href="<?php echo base_url('backend/all_shops');?>"> <?php echo $this->lang->line("all_shops"); ?></a>
            </li>
            <li>
                <?php echo $this->lang->line("souvenirs"); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
			<div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line("banner"); ?></div>
                    <div class="panel-body">
                    <?php echo $this->session->flashdata('info_banner');?>
                        <div class="row">
                            <div class="col-md-8">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/souvenirs/banner_large');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("big_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <img class="img-responsive img-center" src="<?php echo !empty($banner_lg->media_url) ? base_url('assets/upload/shops/souvenirs/thumbnail').'/'.$banner_lg->media_url:base_url('assets/img/blank_img.png'); ?>" alt="souvenirs">
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="file" name="pic_souvenirs_lg" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/souvenirs/banner_small');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("small_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <img class="img-responsive img-center" src="<?php echo !empty($banner_sm->media_url) ? base_url('assets/upload/shops/souvenirs/thumbnail').'/'.$banner_sm->media_url:base_url('assets/img/blank_img.png'); ?>" alt="souvenirs">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-md-8">
                                                    <input type="file" name="pic_souvenirs_sm" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row" id="caption">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/souvenirs/banner_caption');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("caption"); ?></div>
                                        <div class="panel-body">
                                        <?php echo $this->session->flashdata('info_caption');?>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_in"); ?></p></label>
                                                                <textarea class="form-control" name="text_souvenirs_in" rows="5" required><?php echo isset($caption_in->general_content_in) ? $caption_in->general_content_in:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_en"); ?></p></label>
                                                                <textarea class="form-control" name="text_souvenirs_en" rows="5" required><?php echo isset($caption_en->general_content_en) ? $caption_en->general_content_en:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        <div class="row" id="content">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/souvenirs/souvenirs_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("desc_in"); ?></p></label>
                                                <textarea class="form-control" name="text_souvenirs_in" rows="5" required><?php echo isset($content_in->general_content_in) ? $content_in->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("desc_en"); ?></p></label>
                                                <textarea class="form-control" name="text_souvenirs_en" rows="5" required><?php echo isset($content_en->general_content_en) ? $content_en->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <img class="img-responsive img-thumbnail img-center" src="<?php echo !empty($content_img->shop_type_img) ? base_url('assets/upload/shops/souvenirs/thumbnail').'/'.$content_img->shop_type_img:base_url('assets/img/blank_img.png'); ?>" alt="souvenirs">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input type="file" name="souvenirs_img" accept="image/gif, image/x-png, image/jpeg">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>        
            </div>
        </div>
    </section><!-- /.content -->
</div>