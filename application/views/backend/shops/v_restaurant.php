<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("restaurants"); ?>
        </h1>
        <ol class="breadcrumb">
			<li><a href="<?php echo site_url();?>backend/all_shops"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line("all_shops"); ?></a></li>
            <li class="active"><?php echo $this->lang->line("restaurants"); ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

			<div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner Large ########## -->
                            <div class="col-sm-8">
                            <?php 
                                echo $this->session->flashdata('restaurants_banner_large');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/restaurant/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                              <img src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/shops/restaurants/thumbnail').'/'.$banner_large->media_url :  base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Restaurants banner Large" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_large->media_id) ? $banner_large->media_id:''; ?>" name="banner_large_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Small ########## -->
                            <div class="col-sm-4">
                            <?php 
                                echo $this->session->flashdata('restaurants_banner_small');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/restaurant/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                              <img src="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/shops/restaurants/thumbnail').'/'.$banner_small->media_url :  base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_small->media_id) ? $banner_small->media_id:''; ?>" name="banner_small_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Caption ########## -->
                            <div class="col-sm-12" id="restaurants_banner_caption">
                            <?php 
                                echo $this->session->flashdata('restaurants_banner_caption');
                            ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/restaurant/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                             <div class="row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="Content Indonesia" class="control-label">
                                                                <p><?php echo $this->lang->line("content_in"); ?></p>
                                                            </label>
                                                            <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($banner_caption->general_content_in) ? $banner_caption->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="Content English" class="cotrol-label">
                                                                <p><?php echo $this->lang->line("content_en"); ?></p>
                                                            </label>
                                                            <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($banner_caption->general_content_en) ? $banner_caption->general_content_en:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                             
                                
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_caption->general_id) ? $banner_caption->general_id: ''; ?>" name="general_id" >
                                </div>
                            </form>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>

<!-- ########## Restaurant Content ########## -->
            <div class="col-xs-12" id="restaurants_content">
            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/restaurant/save_restaurant_content" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("content"); ?>
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('restaurants_content');
                        ?>
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title English" class="control-label">
                                                <p><?php echo $this->lang->line("content_in"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($restaurants->general_content_in) ? $restaurants->general_content_in:''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title English" class="control-label">
                                                <p><?php echo $this->lang->line("content_en"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($restaurants->general_content_en) ? $restaurants->general_content_en:''; ?></textarea>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>                                                         
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button> 
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($restaurants->general_id) ? $restaurants->general_id: ''; ?>" name="general_id" >
                </div>
            </form>
            </div>

<!-- ########## Restaurants Add + Edit + Delete ########## -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("restaurants"); ?>
                        <button type="button" class="btn btn-sm btn-success pull-right btn-add" data-target="#restaurant_add" role="input"  data-toggle="modal" title="add Restaurant"> 
                            <i class="fa fa-plus"></i>  <?php echo $this->lang->line("restaurants"); ?>
                        </button>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->session->flashdata('banner_single_large'); ?>
                        <?php echo $this->session->flashdata('banner_single_small'); ?>
                        <?php echo $this->session->flashdata('banner_single_caption'); ?>
                        <?php echo $this->session->flashdata('restaurant_update_banner'); ?>
                        <?php echo $this->session->flashdata('restaurant_update_content'); ?>
                        <?php echo $this->session->flashdata('restaurant_insert'); ?>
                        <?php echo $this->session->flashdata('restaurant_update'); ?>
                        <?php echo $this->session->flashdata('restaurant_delete'); ?>
                        <table class="table table-bordered table-striped" id="restaurants">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th><?php echo $this->lang->line("content"); ?> Indonesia</th>
                                    <th><?php echo $this->lang->line("content"); ?> English</th>
                                    <th><?php echo $this->lang->line("action"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                        $no = 1;
                        foreach ($show as $key) {
                         ?>
                            <tr>
                                <td>
                                    <?php echo $no++; ?>    
                                </td>
                                <td>
                                    <?php echo ucfirst($key->shop_type)." Type" ?>
                                </td>
                                <td>
                                    <?php
                                        $data1 = $key->shop_content_in;
                                        if(strlen($data1) <= 40){
                                            echo $data1;
                                        }else{
                                            echo substr($data1, 0, 40) ." ...";
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data2 = $key->shop_content_en;
                                        if(strlen($data2) <= 40){
                                            echo $data2;
                                        }else{
                                            echo substr($data2, 0, 40) ." ...";
                                        }
                                    ?>
                                </td>
                                <td colspan="2" class="text-center">
                                    <button type="button" class="btn btn-xs btn-warning" data-target="#restaurant_edit_banner" id="btn_restaurant_edit_banner" role="input"  data-toggle="modal" data-banner="<?php echo $key->shop_id ?>" title="Edit Banner"> <i class="fa fa-edit"></i> Banner</button> 

                                    <button type="button" class="btn btn-xs btn-warning" data-target="#restaurant_edit_content" id="btn_restaurant_edit_content" role="input"  data-toggle="modal" data-id="<?php echo $key->shop_id ?>" title="Edit Restaurant Content"> <i class="fa fa-edit"></i> Restaurant Content</button> 

                                    <a href="<?php echo site_url('backend/restaurant/delete/'.$key->shop_id); ?>" class="btn btn-xs btn-danger" title="<?php echo $this->lang->line('btn_edit'); ?>"  onClick="return confirm('<?php echo $this->lang->line('confirm'); ?>');"> <i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
<!-- ########## Restaurant map ########## -->
            <div class="col-xs-12" id="restaurant_map">
            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/restaurant/save_restaurant_map" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("map"); ?>
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('restaurant_map');
                        ?>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                              <img src="<?php echo !empty($map->media_url)? base_url('assets/upload/shops/restaurants/thumbnail').'/'.$map->media_url: base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4">
                                <label for="Restaurants banner Large" class="control-label">
                                    <?php echo $this->lang->line("choose_image"); ?>
                                </label>
                                <input type="file" name="media_url">
                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                <br>
                                <small class="color-red"> <?php echo $this->lang->line("image_1200px"); ?> </small>
                                <br>
                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button> 
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($map->media_id) ? $map->media_id: ''; ?>" name="media_id" >
                </div>
            </form>
            </div>


		</div>
    </section><!-- /.content -->
</div>

<!-- Modal Restaurant add -->
<div id="restaurant_add" class="modal fade" tab-index="-1" role="dialog" aria-hidden="true" data-backdrop='static' data-keyboard="false"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("add_restaurant"); ?> </h4>
            </div>
            <div class="modal-body" id="restaurant-add">

            </div>
        </div>
    </div>
</div>


<!-- Modal Restaurant Banner -->
<div id="restaurant_edit_banner" class="modal fade" tab-index="-1" role="dialog" aria-hidden="true" data-backdrop='static' data-keyboard="false"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_edit"); ?> Banner </h4>
            </div>
            <div class="modal-body" id="restaurant-edit-banner">

            </div>
        </div>
    </div>
</div>


<!-- Modal Restaurant Content -->
<div id="restaurant_edit_content" class="modal fade" tab-index="-1" role="dialog" aria-hidden="true" data-backdrop='static' data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_edit"); ?> Content </h4>
            </div>
            <div class="modal-body" id="restaurant-edit-content">

            </div>
        </div>
    </div>
</div>