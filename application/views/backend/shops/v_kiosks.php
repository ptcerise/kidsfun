<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("kiosks"); ?>
        </h1>
        <ol class="breadcrumb">
			<li><a href="<?php echo site_url();?>backend/all_shops"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line("all_shops"); ?></a></li>
            <li class="active"><a href="#"> <?php echo $this->lang->line("kiosks"); ?></a></li>
        </ol>
    </section>

   <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner Large ########## -->
                            <div class="col-sm-8">
                            <?php 
                                echo $this->session->flashdata('banner_large');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/kiosks/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner_large->media_url)? site_url('assets/upload/shops/kiosks/thumbnail').'/'.$banner_large->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_large->media_id) ? $banner_large->media_id:''; ?>" name="banner_large_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Small ########## -->
                            <div class="col-sm-4">
                            <?php 
                                echo $this->session->flashdata('banner_small');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/kiosks/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner_small->media_url) ? site_url('assets/upload/shops/kiosks/thumbnail').'/'.$banner_small->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_small->media_id) ? $banner_small->media_id:''; ?>" name="banner_small_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Caption ########## -->
                            <div class="col-sm-12" id="banner_caption">
                            <?php 
                                echo $this->session->flashdata('banner_caption');
                            ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/kiosks/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Title English" class="control-label">
                                                            <p><?php echo $this->lang->line("caption_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($banner_caption->general_content_in) ? $banner_caption->general_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Title English" class="control-label">
                                                            <p><?php echo $this->lang->line("caption_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($banner_caption->general_content_en) ? $banner_caption->general_content_en:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_caption->general_id) ? $banner_caption->general_id: ''; ?>" name="general_id" >
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
<!-- ########## Kiosks Content ########## -->
            <div class="col-xs-12" id="kiosks_content">
                <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/kiosks/save_kiosks_content" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("kiosks"); ?>
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('kiosks');
                        ?>
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12 text-center">
                                          <img src="<?php echo !empty($kiosks->media_url) ? site_url('assets/upload/shops/kiosks/thumbnail').'/'.$kiosks->media_url : base_url('assets/img/blank_img.png'); ?>" alt="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="Title English" class="col-sm-4">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                          <input type="file" name="media_url">
                                            <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                            <br>
                                            <small class="color-red"> <?php echo $this->lang->line("image_1024px"); ?> </small>
                                            <br>
                                            <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title English" class="control-label">
                                                <p><?php echo $this->lang->line("content_in"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="media_content_in"><?php echo isset($kiosks->media_content_in) ? $kiosks->media_content_in:''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title English" class="control-label">
                                                <p><?php echo $this->lang->line("content_en"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="media_content_en"><?php echo isset($kiosks->media_content_en) ? $kiosks->media_content_en:''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button> 
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($kiosks->media_id) ? $kiosks->media_id: ''; ?>" name="media_id" >
                </div>
            </form>
            </div>

        </div>
    </section><!-- /.content -->
</div>