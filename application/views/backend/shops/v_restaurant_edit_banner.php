<section>
<div class="row">
<!-- ########## Banner Large ########## -->
    <div class="col-sm-7">
    <form class="form-horizontal" method="POST" action="<?php echo site_url('backend/restaurant/update_banner_single_large'); ?>" accept-charset="utf-8" enctype="multipart/form-data" role="form" runat="server">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo $this->lang->line("big_image"); ?>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                          <img src="<?php echo !empty($media1->media_url)? base_url('assets/upload/shops/restaurants/thumbnail').'/'.$media1->media_url: base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Title English" class="control-label">
                            <?php echo $this->lang->line("choose_image"); ?>
                        </label>
                        <input type="file" name="media_url">
                        <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                        <br>
                        <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                        <br>
                        <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="form-group panel-save">
                    <button type="submit" class="btn btn-sm btn-primary"><?php echo $this->lang->line('btn_update'); ?></button>
                </div>
            </div>
            <input type="hidden" value="<?php echo isset($media1->media_id) ? $media1->media_id:''; ?>" name="media1_id" id="media1_id">
        </div>
    </form>
    </div>
<!-- ########## Banner Small ########## -->
    <div class="col-sm-5">
    <form class="form-horizontal" method="POST" action="<?php echo site_url('backend/restaurant/update_banner_single_small'); ?>" accept-charset="utf-8" enctype="multipart/form-data" role="form" runat="server">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo $this->lang->line("small_image"); ?>
            </div>
            <div class="panel-body">
                 <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                          <img src="<?php echo !empty($media2->media_url)? base_url('assets/upload/shops/restaurants/thumbnail').'/'.$media2->media_url :base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Title English" class="control-label">
                            <?php echo $this->lang->line("choose_image"); ?>
                        </label>
                        <input type="file" name="media_url">
                        <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                        <br>
                        <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                        <br>
                        <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="form-group panel-save">
                    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_update'); ?></button>
                </div>
            </div>
            <input type="hidden" value="<?php echo isset($media2->media_id) ? $media2->media_id:''; ?>" name="media2_id" id="media1_id">
        </div>
    </form>
    </div>
<!-- ########## Banner Caption ########## -->
    <div class="col-sm-12">
    <form class="form-horizontal" method="POST" action="<?php echo site_url('backend/restaurant/update_banner_single_caption'); ?>" accept-charset="utf-8" enctype="multipart/form-data" role="form" runat="server">
        <div class="panel panel-info">
            <div class="panel-heading">
                <?php echo $this->lang->line("caption"); ?>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="Title Indonesia" class="control-label">
                                        <?php echo $this->lang->line("title_in"); ?>
                                    </label>
                                    <input type="text" class="form-control" name="media_title_in" value="<?php echo isset($media1->media_title_in) ? $media1->media_title_in:''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for=""><?php echo $this->lang->line('caption_in'); ?></label>
                                    <textarea class="form-control risize wysihtml5_restaurant" rows="7" name="media_content_in"><?php echo isset($media1->media_content_in) ? $media1->media_content_in:''; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="Title English" class="control-label">
                                        <?php echo $this->lang->line("title_en"); ?>
                                    </label>
                                    <input type="text" class="form-control" name="media_title_en" value="<?php echo isset($media1->media_title_en) ? $media1->media_title_en:''; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for=""><?php echo $this->lang->line('caption_en'); ?></label>
                                    <textarea class="form-control risize wysihtml5_restaurant" rows="5" name="media_content_en"><?php echo isset($media1->media_content_en) ? $media1->media_content_en:''; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="form-group panel-save">
                    <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                </div>
            </div>
            <input type="hidden" name="media1_id" value="<?php echo isset($media1->media_id) ? $media1->media_id:''; ?>">
            <input type="hidden" name="temp_id" value="<?php echo isset($media1->temp_id) ? $media1->temp_id:''; ?>">
            <input type="hidden" name="media2_id" value="<?php echo isset($media2->media_id) ? $media2->media_id:''; ?>">
        </div>
    </form>
    </div>
</div> 
<center>
    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('btn_close'); ?></button>
</center>
</section>