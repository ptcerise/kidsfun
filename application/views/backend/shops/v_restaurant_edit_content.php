<section>
    <div class="row">
        <div class="col-sm-12">
            <form class="form-horizontal" method="POST" action="<?php echo site_url('backend/restaurant/restaurant_update'); ?>" accept-charset="utf-8" enctype="multipart/form-data" role="form" runat="server">
                <?php $x= 1; $y=1; $im=1;  foreach ($media as $key) { ?>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="" class="col-sm-12"> <?php echo "Choose Image " .$im++; ?> </label>
                        <div class="col-sm-12">
                            <img src="<?php echo !empty($key->media_url)? site_url('assets/upload/shops/restaurants/thumbnail').'/'.$key->media_url:''; ?>" alt=""class="img-responsive">
                            <br>
                            <input type="file" class="form-control" name="media_url<?php echo $x++;?>">
                            <input type="hidden" class="form-control" name="media_id<?php echo $y++;?>" value="<?php echo $key->media_id ?>">
                            <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                            <br>
                            <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                            <br>
                            <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                        </div>
                       
                    </div>
                </div>
                <?php } ?>
                <div class="col-sm-6">
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label for=""><?php echo $this->lang->line('content_in'); ?></label>
                            <textarea class="form-control" rows="7" name="shop_content_in" required><?php echo $modal->shop_content_in ?>
                            </textarea>
                       </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label for=""><?php echo $this->lang->line('content_en'); ?></label>
                            <textarea class="form-control" rows="7" name="shop_content_en" required><?php echo $modal->shop_content_en ?>
                            </textarea>
                       </div>
                    </div>
                </div>
                
                
                <div class="form-group pull-right">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_update'); ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('btn_close'); ?></button>
                    </div>
                </div>
                <input type="hidden" name="shop_id" value="<?php echo $modal->shop_id ?>">
            </form>
        </div> 
    </div>    
</section>