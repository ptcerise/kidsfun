<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line("user"); ?>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i>
                <?php echo $this->lang->line("user"); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row" id="user_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("user_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_user"); ?>" data-toggle="modal" data-target="#add_page"><i class="fa fa-plus"></i> <?php echo $this->lang->line("user"); ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_user');?>
                        <?php echo $this->session->flashdata('password');?>
                            <table class="table table-hover table-bordered" id="user">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("username_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("fullname_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("userlevel_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($user_list as $key => $row):?>
                                        <?php
                                            if($row->user_level == 1) {
                                                $level = 'Admin';
                                            }
                                            else{
                                                $level = 'Operator';
                                            }
                                        ?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->user_name;?></td>
                                        <td><?php echo $row->user_full_name;?></td>
                                        <td><?php echo $level;?></td>
                                        <td><center>
                                            
                                            <a href="#" id="<?php echo @$row->user_id;?>" class="btn btn-warning btn-xs edit-page" title="<?php echo $this->lang->line("edit_user"); ?>" data-target="#edit-page" data-toggle="modal"><i class="fa fa-edit"></i> <?php echo $this->lang->line("user"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/User/delete_user/'.@$row->user_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_user"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="add_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_form" action="<?php echo base_url('backend/user/save_new');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("new_user"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("username"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="user_name" id="user_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("fullname"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="user_full_name" id="user_full_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("user_level"); ?></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="user_level">
                                        <option>Admin</option>
                                        <option>Operator</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("password"); ?></label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" id="user_pass" name="user_pass" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("confirm_pass"); ?></label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" id="confirm_pass" name="confirm_pass" onInput="check(this);" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="edit-page" tabindex="-1" role="basic" aria-hidden="true"></div>
    </section><!-- /.content -->
</div>