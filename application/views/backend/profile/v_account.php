<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line('account_setting'); ?>
        </h1>
        <ol class="breadcrumb">
			<li class="active"><a href="#"><i class="fa fa-dashboard"></i> Profile</a></li>
        </ol>
    </section>

    <!-- Profile -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-user"></i> <?php echo $this->lang->line('profile'); ?></div>
                    <form method="POST" action="<?php echo base_url('backend/Profile/account/'.@$user->user_id);?>" class="form-horizontal" role="form">
                        <div class="panel-body">
                            <?php $message = $this->session->flashdata('account'); if(@$message!='') : ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <?php echo $message;?>
                            </div>
                            <?php endif ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line('username'); ?></label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $user->user_name ?>" required name="user_name" id="user_name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line('fullname'); ?></label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $user->user_full_name ?>" required name="user_full_name" id="user_full_name">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading"><i class="fa fa-lock"></i> <?php echo $this->lang->line('pass'); ?></div>
                    <form method="POST" action="<?php echo base_url('backend/Profile/password/'.@$user->user_id);?>" class="form-horizontal" role="form">
                        <div class="panel-body">
                            <?php $message = $this->session->flashdata('failed'); if(@$message!='') : ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <?php echo $message;?>
                            </div>
                            <?php endif ?>

                            <?php $message = $this->session->flashdata('success'); if(@$message!='') : ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                <?php echo $message;?>
                            </div>
                            <?php endif ?>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line('current_pass'); ?></label>
                                    <div class="col-md-4">
                                        <input type="password" class="form-control" required name="current" id="current">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line('new_pass'); ?></label>
                                    <div class="col-md-4">
                                        <input type="password" class="form-control" required name="new" id="new">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line('confirm_pass'); ?></label>
                                    <div class="col-md-4">
                                        <input type="password" class="form-control" required name="confirm" id="confirm" required  onInput="check(this);">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </section>
</div><!-- /.profile -->


<script type="text/javascript">
    function check(input){
        if(input.value != $('#new').val()){
            input.setCustomValidity('Password must be match.');
        }
        else{
            input.setCustomValidity('');
        }
    }   
</script>