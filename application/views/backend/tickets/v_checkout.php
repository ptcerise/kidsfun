<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            Checkout
        </h1>
        <ol class="breadcrumb">
			<li class="active">
                <i class="fa fa-dashboard"></i>
                <a href="<?php echo base_url('backend/tickets');?>"> <?php echo $this->lang->line("tickets"); ?>
                </a>
            </li>
            <li>
            Checkout
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/checkout/checkout_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_checkout');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="text_checkout_in" rows="5" required><?php echo $content_in->general_content_in; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="text_checkout_en" rows="5" required><?php echo $content_en->general_content_en; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row" id="success">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/checkout/checkout_success');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading">Checkout success</div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_success');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("title_in"); ?></p></label>
                                                <input type="text" class="form-control" name="title_success_in" rows="5" value="<?php echo isset($success_checkout->general_title_in) ? $success_checkout->general_title_in:''; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("title_en"); ?></p></label>
                                                <input type="text" class="form-control" name="title_success_en" rows="5" value="<?php echo isset($success_checkout->general_title_en) ? $success_checkout->general_title_en:''; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="success_checkout_in" rows="5" required><?php echo $success_checkout->general_content_in; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="success_checkout_en" rows="5" required><?php echo $success_checkout->general_content_en; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row" id="payment">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("payment"); ?>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_payment');?>
                            <table class="table table-hover table-bordered" id="checkout">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("date"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("first_name"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("last_name"); ?></th>
                                        <th class="col-sm-2">Email</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php foreach($checkout as $key => $row):?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo date('d/m/y',strtotime($row->checkout_date));?></td>
                                        <td><?php echo $row->checkout_fname;?></td>
                                        <td><?php echo $row->checkout_lname;?></td>
                                        <td><?php echo $row->checkout_email;?></td>
                                        
                                        <td><center>
                                            <a href="#" id="<?php echo @$row->checkout_id;?>" class="btn btn-success btn-xs detail-page" title="<?php echo $this->lang->line("detail_payment"); ?>" data-target="#detail-page" data-toggle="modal"><i class="fa fa-file-text"></i> <?php echo $this->lang->line("detail"); ?></a>

                                            <a href="#" id="<?php echo @$row->checkout_id;?>" class="btn btn-warning btn-xs edit-page" title="<?php echo $this->lang->line("edit_payment"); ?>" data-target="#edit-page" data-toggle="modal"><i class="fa fa-edit"></i> <?php echo $this->lang->line("payment"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/Checkout/delete_payment/'.@$row->checkout_id);?>" class="btn-delete btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_payment"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="detail-page" tabindex="-1" role="basic" aria-hidden="true"></div>
        <div class="modal fade" id="edit-page" tabindex="-1" role="basic" aria-hidden="true"></div>
    </section><!-- /.content -->
</div>