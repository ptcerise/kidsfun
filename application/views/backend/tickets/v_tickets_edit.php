
<form method="POST" id="userForm" action="<?php echo base_url('backend/Tickets/edit_save');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?php echo $this->lang->line("edit_tickets"); ?></h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">                
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("name_tabel"); ?></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="tickets_name" value="<?php echo $edit_tickets->tickets_name;?>" autofocus required>
                            <input type="hidden" class="form-control" name="tickets_id" value="<?php echo $edit_tickets->tickets_id;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("category_tabel"); ?></label>
                        <div class="col-md-4">
                            <select class="form-control" name="tickets_category" value="<?php echo $tickets_category;?>" required>
                                
                                <option value="vip cards" <?php if($edit_tickets->tickets_category =='vip cards'){echo "selected";}?>>Vip Cards</option>
                                <option value="tickets" <?php if($edit_tickets->tickets_category =='tickets'){echo "selected";}?>>Tickets</option>
                                <option value="birthday" <?php if($edit_tickets->tickets_category =='birthday'){echo "selected";}?>>Birthday</option>
                                <option value="gokart" <?php if($edit_tickets->tickets_category =='gokart'){echo "selected";}?>>Gokart</option>
                                <option value="package" <?php if($edit_tickets->tickets_category =='package'){echo "selected";}?>>Package</option>
                                <option value="lunch pack" <?php if($edit_tickets->tickets_category =='lunch pack'){echo "selected";}?>>Lunck Pack</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("content_in"); ?></label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" name="tickets_content_in" required><?php echo $edit_tickets->tickets_content_in;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("content_en"); ?></label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="5" name="tickets_content_en" required><?php echo $edit_tickets->tickets_content_en;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("price"); ?></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="tickets_price" id="tickets_prices" value="<?php echo $price;?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("discount"); ?></label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="discount" value="<?php echo $edit_tickets->discount;?>">
                        </div>
                        <label class="control-label">%</label>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $('#tickets_prices').priceFormat({
        prefix: 'Rp ',
        centsLimit: 0,
        centsSeparator: ', ',
        thousandsSeparator: '.'
    });
</script>