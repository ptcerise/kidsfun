<form method="POST" id="userForm" class="form-horizontal" enctype="multipart/form-data" role="form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?php echo $this->lang->line("detail_payment"); ?> <br></h4>
            </div>
            <div class="modal-body">
                <h4><i class="fa fa-user"></i> <?php echo $payment_detail->checkout_fname;?> (<?php echo $payment_detail->checkout_email;?>)</h4>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("date"); ?> :</label>
                            <?php $checkout_date = $payment_detail->checkout_date;?>
                            <input type="text" class="form-control" name="checkout_date" value="<?php echo date("d F Y", strtotime($checkout_date)); ?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("visit_date"); ?> :</label>
                            <?php $visit_date = $payment_detail->checkout_visit_date;?>
                            <input type="text" class="form-control" name="checkout_date" value="<?php echo date("d F Y", strtotime($visit_date)); ?>" readonly>
                        </div>
                    </div>                
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("first_name"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_fname" value="<?php echo $payment_detail->checkout_fname;?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("last_name"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_lname" value="<?php echo $payment_detail->checkout_lname;?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("phone"); ?></label>
                            <input type="text" class="form-control" name="checkout_phone" value="<?php echo $payment_detail->checkout_phone;?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("address"); ?></label>
                            <textarea class="form-control" rows="5" name="checkout_address" disabled><?php echo $payment_detail->checkout_address;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("city"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_city" value="<?php echo $payment_detail->checkout_city;?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("zip"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_zip" value="<?php echo $payment_detail->checkout_zip;?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("ticket"); ?></label>
                            <textarea class="form-control" rows="5" name="checkout_visit_date" disabled><?php foreach($data_tickets as $tickets) { ?><?php echo $tickets->tickets_name." : ".$tickets->chart_qty." Item(s) \r\n"; ?><?php } ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("total_pay"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_total" value="<?php echo $payment_detail->checkout_total;?>" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-offset-5 pull-left">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_close"); ?>"><?php echo $this->lang->line("btn_close"); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>