<form method="POST" id="userForm" action="<?php echo base_url('backend/checkout/edit_save');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?php echo $this->lang->line("edit_payment"); ?> <br></h4>
            </div>
            <div class="modal-body">
                <h4><i class="fa fa-user"></i> <?php echo $edit_payment->checkout_fname;?> (<?php echo $edit_payment->checkout_email;?>)</h4>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <?php $checkout_date = $edit_payment->checkout_date;?>
                            <label class="control-label"><?php echo $this->lang->line("date"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_date" value="<?php echo date("d F Y", strtotime($checkout_date)); ?>" readonly>
                            <input type="hidden" class="form-control" name="checkout_id" value="<?php echo $edit_payment->checkout_id;?>">
                        </div>
                        <div class="col-md-6">
                            <?php $visit_date = $edit_payment->checkout_visit_date;?>
                            <label class="control-label"><?php echo $this->lang->line("visit_date"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_visit_date" value="<?php echo date("d F Y", strtotime($visit_date)); ?>" readonly>
                        </div>
                    </div>                
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("first_name"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_fname" value="<?php echo $edit_payment->checkout_fname;?>" readonly>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("last_name"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_lname" value="<?php echo $edit_payment->checkout_lname;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("phone"); ?> :</label>
                            <input type="" class="form-control" name="checkout_phone" value="<?php echo $edit_payment->checkout_phone;?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("address"); ?> :</label>
                            <textarea class="form-control" rows="5" name="checkout_address"><?php echo $edit_payment->checkout_address;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("city"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_city" value="<?php echo $edit_payment->checkout_city;?>">
                        </div>
                        <div class="col-md-6">
                            <label class="control-label"><?php echo $this->lang->line("zip"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_zip" value="<?php echo $edit_payment->checkout_zip;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("ticket"); ?></label>
                            <textarea class="form-control" rows="5" name="checkout_visit_date" disabled><?php foreach($data_tickets as $tickets) { ?><?php echo $tickets->tickets_name." : ".$tickets->chart_qty." Item(s) \r\n"; ?><?php } ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("total_pay"); ?> :</label>
                            <input type="text" class="form-control" name="checkout_total" value="<?php echo $edit_payment->checkout_total;?>" readonly>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("btn_cancel"); ?></button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>