<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line("cart"); ?>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i>
                <a href="<?php echo base_url('backend/tickets');?>"> <?php echo $this->lang->line("tickets"); ?>
                </a>
            </li>
            <li>
                <?php echo $this->lang->line("cart"); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <?php echo $this->session->flashdata('info_rides');?>
        <div class="row">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/shopping_cart/header_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control" name="text_cart_in" rows="5" required><?php echo isset($content_in->general_content_in) ? $content_in->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control" name="text_cart_en" rows="5" required><?php echo isset($content_en->general_content_en) ? $content_en->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>        
            </div>
        </div>
    </section><!-- /.content -->
</div>