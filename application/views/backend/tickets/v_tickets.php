
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $this->lang->line("tickets"); ?>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> 
                <?php echo $this->lang->line("tickets"); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
			<div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line("banner"); ?></div>
                    <div class="panel-body">
                    <?php echo $this->session->flashdata('info_banner');?>
                        <div class="row">
                            <div class="col-md-8">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/tickets/banner_large');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("big_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <center>
                                                    <img class="img-responsive" src="<?php echo !empty($banner_lg->media_url) ? base_url('assets/upload/tickets/thumbnail').'/'.$banner_lg->media_url:base_url('assets/img/blank_img.png');?>" alt="tickets">
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="pic_tickets_lg" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/tickets/banner_small');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("small_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <center>
                                                    <img class="img-responsive" src="<?php echo !empty($banner_sm->media_url) ? base_url('assets/upload/tickets/thumbnail').'/'.$banner_sm->media_url:base_url('assets/img/blank_img.png');?>" alt="tickets">
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="pic_tickets_sm" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row" id="caption">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/tickets/banner_caption');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("caption"); ?></div>
                                        <div class="panel-body">
                                        <?php echo $this->session->flashdata('info_caption');?>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_in"); ?></p></label>
                                                                <textarea class="form-control" name="text_tickets_in" rows="5" required><?php echo isset($caption_in->general_content_in) ? $caption_in->general_content_in:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_en"); ?></p></label>
                                                                <textarea class="form-control" name="text_tickets_en" rows="5" required><?php echo isset($caption_en->general_content_en) ? $caption_en->general_content_en:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        <div class="row" id="content">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/tickets/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control" name="div_tickets_in" rows="5" required><?php echo isset($content_in->general_content_in) ? $content_in->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control" name="div_tickets_en" rows="5" required><?php echo isset($content_en->general_content_en) ? $content_en->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" id="tickets_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("tickets"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_tickets"); ?>" data-toggle="modal" data-target="#add_page"><i class="fa fa-plus"></i> <?php echo $this->lang->line("ticket"); ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_list');?>
                            <table class="table table-hover table-bordered" id="tickets">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("name_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("category_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("content_tabel_in"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("content_tabel_en"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("price_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("discount_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($tickets as $key => $row):?>
                                        <?php $char_en = strlen($row->tickets_content_en); ?>
                                        <?php $char_in = strlen($row->tickets_content_in); ?>
                                        <?php $price = $row->tickets_price; ?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->tickets_name;?></td>
                                        <td><?php echo $row->tickets_category;?></td>
                                        <td><?php echo substr($row->tickets_content_in,0,100);?> <?php if($char_in>=100) {echo "...";} elseif($char_in<100) {echo "";} ?></td>
                                        <td><?php echo substr($row->tickets_content_en,0,100);?> <?php if($char_en>=100) {echo "...";} elseif($char_en<100) {echo "";} ?></td>
                                        <td>Rp <?php echo number_format($price,0,', ','.') ?></td>
                                        <td><?php echo $row->discount;?></td>
                                        <td><center>
                                            <a href="#" id="<?php echo @$row->tickets_id;?>" class="btn btn-warning btn-xs edit-page" title="<?php echo $this->lang->line("edit_tickets"); ?>" data-target="#edit-page" data-toggle="modal"><i class="fa fa-edit"></i> <?php echo $this->lang->line("ticket"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/Tickets/delete_tickets/'.@$row->tickets_id);?>" class="btn-delete btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_tickets"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="add_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="userForm" action="<?php echo base_url('backend/Tickets/add_save');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("new_tickets"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">                
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("name"); ?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="tickets_name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("category"); ?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" name="tickets_category" required>
                                            <option value=""><?php echo $this->lang->line("choose_category"); ?></option>
                                            <option value="vip cards">Vip Cards</option>
                                            <option value="tickets">Tickets</option>
                                            <option value="birthday">Birthday</option>
                                            <option value="gokart">Gokart</option>
                                            <option value="package">Package</option>
                                            <option value="lunch pack">Lunch Pack</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("content_en"); ?></label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="tickets_content_en"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("content_in"); ?></label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" name="tickets_content_in"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("price"); ?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="tickets_price" name="tickets_price" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("discount"); ?></label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" name="discount">
                                    </div>
                                    <label class="control-label">%</label>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="edit-page" tabindex="-1" role="basic" aria-hidden="true"></div>
    </section><!-- /.content -->
</div>