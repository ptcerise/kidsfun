<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("contact_us"); ?>
        </h1>
        <ol class="breadcrumb">
			<li class="active">
                <i class="fa fa-dashboard"></i> 
                <?php echo $this->lang->line("contact_us"); ?>
            </li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line("banner"); ?></div>
                    <div class="panel-body">
                    <?php echo $this->session->flashdata('info_banner');?>
                        <div class="row">
                            <div class="col-md-8">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/banner_large');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("big_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <center>
                                                    <img class="img-responsive" src="<?php echo !empty($banner_lg->media_url) ? base_url('assets/upload/contact/thumbnail').'/'.$banner_lg->media_url:base_url('assets/img/blank_img.png'); ?>" alt="contact">
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="pic_contact_lg" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/banner_small');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("small_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <center>
                                                    <img class="img-responsive" src="<?php echo !empty($banner_sm->media_url) ? base_url('assets/upload/contact/thumbnail').'/'.$banner_sm->media_url:base_url('assets/img/blank_img.png'); ?>" alt="contact">
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="pic_contact_sm" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row" id="caption">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/banner_caption');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("caption"); ?></div>
                                        <div class="panel-body">
                                        <?php echo $this->session->flashdata('info_caption');?>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-md-12"> 
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_in"); ?></p></label>
                                                                <textarea class="form-control wysihtml5" name="text_contact_in" rows="5" required><?php echo isset($caption_in->general_content_in) ? $caption_in->general_content_in:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="col-md-12"> 
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_en"); ?></p></label>
                                                                <textarea class="form-control wysihtml5" name="text_contact_en" rows="5" required><?php echo isset($caption_en->general_content_en) ? $caption_en->general_content_en:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="content">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12"> 
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control wysihtml5" name="div_contact_in" rows="5" required><?php echo isset($content_in->general_content_in) ? $content_in->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12"> 
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control wysihtml5" name="div_contact_en" rows="5" required><?php echo isset($content_en->general_content_en) ? $content_en->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" id="message">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("message"); ?>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_message');?>
                            <table class="table table-hover table-bordered" id="contact">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("date"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("sender"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("phone"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("subject"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("status"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php foreach($message as $key => $row):?>
                                    <tr>
                                        <td><?php echo $key+1; ?></td>
                                        <td><?php echo date('d/m/y',strtotime($row->contact_date));?></td>
                                        <td><?php echo $row->contact_name;?></td>
                                        <td><?php echo $row->contact_phone;?></td>
                                        <td><?php echo $row->contact_subject;?></td>
                                        <td><center>
                                            <a href="#" id="<?php echo @$row->contact_id;?>" class="btn btn-success btn-xs view-page" title="<?php echo $this->lang->line("detail_message"); ?>" data-target="#view-page" data-toggle="modal"><i class="fa fa-file-text"></i> <?php echo $this->lang->line("detail"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/Contact_us/delete_message/'.@$row->contact_id);?>" class="btn-delete btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_message"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                        <td>
                                        <center>
                                            <p id="<?php echo @$key+1;?>"><i class='fa fa-circle' style='color:green'></i> <i><b>Unread</b></i></p>
                                        </center>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row" id="info">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/contact_us/info');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("info"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_contact');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("phone"); ?></p></label>
                                                <textarea class="form-control wysihtml5" name="phone" rows="5" required><?php echo isset($phone->general_content_en) ? $phone->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label"><p><?php echo $this->lang->line("address"); ?></p></label>
                                                <textarea class="form-control wysihtml5" name="address" rows="5" required><?php echo isset($address->general_content_en) ? $address->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                            <label class="control-label"><p>
                                                Google Map Link
                                            </p></label>
                                                <input class="form-control" name="location" rows="5" value="<?php echo isset($location->general_content_en) ? $location->general_content_en:''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="view-page" tabindex="-1" role="basic" aria-hidden="true"></div>
    </section><!-- /.content -->
</div>