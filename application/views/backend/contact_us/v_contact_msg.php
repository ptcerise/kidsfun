<form method="POST" id="userForm" class="form-horizontal" enctype="multipart/form-data" role="form">
    <div class="modal-dialog modal-md" id="modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onClick="location.reload()"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title"><?php echo $this->lang->line("detail_msg"); ?> <br></h4>
            </div>
            <div class="modal-body">
                <h4><i class="fa fa-user"></i> <?php echo $view_message->contact_name;?> (<?php echo $view_message->contact_email;?>)</h4>
                <div class="panel-body">                
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("from"); ?> :</label>
                            <label class="form-control" name="contact_name"><?php echo $view_message->contact_name;?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("date"); ?> :</label>
                            <label class="form-control" name="contact_date"><?php echo date('d/m/y', strtotime($view_message->contact_date));?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("subject"); ?> :</label>
                            <label class="form-control" name="contact_subject"><?php echo $view_message->contact_subject;?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label class="control-label"><?php echo $this->lang->line("message"); ?> :</label>
                            <textarea class="form-control" rows="5" name="contact_message" readonly><?php echo $view_message->contact_message;?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-offset-5 pull-left">
                            <button id="close-message" type="button" class="btn btn-danger" onClick="location.reload()"><?php echo $this->lang->line("btn_close"); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>