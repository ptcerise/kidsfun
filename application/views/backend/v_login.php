<!DOCTYPE html>
<html>
<head>
	<title>Login | KidsFun</title>
	<meta name="viewport" content="width=1360px, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.css">
</head>
<body>
	<section>
		<div class="logo"></div>
		<p class="welcome">Backend of Kidsfun Website</p>
		<div class="container">
			<form class="form-signin" role="form" method="POST" action="<?php echo base_url('backend/Login/process');?>">
			<?php echo $this->session->flashdata('Login');?>
				<input type="text" name="user_name" class="form-control" placeholder="Username" autocomplete="off" required autofocus>
				<input type="password" name="user_pass" class="form-control" placeholder="Password" required autocomplete="off">
				<center><button class="btn btn-lg btn-default" type="submit">Sign in</button></center>
			</form>
		</div>
	</section>
	<footer>
		<div class="line"></div>
	</footer>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>