<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            Events
        </h1>
        <ol class="breadcrumb">
			<li class="active"><a href="#"><i class="fa fa-dashboard"></i>Event</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
			<div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner Large ########## -->
                            <div class="col-sm-8">
                            <?php 
                                echo $this->session->flashdata('events_banner_large');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/events/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                              <img src="<?php echo !empty($banner_large->media_url)? site_url('assets/upload/events/thumbnail').'/'.$banner_large->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="banner Large" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="panel-footer">
                                         <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_large->media_id) ? $banner_large->media_id:''; ?>" name="banner_large_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Small ########## -->
                            <div class="col-sm-4">
                            <?php 
                                echo $this->session->flashdata('events_banner_small');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/events/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                              <img src="<?php echo !empty($banner_small->media_url) ? site_url('assets/upload/events/thumbnail').'/'.$banner_small->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="banner small" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_small->media_id) ? $banner_small->media_id:''; ?>" name="banner_small_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Caption ########## -->
                            <div class="col-sm-12" id="events_banner_caption">
                            <?php 
                                echo $this->session->flashdata('banner_caption');
                            ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/events/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="Content Indonesia" class="control-label">
                                                                <p><?php echo $this->lang->line("caption_in"); ?></p>
                                                            </label>
                                                            <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($banner_caption->general_content_in) ? $banner_caption->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="Content English" class="col-ntrol-label">
                                                                <p><?php echo $this->lang->line("caption_en"); ?></p>
                                                            </label>
                                                            <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($banner_caption->general_content_en) ? $banner_caption->general_content_en:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                             
                                
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_caption->general_id) ? $banner_caption->general_id: ''; ?>" name="general_id" >
                                </div>
                            </form>
                            </div>
                        </div>                  
                    </div>
                </div>
            </div>
            
<!-- ########## All events Content ########## -->
            <div class="col-xs-12" id="events_content">
            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/events/save_events_content" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("content"); ?>
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('events_content');
                        ?>
                        <div class="well">                            
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Content Indonesia" class="control-label">
                                                <p><?php echo $this->lang->line("content_in"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($events_content->general_content_in) ? $events_content->general_content_in:''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Content English" class="control-label">
                                                <p><?php echo $this->lang->line("content_en"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($events_content->general_content_en) ? $events_content->general_content_en:''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>                                                         
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button> 
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($events_content->general_id) ? $events_content->general_id: ''; ?>" name="general_id" >
                </div>
            </form>
            </div>

<!-- ########## Event Add + Edit + Delete ########## -->
            <div class="col-xs-12" id="events">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("events"); ?>
                        <button type="button" class="btn btn-sm btn-success pull-right btn-add" data-target="#events_add" role="input"  data-toggle="modal" title="add events"> 
                            <i class="fa fa-plus"></i>  Event
                        </button>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->session->flashdata('events_insert'); ?>
                        <?php echo $this->session->flashdata('events_update'); ?>
                        <?php echo $this->session->flashdata('events_delete'); ?>
                        
                        <div class="well">
                            <table class="table table-hover table-bordered" id="tbl_events"> 
                                <thead> 
                                    <tr> 
                                        <th>No</th>
                                        <th><?php echo $this->lang->line('date'); ?></th>
                                        <th><?php echo $this->lang->line('title_tabel_in'); ?></th>
                                        <th><?php echo $this->lang->line('title_tabel_en'); ?></th>
                                        <th><?php echo $this->lang->line('content_tabel_in'); ?></th>
                                        <th><?php echo $this->lang->line('content_tabel_en'); ?></th>
                                        <th><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i= 1;
                                    foreach ($events as $key) {
                                    ?>
                                    <tr>
                                        <td class="text-center"> <?php echo $i++; ?></td>
                                        <td class="text-center"> <?php echo date(' d/m/y',strtotime($key->events_date)) ?></td>
                                        <td class="text-center"> <?php echo $key->events_title_in ?></td>
                                        <td class="text-center"> <?php echo $key->events_title_en ?></td>
                                        <td class="text-center"> 
                                            <?php
                                                $data1 = $key->events_content_in;
                                                if(strlen($data1) <= 40){
                                                    echo $data1;
                                                }else{
                                                    echo substr($data1, 0, 40) ." ...";
                                                }
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                                $data2 = $key->events_content_en;
                                                if(strlen($data2) <= 40){
                                                    echo $data2;
                                                }else{
                                                    echo substr($data2, 0, 40) ." ...";
                                                }
                                            ?>
                                        </td>
                                        <td colspan="2" class="text-center">
                                             <button type="button" class="btn btn-xs btn-warning" data-target="#events_edit" id="btn_events_edit" role="input"  data-toggle="modal" data-id="<?php echo $key->events_id ?>" title="<?php echo $this->lang->line('edit'); ?>"> <i class="fa fa-pencil"></i></button> 
                                            <a href="<?php echo site_url('backend/events/events_delete/'.$key->events_id); ?>" class="btn btn-xs btn-danger" title="<?php echo $this->lang->line('btn_edit'); ?>"  onClick="return confirm('<?php echo $this->lang->line('confirm'); ?>');"> <i class="fa fa-trash-o"></i>
                                        </td>
                                    </tr>
                                    <?php 
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    
                    </div>
                </div>
            </div>

		</div>
    </section><!-- /.content -->
</div>

<!-- Events Add-->
<div id="events_add" class="modal modal-fade a" tab-index="-1" role="dialog"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_add"); ?> Event  </h4>
            </div>
            <div class="modal-body" id="events-add">
                
            </div>
        </div>
    </div>
</div>

<!-- Events edit -->
<div id="events_edit" class="modal modal-fade a" tab-index="-1" role="dialog"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_edit"); ?> Event </h4>
            </div>
            <div class="modal-body" id="events-edit">
                
            </div>
        </div>
    </div>
</div>