<section>
    <div class="row">
        <div class="col-sm-12">
            <form class="form-horizontal" method="POST" action="<?php echo site_url('backend/events/save_events_add'); ?>" accept-charset="utf-8" enctype="multipart/form-data" role="form" runat="server">
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Event date" class="control-label">
                                <?php echo $this->lang->line("date"); ?> Event
                            </label>
                               <input type="text" class="form-control tgl" name="events_date" id="event_add_tgl" required>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="control-label">
                                <?php echo $this->lang->line("title_in"); ?>
                            </label>
                               <input type="text" class="form-control" name="events_title_in" required>
                        </div>
                    </div>
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label for=""><?php echo $this->lang->line('content_in'); ?></label>
                            <textarea class="form-control" rows="7" name="events_content_in"></textarea>
                       </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="control-label">
                                <?php echo $this->lang->line("title_en"); ?>
                            </label>
                               <input type="text" class="form-control" name="events_title_en" required>
                        </div>
                    </div>
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label for=""><?php echo $this->lang->line('content_en'); ?></label>
                            <textarea class="form-control" rows="7" name="events_content_en"></textarea>
                       </div>
                    </div>
                </div>

                <div class="form-group pull-right">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save'); ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('btn_close'); ?></button>
                    </div>
                </div>
            </form>
        </div> 
    </div>    
</section>