<?php
  if($this->session->userdata('status_login') != TRUE){
    redirect('backend/Login');
  }
  $query = $this->db->query("SELECT * FROM contact WHERE contact_status='0'")->result();
  $unread_message = COUNT($query);
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Backend | <?php echo ucfirst($current); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico');?>">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Datepicker -->
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/css/bootstrap-datetimepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/skins/_all-skins.min.css">
  
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom_bo.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/css/bootstrap-datetimepicker.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">KF</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">KIDSFUN</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <?php if($this->session->userdata('be_lang') == 'indonesia'): ?>
                <a href="<?php echo base_url('backend/languageSwitcher/switchLang/english');?>" class="flag-size">
                  <img src="<?php echo site_url('assets/icon/english.png'); ?>">
                <?php else:?>
                <a href="<?php echo base_url('backend/languageSwitcher/switchLang/indonesia');?>" class="flag-size">
                  <img src="<?php echo site_url('assets/icon/indonesia.png'); ?>">
                    <?php endif; ?>
                </a>
          </li>

          <li class="dropdown messages-menu">
            <a href="<?php echo base_url('backend/contact_us#message');?>" class="dropdown-toggle">
              <i class="fa fa-envelope-o"></i> 
              <span class="label label-danger"><?php echo $unread_message; ?> <?php if($unread_message!=0) {echo $this->lang->line('message_received');} else {echo "";}?></span>
            </a>
          </li>
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php echo $this->session->userdata('user_full_name');?>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <p><?php echo $this->session->userdata('user_full_name');?></p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('backend/Profile');?>" class="btn btn-default btn-flat"><i class="fa fa-user"></i> <?php echo $this->lang->line('profile'); ?></a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('backend/Login/logout');?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> <?php echo $this->lang->line('sign_out'); ?></a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN MENU</li>
        <li <?php
              if ($current == "home") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/home">
            <i class="fa fa-home"></i> <span><?php echo $this->lang->line("home"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "All Attractions" || $current == "rides" || $current == "rides_single" || $current == "gokart" || $current == "aquasplash") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="#">
            <i class="fa fa-ellipsis-v"></i>
            <span><?php echo $this->lang->line("attractions"); ?></span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <!-- <li <?php
              if ($current == "All Attractions") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/all_attractions"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("all_attractions"); ?></a>
            </li> -->

            <li <?php
              if ($current == "rides") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/rides"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("rides"); ?></a>
            </li>
            <li <?php
              if ($current == "gokart") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/gokart"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("gokart"); ?></a>
            </li>

            <li <?php
              if ($current == "aquasplash") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/aquasplash"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("aquasplash"); ?></a>
            </li>
          </ul>
        </li>
        <li <?php
              if ($current == "All Shops" || $current == "restaurant" || $current == "souvenirs" || $current == "kiosks") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="#">
            <i class="fa fa-shopping-cart"></i>
            <span><?php echo $this->lang->line("shops"); ?></span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <!-- <li <?php
              if ($current == "All Shops") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/all_shops"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("all_shops"); ?></a>
            </li> -->

            <li <?php
              if ($current == "restaurant") {
                  echo "class='active treeview'";
              }
              ?>>
              <a href="<?php echo base_url();?>backend/restaurant"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("restaurants"); ?></a>
            </li>
            <li <?php
              if ($current == "souvenirs") {
                  echo "class='active treeview'";
              }
              ?>>
                <a href="<?php echo base_url();?>backend/souvenirs"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("souvenirs"); ?></a>
            </li>
            
            <li <?php
              if ($current == "kiosks") {
                  echo "class='active treeview'";
              }
              ?>>
                <a href="<?php echo base_url();?>backend/kiosks"><i class="fa fa-circle-o"></i> <?php echo $this->lang->line("kiosks"); ?></a>
            </li>
          </ul>
        </li>
        <li <?php
              if ($current == "shows") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/shows">
            <i class="fa fa-star"></i> <span><?php echo $this->lang->line("shows"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "events") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/events">
            <i class="fa fa-calendar"></i> <span><?php echo $this->lang->line("events"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "news") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/news">
            <i class="fa fa-newspaper-o"></i> <span><?php echo $this->lang->line("news"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "info") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/info">
            <i class="fa fa-info-circle"></i> <span><?php echo $this->lang->line("info"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "contact_us") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/contact_us">
            <i class="fa fa-phone-square"></i> <span><?php echo $this->lang->line("contact_us"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "find_us") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/find_us">
           <i class="fa fa-search"></i> <span><?php echo $this->lang->line("find_us"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "follow_us") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/follow_us">
           <i class="fa fa-user-plus"></i> <span><?php echo $this->lang->line("follow_us"); ?></span>
          </a>
        </li>
        <li <?php
              if ($current == "tickets" || $current == "cart" || $current == "checkout") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="#">
            <i class="fa fa-ticket"></i>
            <span><?php echo $this->lang->line("tickets_pack"); ?></span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php
                  if ($current == "tickets") {
                      echo "class='active treeview'";
                  }
                ?>>
              <a href="<?php echo base_url();?>backend/tickets">
                <i class="fa fa-circle-o"></i> <span><?php echo $this->lang->line("tickets"); ?></span>
              </a>
            </li>
            <li <?php
                  if ($current == "cart") {
                      echo "class='active treeview'";
                  }
                ?>>
              <a href="<?php echo base_url();?>backend/shopping_cart">
                <i class="fa fa-circle-o"></i> <span><?php echo $this->lang->line("cart"); ?></span>
              </a>
            </li>
            <li <?php
                  if ($current == "checkout") {
                      echo "class='active treeview'";
                  }
                ?>>
              <a href="<?php echo base_url();?>backend/checkout">
                <i class="fa fa-circle-o"></i> <span>Checkout</span>
              </a>
            </li>
          </ul>
        </li>
        <li <?php
              if ($current == "user") {
                  echo "class='active treeview'";
              }
            ?>>
          <a href="<?php echo base_url();?>backend/user">
           <i class="fa fa-user"></i> <span><?php echo $this->lang->line("user"); ?></span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  
  <?php echo $content;?>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs"></div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> Kids Fun | developed by <a href="http://ptcerise.com" target="blank">Cerise</a>.</strong> All rights reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

 <!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/js/bootstrap-datetimepicker.min.js"></script>
<?php echo @$script ?>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/js/app.min.js"></script>
<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- TinyMCE -->
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
  tinymce.init({
    selector: 'textarea',
    toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor',
    plugins: 'code link textcolor',
    menubar: false
  });
</script>

<!-- Price Format -->
<script src="<?php echo base_url();?>assets/js/jquery-priceformat.min.js"></script>
<!-- Datepicker Js -->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/js/bootstrap-datetimepicker.min.js"></script>

<script src="<?php echo base_url();?>assets/js/main.js"></script>
<!-- MD5 js -->
<script src="<?php echo base_url();?>assets/js/md5.js"></script>

</body>
</html>