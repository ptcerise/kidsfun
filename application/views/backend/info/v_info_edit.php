<form method="POST" id="userForm" action="<?php echo base_url('backend/Info/edit_save');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit Info</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">                
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("title_in"); ?></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="info_title_in" value="<?php echo $edit_info->info_title_in;?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("title_en"); ?></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="info_title_en" value="<?php echo $edit_info->info_title_en;?>" autofocus required>
                            <input type="hidden" class="form-control" name="info_id" value="<?php echo $edit_info->info_id;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("content_in"); ?></label>
                        <div class="col-md-8">
                            <textarea class="form-control modal_wysihtml5" rows="5" name="info_content_in" required><?php echo $edit_info->info_content_in;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo $this->lang->line("content_en"); ?></label>
                        <div class="col-md-8">
                            <textarea class="form-control modal_wysihtml5" rows="5" name="info_content_en" required><?php echo $edit_info->info_content_en;?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center>
                             <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>