<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("news"); ?> </h1>
        
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("news"); ?> </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner Large ########## -->
                            <div class="col-sm-8">
                            <?php 
                                echo $this->session->flashdata('news_banner_large');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/news/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                              <img src="<?php echo !empty($banner_large->media_url)? site_url('assets/upload/news/thumbnail').'/'.$banner_large->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="news banner Large" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                              <p><small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_large->media_id) ? $banner_large->media_id:''; ?>" name="banner_large_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Small ########## -->
                            <div class="col-sm-4">
                            <?php 
                                echo $this->session->flashdata('news_banner_small');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/news/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                              <img src="<?php echo !empty($banner_small->media_url) ? site_url('assets/upload/news/thumbnail').'/'.$banner_small->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                              <p><small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_small->media_id) ? $banner_small->media_id:''; ?>" name="banner_small_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Caption ########## -->
                            <div class="col-sm-12" id="news_banner_caption">
                            <?php 
                                echo $this->session->flashdata('news_banner_caption');
                            ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/news/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                             <div class="row">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="Content Indonesia" class="control-label">
                                                                <p><?php echo $this->lang->line("content_in"); ?></p>
                                                            </label>
                                                            <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($banner_caption->general_content_in) ? $banner_caption->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label for="Content English" class="control-label">
                                                                <p><?php echo $this->lang->line("content_en"); ?></p>
                                                            </label>
                                                            <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($banner_caption->general_content_en) ? $banner_caption->general_content_en:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                             
                                
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_caption->general_id) ? $banner_caption->general_id: ''; ?>" name="general_id" >
                                </div>
                            </form>
                            </div>
                    
                    </div>
                </div>
            </div>
            
<!-- ########## news content ##########-->
            <div class="col-xs-12" id="news_content">
            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/news/save_news_content" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("content"); ?>
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('news_content');
                        ?>
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title English" class="control-label">
                                                <p><?php echo $this->lang->line("content_in"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($news->general_content_in) ? $news->general_content_in:''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title English" class="control-label">
                                                <p><?php echo $this->lang->line("content_en"); ?></p>
                                            </label>
                                            <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($news->general_content_en) ? $news->general_content_en:''; ?></textarea>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>                                                         
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button> 
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($news->general_id) ? $news->general_id: ''; ?>" name="general_id" >
                </div>
            </form>
            </div>

<!-- ########## news Add + Edit + Delete ########## -->
            <div class="col-xs-12" id="news">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("news"); ?>
                        <button type="button" class="btn btn-sm btn-success pull-right btn-add" data-target="#news_add" role="input"  data-toggle="modal" title="add news"> 
                            <i class="fa fa-plus"></i>  <?php echo $this->lang->line("news"); ?>
                        </button>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->session->flashdata('banner_single_large'); ?>
                        <?php echo $this->session->flashdata('banner_single_small'); ?>
                        <?php echo $this->session->flashdata('banner_single_caption'); ?>
                        <?php echo $this->session->flashdata('news_update_banner'); ?>
                        <?php echo $this->session->flashdata('news_update_content'); ?>
                        <?php echo $this->session->flashdata('news_insert'); ?>
                        <?php echo $this->session->flashdata('news_update'); ?>
                        <?php echo $this->session->flashdata('news_delete'); ?>
                        <table class="table table-bordered table-striped" id="news_Table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th><?php echo $this->lang->line("title"); ?> Indonesia </th>
                                    <th><?php echo $this->lang->line("title"); ?> English</th>
                                    <th><?php echo $this->lang->line("content"); ?> Indonesia</th>
                                    <th><?php echo $this->lang->line("content"); ?> English</th>
                                    <th><?php echo $this->lang->line("action"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                        $no = 1;
                        foreach ($show as $key) {
                         ?>
                            <tr>
                                <td><?php echo $no++; ?>    </td>
                                <td><?php echo $key->news_title_in ?></td>
                                <td><?php echo $key->news_title_en ?></td>
                                <td>
                                    <?php
                                        $data1 = $key->news_content_in;
                                        if(strlen($data1) <= 40){
                                            echo $data1;
                                        }else{
                                            echo substr($data1, 0, 40) ." ...";
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                        $data2 = $key->news_content_en;
                                        if(strlen($data2) <= 40){
                                            echo $data2;
                                        }else{
                                            echo substr($data2, 0, 40) ." ...";
                                        }
                                    ?>
                                </td>
                                <td colspan="2" class="text-center">
                                    <button type="button" class="btn btn-xs btn-warning" data-target="#news_edit_banner" id="btn_news_edit_banner" role="input"  data-toggle="modal" data-banner="<?php echo $key->news_id ?>" title="Edit Banner"> <i class="fa fa-edit"></i> Banner</button> 

                                    <button type="button" class="btn btn-xs btn-warning" data-target="#news_edit_content" id="btn_news_edit_content" role="input"  data-toggle="modal" data-id="<?php echo $key->news_id ?>" title="Edit news Content"> <i class="fa fa-edit"></i> News Content</button> 

                                    <a href="<?php echo site_url('backend/news/delete/'.$key->news_id); ?>" class="btn btn-xs btn-danger" title="<?php echo $this->lang->line('btn_edit'); ?>"  onClick="return confirm('<?php echo $this->lang->line('confirm'); ?>');"> <i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

		</div>
    </section><!-- /.content -->
</div>

<!-- Modal news add -->
<div class="modal fade" id="news_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("add_news"); ?> </h4>
            </div>
            <div class="modal-body" id="news-add">

            </div>
        </div>
    </div>
</div>

<!-- Modal News Banner -->
<div class="modal fade" id="news_edit_banner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> 
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_edit"); ?> Banner </h4>
            </div>
            <div class="modal-body" id="news-edit-banner">

            </div>
        </div>
    </div>
</div>


<!-- Modal News Content -->
<div class="modal fade" id="news_edit_content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_edit"); ?> Content </h4>
            </div>
            <div class="modal-body" id="news-edit-content">

            </div>
        </div>
    </div>
</div>