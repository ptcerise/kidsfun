<section>
    <div class="row">
    <form class="form-horizontal" action="<?php echo site_url();?>backend/news/save" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
        <div class="col-xs-12">
            <div class="form-group col-xs-12">
                <label for=""> <i class="fa fa-plus-circle"></i>  Banner</label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="well">
                <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('choose_banner_large'); ?></label>
                    <div class="col-xs-8">
                       <input type="file" name="banner_large" class="form-control" required>
                       <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                       <br>
                       <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                       <br>
                       <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('choose_banner_small'); ?></label>
                    <div class="col-xs-8">
                       <input type="file" name="banner_small" class="form-control" required> 
                       <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                       <br>
                       <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                       <br>
                       <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Title Indonesia" class="col-sm-4 control-label">
                        <?php echo $this->lang->line("title_in"); ?>
                    </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="media_title_in" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Title Indonesia" class="col-sm-4 control-label">
                        <?php echo $this->lang->line("title_en"); ?>
                    </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="media_title_en" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('content_in'); ?></label>
                    <div class="col-xs-8">
                       <textarea class="form-control wysihtml5_news" rows="7" name="media_content_in"></textarea> 
                    </div>
                </div>
                 <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('content_en'); ?></label>
                    <div class="col-xs-8">
                       <textarea class="form-control wysihtml5_news" rows="7" name="media_content_en"></textarea> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group col-xs-12">
                <label for=""> <i class="fa fa-plus-circle"></i>  <?php echo $this->lang->line('content'); ?></label>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="well">
                <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('choose_image1'); ?></label>
                    <div class="col-xs-8">
                        <input type="file" name="news_url1" class="form-control" required>
                        <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                        <br>
                        <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                        <br>
                        <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('choose_image2'); ?></label>
                    <div class="col-xs-8">
                       <input type="file" name="news_url2" class="form-control" required> 
                       <p>
                       <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                       <br>
                       <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                       <br>
                       <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Title Indonesia" class="col-sm-4 control-label">
                        <?php echo $this->lang->line("title_in"); ?>
                    </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="news_title_in" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Title Indonesia" class="col-sm-4 control-label">
                        <?php echo $this->lang->line("title_en"); ?>
                    </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="news_title_en" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('content_in'); ?></label>
                    <div class="col-xs-8">
                       <textarea class="form-control wysihtml5_news" rows="7" name="news_content_in"></textarea> 
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-xs-4"><?php echo $this->lang->line('content_en'); ?></label>
                    <div class="col-xs-8">
                        <textarea class="form-control wysihtml5_news" rows="7" name="news_content_en"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="form-group pull-right">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save'); ?></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('btn_cancel'); ?></button>
                </div>
            </div>
        </div>
        </form>
    </div>
</section>