
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit <?php echo $this->lang->line("shows"); ?></h4>
            </div>
            <div class="modal-body">
                <form method="POST" id="userForm" action="<?php echo base_url('backend/Shows_single/edit_shows');?>" class="form-horizontal" enctype="multipart/form-data" role="form" runat="server">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">               
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("title_in"); ?></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="show_title_in" value="<?php echo $edit_shows->show_title_in;?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("title_en"); ?></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="show_title_en" value="<?php echo $edit_shows->show_title_en;?>" required>
                                    <input type="hidden" class="form-control" name="show_id" value="<?php echo $edit_shows->show_id;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("content_in"); ?></label>
                                <div class="col-md-8">
                                    <textarea class="form-control modal_wysihtml5" rows="5" name="show_content_in" required><?php echo $edit_shows->show_content_in;?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("content_en"); ?></label>
                                <div class="col-md-8">
                                    <textarea class="form-control modal_wysihtml5" rows="5" name="show_content_en" required><?php echo $edit_shows->show_content_en;?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
                <form method="POST" id="userForm" action="<?php echo base_url('backend/Shows_single/save_image');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <?php echo $this->lang->line("content_image"); ?>
                        </div>
                        <div class="panel-body">                
                            <div class="row">
                                <?php $i=1 ?>                            
                                <?php foreach($images as $data):?>
                                <div class="col-md-6">
                                    <center>
                                    <img class="img-responsive img-thumbnail" src="<?php echo base_url('assets/upload/shows/shows_single/thumbnail/'.@$data->media_url);?>">
                                    </center>
                                    <input type="file" name="shows_img<?php echo $i;?>" class="form-control">
                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                    <input type="hidden" name="media_id<?php echo $i;?>" value="<?php echo $data->media_id;?>">
                                </div>
                                <?php $i++;?>
                                <?php endforeach;?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <center>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_close"); ?>"><?php echo $this->lang->line("btn_close"); ?></button>
                </center>
            </div>
        </div>
    </div>
</form>