    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit <?php echo $this->lang->line("rides"); ?></h4>
            </div>
            <div class="modal-body">
                <form method="POST" id="userForm" action="<?php echo base_url('backend/Rides_single/edit_rides');?>" class="form-horizontal" enctype="multipart/form-data" role="form" runat="server">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("desc"); ?></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rides_name" value="<?php echo $edit_rides->rides_name;?>" autofocus required>
                                    <input type="hidden" class="form-control" name="rides_id" value="<?php echo $edit_rides->rides_id;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("category"); ?></label>
                                <div class="col-md-4">
                                    <select class="form-control" name="rides_category" value="<?php echo $rides_category;?>" required>
                                        
                                        <option value="kids" <?php if($edit_rides->rides_category =='kids'){echo "selected";}?>>Kids</option>
                                        <option value="family" <?php if($edit_rides->rides_category =='family'){echo "selected";}?>>Family</option>
                                        <option value="thrill" <?php if($edit_rides->rides_category =='thrill'){echo "selected";}?>>Thrill</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("desc_in"); ?></label>
                                <div class="col-md-8">
                                    <textarea class="form-control modal_wysihtml5" rows="5" name="rides_desc_in"><?php echo $edit_rides->rides_desc_in;?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("desc_en"); ?></label>
                                <div class="col-md-8">
                                    <textarea class="form-control modal_wysihtml5" rows="5" name="rides_desc_en"><?php echo $edit_rides->rides_desc_en;?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
                <form method="POST" id="userForm" class="form-horizontal" action="<?php echo base_url('backend/Rides_single/save_image');?>" enctype="multipart/form-data" role="form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("content_image"); ?>
                                </div>
                                <div class="panel-body">                
                                    <div class="row">
                                        <?php $i=1 ?>                            
                                        <?php foreach($images as $data):?>
                                        <div class="col-md-6">
                                        <center>
                                            <img class="img-responsive img-thumbnail" src="<?php echo base_url('assets/upload/attractions/rides_single/thumbnail/'.@$data->img_url);?>">
                                        </center>
                                            <input type="file" name="rides_img<?php echo $i;?>" class="form-control">
                                            <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                            <br>
                                            <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                            <br>
                                            <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                            <input type="hidden" name="img_id<?php echo $i;?>" value="<?php echo $data->img_id;?>">
                                        </div>
                                        <?php $i++;?>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_close"); ?>"><?php echo $this->lang->line("btn_close"); ?></button>
                    </center>
                </div>
            </div>
        </div>
    </div>
</form>