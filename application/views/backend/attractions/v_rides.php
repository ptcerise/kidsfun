<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("rides"); ?>
        </h1>
        <ol class="breadcrumb">
			<li class="active">
                <i class="fa fa-dashboard"></i>
                <a href="<?php echo base_url('backend/all_attractions');?>"> <?php echo $this->lang->line("all_attractions"); ?>
                </a>
            </li>
            <li>
                <?php echo $this->lang->line("rides"); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line("banner"); ?></div>
                    <div class="panel-body">
                    <?php echo $this->session->flashdata('info_banner');?>
                        <div class="row">
                            <div class="col-md-8">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/rides/banner_large');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("big_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <img class="img-responsive img-center" src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/rides/thumbnail').'/'.$banner_large->media_url:base_url('assets/img/blank_img.png'); ?>" alt="rides">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="pic_rides_lg" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small><br>
                                                    <small class="color-red"><?php echo $this->lang->line("image_1920px"); ?></small><br>
                                                    <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/rides/banner_small');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("small_image"); ?></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-md-12 text-center">
                                                    <img class="img-responsive img-center" src="<?php echo !empty($banner_small->media_url) ? base_url('assets/upload/attractions/rides/thumbnail').'/'.$banner_small->media_url:base_url('assets/img/blank_img.png'); ?>" alt="rides">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                    <input type="file" name="pic_rides_sm" accept="image/gif, image/x-png, image/jpeg">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row" id="caption">
                            <div class="col-md-12">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/rides/banner_caption');?>" class="form-horizontal" role="form">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><?php echo $this->lang->line("caption"); ?></div>
                                        <div class="panel-body">
                                        <?php echo $this->session->flashdata('info_caption');?>
                                            <div class="well">
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_in"); ?></p></label>
                                                                <textarea class="form-control textarea" name="text_rides_in" rows="5" required><?php echo isset($caption_in->general_content_in) ? $caption_in->general_content_in:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <label class="control-label"><p><?php echo $this->lang->line("caption_en"); ?></p></label>
                                                                <textarea class="form-control textarea" name="text_rides_en" rows="5" required><?php echo isset($caption_en->general_content_en) ? $caption_en->general_content_en:''; ?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="content"> 
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/rides/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?> <?php echo $this->lang->line("rides"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="text_content_in" rows="5" required><?php echo isset($content_in->general_content_in) ? $content_in->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="text_content_en" rows="5" required><?php echo isset($content_en->general_content_en) ? $content_en->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row" id="rides_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("rides_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_rides"); ?>" data-toggle="modal" data-target="#add_page"><i class="fa fa-plus"></i> Ride</a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_list');?>
                            <table class="table table-hover table-bordered" id="rides">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("name_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("category_tabel"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_tabel_in"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_tabel_en"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($rides_single as $key => $row):?>
                                        <?php $char_en = strlen($row->rides_desc_en); ?>
                                        <?php $char_in = strlen($row->rides_desc_in); ?>

                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->rides_name;?></td>
                                        <td><?php echo $row->rides_category;?></td>
                                        <td><?php echo substr($row->rides_desc_in,0,100);?> <?php if($char_in>=100) {echo "...";} elseif($char_in<100) {echo "";} ?></td>
                                        <td><?php echo substr($row->rides_desc_en,0,100);?> <?php if($char_en>=100) {echo "...";} elseif($char_en<100) {echo "";} ?></td>
                                        <td><center>
                                            <a href="#" id="<?php echo @$row->rides_id;?>" class="btn btn-warning btn-xs edit-image-bg" title="<?php echo $this->lang->line("edit_banner"); ?>" data-target="#edit-image-bg" data-toggle="modal"><i class="fa fa-edit"></i> Banner</a>

                                            <a href="#" id="<?php echo @$row->rides_id;?>" class="btn btn-warning btn-xs edit-page" title="<?php echo $this->lang->line("edit_rides"); ?>" data-target="#edit-page" data-toggle="modal"><i class="fa fa-edit"></i> Ride</a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/Rides_single/delete_rides/'.@$row->rides_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_rides"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row" id="map"> 
            <div class="col-md-12">
                <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/rides_single/edit_map" accept-charset="utf-8" enctype="multipart/form-data">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <?php echo $this->lang->line("map"); ?>
                        </div>
                        <div class="panel-body">
                            <?php 
                                echo $this->session->flashdata('info_map');
                            ?>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                  <img src="<?php echo !empty($rides_map->media_url)? base_url('assets/upload/attractions/rides_single/thumbnail').'/'.$rides_map->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <label class="control-label">
                                        <?php echo $this->lang->line("choose_image"); ?>
                                    </label>
                                    <input type="file" name="rides_map">
                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("image_1200px"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                    <input type="hidden" value="<?php echo isset($rides_map->media_id) ? $rides_map->media_id: ''; ?>" name="map_id" >
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                    <?php echo $this->lang->line("btn_update"); ?>
                                </button> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal fade" id="add_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="userForm" action="<?php echo base_url('backend/Rides_single/add_rides');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("new_rides"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("name"); ?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="rides_name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo $this->lang->line("category"); ?></label>
                                    <div class="col-md-4">
                                        <select class="form-control" name="rides_category" required>
                                            <option value=""><?php echo $this->lang->line("choose_category"); ?></option>
                                            <option value="kids">Kids</option>
                                            <option value="family">Family</option>
                                            <option value="thrill">Thrill</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <span><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line("banner"); ?></span>
                                    <div class="well">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("choose_banner_large"); ?></label>
                                            <div class="col-md-8">
                                                <input type="file" class="form-control" name="rides_single_lg" required>
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("choose_banner_small"); ?></label>
                                            <div class="col-md-8">
                                                <input type="file" class="form-control" name="rides_single_sm" required>
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_in"); ?></label>
                                            <div class="col-md-8">
                                                <textarea class="form-control textarea" rows="5" name="text_single_in"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_en"); ?></label>
                                            <div class="col-md-8">
                                                <textarea class="form-control textarea" rows="5" name="text_single_en"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <span><i class="fa fa-plus-circle"></i> <?php echo $this->lang->line("content"); ?></span>
                                    <div class="well">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("content_image"); ?></label>
                                            <div class="col-md-8">
                                                <input type="file" class="form-control" name="rides_img[]" required>
                                                <input type="file" class="form-control" name="rides_img[]" required>
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("desc_in"); ?></label>
                                            <div class="col-md-8">
                                                <textarea class="form-control textarea" rows="5" name="rides_desc_in"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("desc_en"); ?></label>
                                            <div class="col-md-8">
                                                <textarea class="form-control textarea" rows="5" name="rides_desc_en"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <center>
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="edit-page" tabindex="-1" role="basic" aria-hidden="true"></div>
        <div class="modal fade" id="edit-image-bg" tabindex="-1" role="basic" aria-hidden="true"></div>
    </section>
</div>