<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title"><?php echo $rides_name->rides_name; ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-7">
                    <form method="POST" id="userForm1" action="<?php echo base_url('backend/Rides_single/banner_large');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <?php echo $this->lang->line("banner"); ?> <?php echo $this->lang->line("big_image"); ?>
                            </div>
                            <div class="panel-body"> 
                                <div class="col-md-12">               
                                    <div class="form-group">
                                        <center>
                                        <img class="img-responsive img-thumbnail" src="<?php echo base_url('assets/upload/attractions/rides_single/thumbnail/'.$banner_lg->media_url)?>" alt="rides">
                                        </center>
                                    </div>
                                    <div class="form-group">
                                        <label for="banner Large" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                            <input type="file" name="rides_single_lg" accept="image/gif, image/x-png, image/jpeg">
                                            <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                            <br>
                                            <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                            <br>
                                            <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            <input type="hidden" name="media_id_lg" value="<?php echo $banner_lg->media_id;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group panel-save">
                                    <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <form method="POST" id="userForm2" action="<?php echo base_url('backend/Rides_single/banner_small');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <?php echo $this->lang->line("banner"); ?> <?php echo $this->lang->line("small_image"); ?>
                            </div>
                            <div class="panel-body">                
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <center>
                                        <img class="img-responsive img-thumbnail" src="<?php echo base_url('assets/upload/attractions/rides_single/thumbnail/'.$banner_sm->media_url)?>" alt="rides">
                                        </center>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="banner Large" class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="file" name="rides_single_sm" accept="image/gif, image/x-png, image/jpeg">
                                        <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                        <input type="hidden" name="media_id_sm" value="<?php echo $banner_sm->media_id;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group panel-save">
                                    <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="row">
                <form method="POST" id="userForm1" action="<?php echo base_url('backend/Rides_single/edit_caption');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("caption"); ?></div>
                            <div class="panel-body">
                                <div class="well">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_in"); ?></label>
                                            <div class="col-md-8">
                                                <textarea class="form-control modal_wysihtml5" rows="5" name="text_single_in" required><?php echo $edit_caption->media_content_in;?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_en"); ?></label>
                                            <div class="col-md-8">
                                                <textarea class="form-control modal_wysihtml5" rows="5" name="text_single_en" required><?php echo $edit_caption->media_content_en;?></textarea>
                                                <input type="hidden" class="form-control" name="rides_id" value="<?php echo $rides_name->rides_id;?>">
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group panel-save">
                                    <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <center>
                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_close"); ?>"><?php echo $this->lang->line("btn_close"); ?></button>
            </center>
        </div>
    </div>
</div>