<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Gokart
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url();?>backend/all_attractions"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line("all_attractions"); ?></a></li>
            <li class="active"> Gokart</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner Large ########## -->
                            <div class="col-sm-8">
                            <?php 
                                echo $this->session->flashdata('gokart_banner_large');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/gokart/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner_large->media_url)? site_url('assets/upload/attractions/gokart/thumbnail').'/'.$banner_large->media_url :  base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="banner Large" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_large->media_id) ? $banner_large->media_id:''; ?>" name="banner_large_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Small ########## -->
                            <div class="col-sm-4">
                            <?php 
                                echo $this->session->flashdata('gokart_banner_small');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/gokart/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner_small->media_url) ? site_url('assets/upload/attractions/gokart/thumbnail').'/'.$banner_small->media_url:  base_url('assets/img/blank_img.png'); ?>" alt=" " class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="banner small" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_small->media_id) ? $banner_small->media_id:''; ?>" name="banner_small_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Caption ########## -->
                            <div class="col-sm-12" id="gokart_banner_caption">
                            <?php 
                                echo $this->session->flashdata('gokart_banner_caption');
                            ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/gokart/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content Indonesia" class="control-label">
                                                            <p><?php echo $this->lang->line("content_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($banner_caption->general_content_in) ? $banner_caption->general_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("content_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($banner_caption->general_content_en) ? $banner_caption->general_content_en:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_caption->general_id) ? $banner_caption->general_id: ''; ?>" name="general_id" >
                                </div>
                            </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

<!-- ########## gokart ########## -->
            <div class="col-xs-12" id="gokart">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Gokart
                    </div>
                    <div class="panel-body">
                        <?php
                            $aflag = 1;
                            $x = 1;
                            $y = 1;
                            $z = 1;
                            foreach ($gokart_image as $key ) {
                                 # code...
                        ?>
                        <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/gokart/save_gokart_image" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="col-sm-3" id="gokart_image<?php echo $x++; ?>">
                            <?php 
                                echo $this->session->flashdata('gokart_image'.$y++);
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("image"); ?> <?php echo $z++ ?>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="col-xs-12">
                                                <img src="<?php echo !empty($key->attraction_url) ? base_url('assets/upload/attractions/gokart').'/'.$key->attraction_url :  base_url('assets/img/blank_img.png'); ?> " class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title Indonesia" class="control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <input type="file" class="form-control" name="attraction_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1024px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" name="attraction_id" value="<?php echo isset($key->attraction_id) ? $key->attraction_id: ''; ?>">
                                <input type="hidden" value="<?php echo $aflag++; ?>" name="aflag">
                            </div>
                        </div>
                        </form>
                        <?php
                             } 
                        ?>
<!-- ########## gokart Content ########## -->
                        <div class="col-sm-12" id="gokart_content">
                            <?php echo $this->session->flashdata('gokart_content'); ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/gokart/save_gokart_content" accept-charset="utf-8" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("content"); ?>
                                </div>
                                <div class="panel-body">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                    <label for="Content Indonesia" class="control-label">
                                                        <p><?php echo $this->lang->line("content_in"); ?></p>
                                                    </label>
                                                      <textarea class="form-control" rows="10"  name="attraction_content_in"><?php echo isset($gokart_content->attraction_content_in) ? $gokart_content->attraction_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                    <label for="Content English" class="cocontrol-label">
                                                        <p><?php echo $this->lang->line("content_en"); ?></p>
                                                    </label>
                                                      <textarea class="form-control" rows="10"  name="attraction_content_en"><?php echo isset($gokart_content->attraction_content_en) ? $gokart_content->attraction_content_en:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo isset($gokart_content->attraction_id) ? $gokart_content->attraction_id: ''; ?>" name="attraction_id" >
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            
<!-- ########## karts ########## -->
            <div class="col-xs-12" id="gokart_karts">
                <div class="panel panel-info">
                    <div class="panel-heading">
                            karts
                        <button type="button" class="btn btn-sm btn-success pull-right btn-add" data-target="#karts_add" role="input"  data-toggle="modal" title="add gokart"> 
                            <i class="fa fa-plus"></i>  karts
                        </button>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->session->flashdata('karts_insert'); ?>
                        <?php echo $this->session->flashdata('karts_update'); ?>
                        <?php echo $this->session->flashdata('karts_delete'); ?>
                        
                        <div class="well">
                            <table class="table table-hover table-bordered" id="tbl_karts"> 
                                <thead> 
                                    <tr> 
                                        <th>No</th>
                                        <th><?php echo $this->lang->line('date'); ?></th>
                                        <th><?php echo $this->lang->line('title_tabel_in'); ?></th>
                                        <th><?php echo $this->lang->line('title_tabel_en'); ?></th>
                                        <th><?php echo $this->lang->line('content_tabel_in'); ?></th>
                                        <th><?php echo $this->lang->line('content_tabel_en'); ?></th>
                                        <th><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i= 1;
                                    foreach ($karts as $key) {
                                    ?>
                                    <tr>
                                        <td class="text-center"> <?php echo $i++; ?></td>
                                        <td class="text-center"> <?php echo date(' d/m/y',strtotime($key->attraction_date)) ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_title_in ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_title_en ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_content_in ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_content_en ?></td>
                                        <td colspan="2" class="text-center">
                                             <button type="button" class="btn btn-xs btn-warning" data-target="#karts_edit" id="btn_karts_edit" role="input"  data-toggle="modal" data-id="<?php echo $key->attraction_id ?>" title="<?php echo $this->lang->line('edit'); ?>"> <i class="fa fa-pencil"></i></button> 
                                            <a href="<?php echo site_url('backend/gokart/karts_delete/'.$key->attraction_id); ?>" class="btn btn-xs btn-danger" title="<?php echo $this->lang->line('btn_delete'); ?>"  onClick="return confirm('<?php echo $this->lang->line('confirm'); ?>');"> <i class="fa fa-trash-o"></i>
                                        </td>
                                    </tr>
                                    <?php 
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    
                    </div>
                </div>
            </div>

<!-- ########## tracks ########## -->
            <div class="col-xs-12" id="tracks">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Tracks
                    </div>
                    <div class="panel-body">
<!-- ########## tracks Image 1 ########## -->
                        <?php
                            $pflag = 1;
                            $a = 1;
                            $b = 1;
                            $c = 1;
                            foreach ($tracks_image as $key ) {
                                 # code...
                        ?>
                        <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/gokart/save_tracks_image" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="col-sm-3" id="tracks_image<?php echo $a++; ?>">
                            <?php 
                                echo $this->session->flashdata('tracks_image'.$b++);
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("image"); ?> <?php echo $c++ ?>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="col-xs-12">
                                                <img src="<?php echo !empty($key->attraction_url) ? base_url('assets/upload/attractions/gokart').'/'.$key->attraction_url :  base_url('assets/img/blank_img.png'); ?> " class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title Indonesia" class="control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <input type="file" class="form-control" name="attraction_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_512px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" name="attraction_id" value="<?php echo isset($key->attraction_id) ? $key->attraction_id: ''; ?>">
                                <input type="hidden" value="<?php echo $pflag++; ?>" name="pflag">
                            </div>
                        </div>
                        </form>
                        <?php
                             } 
                        ?>
<!-- ########## tracks Content ########## -->
                        <div class="col-sm-12" id="tracks_content">
                            <?php echo $this->session->flashdata('tracks_content'); ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/gokart/save_tracks_content" accept-charset="utf-8" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("content"); ?>
                                </div>
                                <div class="panel-body">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content Indonesia" class="control-label">
                                                            <p><?php echo $this->lang->line("content_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="attraction_content_in"><?php echo isset($tracks_content->attraction_content_in) ? $tracks_content->attraction_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("content_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="attraction_content_en"><?php echo isset($tracks_content->attraction_content_en) ? $tracks_content->attraction_content_en:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo isset($tracks_content->attraction_id) ? $tracks_content->attraction_id: ''; ?>" name="attraction_id" >
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section><!-- /.content -->
</div>

<!-- karts Add-->
<div id="karts_add" class="modal modal-fade a" tab-index="-1" role="dialog"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"> <?php echo $this->lang->line("btn_add"); ?> karts </h4>
            </div>
            <div class="modal-body" id="karts-add">
                
            </div>
        </div>
    </div>
</div>

<!-- karts edit -->
<div id="karts_edit" class="modal modal-fade a" tab-index="-1" role="dialog"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_edit"); ?> karts </h4>
            </div>
            <div class="modal-body" id="karts-edit">
                
            </div>
        </div>
    </div>
</div>