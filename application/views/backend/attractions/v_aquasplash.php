<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            Aquasplash
        </h1>
        <ol class="breadcrumb">
			<li><a href="<?php echo site_url();?>backend/all_attractions"><i class="fa fa-dashboard"></i> <?php echo $this->lang->line("all_attractions"); ?></a></li>
            <li class="active"> Aquasplash</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner Large ########## -->
                            <div class="col-sm-8">
                            <?php 
                                echo $this->session->flashdata('aquasplash_banner_large');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/aquasplash/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner_large->media_url) ? base_url('assets/upload/attractions/aquasplash/thumbnail').'/'.$banner_large->media_url :  base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="banner Large" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1920px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_large->media_id) ? $banner_large->media_id:''; ?>" name="banner_large_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Small ########## -->
                            <div class="col-sm-4">
                            <?php 
                                echo $this->session->flashdata('aquasplash_banner_small');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/aquasplash/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner_small->media_url) ? site_url('assets/upload/attractions/aquasplash/thumbnail').'/'.$banner_small->media_url :  base_url('assets/img/blank_img.png'); ?>" alt=" " class="img-responsive img-center">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="banner small" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_small->media_id) ? $banner_small->media_id:''; ?>" name="banner_small_id">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner Caption ########## -->
                            <div class="col-sm-12" id="aquasplash_banner_caption">
                            <?php 
                                echo $this->session->flashdata('aquasplash_banner_caption');
                            ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/aquasplash/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content Indonesia" class="control-label">
                                                            <p><?php echo $this->lang->line("content_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="general_content_in"><?php echo isset($banner_caption->general_content_in) ? $banner_caption->general_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("content_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="general_content_en"><?php echo isset($banner_caption->general_content_en) ? $banner_caption->general_content_en:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner_caption->general_id) ? $banner_caption->general_id: ''; ?>" name="general_id" >
                                </div>
                            </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

<!-- ########## Aquasplash ########## -->
            <div class="col-xs-12" id="aquasplash">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Aquasplash
                    </div>
                    <div class="panel-body">
                        <?php
                            $aflag = 1;
                            $x = 1;
                            $y = 1;
                            $z = 1;
                            foreach ($aquasplash_image as $key ) {
                                 # code...
                        ?>
                        <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/aquasplash/save_quasplash_image" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="col-sm-3" id="aquasplash_image<?php echo $x++; ?>">
                            <?php 
                                echo $this->session->flashdata('aquasplash_image'.$y++);
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("image"); ?> <?php echo $z++ ?>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="col-xs-12">
                                                <img src="<?php echo !empty($key->attraction_url) ? base_url('assets/upload/attractions/aquasplash').'/'.$key->attraction_url :  base_url('assets/img/blank_img.png'); ?> " class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title Indonesia" class="control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <input type="file" class="form-control" name="attraction_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1024px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" name="attraction_id" value="<?php echo isset($key->attraction_id) ? $key->attraction_id: ''; ?>">
                                <input type="hidden" value="<?php echo $aflag++; ?>" name="aflag">
                            </div>
                        </div>
                        </form>
                        <?php
                             } 
                        ?>
<!-- ########## Aquasplash Content ########## -->
                        <div class="col-sm-12" id="aquasplash_content">
                            <?php echo $this->session->flashdata('aquasplash_content'); ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/aquasplash/save_aquasplash_content" accept-charset="utf-8" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("content"); ?>
                                </div>
                                <div class="panel-body">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content Indonesia" class="control-label">
                                                            <p><?php echo $this->lang->line("content_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="attraction_content_in"><?php echo isset($aquasplash_content->attraction_content_in) ? $aquasplash_content->attraction_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("content_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="attraction_content_en"><?php echo isset($aquasplash_content->attraction_content_en) ? $aquasplash_content->attraction_content_en:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo isset($aquasplash_content->attraction_id) ? $aquasplash_content->attraction_id: ''; ?>" name="attraction_id" >
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            
<!-- ########## Slides ########## -->
            <div class="col-xs-12" id="aquasplash_slides">
                <div class="panel panel-info">
                    <div class="panel-heading">
                            Slides
                        <button type="button" class="btn btn-sm btn-success pull-right btn-add" data-target="#slides_add" role="input"  data-toggle="modal" title="add aquasplash"> 
                            <i class="fa fa-plus"></i>  Slides
                        </button>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->session->flashdata('slides_insert'); ?>
                        <?php echo $this->session->flashdata('slides_update'); ?>
                        <?php echo $this->session->flashdata('slides_delete'); ?>
                        
                        <div class="well">
                            <table class="table table-hover table-bordered" id="tbl_slides"> 
                                <thead> 
                                    <tr> 
                                        <th>No</th>
                                        <th><?php echo $this->lang->line('date'); ?></th>
                                        <th><?php echo $this->lang->line('title_tabel_in'); ?></th>
                                        <th><?php echo $this->lang->line('title_tabel_en'); ?></th>
                                        <th><?php echo $this->lang->line('content_tabel_in'); ?></th>
                                        <th><?php echo $this->lang->line('content_tabel_en'); ?></th>
                                        <th><?php echo $this->lang->line('action'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i= 1;
                                    foreach ($slides as $key) {
                                    ?>
                                    <tr>
                                        <td class="text-center"> <?php echo $i++; ?></td>
                                        <td class="text-center"> <?php echo date(' d/m/y',strtotime($key->attraction_date)) ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_title_in ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_title_en ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_content_in ?></td>
                                        <td class="text-center"> <?php echo $key->attraction_content_en ?></td>
                                        <td colspan="2" class="text-center">
                                             <button type="button" class="btn btn-xs btn-warning" data-target="#slides_edit" id="btn_slides_edit" role="input"  data-toggle="modal" data-id="<?php echo $key->attraction_id ?>" title="<?php echo $this->lang->line('edit'); ?>"> <i class="fa fa-pencil"></i></button> 
                                            <a href="<?php echo site_url('backend/aquasplash/slides_delete/'.$key->attraction_id); ?>" class="btn btn-xs btn-danger" title="<?php echo $this->lang->line('btn_delete'); ?>"  onClick="return confirm('<?php echo $this->lang->line('confirm'); ?>');"> <i class="fa fa-trash-o"></i>
                                        </td>
                                    </tr>
                                    <?php 
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    
                    </div>
                </div>
            </div>

<!-- ########## Pools ########## -->
            <div class="col-xs-12" id="pools">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Pools
                    </div>
                    <div class="panel-body">
<!-- ########## Pools Image 1 ########## -->
                        <?php
                            $pflag = 1;
                            $a = 1;
                            $b = 1;
                            $c = 1;
                            foreach ($pools_image as $key ) {
                                 # code...
                        ?>
                        <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/aquasplash/save_pools_image" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="col-sm-3" id="pools_image<?php echo $a++; ?>">
                            <?php 
                                echo $this->session->flashdata('pools_image'.$b++);
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("image"); ?> <?php echo $c++ ?>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="col-xs-12">
                                                <img src="<?php echo !empty($key->attraction_url) ? base_url('assets/upload/attractions/aquasplash').'/'.$key->attraction_url :  base_url('assets/img/blank_img.png'); ?> " class="img-responsive" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label for="Title Indonesia" class="control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <input type="file" class="form-control" name="attraction_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_512px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" name="attraction_id" value="<?php echo isset($key->attraction_id) ? $key->attraction_id: ''; ?>">
                                <input type="hidden" value="<?php echo $pflag++; ?>" name="pflag">
                            </div>
                        </div>
                        </form>
                        <?php
                             } 
                        ?>
<!-- ########## Pools Content ########## -->
                        <div class="col-sm-12" id="pools_content">
                            <?php echo $this->session->flashdata('pools_content'); ?>
                            <form class="form-horizontal"  method="POST" action="<?php echo site_url()?>backend/aquasplash/save_pools_content" accept-charset="utf-8" enctype="multipart/form-data">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("content"); ?>
                                </div>
                                <div class="panel-body">
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content Indonesia" class="control-label">
                                                            <p><?php echo $this->lang->line("content_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="attraction_content_in"><?php echo isset($pools_content->attraction_content_in) ? $pools_content->attraction_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("content_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control" rows="10"  name="attraction_content_en"><?php echo isset($pools_content->attraction_content_en) ? $pools_content->attraction_content_en:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo isset($pools_content->attraction_id) ? $pools_content->attraction_id: ''; ?>" name="attraction_id" >
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section><!-- /.content -->
</div>

<!-- slides Add-->
<div id="slides_add" class="modal modal-fade a" tab-index="-1" role="dialog"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"> <?php echo $this->lang->line("btn_add"); ?> slides </h4>
            </div>
            <div class="modal-body" id="slides-add">
                
            </div>
        </div>
    </div>
</div>

<!-- slides edit -->
<div id="slides_edit" class="modal modal-fade a" tab-index="-1" role="dialog"> 
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->lang->line("btn_edit"); ?> slides </h4>
            </div>
            <div class="modal-body" id="slides-edit">
                
            </div>
        </div>
    </div>
</div>