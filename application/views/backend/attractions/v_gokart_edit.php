<section>
    <div class="row">
        <div class="col-sm-12">
            <form class="form-horizontal" method="POST" action="<?php echo site_url('backend/gokart/save_karts_update'); ?>" accept-charset="utf-8" enctype="multipart/form-data" role="form" runat="server">
                <div class="col-xs-12">
                     <div class="form-group">
                        <div class="col-sm-12">
                            <div class="col-xs-9 col-xs-offset-3">
                                <img src="<?php echo isset($modal->attraction_url) ? base_url('assets/upload/attractions/gokart').'/'.$modal->attraction_url : base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-crop" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="col-xs-3 control-label">
                                <?php echo $this->lang->line("choose_image"); ?>
                            </label>
                            <div class="col-xs-9">
                                <input type="file" class="form-control" name="attraction_url">
                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("image_512px"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="col-xs-3 control-label">
                                <?php echo $this->lang->line("title_in"); ?>
                            </label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" name="attraction_title_in" value="<?php echo $modal->attraction_title_in ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="col-xs-3 control-label">
                                <?php echo $this->lang->line("title_en"); ?>
                            </label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" name="attraction_title_en" value="<?php echo $modal->attraction_title_en ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label class="col-xs-3 control-label"><?php echo $this->lang->line('content_in'); ?></label>
                            <div class="col-xs-9">
                                <textarea class="form-control risize wysihtml5_karts" rows="7" name="attraction_content_in" required><?php echo $modal->attraction_content_in ?></textarea>
                            </div>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label class="col-xs-3 control-label"><?php echo $this->lang->line('content_en'); ?></label>
                            <div class="col-xs-9">
                                <textarea class="form-control risize wysihtml5_karts" rows="7" name="attraction_content_en" required><?php echo $modal->attraction_content_en ?></textarea>
                            </div>
                       </div>
                    </div>
                </div>

                <div class="form-group pull-right">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?> </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('btn_close'); ?></button>
                    </div>
                </div>
                <input type="hidden" name="attraction_id" value="<?php echo $modal->attraction_id ?>">
            </form>
        </div> 
    </div>    
</section>