<section>
    <div class="row">
        <div class="col-sm-12">
            <form class="form-horizontal" method="POST" action="<?php echo site_url('backend/gokart/save_karts_add'); ?>" accept-charset="utf-8" enctype="multipart/form-data" role="form" runat="server">
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="col-xs-3 control-label">
                                <?php echo $this->lang->line("choose_image"); ?>
                            </label>
                            <div class="col-xs-9">
                                <input type="file" class="form-control" name="attraction_url" required>
                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("image_512px"); ?> </small>
                                    <br>
                                    <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="col-xs-3 control-label">
                                <?php echo $this->lang->line("title_in"); ?>
                            </label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" name="attraction_title_in" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="Title Indonesia" class="col-xs-3 control-label">
                                <?php echo $this->lang->line("title_en"); ?>
                            </label>
                            <div class="col-xs-9">
                                <input type="text" class="form-control" name="attraction_title_en" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label class="col-xs-3 control-label"><?php echo $this->lang->line('content_in'); ?></label>
                            <div class="col-xs-9">
                                <textarea class="form-control risize wysihtml5_karts" rows="7" name="attraction_content_in" required>
                                </textarea>
                            </div>
                       </div>
                    </div>
                    <div class="form-group">
                       <div class="col-sm-12">
                            <label class="col-xs-3 control-label"><?php echo $this->lang->line('content_en'); ?></label>
                            <div class="col-xs-9">
                                <textarea class="form-control risize wysihtml5_karts" rows="7" name="attraction_content_en" required>
                                </textarea>
                            </div>
                       </div>
                    </div>
                </div>

                <div class="form-group pull-right">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btn_save'); ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $this->lang->line('btn_close'); ?></button>
                    </div>
                </div>
            </form>
        </div> 
    </div>    
</section>