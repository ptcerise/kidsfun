<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("home"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("home"); ?> </li>
        </ol>
    </section>
 
    <!-- Main content -->
    <section class="content">
        <div class="row">
<!-- ########## Video ########## -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                      Video   
                    </div>
<!-- ########## Video ########## -->
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-sm-6">
                                <?php 
                                    echo $this->session->flashdata('video');
                                ?>
                                <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_video" accept-charset="utf-8" enctype="multipart/form-data">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Video
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-XS-12 bg-black1">
                                                    <?php 
                                                        $break =  explode('.', $video->media_url) ;
                                                        $type = strtolower($break[count($break) - 1]);
                                                    ?>
                                                    <video id="home_video" muted class="video-size" controls>
                                                        <source src="<?php echo !empty($video->media_url)? site_url('assets/upload/video').'/'.$video->media_url : base_url('assets/img/blank_img.png'); ?>" type="video/<?php echo $type ?>">
                                                        </source>
                                                    </video>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Title English" class="col-sm-4 control-label">
                                                    <?php echo $this->lang->line("choose_video"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                  <input type="file" name="video" id="video_upp">
                                                  <p><small class="color-red"> <?php echo $this->lang->line("video_8mb"); ?> </small><span class="clearfix"></span>
                                                  <small class="color-red"> <?php echo $this->lang->line("video_type"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <input type="hidden" value=" <?php echo isset($video->media_id) ? $video->media_id:''; ?>" name="media_id">
                                                <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                    <?php echo $this->lang->line("btn_update"); ?>
                                                </button> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

<!-- ########## Video Banner ########## -->
                            <div class="col-sm-6">
                                <?php 
                                    echo $this->session->flashdata('video_image');
                                ?>
                                <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_video_banner" accept-charset="utf-8" enctype="multipart/form-data">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Video Banner 
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <div class="col-xs-12 text-center">
                                                    <img src="<?php echo !empty($video_banner->media_url) ? base_url('assets/upload/home/thumbnail').'/'.$video_banner->media_url : base_url('assets/img/blank_img.png'); ?>" alt="" class="img-responsive img-center">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Title English" class="col-sm-4 control-label">
                                                    <?php echo $this->lang->line("choose_image"); ?>
                                                </label>
                                                <div class="col-sm-8">
                                                  <input type="file" name="media_url">
                                                    <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_1024px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="form-group panel-save">
                                                <input type="hidden" value="<?php echo isset($video_banner->media_id) ? $video_banner->media_id:''; ?>" name="media_id">
                                                <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                    <?php echo $this->lang->line("btn_update"); ?>
                                                </button> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
<!-- ########## Video Content ########## -->
                        <div class="col-sm-12" id="video_content">
                        <?php 
                            echo $this->session->flashdata('video_content');
                        ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_video_content" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="Title English" class="col-sm-4 col-md-3 control-label">
                                                            <?php echo $this->lang->line("title_in"); ?>
                                                        </label>
                                                        <div class="col-sm-8 col-md-9">
                                                          <input type="text" class="form-control" id="general_title_in" name="general_title_in" value="<?php echo isset($video_content->general_title_in) ? $video_content->general_title_in:''; ?> " required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group"> 
                                                        <label for="Title English" class="col-sm-4 col-md-3 control-label">
                                                            <?php echo $this->lang->line("content_in"); ?>
                                                        </label>
                                                        <div class="col-sm-8 col-md-9">
                                                            <textarea class="form-control wysihtml5  " rows="7" id="general_content_in" name="general_content_in"><?php echo isset($video_content->general_content_in) ? $video_content->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="Title English" class="col-sm-4 control-label">
                                                            <?php echo $this->lang->line("title_en"); ?>
                                                        </label>
                                                        <div class="col-sm-8">
                                                          <input type="text" class="form-control" id="general_title_en" name="general_title_en" value="<?php echo isset($video_content->general_title_en) ? $video_content->general_title_en:''; ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Title English" class="col-sm-4 control-label">
                                                            <?php echo $this->lang->line("content_en"); ?>
                                                        </label>
                                                        <div class="col-sm-8">
                                                            <textarea class="form-control wysihtml5" rows="7" id="general_content_en" name="general_content_en"><?php echo isset($video_content->general_content_en) ? $video_content->general_content_en:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($video_content->general_id) ? $video_content->general_id:''; ?>" name="general_id">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- ########## Divider 1 ########## -->
            <div class="col-xs-12" id="dividers1">
            <form class="form-horizontal" method="post" action="<?php echo site_url()?>backend/home/save_dividers" accept-charset="utf-8" enctype="multipart/form-data">

                <div class="panel panel-info">
                    <div class="panel-heading">
                       <?php echo $this->lang->line("dividers1"); ?> 
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('dividers1');
                        ?>
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="Title English" class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("content_in"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="divider1_en" name="general_content_in" value="<?php echo isset($divider_top->general_content_in) ? $divider_top->general_content_in:''; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="Title English" class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("content_en"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="divider1_in" name="general_content_en" value="<?php echo isset($divider_top->general_content_en) ? $divider_top->general_content_en:''; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>                                   
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button> 
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($divider_top->general_id) ? $divider_top->general_id:''; ?>" name="general_id" id="general_id">
                    <input type="hidden" name="divider_val" value="1">
                </div>
            </form>
            </div>
<!-- ########## Rides ########## -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                       <?php echo $this->lang->line("rides"); ?> 
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_rides_content" accept-charset="utf-8" enctype="multipart/form-data">
                            <div class="col-sm-12" id="rides_content">
                                <div class="panel panel-success">
                                <div class="panel-heading">Rides Content</div>
                                <div class="panel-body">
                                    <?php 
                                        echo $this->session->flashdata('rides_content');
                                    ?>
                                    <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content Indonesia" class="control-label">
                                                            <p><?php echo $this->lang->line("content_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control wysihtml5" rows="10" id="general_content_in" name="general_content_in"><?php echo isset($rides_content->general_content_in) ? $rides_content->general_content_in:''; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("content_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control wysihtml5" rows="10" id="general_content_en" name="general_content_en"><?php echo isset($rides_content->general_content_en) ? $rides_content->general_content_en:''; ?></textarea>
                                                    </div>
                                                </div>  
                                            </div>                             
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer panel-success">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button> 
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo isset($rides_content->general_id) ? $rides_content->general_id:''; ?>" name="rides_id" id="rides_id">
                            </div>
                            </div>
                        </form>
                    </div>

                    <div class="panel-body">
                    <?php 
                        $title = array($this->lang->line("kids_rides"),$this->lang->line("family_rides"),$this->lang->line("thrill_rides"));
                        $a=0;
                        $rides_val = 1;
                        $b = 1;
                        $c = 1;
                        foreach ($rides as $key) {
                    ?>
                    <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_rides" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="col-sm-12" id="rides<?php echo $b++; ?>">
                            <?php 
                                echo $this->session->flashdata('rides'.$c++);
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $title[$a];
                                    $a++; ?>
                                </div>
                                <div class="panel-body">
                                    <div class="well">
                                    <div class="row">
                                        <div class="col-xs-6">
                                           
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="Content Indonesia" class="control-label">
                                                        <p><?php echo $this->lang->line("content_in"); ?></p>
                                                    </label>
                                                    <textarea class="form-control wysihtml5" rows="10" id="general_content_in" name="general_content_in" ><?php echo isset($key->general_content_in) ? $key->general_content_in:''; ?> </textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-6">
                                            
                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="Content English" class="control-label">
                                                        <p><?php echo $this->lang->line("content_en"); ?></p>
                                                    </label>
                                                    <textarea class="form-control wysihtml5" rows="10" id="general_content_en" name="general_content_en" ><?php echo isset($key->general_content_en) ? $key->general_content_en:''; ?> </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button>
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo isset($key->general_id) ? $key->general_id:''; ?>" name="general_id" id="rides_id">
                                <input type="hidden" name="rides_val" value="<?php echo $rides_val++; ?>">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                    </div>
                </div>
            </div>

<!-- ########## Divider 2 ########## -->
            <div class="col-xs-12" id="dividers2">
            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_dividers" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="panel panel-info">
                    <div class="panel-heading">
                       <?php echo $this->lang->line("dividers2"); ?> 
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('dividers2');
                        ?>
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    
                                    <div class="form-group">
                                        <label for="Title English" class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("content_in"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="divider2_in" name="general_content_in" value="<?php echo isset($divider_middle->general_content_in) ? $divider_middle->general_content_in:''; ?>">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    
                                    <div class="form-group">
                                        <label for="Title English" class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("content_en"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="divider2_en" name="general_content_en" value="<?php echo isset($divider_middle->general_content_en) ? $divider_middle->general_content_en:''; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>                                
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button> 
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($divider_middle->general_id) ? $divider_middle->general_id:''; ?>" name="general_id" id="general_id">
                    <input type="hidden" name="divider_val" value="2">
                </div>
            </form>
            </div>
<!-- ########## Banner 1 ########## -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Aquasplash 
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner 1 Large ########## -->
                            <div class="col-sm-8" id="banner_large1">
                            <?php 
                                echo $this->session->flashdata('banner_large1');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner1_large->media_url) ? base_url('assets/upload/home/thumbnail').'/'.$banner1_large->media_url : base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1024px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner1_large->media_id) ? $banner1_large->media_id:''; ?>" name="banner_large_id" id="banner_large_id">
                                    <input type="hidden" name="banner_large_val" value="1" >
                                </div>
                            </form>
                            </div>
<!-- ########## Banner 1 Small ########## -->
                            <div class="col-sm-4" id="banner_small1">
                            <?php 
                                echo $this->session->flashdata('banner_small1');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner1_small->media_url)? base_url('assets/upload/home/thumbnail').'/'.$banner1_small->media_url : base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner1_small->media_id) ? $banner1_small->media_id:''; ?>" name="banner_small_id" id="banner_small_id">
                                    <input type="hidden" name="banner_small_val" value="1">
                                </div>
                            </form>
                            </div>
<!-- ########## Banner 1 Caption ########## -->
                            <div class="col-sm-12" id="banner_caption1">
                            <?php 
                                echo $this->session->flashdata('banner_caption1');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6">

                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="Title English" class="control-label">
                                                                <p><?php echo $this->lang->line("caption_in"); ?></p>
                                                            </label>
                                                            <textarea class="form-control wysihtml5" rows="10" id="general_content_in" name="general_content_in"><?php echo isset($banner1_caption->general_content_in) ? $banner1_caption->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                   
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="Title English" class="control-label">
                                                                <p><?php echo $this->lang->line("caption_en"); ?></p>
                                                            </label>
                                                            <textarea class="form-control wysihtml5" rows="10" id="general_content_en" name="general_content_en"><?php echo isset($banner1_caption->general_content_en) ? $banner1_caption->general_content_en:''; ?></textarea>
                                                            </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button> 
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner1_caption->general_id) ? $banner1_caption->general_id:''; ?>" name="general_id" id="general1_id">
                                    <input type="hidden" name="banner_caption_val" value="1">
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- ########## Divider 3 ########## -->
            <div class="col-xs-12" id="dividers3">
                <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_dividers" accept-charset="utf-8" enctype="multipart/form-data">
                <div class="panel panel-info">
                    <div class="panel-heading">
                       <?php echo $this->lang->line("dividers3"); ?> 
                    </div>
                    <div class="panel-body">
                        <?php 
                            echo $this->session->flashdata('dividers3');
                        ?> 
                        <div class="well">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    
                                    <div class="form-group">
                                        <label for="Title English" class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("content_in"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="divider3_en" name="general_content_in" value="<?php echo isset($divider_bottom->general_content_in) ? $divider_bottom->general_content_in:''; ?>">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    
                                    <div class="form-group">
                                        <label for="Title English" class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("content_en"); ?>
                                        </label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="divider3_in" name="general_content_en" value="<?php echo isset($divider_bottom->general_content_en) ? $divider_bottom->general_content_en:''; ?>">
                                        </div>
                                    </div>

                                   
                                </div>
                            </div>                             
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group panel-save">
                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                <?php echo $this->lang->line("btn_update"); ?>
                            </button>
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo isset($divider_bottom->general_id) ? $divider_bottom->general_id:''; ?>" name="general_id" id="dvd_bottom">
                    <input type="hidden" name="divider_val" value="3">
                </div>
            </form>
            </div>

<!-- ########## Banner 2 ########## -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                       <?php echo $this->lang->line("restaurants"); ?> 
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner 2 Large ########## -->
                            <div class="col-sm-8" id="banner_large2">
                            <?php 
                                echo $this->session->flashdata('banner_large2');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner2_large->media_url)? base_url('assets/upload/home/thumbnail').'/'.$banner2_large->media_url: base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1024px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner2_large->media_id) ? $banner2_large->media_id:''; ?>" name="banner_large_id" id="banner_large_id">
                                    <input type="hidden" name="banner_large_val" value="2" >
                                </div>
                            </form>
                            </div>
<!-- ##########  Banner 2 Small ########## --> 
                            <div class="col-sm-4" id="banner_small2">
                            <?php 
                                echo $this->session->flashdata('banner_small2');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner2_small->media_url)? base_url('assets/upload/home/thumbnail').'/'.$banner2_small->media_url: base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner2_small->media_id) ? $banner2_small->media_id:''; ?>" name="banner_small_id" id="banner_small_id">
                                    <input type="hidden" name="banner_small_val" value="2">
                                </div>
                            </form>
                            </div>
<!-- ##########  Banner 2 Caption ########## --> 
                            <div class="col-sm-12" id="banner_caption2">
                            <?php 
                                echo $this->session->flashdata('banner_caption2');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6">

                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="Content Indonesia" class="control-label">
                                                                <p><?php echo $this->lang->line("caption_in"); ?></p>
                                                            </label>
                                                            <textarea class="form-control wysihtml5" rows="10" id="general_content_in" name="general_content_in"><?php echo isset($banner2_caption->general_content_in) ? $banner2_caption->general_content_in:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                   
                                                    <div class="form-group">
                                                        <div class="col-xs-12">
                                                            <label for="Content English" class="control-label">
                                                                <p><?php echo $this->lang->line("caption_en"); ?></p>
                                                            </label>
                                                            <textarea class="form-control wysihtml5" rows="10" id="general_content_en" name="general_content_en"> <?php echo isset($banner2_caption->general_content_en) ? $banner2_caption->general_content_en:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner2_caption->general_id) ? $banner2_caption->general_id:''; ?>" name="general_id" id="general2_id">
                                    <input type="hidden" name="banner_caption_val" value="2">
                                </div>
                            </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>

<!-- ########## Announcement ########## -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                       <?php echo $this->lang->line("announcement"); ?> 
                    </div>

                    <div class="panel-body">
                    <?php 
                        $title = array($this->lang->line("op"),$this->lang->line("me"),$this->lang->line("wtb"),$this->lang->line("acc"));
                        $i=0;
                        $announcement_val = 1;
                        $x = 1;
                        $y = 1;
                        foreach ($announcement as $key) {
                    ?>
                    <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_announcement" accept-charset="utf-8" enctype="multipart/form-data">
                        <div class="col-sm-12" id="announcement<?php echo $x++; ?>">
                            <?php 
                                echo $this->session->flashdata('announcement'.$y++);
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <?php echo $title[$i];
                                    $i++; ?>
                                </div>
                                <div class="panel-body">
                                    <div class="well">
                                    <div class="row">
                                        <div class="col-xs-6">
                                           
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="Content Indonesia" class="control-label">
                                                        <p><?php echo $this->lang->line("content_in"); ?></p>
                                                    </label>
                                                    <textarea class="form-control wysihtml5" rows="10" id="general_content_in" name="general_content_in" ><?php echo isset($key->general_content_in) ? $key->general_content_in:''; ?> </textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-xs-6">
                                            
                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <label for="Content English" class="control-label">
                                                        <p><?php echo $this->lang->line("content_en"); ?></p>
                                                    </label>
                                                    <textarea class="form-control wysihtml5" rows="10" id="general_content_en" name="general_content_en" ><?php echo isset($key->general_content_en) ? $key->general_content_en:''; ?> </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                            <?php echo $this->lang->line("btn_update"); ?>
                                        </button>
                                    </div>
                                </div>
                                <input type="hidden" value="<?php echo isset($key->general_id) ? $key->general_id:''; ?>" name="general_id" id="announcement1_id">
                                <input type="hidden" name="announcement_val" value="<?php echo $announcement_val++; ?>">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                    </div>
                </div>
            </div>

<!-- ########## Banner 3 ########## -->
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                       <?php echo $this->lang->line("shows"); ?> 
                    </div>
                    <div class="panel-body">
                        <div class="row">
<!-- ########## Banner 3 Large ########## -->
                            <div class="col-sm-8" id="banner_large3">
                            <?php 
                                echo $this->session->flashdata('banner_large3');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_large" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("big_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner3_large->media_url)? base_url('assets/upload/home/thumbnail').'/'.$banner3_large->media_url: base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1024px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("landscape_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner3_large->media_id) ? $banner3_large->media_id:''; ?>" name="banner_large_id" id="banner_large_id">
                                    <input type="hidden" name="banner_large_val" value="3" >
                                </div>
                            </form>
                            </div>
<!-- ########## Banner 3 Small ########## -->
                            <div class="col-sm-4" id="banner_small3">
                            <?php 
                                echo $this->session->flashdata('banner_small3');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_small" accept-charset="utf-8" enctype="multipart/form-data">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("small_image"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-center">
                                              <img src="<?php echo !empty($banner3_small->media_url)? base_url('assets/upload/home/thumbnail').'/'.$banner3_small->media_url: base_url('assets/img/blank_img.png'); ?> " class="img-responsive img-center" alt="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Title English" class="col-sm-4 control-label">
                                                <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-sm-8">
                                              <input type="file" name="media_url">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_752px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("portrait_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner3_small->media_id) ? $banner3_small->media_id:''; ?>" name="banner_small_id" id="banner_small_id">
                                    <input type="hidden" name="banner_small_val" value="3" >
                                </div>
                            </form>
                            </div>
<!-- ########## Banner 3 Caption ########## -->
                            <div class="col-sm-12" id="banner_caption3">
                            <?php 
                                echo $this->session->flashdata('banner_caption3');
                            ?>
                            <form class="form-horizontal" method="POST" action="<?php echo site_url()?>backend/home/save_banner_caption" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <?php echo $this->lang->line("caption"); ?>
                                    </div>
                                    <div class="panel-body">
                                        <div class="well">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">

                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("caption_in"); ?></p>
                                                        </label>
                                                        <textarea class="form-control wysihtml5" rows="10" id="general_content_in" name="general_content_in"><?php echo isset($banner3_caption->general_content_in) ? $banner3_caption->general_content_in:''; ?></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                               
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <label for="Content English" class="control-label">
                                                            <p><?php echo $this->lang->line("caption_en"); ?></p>
                                                        </label>
                                                        <textarea class="form-control wysihtml5" rows="10" id="general_content_en" name="general_content_en"><?php echo isset($banner3_caption->general_content_en) ? $banner3_caption->general_content_en:''; ?></textarea>
                                                    </div>
                                                </div>

                                            </div>    
                                        </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="form-group panel-save">
                                            <button type="submit" class="btn btn-md btn-primary" title="<?php echo $this->lang->line("update_data"); ?>">
                                                <?php echo $this->lang->line("btn_update"); ?>
                                            </button>
                                        </div>
                                    </div>
                                    <input type="hidden" value="<?php echo isset($banner3_caption->general_id) ? $banner3_caption->general_id:''; ?>" name="general_id" id="general3_id">
                                    <input type="hidden" name="banner_caption_val" value="3">
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>   
    </section><!-- /.content -->
</div>