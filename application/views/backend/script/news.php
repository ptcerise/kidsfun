<script type="text/javascript">
$(document).ready(function(){
    // data Tabel
    $('#news_Table').DataTable({
      "columns": [
        null,
        null,
        null,
        null,
        null,
        { "orderable": false }
      ]
    });

    // news Add
    $('#news_add').on('show.bs.modal',function(){
        var modal = $(this);
        $('#news-add').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/news/news_add"); ?>',
            dataType : 'html',
            type : 'get',
            success : function(data){
                modal.find('#news-add').html(data);
                $('.loader').remove();
                tinyMCE();
            }
        });
    });

    // news Edit Banner
    $('#news_edit_banner').on('show.bs.modal',function(event){
        
        var trigger = $(event.relatedTarget); // element that triggered modal
        var banner = trigger.data('banner');
        var modal = $(this);
        $('#news-edit-banner').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/news/news_edit_banner"); ?>/'+banner,
            dataType : 'html',
            type : 'get',
            success : function(data){
                // console.log(id);
                modal.find('#news-edit-banner').html(data);
                $('.loader').remove();
                tinyMCE();
                
            }
        });
    });

    // news Edit content
    $('#news_edit_content').on('show.bs.modal',function(event){
        
        var trigger = $(event.relatedTarget); // element that triggered modal
        var id = trigger.data('id');
        var modal = $(this);
        $('#news-edit-content').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/news/news_edit_content"); ?>/'+id,
            dataType : 'html',
            type : 'get',
            success : function(data){
                modal.find('#news-edit-content').html(data);
                $('.loader').remove();
                tinyMCE();
            }
        });
    });

    function tinyMCE(){
        tinymce.init({
            selector: 'textarea',
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor',
            plugins: 'code link textcolor',
            menubar: false
        });
    }
});
</script>