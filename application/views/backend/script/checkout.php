<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.detail-page').click(function(){
            var checkout_id = $(this).attr('id');
            var info = 'checkout_id='+checkout_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/Checkout/payment_detail') ?>",
                data: info,
                success: function(html){
                    $('#detail-page').html(html);
                }
            });
        });
        $('.edit-page').click(function(){
            var checkout_id = $(this).attr('id');
            var info = 'checkout_id='+checkout_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/Checkout/edit_form') ?>",
                data: info,
                success: function(html){
                    $('#edit-page').html(html);
                }
            });
        });
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")==1){
                $(".type-img").css("display","block");
                $(".type-text").css("display","none");
            }
            if($(this).attr("value")==2){
                $(".type-text").css("display","block");
                $(".type-img").css("display","none");
            }
        });
        $('#checkout').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "15%" }
            ]
        });
    });
</script>