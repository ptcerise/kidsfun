<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#user_name').on('input', function(){
			var user_name = $('#user_name').val();
			var data = 'user_name='+user_name;
			$.ajax({
				type: "POST",
				data: data,
				url: "<?php echo base_url('backend/user/check') ?>",
				dataType: "json",
				success: function(result){
					if( result.status == 1)
					{
						alert('<?php echo $this->lang->line("user_check") ?>');
						$('#user_name').val('');
						$('#user_name').focus();
					}
				}
			});
		});
        // $('#confirm_pass').on('input', function(){
        //     var confirm_pass = $('#confirm_pass').val();
        //     var data = 'confirm_pass='+confirm_pass;
        //     $.ajax({
        //         type: "POST",
        //         data: data,
        //         url: "<?php echo base_url('backend/user/check_pass') ?>",
        //         dataType: "json",
        //         success: function(result){
        //             if( result.status == 1)
        //             {
        //                 alert('<?php echo $this->lang->line("pass_check") ?>');
        //                 $('#confirm_pass').val('');
        //                 $('#confirm_pass').focus();
        //             }
        //         }
        //     });
        // });
		$('.edit-page').click(function(){
            var user_id = $(this).attr('id');
            var info = 'user_id='+user_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/user/edit_form') ?>",
                data: info,
                success: function(html){
                    $('#edit-page').html(html);
                }
            });
        });
		$('#user').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "10%" }
            ]
        });
	});

</script>
<script type="text/javascript">
    function check(input){
        if(input.value != $('#user_pass').val()){
            input.setCustomValidity('Password must be match.');
        }
        else{
            input.setCustomValidity('');
        }
    }   
</script>
