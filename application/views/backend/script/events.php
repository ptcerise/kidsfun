<script type="text/javascript">
$(document).ready(function(){
// Add restaurant single
    $('#tbl_events').DataTable({
      "columns": [
        null,
        null,
        null,
        null,
        null,
        null,
        { "orderable": false }
      ]
    });

    // Event add
    $('#events_add').on('show.bs.modal',function(event){
        var trigger = $(event.relatedTarget); // element that triggered modal
        var id = trigger.data('id');
        var shop = trigger.data('shop');
        var modal = $(this);
        $('#events-add').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/events/events_add"); ?>/'+id,
            dataType : 'html',
            type : 'post',
            success : function(data){
                console.log(id);
                modal.find('#events-add').html(data);
                $('.loader').remove();
                tinyMCE();
                $('#event_add_tgl').datetimepicker({ format: 'YYYY-MM-DD' });
            }
        });
    });

    // Events edit
    $('#events_edit').on('show.bs.modal',function(event){
        
        var trigger = $(event.relatedTarget); // element that triggered modal
        var id = trigger.data('id');
        var shop = trigger.data('shop');
        var modal = $(this);
        $('#events-edit').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/events/events_edit"); ?>/'+id,
            dataType : 'html',
            type : 'post',
            success : function(data){
                console.log(id);
                modal.find('#events-edit').html(data);
                $('.loader').remove();
                tinyMCE();
                $('#event_edit_tgl').datetimepicker({ format: 'YYYY-MM-DD' });
            }
        });
    });

    function tinyMCE(){
        tinymce.init({
            selector: 'textarea',
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor',
            plugins: 'code link textcolor',
            menubar: false
        });
    }
});
</script>