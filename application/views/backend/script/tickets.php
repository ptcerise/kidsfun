<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.edit-page').click(function(){
            var tickets_id = $(this).attr('id');
            var info = 'tickets_id='+tickets_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/Tickets/edit_form') ?>",
                data: info,
                success: function(html){
                    $('#edit-page').html(html);
                    tinyMCE();
                }
            });
        });
        function tinyMCE(){
            tinymce.init({
                selector: 'textarea',
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor',
                plugins: 'code link textcolor',
                menubar: false
            });
        }
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")==1){
                $(".type-img").css("display","block");
                $(".type-text").css("display","none");
            }
            if($(this).attr("value")==2){
                $(".type-text").css("display","block");
                $(".type-img").css("display","none");
            }
        });
        $('#tickets').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "10%" },
            { "width": "10%" }
            ]
        });
        $('#tickets_price').priceFormat({
            prefix: 'Rp ',
            centsLimit: 0,
            centsSeparator: ', ',
            thousandsSeparator: '.'
        });
    });
</script>