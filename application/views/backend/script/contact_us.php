<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.view-page').click(function(){
            var status_id = $('p').attr('id');
            $('#'+status_id).html("<i class='fa fa-check'></i> <i>Read</i>");

            var contact_id = $(this).attr('id');
            var info = 'contact_id='+contact_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/Contact_us/view_message') ?>",
                data: info,
                success: function(html){
                    $('#view-page').html(html);
                }
            });
        });
        
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")==1){
                $(".type-img").css("display","block");
                $(".type-text").css("display","none");
            }
            if($(this).attr("value")==2){
                $(".type-text").css("display","block");
                $(".type-img").css("display","none");
            }
        });
        $('#contact').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "10%" },
            { "width": "10%" }
            ]
        });
    });
</script>