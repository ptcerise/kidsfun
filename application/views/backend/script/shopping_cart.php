<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	$('#cart').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "15%" }
            ]
        });
        if ($('.textarea')) {
            $('.textarea').wysihtml5({
                "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
                "font-styles": false,
                "html": false,
                "link": false,
                "image": false
            });
        }
    });
</script>