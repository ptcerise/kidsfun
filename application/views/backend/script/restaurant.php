<script type="text/javascript">
$(document).ready(function(){
// Add restaurant single
    $('#restaurants').DataTable({
      "columns": [
        null,
        { "orderable": false },
        null,
        null,
        { "orderable": false }
      ]
    });

    // Restaurant Add
    $('#restaurant_add').on('show.bs.modal',function(){
        var modal = $(this);
        $('#restaurant-add').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/restaurant/restaurant_add"); ?>',
            dataType : 'html',
            type : 'get',
            success : function(data){
                modal.find('#restaurant-add').html(data);
                $('.loader').remove();
                tinyMCE();
            }
        });
    });

    // Restaurant Edit Banner
    $('#restaurant_edit_banner').on('show.bs.modal',function(event){
        
        var trigger = $(event.relatedTarget); // element that triggered modal
        var banner = trigger.data('banner');
        var modal = $(this);
        $('#restaurant-edit-banner').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/restaurant/restaurant_edit_banner"); ?>/'+banner,
            dataType : 'html',
            type : 'get',
            success : function(data){
                // console.log(id);
                modal.find('#restaurant-edit-banner').html(data);
                $('.loader').remove();
                tinyMCE();
            }
        });
    });

    // Restaurant Edit content
    $('#restaurant_edit_content').on('show.bs.modal',function(event){
        
        var trigger = $(event.relatedTarget); // element that triggered modal
        var id = trigger.data('id');
        var modal = $(this);
        $('#restaurant-edit-content').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/restaurant/restaurant_edit_content"); ?>/'+id,
            dataType : 'html',
            type : 'get',
            success : function(data){
                // console.log(id);
                modal.find('#restaurant-edit-content').html(data);
                $('.loader').remove();
                tinyMCE();
            }
        });
    });

    function tinyMCE(){
        tinymce.init({
            selector: 'textarea',
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor',
            plugins: 'code link textcolor',
            menubar: false
        });
    }
});
</script>