<script type="text/javascript">
$(document).ready(function(){
// Add restaurant single
    $('#tbl_slides').DataTable({
      "columns": [
        null,
        null,
        null,
        null,
        null,
        null,
        { "orderable": false }
      ]
    });

    // slides add
    $('#slides_add').on('show.bs.modal',function(event){
        
        var trigger = $(event.relatedTarget); // element that triggered modal
        var id = trigger.data('id');
        var shop = trigger.data('shop');
        var modal = $(this);
        $('#slides-add').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/aquasplash/slides_add"); ?>/'+id,
            dataType : 'html',
            type : 'get',
            success : function(data){
                console.log(id);
                modal.find('#slides-add').html(data);
                $('.loader').remove();
                tinyMCE();
            }
        });
    });

    // slides edit
    $('#slides_edit').on('show.bs.modal',function(event){
        
        var trigger = $(event.relatedTarget); // element that triggered modal
        var id = trigger.data('id');
        var shop = trigger.data('shop');
        var modal = $(this);
        $('#slides-edit').html('<div class="text-center loader"><img src="<?php echo base_url();?>/assets/img/ajax-loader.gif"><div>');
        $.ajax({
            url : '<?php echo site_url("backend/aquasplash/slides_edit"); ?>/'+id,
            dataType : 'html',
            type : 'get',
            success : function(data){
                console.log(id);
                modal.find('#slides-edit').html(data);
                $('.loader').remove();
                tinyMCE();
            }
        });
    });

    function tinyMCE(){
        tinymce.init({
            selector: 'textarea',
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor',
            plugins: 'code link textcolor',
            menubar: false
        });
    }
});
</script>