<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.edit-page').click(function(){
            var rides_id = $(this).attr('id');
            var info = 'rides_id='+rides_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/Rides_single/edit_form') ?>",
                data: info,
                success: function(html){
                    $('#edit-page').html(html);
                tinyMCE();
                }
            });
        });
        $('.edit-image').click(function(){
            var rides_id = $(this).attr('id');
            var info = 'rides_id='+rides_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/Rides_single/image_form') ?>",
                data: info,
                success: function(html){
                    $('#edit-image').html(html);
                }
            });
        });
        $('.edit-image-bg').click(function(){
            var rides_id = $(this).attr('id');
            var info = 'rides_id='+rides_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/Rides_single/image_banner') ?>",
                data: info,
                success: function(html){
                    $('#edit-image-bg').html(html);
                tinyMCE();
                }
            });
        });
        function tinyMCE(){
            tinymce.init({
                selector: 'textarea',
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor',
                plugins: 'code link textcolor',
                menubar: false
            });
        }
        $('input[type="radio"]').click(function(){
            if($(this).attr("value")==1){
                $(".type-img").css("display","block");
                $(".type-text").css("display","none");
            }
            if($(this).attr("value")==2){
                $(".type-text").css("display","block");
                $(".type-img").css("display","none");
            }
        });
        $('#rides').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "15%" }
            ]
        });
    });
</script>