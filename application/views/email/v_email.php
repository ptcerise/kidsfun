<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!--[if !mso]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!--<![endif]-->
        <title></title>

        <!--[if gte mso 15]>
        <style type="text/css">
            table { font-size:1px; line-height:0; mso-margin-top-alt:1px;mso-line-height-rule: exactly; }
            * { mso-line-height-rule: exactly; }
        </style>
        <![endif]-->  
    </head>
    <body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" style="background-color:#F6F6F6;  font-family:Arial,serif; margin:0; padding:0; min-width: 100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
        <table>
            <tr>
                <td colspan="5" style="padding: 0 18% 2% 18%;">
                    Hi……<br>
                    Warm Greeting From Kids Fun Yogyakarta<br>
                    Thank you for buying your entrance ticket(s) online.<br>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="padding: 0 18% 2% 18%;">
                    Here is your ticket information : <br>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">First Name</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : <?php echo $data_checkout->checkout_fname; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Last Name</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : <?php echo $data_checkout->checkout_lname; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Email</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : <?php echo $data_checkout->checkout_email; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Phone Number</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : <?php echo $data_checkout->checkout_phone; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Address</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : <?php echo $data_checkout->checkout_address; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Tickets</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    :
                    <?php foreach($data_tickets as $tickets) { ?>
                        <?php print_r($tickets['tickets_name'].", Qty (".$tickets['chart_qty'].")<br>"); ?>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Visit Date</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;"> :
                    <?php $visit_date = $data_checkout->checkout_visit_date; ?>
                    <?php echo date("d F Y", strtotime($visit_date)); ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Total Payment</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : <?php echo $data_checkout->checkout_total; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Payment Code</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : <?php echo $data_payment->payment_transid; ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Payment Date</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;"> :
                    <?php $date = $data_checkout->checkout_date; ?>
                    <?php echo date("d F Y", strtotime($date)); ?>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 0 18% 2% 18%;">Payment Method</td>
                <td colspan="3" style="padding: 0 18% 2% 18%;">
                    : Credit Card
                </td>
            </tr>

            <tr>
                <td colspan="5" style="padding: 0 18% 2% 18%;">
                    You can find your ticket in the attachment. Print this ticket and show it to the casier or use the digital version on your phone.
                </td>
            </tr>

            <tr>
                <td colspan="5" style="padding: 0 18% 2% 18%;">
                    For more information you can call (0274) 435 34 35 ext 107.
                </td>
            </tr>

            <tr>
                <td colspan="5" style="padding: 10% 18% 2% 18%;">
                    Regards,
                </td>
            </tr>
        </table>
    </body>
</html>