<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ticket << Payment Code >></title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
<style type="text/css">
	body{
		font-family: Arial Rounded MT Bold,Helvetica Rounded,Arial,sans-serif;
		font-weight: 500;
	}

	main{
		width: 700px;
		height: 1000px;
        /*margin-left: auto;
        margin-right: auto;*/
        background-image: url(<?php echo base_url('assets/img/email/logo-kidsfun-trans.png'); ?>);
        background-position: center center;
        background-repeat: no-repeat;
        background-size: 75%;
        padding-bottom: 20px;
	}

	h1{
		font-size: 24px;
		margin-top: 0;
		font-weight: 600;
	}

	p{
		font-size: 12px;
	}

	p.lead{
		font-size: 14px;
		margin: 0;
		margin-bottom: 5px;
	}

	.content{
		padding: 10px 5px;
	}
</style>

<main>
<img class="img-responsive" alt="line" src="<?php echo base_url('assets/img/email/line.png'); ?>">

<div class="content">
<header class="container-fluid">
	<div class="row">
		<div class="col-xs-6 col-sm-6">
			<img class="img-responsive" alt="logo" src="<?php echo base_url('assets/img/email/logo-kidsfun.png'); ?>" style="padding-left:2.5px;padding-right:2.5px;width: 55px;display: inline-block;">
			<img class="img-responsive" alt="logo" src="<?php echo base_url('assets/img/email/logo-gokart.png'); ?>" style="padding-left:2.5px;padding-right:2.5px;width: 55px;display: inline-block;">
			<img class="img-responsive" alt="logo" src="<?php echo base_url('assets/img/email/logo-aquasplash.png'); ?>" style="padding-left:2.5px;padding-right:2.5px;padding-top:5px;width: 80px;display: inline-block;">
			<h1>Ticket Online</h1>
		</div>
		<div class="col-xs-4 col-sm-4">
		<br>
			<p class="lead">Payment Code</p>
			<h1 style="color:red;font-size: 32px;margin-left: 20px;margin-top: -10px;"><?php echo !empty($data_payment->payment_transid) ? $data_payment->payment_transid:""; ?></h1>
		</div>
	</div>
</header>

<div class="row">
	<p class="lead" style="color:#F8A98F;font-weight: bold;margin-left: 15px;margin-top: 25px;">Contact Information</p>
	<div class="col-xs-3">
		<div class="row">
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12); margin-bottom: 5px;margin-left: 15px;height: 55px;">
				<span style="font-weight: bold;font-size: 12px;">First Name</span>
				<p><?php echo $data_checkout->checkout_fname; ?></p>
			</div>
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12);margin-left: 15px;height: 55px;">
				<span style="font-weight: bold;font-size: 12px;">Last Name</span>
				<p><?php echo $data_checkout->checkout_lname; ?></p>
			</div>
		</div>
	</div>
	<div class="col-xs-4">
		<div class="row">
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12);height: 115px;margin-left: 2.5px;margin-right: 2.5px;">
				<span style="font-weight: bold;font-size: 12px">Address</span>
				<p>
					<?php echo $data_checkout->checkout_address; ?>
				</p>
			</div>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="row">
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12); margin-bottom: 5px;margin-left: 2.5px;height: 55px;">
				<span style="font-weight: bold;font-size: 12px;">Phone Number</span>
				<p><?php echo $data_checkout->checkout_phone; ?></p>
			</div>
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12);height: 55px;margin-left: 2.5px;">
				<span style="font-weight: bold;font-size: 12px;">Email</span>
				<p><?php echo $data_checkout->checkout_email; ?></p>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<p class="lead" style="color:#F8A98F;font-weight: bold;margin-left: 15px;margin-top: 25px;">Ticket Information</p>
	<div class="col-xs-7">
		<div class="row">
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12);height: 115px;margin-left: 15px;">
				<span style="font-weight: bold;font-size: 12px;">Ticket</span>
				<p>
					<?php foreach($data_tickets as $tickets) { ?>
		                <?php echo $tickets->tickets_name.", Qty (".$tickets->chart_qty.")<br>"; ?>
		            <?php } ?>
		        </p>
			</div>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="row">
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12);height: 115px;margin-left: 2.5px;">
				<span style="font-weight: bold;font-size: 12px;">Visit Date</span>
				<p>
					<?php $visit_date = $data_checkout->checkout_visit_date; ?>
					<?php echo date("d F Y", strtotime($visit_date)); ?>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<p class="lead" style="color:#F8A98F;font-weight: bold;margin-left: 15px;margin-top: 25px;">Payment Information</p>
	<div class="col-xs-5" style="height: 115px;">
		<div class="row">
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12); margin-bottom: 5px;height: 55px;margin-left: 15px;">
				<span style="font-weight: bold;font-size: 12px;">Total Payment (IDR)</span>
				<p><?php echo $data_checkout->checkout_total; ?></p>
			</div>
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12);height: 55px;margin-left: 15px;">
				<span style="font-weight: bold;font-size: 12px;">Payment Date</span>
				<p>
					<?php $date = $data_checkout->checkout_date; ?>
                    <?php echo date("d F Y", strtotime($date)); ?>
                </p>
			</div>
		</div>
	</div>
	<div class="col-xs-5">
		<div class="row">
			<div class="col-xs-12" style="background-color: rgba(0,0,0,0.12);height: 115px;margin-left: 2.5px;">
				<span style="font-weight: bold;font-size: 12px;">Payment Method</span>
				<p>
					Credit Card
				</p>
			</div>
		</div>
	</div>
</div>

<footer style="margin-top: 370px;margin-bottom: 20px">
	<div class="text-center" style="padding-left: 20px;padding-right: 20px">
		<p style="font-size: 8px;">Print this ticket and show it to the cashier or use the digital version on your phone. For more information you can call (0274) 435 34 35 ext 107.</p>
	</div>
	<img class="img-responsive" alt="line" src="<?php echo base_url('assets/img/email/line.png'); ?>">
	<div class="text-center" style="padding-left: 20px;padding-right: 20px;padding-top: 5px;font-size: 8px;font-weight: 500">
		<div class="col-xs-12">
			<span><img alt="address" src="<?php echo base_url('assets/img/email/icon-address.png'); ?>" width="10px"> : Jl. Wonosari km. 10 Yogyakarta</span>
			<span style="margin-left: 5px;"><img alt="address" src="<?php echo base_url('assets/img/email/icon-phone.png'); ?>" width="10px"> : (0274) 435 34 35</span>
			<span style="margin-left: 5px;"><img alt="address" src="<?php echo base_url('assets/img/email/icon-fax.png'); ?>" width="10px"> : (0274) 435 34 36</span>
		</div>
		<div class="col-xs-12">
			<span><img alt="address" src="<?php echo base_url('assets/img/email/icon-facebook.png'); ?>" width="10px"> : kids fun yogyakarta / yogya go-kart</span>
			<span style="margin-left: 5px;"><img alt="address" src="<?php echo base_url('assets/img/email/icon-twitter.png'); ?>" width="10px"> : @Kidsfun_Yogya / @Yogya_gokart</span>
			<span style="margin-left: 5px;"><img alt="address" src="<?php echo base_url('assets/img/email/icon-instagram.png'); ?>" width="10px"> : @KIDS_FUN_YOGYAKARTA</span>
		</div>
		<div class="col-xs-12">
			<span><img alt="website" src="<?php echo base_url('assets/img/email/icon-web.png'); ?>" width="10px"> : www.kidsfun.co.id / www.yogyagokart.com / www.produkrekreasi.com</span>
		</div>
	</div>
</footer>
</main>

</body>
</html>