<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
if ( ! function_exists('skema'))
{
	function skema($data)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
}

if ( ! function_exists('msg'))
{
	function msg($msg,$tipe)
	{
		echo "<div class='alert alert-".$tipe."'>";
		echo $msg;
		echo "</div>";
	}
}